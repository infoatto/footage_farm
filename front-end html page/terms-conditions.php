<?php require_once('include/head.php') ?>
<?php require_once('include/navigation.php') ?>
<!-- Innerpage Search -->
<section class="page-container innerpage-top">
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="searchbar-top">
		<div class="home-search-wrapper">
			<form class="home-search" action="">
				<input type="text" placeholder="Search for footages and themes" name="home-search">
				<button type="submit"><img src="images/search-icon.svg" alt=""></button>
			</form>
		</div>
	</div>
</section>
<!-- Innerpage Search -->
<section class="page-container">
	<!-- Mobile Search -->
	<div class="multicolored-line multicolored-line-mobile">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="home-search-wrapper search-wrapper-mobile">
		<form class="home-search" action="">
			<input type="text" placeholder="Search for footages and themes" name="home-search">
			<button type="submit"><img src="images/search-icon.svg" alt=""></button>
		</form>
	</div>
	<!-- Mobile Search -->
	<!-- Innerpage Banner -->
	<section class="page-section mb-0">
		<div class="innerpage-banner innerpage-banner-transparent pt-50 pb-0">
			<div class="innerpage-banner-grey-bg full-height"></div>
			<div class="container">
				<div class="row">
					<div class="col-12 no-padding-mobile">													
						<h1 class="page-title-large pp-title text-uppercase">Terms and Conditions</h1>	
						<div class="profile-tabs-wrapper editprofile-tabs-wrapper">
							<div class="tabs">
								<ul class="tabs__items">
									<li><a href="#/" class="tnc-link active">Terms & Conditions</a></li>
									<li><a href="#/" class="pp-link">Privacy Policy</a></li>	
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Innerpage Banner -->
	<!-- Terms & Conditions Section -->
	<section class="page-section pt-50 pb-50 ptmob-25 pbmob-25" id="terms-conditions">
		<div class="container">
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 offset-lg-2 col-lg-8">
					<div class="tnc-wrapper">
						<div class="tnc-question">
							<p>What information do we collect?</p>
						</div>
						<div class="tnc-reply">
							<p>We collect information from you when you subscribe to our newsletter or fill out a form.</p>
							<p>When ordering or registering on our site, as appropriate, you may be asked to enter your: name, e-mail address or phone number. You may, however, visit our site anonymously.</p>
						</div>
						<div class="tnc-question">
							<p>What do we use your information for?</p>
						</div>
						<div class="tnc-reply">
							<p>Any of the information we collect from you may be used in one of the following ways:</p>
							<ul>
								<li>To personalize your experience<br/>(your information helps us to better respond to your individual needs)</li>
								<li>To improve our website<br/>(we continually strive to improve our website offerings based on the information and feedback we receive from you)</li>
								<li>To improve customer service<br/>(your information helps us to more effectively respond to your customer service requests and support needs)</li>
								<li>To administer a contest, promotion, survey or other site feature</li>
								<li>To send periodic emails</li>								
							</ul>
							<p>The email address you provide may be used to send you information, respond to inquiries, and/or other requests or questions.</p>
							<p>Note: If at any time you would like to unsubscribe from receiving future emails, we include detailed unsubscribe instructions at the bottom of each email.</p>
						</div>
						<div class="tnc-question">
							<p>Do we use cookies?</p>
						</div>
						<div class="tnc-reply">
							<p>We do not use cookies.</p>
						</div>
						<div class="tnc-question">
							<p>Do we disclose any information to outside parties?</p>
						</div>
						<div class="tnc-reply">
							<p>We do not sell, trade, or otherwise transfer to outside parties your personally identifiable information. This does not include trusted third parties who assist us in operating our website, conducting our business, or servicing you, so long as those parties agree to keep this information confidential. We may also release your information when we believe release is appropriate to comply with the law, enforce our site policies, or protect ours or others rights, property, or safety. However, non-personally identifiable visitor information may be provided to other parties for marketing, advertising, or other uses.</p>
							<p><strong>California Online Privacy Protection Act Compliance</strong><br/>Because we value your privacy we have taken the necessary precautions to be in compliance with the California Online Privacy Protection Act. We therefore will not distribute your personal information to outside parties without your consent.</p>
							<p><strong>Childrens Online Privacy Protection Act Compliance</strong><br/>We are in compliance with the requirements of COPPA (Childrens Online Privacy Protection Act), we do not collect any information from anyone under 13 years of age. Our website, products and services are all directed to people who are at least 13 years old or older.</p>
							<p><strong>Your Consent</strong><br/>By using our site, you consent to our web site privacy policy.</p>
							<p><strong>Changes to our Privacy Policy</strong><br/>If we decide to change our privacy policy, we will update the Privacy Policy modification date below.</p>
							<p><strong>Contacting Us</strong><br/>If there are any questions regarding this privacy policy you may contact us using the information below:<br/>+44(0) 207 631 3773<br/>info@footagelfarm.com</p>
						</div>
						<div class="info-msg-wrapper">
							<img src="images/informartion.svg" alt="">
							<span>This policy was last modified on May 1, 2020</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Terms & Conditions Section -->
	<!-- Privacy Policy Section -->
	<section class="page-section pt-50 pb-50 ptmob-25 pbmob-25" id="privacy-policy">
		<div class="container">		
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 offset-lg-2 col-lg-8">
				<div class="tnc-wrapper">
						<div class="tnc-question">
							<p>What information do we collect?</p>
						</div>
						<div class="tnc-reply">
							<p>We collect information from you when you subscribe to our newsletter or fill out a form.</p>
							<p>When ordering or registering on our site, as appropriate, you may be asked to enter your: name, e-mail address or phone number. You may, however, visit our site anonymously.</p>
						</div>
						<div class="tnc-question">
							<p>What do we use your information for?</p>
						</div>
						<div class="tnc-reply">
							<p>Any of the information we collect from you may be used in one of the following ways:</p>
							<ul>
								<li>To personalize your experience<br/>(your information helps us to better respond to your individual needs)</li>
								<li>To improve our website<br/>(we continually strive to improve our website offerings based on the information and feedback we receive from you)</li>
								<li>To improve customer service<br/>(your information helps us to more effectively respond to your customer service requests and support needs)</li>
								<li>To administer a contest, promotion, survey or other site feature</li>
								<li>To send periodic emails</li>								
							</ul>
							<p>The email address you provide may be used to send you information, respond to inquiries, and/or other requests or questions.</p>
							<p>Note: If at any time you would like to unsubscribe from receiving future emails, we include detailed unsubscribe instructions at the bottom of each email.</p>
						</div>
						<div class="tnc-question">
							<p>Do we use cookies?</p>
						</div>
						<div class="tnc-reply">
							<p>We do not use cookies.</p>
						</div>
						<div class="tnc-question">
							<p>Do we disclose any information to outside parties?</p>
						</div>
						<div class="tnc-reply">
							<p>We do not sell, trade, or otherwise transfer to outside parties your personally identifiable information. This does not include trusted third parties who assist us in operating our website, conducting our business, or servicing you, so long as those parties agree to keep this information confidential. We may also release your information when we believe release is appropriate to comply with the law, enforce our site policies, or protect ours or others rights, property, or safety. However, non-personally identifiable visitor information may be provided to other parties for marketing, advertising, or other uses.</p>
							<p><strong>California Online Privacy Protection Act Compliance</strong><br/>Because we value your privacy we have taken the necessary precautions to be in compliance with the California Online Privacy Protection Act. We therefore will not distribute your personal information to outside parties without your consent.</p>
							<p><strong>Childrens Online Privacy Protection Act Compliance</strong><br/>We are in compliance with the requirements of COPPA (Childrens Online Privacy Protection Act), we do not collect any information from anyone under 13 years of age. Our website, products and services are all directed to people who are at least 13 years old or older.</p>
							<p><strong>Your Consent</strong><br/>By using our site, you consent to our web site privacy policy.</p>
							<p><strong>Changes to our Privacy Policy</strong><br/>If we decide to change our privacy policy, we will update the Privacy Policy modification date below.</p>
							<p><strong>Contacting Us</strong><br/>If there are any questions regarding this privacy policy you may contact us using the information below:<br/>+44(0) 207 631 3773<br/>info@footagelfarm.com</p>
						</div>
						<div class="info-msg-wrapper">
							<img src="images/informartion.svg" alt="">
							<span>This policy was last modified on May 1, 2020</span>
						</div>
					</div>
				</div>
			</div>			
		</div>
	</section>
	<!-- Privacy Policy Section -->
</section>
<?php require_once('include/footer.php') ?>
<?php require_once('include/footer-scripts.php') ?>
<script>
	$(document).ready(function() {
		$('.tnc-link').click(function() {
			$('#privacy-policy').hide();
			$('#terms-conditions').show();
			$('.pp-title').text('Terms & Conditions');
		})
		$('.pp-link').click(function() {
			$('#terms-conditions').hide();
			$('#privacy-policy').show();
			$('.pp-title').text('Privacy Policy');
		})
		$('.tabs__items li a').click(function() {
			$('.tabs__items li a').removeClass('active');
			$(this).addClass('active');
		})	
	});
</script>
</body>

</html>