<?php require_once('include/head.php') ?>
<div class="wrapper-404">
	<div class="content-404">
		<h2 class="error-404-title">Error 404</h2>
		<a href="index.php" class="btn-ff btn-tertiary-dark btn-404 text-uppercase">Go to Homepage</a>
	</div>	
</div>
<div class="multicolored-line multicolored-line-404">
	<div></div>
	<div></div>
	<div></div>
	<div></div>
	<div></div>
	<div></div>
	<div></div>
</div>
<div class="multicolored-line multicolored-line-404-2">
	<div></div>
	<div></div>
	<div></div>
	<div></div>
	<div></div>
	<div></div>
	<div></div>
</div>
<div class="multicolored-line multicolored-line-404-3">
	<div></div>
	<div></div>
	<div></div>
	<div></div>
	<div></div>
	<div></div>
	<div></div>
	<div></div>
</div>
<?php require_once('include/footer-scripts.php') ?>
</body>
</html>