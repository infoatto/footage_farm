<?php require_once('include/head.php') ?>
<?php require_once('include/navigation.php') ?>
<!-- Innerpage Search -->
<section class="page-container innerpage-top">
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="searchbar-top">
		<div class="home-search-wrapper">
			<form class="home-search" action="">
				<input type="text" placeholder="Search for footages and themes" name="home-search">
				<button type="submit"><img src="images/search-icon.svg" alt=""></button>
			</form>
		</div>
	</div>
</section>
<!-- Innerpage Search -->
<section class="page-container">
	<!-- Mobile Search -->
	<div class="multicolored-line multicolored-line-mobile">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="home-search-wrapper search-wrapper-mobile">
		<form class="home-search" action="">
			<input type="text" placeholder="Search for footages and themes" name="home-search">
			<button type="submit"><img src="images/search-icon.svg" alt=""></button>
		</form>
	</div>
	<!-- Mobile Search -->
	<!-- Innerpage Banner -->
	<section class="page-section mb-0">
		<div class="innerpage-banner pt-50 pb-50 ptmob-25 pbmob-25 how-we-work-banner">
			<div class="innerpage-banner-grey-bg full-height"></div>
			<div class="container">
				<div class="row">
					<div class="col-12 offset-sm-1 col-sm-10">
						<h1 class="page-title-large text-uppercase">How we operate</h1>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Innerpage Banner -->
	<!-- How we work content -->
	<section class="page-section pt-40 mb-0">		
		<div class="container">
			<div class="row">
				<div class="col-12 offset-sm-1 col-sm-10">
					<div class="howwework-content-wrapper">
						<p>Footage Farm is the world’s leading video archive with over 1.7 million video stories dating back to 1895. Our collection covers everything from global news and entertainment to lifestyle and sport, sourced from Footage Farm’s own coverage and from our premium content partners. </p>

						<p>We offer a quick and easy self-serve solution allowing customers to research independently, as well as dedicated research help and advice from our team of experts.</p>

						<p>Below you’ll find details of how to use our archive. For a quick outline, check out this short video which summarises everything you need to know.</p>

						<p>If you need help with your research or just some advice and guidance on using our website, please <a href="#/" class="primary-link">contact us</a></p>
					</div>
					<div class="row pointer-section">
						<div class="col-12 col-sm-4 col-lg-3">
							<h2 class="hww-title"><img src="images/oval.svg" alt=""> <span>Research</span></h2>
						</div>
						<div class="col-12 col-sm-8 col-lg-9">
							<div class="hww-pointers">
								<div class="hww-pointer-single">
									<div class="pointer-number">
										<p>1.</p>
									</div>
									<div class="pointer-content">
										<p>Our simple video search allows you to find the video footage you need quickly and easily. If you are searching for a specific story, the Advanced Search feature will help you narrow down your results.</p>
									</div>
								</div>
								<div class="hww-pointer-single">
									<div class="pointer-number">
										<p>2.</p>
									</div>
									<div class="pointer-content">
										<p>You are able to view the associated thumbnail timeline, shotlist and metadata for all our videos. </p>
									</div>
								</div>
								<div class="hww-pointer-single">
									<div class="pointer-number">
										<p>3.</p>
									</div>
									<div class="pointer-content">
										<p>If you are searching for video footage on a person, event, or location, then take a look at our ready-made compilations - we may have already created a compilation on that subject.</p>
									</div>
								</div>
								<div class="hww-pointer-single">
									<div class="pointer-number">
										<p>4.</p>
									</div>
									<div class="pointer-content">
										<p>Once you find your video you can request a screener (via download or DVD) which allows you to review the video in full - all screeners include a Burnt-In Timecode (BITC).</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row pointer-section">
						<div class="col-12 col-sm-4 col-lg-3">
							<h2 class="hww-title"><img src="images/path.svg" alt=""> <span>Previews</span></h2>
						</div>
						<div class="col-12 col-sm-8 col-lg-9">
							<div class="hww-pointers">
								<div class="hww-pointer-single">
									<div class="pointer-number">
										<p>1.</p>
									</div>
									<div class="pointer-content">
										<p>Our simple video search allows you to find the video footage you need quickly and easily. If you are searching for a specific story, the Advanced Search feature will help you narrow down your results.</p>
									</div>
								</div>
								<div class="hww-pointer-single">
									<div class="pointer-number">
										<p>2.</p>
									</div>
									<div class="pointer-content">
										<p>You are able to view the associated thumbnail timeline, shotlist and metadata for all our videos. </p>
									</div>
								</div>
								<div class="hww-pointer-single">
									<div class="pointer-number">
										<p>3.</p>
									</div>
									<div class="pointer-content">
										<p>If you are searching for video footage on a person, event, or location, then take a look at our ready-made compilations - we may have already created a compilation on that subject.</p>
									</div>
								</div>
								<div class="hww-pointer-single">
									<div class="pointer-number">
										<p>4.</p>
									</div>
									<div class="pointer-content">
										<p>Once you find your video you can request a screener (via download or DVD) which allows you to review the video in full - all screeners include a Burnt-In Timecode (BITC).</p>
									</div>
								</div>
								<div class="hww-pointer-single">
									<div class="pointer-number">
										<p>5.</p>
									</div>
									<div class="pointer-content">
										<p>Click the Request Footage icon (located beneath each video).</p>
									</div>
								</div>
								<div class="hww-pointer-single">
									<div class="pointer-number">
										<p>6.</p>
									</div>
									<div class="pointer-content">
										<p>Select Screener and your Delivery Method – please note, Online Download will allow you to view the screener immediately (if the video has been digitised) by clicking in your ORDER HISTORY and selecting Download Now.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row pointer-section">
						<div class="col-12 col-sm-4 col-lg-3">
							<h2 class="hww-title"><img src="images/rectangle.svg" alt=""> <span>Masters</span></h2>
						</div>
						<div class="col-12 col-sm-8 col-lg-9">
							<div class="hww-pointers">
								<div class="hww-pointer-single">
									<div class="pointer-number">
										<p>1.</p>
									</div>
									<div class="pointer-content">
										<p>Our simple video search allows you to find the video footage you need quickly and easily. If you are searching for a specific story, the Advanced Search feature will help you narrow down your results.</p>
									</div>
								</div>
								<div class="hww-pointer-single">
									<div class="pointer-number">
										<p>2.</p>
									</div>
									<div class="pointer-content">
										<p>You are able to view the associated thumbnail timeline, shotlist and metadata for all our videos. </p>
									</div>
								</div>
								<div class="hww-pointer-single">
									<div class="pointer-number">
										<p>3.</p>
									</div>
									<div class="pointer-content">
										<p>If you are searching for video footage on a person, event, or location, then take a look at our ready-made compilations - we may have already created a compilation on that subject.</p>
									</div>
								</div>
								<div class="hww-pointer-single">
									<div class="pointer-number">
										<p>4.</p>
									</div>
									<div class="pointer-content">
										<p>Once you find your video you can request a screener (via download or DVD) which allows you to review the video in full - all screeners include a Burnt-In Timecode (BITC).</p>
									</div>
								</div>
								<div class="hww-pointer-single">
									<div class="pointer-number">
										<p>5.</p>
									</div>
									<div class="pointer-content">
										<p>Click the Request Footage icon (located beneath each video).</p>
									</div>
								</div>
								<div class="hww-pointer-single">
									<div class="pointer-number">
										<p>6.</p>
									</div>
									<div class="pointer-content">
										<p>Select Screener and your Delivery Method – please note, Online Download will allow you to view the screener immediately (if the video has been digitised) by clicking in your ORDER HISTORY and selecting Download Now.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 offset-sm-2 col-sm-8"></div>
			</div>
		</div>
	</section>
	<section class="page-section pt-75 mb-0 similar-section-wrapper">
		<div class="grey-bg"></div>
		<div class="container">
			<div class="row">
				<div class="col-12 offset-sm-2 col-sm-8">
					<div class="hww-bottom-content">
						<h3>Ordering your footage</h3>
						<p>Once you have signed and returned your License Agreement (and made a minimum payment (for new customers)) you can download your high-resolution master footage from the website.</p>

						<p>Click Request Footage, select Master as the format, and then your preferred delivery method.</p>

						<p>Once you have submitted your request, go to ORDER HISTORY where you will be able to download your master copy. </p>
					</div>					
					<div class="hww-bottom-content">
						<h3>Declaring usage</h3>
						<p>When you have completed the final edit of your master copy, go to ORDER HISTORY, click the Licenses tab and then Declare on your license.</p>

						<p>Declare how many minutes/seconds you have used. We will charge you for your final total in increments of 15 seconds.</p>
					</div>
					<div class="hww-bottom-content">
						<h3>Research Help</h3>
						<p>Our Archive team offers a bespoke, personal service tailored to our customers’ research requirements – costing depends upon research time, and the rights requested and intended use of the footage.</p>

						<p>Our Researchers have an in-depth knowledge of our collection, acute research skills and great instinct, so if you need help with your search, please contact us.</p>

						<p>You can begin your search by <a href="#/" class="primary-link">registering here</a></p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- How we work content -->
</section>
<!-- Share Blog Post -->
<div style="display: none;max-width:580px;" id="sharetheme-content" class="ff-modal">
	<div class="modal-header">
		<h3>Share Blog</h3>
	</div>
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="ff-modal-content">
		<form class="share-reel-form">
			<div class="modal-section">
				<div class="form-group">
					<input type="text" class="form-control ff-field-dark linkfield" id="reelink" name="reelink" placeholder="Blog link will be shown here">
					<a href="#/" class="copylink">Copy Link</a>
				</div>
				<div class="form-group">
					<label for="social">Social</label>
					<div class="modal-social-share">
						<a href="#/"><img src="images/twitter-black.svg" alt="Share on Twitter"></a>
						<a href="#/"><img src="images/facebook-black.svg" alt="Share on Facebook"></a>
						<a href="#/"><img src="images/linkedin-black.svg" alt="Share on Linkedin"></a>
					</div>
				</div>
				<div class="form-group">
					<label for="sendtoemail">Send to Email Address</label>
					<input type="text" class="form-control ff-field-dark" id="sendtoemail" name="sendtoemail" required>
					<span class="emailaddress">niraali@bokapp.design <a href="#/"><img src="images/close-black.svg" alt=""></a></span>
					<span class="emailaddress">gonzalo.c@footagefarm.com <a href="#/"><img src="images/close-black.svg" alt=""></a></span>
					<span class="emailaddress">gintare.k@footagefarm.com <a href="#/"><img src="images/close-black.svg" alt=""></a></span>
					<span class="emailaddress">mat.f@footagefarm.com <a href="#/"><img src="images/close-black.svg" alt=""></a></span>
				</div>
			</div>
			<div class="modal-content-wrapper">
				<div class="modal-actions">
					<input type="submit" data-fancybox-close class="btn-ff btn-secondary-dark text-uppercase" value="Cancel">
					<input type="submit" class="btn-ff btn-primary-dark text-uppercase" value="Send">
				</div>
			</div>
		</form>
	</div>
</div>
<!-- Share Blog Post -->
<?php require_once('include/footer.php') ?>
<?php require_once('include/footer-scripts.php') ?>
<script>
	$(document).ready(function() {
		/*Subtheme video select*/
		if ($(window).width() > 991) {
			$('.blog-single a').hover(function() {

				$(this).children('.hover-options-container').children('.box-single-checkbox').show();
				$(this).children('.hover-options-container').children('.box-single-share').show();

			}, function() {

				$(this).children('.hover-options-container').children('.box-single-checkbox').hide();
				$(this).children('.hover-options-container').children('.box-single-share').hide();

				if ($(this).children('.hover-options-container').children('.box-single-checkbox').children('label.checkbox-container').children('input[type="checkbox"]').is(':checked')) {

					$(this).children('.hover-options-container').children('.box-single-checkbox').addClass('selected');
					$(this).children('.hover-options-container').addClass('selected');

				} else {

					$(this).children('.hover-options-container').children('.box-single-checkbox').removeClass('selected');
					$(this).children('.hover-options-container').removeClass('selected');

				}
			});
		}
		/*Subtheme video select*/
	})
</script>
</body>

</html>