<?php require_once('include/head.php') ?>
<?php require_once('include/navigation.php') ?>
<!-- Innerpage Search -->
<section class="page-container innerpage-top">
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="searchbar-top">
		<div class="home-search-wrapper">
			<form class="home-search" action="">
				<input type="text" placeholder="Search for footages and themes" name="home-search">
				<button type="submit"><img src="images/search-icon.svg" alt=""></button>
			</form>
		</div>
	</div>
</section>
<!-- Innerpage Search -->
<section class="page-container">
	<!-- Mobile Search -->
	<div class="multicolored-line multicolored-line-mobile">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="home-search-wrapper search-wrapper-mobile">
		<form class="home-search" action="">
			<input type="text" placeholder="Search for footages and themes" name="home-search">
			<button type="submit"><img src="images/search-icon.svg" alt=""></button>
		</form>
	</div>
	<!-- Mobile Search -->
	<!-- Innerpage Banner -->
	<section class="page-section mb-0">
		<div class="innerpage-banner innerpage-banner-transparent pt-50 ptmob-25 pb-0">
			<div class="innerpage-banner-grey-bg full-height"></div>
			<div class="container">
				<div class="row">
					<div class="col-12 no-padding-mobile">
						<div class="profile-top-wrapper">							
							<h1 class="page-title-large text-uppercase">My Profile</h1>							
						</div>
						<div class="profile-tabs-wrapper editprofile-tabs-wrapper">
							<div class="tabs">
								<ul class="tabs__items">
									<li><a href="#/" class="edit-profile-link active">Edit Profile</a></li>
									<li><a href="#/" class="change-password-link">Change Password</a></li>	
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Innerpage Banner -->
	<!-- Edit Profile Section -->
	<section class="page-section pt-50 pb-50 ptmob-25 pbmob-25" id="edit-profile">
		<div class="container">
			<div class="row">
				<div class="col-12 offset-sm-2 col-sm-6">
					<div class="edit-profile-wrapper">
						<form>
							<div class="form-group">
								<div class="profile-pic-wrapper">
									<img src="https://via.placeholder.com/100x100.png" alt="" class="edit-profilepic">
									<div class="changepic-btn">
										<input type="button" class="btn-ff btn-secondary-dark text-uppercase imgupload-btn" value="Update Photo">
										<input type="file" id="imgupload" style="display:none"/> 
										<p>Add a jpeg file under 200Kb.</p>
									</div>
								</div>
								
							</div>
							<div class="form-group">
								<label for="username">Name</label>
								<input type="text" class="form-control ff-field-dark" id="username" name="username">						
							</div>
							<div class="form-group">
								<label for="useremail">Email</label>
								<input type="email" class="form-control ff-field-dark" id="useremail" name="useremail">						
							</div>
							<div class="form-group">
								<label for="userphone">Phone</label>
								<input type="text" class="form-control ff-field-dark" id="userphone" name="userphone">						
							</div>
							<div class="form-group">
								<input type="submit" class="btn-ff btn-primary-dark text-uppercase" value="Update Account">
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Edit Profile Section -->
	<!-- Update Password Section -->
	<section class="page-section pt-50 pb-50 ptmob-25 pbmob-25" id="change-password">
		<div class="container">		
			<div class="row">
				<div class="col-12 offset-sm-2 col-sm-6">
					<div class="edit-profile-wrapper">
						<form>
							<div class="form-group">
								<label for="changepassword">Change Password</label>
								<input type="password" class="form-control ff-field-dark" id="changepassword" name="changepassword">						
							</div>
							<div class="form-group">
								<label for="setpassword">Set a Password</label>
								<input type="password" class="form-control ff-field-dark" id="password" name="password">
								<a href="#/" class="showpassword"><img src="images/show.svg" alt=""></a>	
							</div>
							<div class="form-group">
								<label for="confirmpassword">Password Confirmation</label>
								<input type="password" class="form-control ff-field-dark" id="confirmpassword" name="confirmpassword">
								<a href="#/" class="confirmpassword"><img src="images/show.svg" alt=""></a>	
							</div>
							<div class="form-group">
								<input type="submit" class="btn-ff btn-primary-dark text-uppercase" value="Change Password">
							</div>
						</form>
					</div>
				</div>
			</div>			
		</div>
	</section>
	<!-- Update Password Section -->	
</section>
<?php require_once('include/footer.php') ?>
<?php require_once('include/footer-scripts.php') ?>
<script>
	$(document).ready(function() {
		$('.edit-profile-link').click(function() {
			$('#change-password').hide();
			$('#edit-profile').show();
		})
		$('.change-password-link').click(function() {
			$('#edit-profile').hide();
			$('#change-password').show();
		})
		$('.tabs__items li a').click(function() {
			$('.tabs__items li a').removeClass('active');
			$(this).addClass('active');
		})		
		$('.imgupload-btn').click(function(){
			$('#imgupload').trigger('click'); 
		})
	});
</script>
</body>

</html>