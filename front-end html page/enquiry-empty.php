<?php require_once('include/head.php') ?>
<?php require_once('include/navigation.php') ?>
<!-- Innerpage Search -->
<section class="page-container innerpage-top">	
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="searchbar-top">
		<div class="home-search-wrapper">
			<form class="home-search" action="">
				<input type="text" placeholder="Search for footages and themes" name="home-search">
				<button type="submit"><img src="images/search-icon.svg" alt=""></button>
			</form>
		</div>
	</div>				
</section>
<!-- Innerpage Search -->
<section class="page-container">
	<!-- Mobile Search -->
	<div class="multicolored-line multicolored-line-mobile">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="home-search-wrapper search-wrapper-mobile">
		<form class="home-search" action="">
			<input type="text" placeholder="Search for footages and themes" name="home-search">
			<button type="submit"><img src="images/search-icon.svg" alt=""></button>
		</form>
	</div>
	<!-- Mobile Search -->	
	<!-- Empty cart section -->
	<section class="page-section pt-50 pb-50 ptmob-25 pbmob-25">
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-12 col-lg-5">
					<h3 class="section-title-small">Your Cart</h3>
					<div class="empty-cart-left">
						<p class="empty-cart-text">Add Reels to cart to make a collective Enquiry. </p>
						<a href="#/" class="btn-ff btn-primary-dark text-uppercase">Explore Reels</a>
					</div>
				</div>
				<div class="col-12 col-md-12 col-lg-7">
					<h3 class="section-title-small">Have any questions? Write to us your specific reel request</h3>
					<div class="enquiry-form-container">
						<form action="" class="cart-enquiry-form">
							<div class="enquiry-form">
								<div class="form-group">
									<label for="name">Name</label>
									<input type="text" class="form-control ff-field-dark" id="name" name="name" required>
								</div>
								<div class="form-group-half-wrapper">
									<div class="form-group form-group-half">
										<label for="email">Email</label>
										<input type="email" class="form-control ff-field-dark" id="email" name="email" required>
									</div>
									<div class="form-group form-group-half">
										<label for="phone">Phone Number (optional)</label>
										<input type="text" class="form-control ff-field-dark" id="phone" name="phone" required>
									</div>
								</div>
								<div class="form-group-half-wrapper">
									<div class="form-group form-group-half">
										<label for="company">Company Name*</label>
										<input type="email" class="form-control ff-field-dark" id="company" name="company" required>
									</div>
									<div class="form-group form-group-half">
										<label for="project">Project Name (optional)</label>
										<input type="text" class="form-control ff-field-dark" id="project" name="project" required>
									</div>
								</div>
								<div class="form-group">
									<label for="reelurl">URL of the Reel (optional)</label>
									<input type="text" class="form-control ff-field-dark" id="reelurl" name="reelurl" required>
								</div>
								<div id="enquiry-addmore-container">

								</div>
								<div class="form-addmore">
									<a href="#/" class="form-addmore-link enquiry-addmore-link"><img src="images/add-black.svg" alt=""> <span>Add another URL</span></a>
								</div>
								
								<div class="form-group">
									<label for="projectdesc">Message (optional)</label>
									<textarea class="form-control ff-field-dark" rows="5"></textarea>	
								</div>
							</div>
							<div class="enquiry-form-actions">								
								<a href="#/" class="btn-ff btn-secondary-dark text-uppercase">Cancel</a>
								<input type="submit" class="btn-ff btn-primary-dark text-uppercase" value="Send">	
							</div>							
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>		
	<!-- Empty cart section -->
</section>
<?php require_once('include/footer.php') ?>
<?php require_once('include/footer-scripts.php') ?>
<script>
	$(document).ready(function(){
		$('.cart-enquiry-form').validate();	
		/*Addmore Start/ End TC*/
		$('.enquiry-addmore-link').click(function(){
			$('#enquiry-addmore-container').append(
				"<div class='form-group addon-url'>"+
				"<label for='reelurl'>URL of the Reel (optional)</label>"+
				"<input type='text' class='form-control ff-field-dark' id='reelurl' name='reelurl' required>"+
				"<a href='#/' class='remove-enquiry-url'><img src='images/close.svg'/></a>"+
				"</div>"
			)
		});	
		$('body').on('click', '.remove-enquiry-url', function() {
				$(this).closest('.addon-url').remove();       
    });
	});
</script>
</body>
</html>