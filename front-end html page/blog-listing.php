<?php require_once('include/head.php') ?>
<?php require_once('include/navigation.php') ?>
<!-- Innerpage Search -->
<section class="page-container innerpage-top">
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="searchbar-top">
		<div class="home-search-wrapper">
			<form class="home-search" action="">
				<input type="text" placeholder="Search for footages and themes" name="home-search">
				<button type="submit"><img src="images/search-icon.svg" alt=""></button>
			</form>
		</div>
	</div>
</section>
<!-- Innerpage Search -->
<section class="page-container">
	<!-- Mobile Search -->
	<div class="multicolored-line multicolored-line-mobile">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="home-search-wrapper search-wrapper-mobile">
		<form class="home-search" action="">
			<input type="text" placeholder="Search for footages and themes" name="home-search">
			<button type="submit"><img src="images/search-icon.svg" alt=""></button>
		</form>
	</div>
	<!-- Mobile Search -->
	<!-- Innerpage Banner -->
	<section class="page-section mb-0">
		<div class="innerpage-banner pbmob-25">
			<div class="innerpage-banner-grey-bg full-height"></div>
			<div class="container">
				<div class="row">
					<div class="col-12">
						<h1 class="page-title-large text-uppercase">Blog</h1>
					</div>
					<div class="col-12">
						<div class="ff-filters allthemes-filters filters-left">
							<div class="allthemes-filter-single">
								<select name="" id="" class="niceselect">
									<option value="">Category</option>
									<option value="Early Reel (423)">Early Reel (423)</option>
									<option value="Personalities (34)">Personalities (34)</option>
									<option value="Educational Flims (18)">Educational Flims (18)</option>
									<option value="Hungarian Revolution (45)">Hungarian Revolution (45)</option>
									<option value="Space Exploration (12)">Space Exploration (12)</option>
									<option value="Russian Revolution (3)">Russian Revolution (3)</option>
									<option value="World War I (131)">World War I (131)</option>
									<option value="World War II (343)">World War II (343)</option>
									<option value="1960’s Civil Right Movements (7)">1960’s Civil Right Movements (7)</option>
									<option value="Home Movies (81)">Home Movies (81)</option>
								</select>
							</div>
						</div>
						<div class="ff-filters allthemes-filters filters-right blog-filter-right">
							<p>Sort By</p>
							<div class="allthemes-filter-single">
								<select name="" id="" class="niceselect">
									<option value="Latest">Latest</option>
									<option value="Most Viewed">Most Viewed</option>
									<option value="Popular">Popular</option>
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Innerpage Banner -->
	<!-- Blog Listing -->
	<section class="page-section pt-40">
		<div class="theme-select-overlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-6 col-lg-4">
					<div class="blog-single">
						<a href="#/">
							<div class="blog-title-date">
								<h3>Blog Title Will Come Here Like This In Three Lines Maximum</h3>
								<div class="date-info">
									<span class="date-number">16</span>
									<span class="month">June</span>
									<span class="year">2020</span>
								</div>
							</div>
							<div class="blog-author">
								<div class="arrow-right-dark-small"></div>
								<p>Jordan Megyery</p>
							</div>
						</a>
						<div class="blog-tags">
							<a href="#/">film</a>
							<a href="#/">care</a>
							<a href="#/">archive</a>
						</div>
						<a href="#/">
							<div class="hover-options-container">								
								<div class="box-single-share">									
									<span class="share-single share-social" data-fancybox data-src="#sharetheme-content" data-options='{"touch" : false}'></span>
									<span class="share-msg-text share-social-text">
										Share
									</span>									
								</div>
								<img src="https://via.placeholder.com/377x212.png" alt="" class="img-fluid">
							</div>
						</a>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-4">
					<div class="blog-single">
						<a href="#/">
							<div class="blog-title-date">
								<h3>Blog Title Will Come Here Like This In Three Lines Maximum</h3>
								<div class="date-info">
									<span class="date-number">16</span>
									<span class="month">June</span>
									<span class="year">2020</span>
								</div>
							</div>
							<div class="blog-author">
								<div class="arrow-right-dark-small"></div>
								<p>Jordan Megyery</p>
							</div>
						</a>
						<div class="blog-tags">
							<a href="#/">film</a>
							<a href="#/">care</a>
							<a href="#/">archive</a>
						</div>
						<a href="#/">
							<div class="hover-options-container">								
								<div class="box-single-share">									
									<span class="share-single share-social" data-fancybox data-src="#sharetheme-content" data-options='{"touch" : false}'></span>
									<span class="share-msg-text share-social-text">
										Share
									</span>									
								</div>
								<img src="https://via.placeholder.com/377x212.png" alt="" class="img-fluid">
							</div>
						</a>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-4">
					<div class="blog-single">
						<a href="#/">
							<div class="blog-title-date">
								<h3>Blog Title Will Come Here Like This In Three Lines Maximum</h3>
								<div class="date-info">
									<span class="date-number">16</span>
									<span class="month">June</span>
									<span class="year">2020</span>
								</div>
							</div>
							<div class="blog-author">
								<div class="arrow-right-dark-small"></div>
								<p>Jordan Megyery</p>
							</div>
						</a>
						<div class="blog-tags">
							<a href="#/">film</a>
							<a href="#/">care</a>
							<a href="#/">archive</a>
						</div>
						<a href="#/">
							<div class="hover-options-container">								
								<div class="box-single-share">									
									<span class="share-single share-social" data-fancybox data-src="#sharetheme-content" data-options='{"touch" : false}'></span>
									<span class="share-msg-text share-social-text">
										Share
									</span>									
								</div>
								<img src="https://via.placeholder.com/377x212.png" alt="" class="img-fluid">
							</div>
						</a>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-4">
					<div class="blog-single">
						<a href="#/">
							<div class="blog-title-date">
								<h3>Blog Title Will Come Here Like This In Three Lines Maximum</h3>
								<div class="date-info">
									<span class="date-number">16</span>
									<span class="month">June</span>
									<span class="year">2020</span>
								</div>
							</div>
							<div class="blog-author">
								<div class="arrow-right-dark-small"></div>
								<p>Jordan Megyery</p>
							</div>
						</a>
						<div class="blog-tags">
							<a href="#/">film</a>
							<a href="#/">care</a>
							<a href="#/">archive</a>
						</div>
						<a href="#/">
							<div class="hover-options-container">								
								<div class="box-single-share">									
									<span class="share-single share-social" data-fancybox data-src="#sharetheme-content" data-options='{"touch" : false}'></span>
									<span class="share-msg-text share-social-text">
										Share
									</span>									
								</div>
								<img src="https://via.placeholder.com/377x212.png" alt="" class="img-fluid">
							</div>
						</a>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-4">
					<div class="blog-single">
						<a href="#/">
							<div class="blog-title-date">
								<h3>Blog Title Will Come Here Like This In Three Lines Maximum</h3>
								<div class="date-info">
									<span class="date-number">16</span>
									<span class="month">June</span>
									<span class="year">2020</span>
								</div>
							</div>
							<div class="blog-author">
								<div class="arrow-right-dark-small"></div>
								<p>Jordan Megyery</p>
							</div>
						</a>
						<div class="blog-tags">
							<a href="#/">film</a>
							<a href="#/">care</a>
							<a href="#/">archive</a>
						</div>
						<a href="#/">
							<div class="hover-options-container">								
								<div class="box-single-share">									
									<span class="share-single share-social" data-fancybox data-src="#sharetheme-content" data-options='{"touch" : false}'></span>
									<span class="share-msg-text share-social-text">
										Share
									</span>									
								</div>
								<img src="https://via.placeholder.com/377x212.png" alt="" class="img-fluid">
							</div>
						</a>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-4">
					<div class="blog-single">
						<a href="#/">
							<div class="blog-title-date">
								<h3>Blog Title Will Come Here Like This In Three Lines Maximum</h3>
								<div class="date-info">
									<span class="date-number">16</span>
									<span class="month">June</span>
									<span class="year">2020</span>
								</div>
							</div>
							<div class="blog-author">
								<div class="arrow-right-dark-small"></div>
								<p>Jordan Megyery</p>
							</div>
						</a>
						<div class="blog-tags">
							<a href="#/">film</a>
							<a href="#/">care</a>
							<a href="#/">archive</a>
						</div>
						<a href="#/">
							<div class="hover-options-container">								
								<div class="box-single-share">									
									<span class="share-single share-social" data-fancybox data-src="#sharetheme-content" data-options='{"touch" : false}'></span>
									<span class="share-msg-text share-social-text">
										Share
									</span>									
								</div>
								<img src="https://via.placeholder.com/377x212.png" alt="" class="img-fluid">
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Blog Listing -->
</section>
<!-- Share Blog Post -->
<div style="display: none;max-width:580px;" id="sharetheme-content" class="ff-modal">
	<div class="modal-header">		
		<h3>Share Blog</h3>		
	</div>
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="ff-modal-content">
		<form class="share-reel-form">
			<div class="modal-section">			
					<div class="form-group">					
						<input type="text" class="form-control ff-field-dark linkfield" id="reelink" name="reelink" placeholder="Blog link will be shown here">
						<a href="#/" class="copylink">Copy Link</a>						
					</div>
					<div class="form-group">
						<label for="social">Social</label>
						<div class="modal-social-share">
							<a href="#/"><img src="images/twitter-black.svg" alt="Share on Twitter"></a>
							<a href="#/"><img src="images/facebook-black.svg" alt="Share on Facebook"></a>
							<a href="#/"><img src="images/linkedin-black.svg" alt="Share on Linkedin"></a>				
						</div>
					</div>
					<div class="form-group">
						<label for="sendtoemail">Send to Email Address</label>
						<input type="text" class="form-control ff-field-dark" id="sendtoemail" name="sendtoemail" required>		
						<span class="emailaddress">niraali@bokapp.design <a href="#/"><img src="images/close-black.svg" alt=""></a></span>
						<span class="emailaddress">gonzalo.c@footagefarm.com <a href="#/"><img src="images/close-black.svg" alt=""></a></span>
						<span class="emailaddress">gintare.k@footagefarm.com <a href="#/"><img src="images/close-black.svg" alt=""></a></span>
						<span class="emailaddress">mat.f@footagefarm.com <a href="#/"><img src="images/close-black.svg" alt=""></a></span>						 		
					</div>	
			</div>
			<div class="modal-content-wrapper">			
				<div class="modal-actions">
					<input type="submit" data-fancybox-close class="btn-ff btn-secondary-dark text-uppercase"  value="Cancel">
					<input type="submit" class="btn-ff btn-primary-dark text-uppercase" value="Send">
				</div>
			</div>
		</form>		
	</div>
</div>
<!-- Share Blog Post -->
<?php require_once('include/footer.php') ?>
<?php require_once('include/footer-scripts.php') ?>
<script>
$(document).ready(function(){
	/*Subtheme video select*/
	if($(window).width()>991){			
		$('.blog-single a').hover(function () {

			$(this).children('.hover-options-container').children('.box-single-checkbox').show();
			$(this).children('.hover-options-container').children('.box-single-share').show();

		}, function () {

			$(this).children('.hover-options-container').children('.box-single-checkbox').hide();
			$(this).children('.hover-options-container').children('.box-single-share').hide();

			if ($(this).children('.hover-options-container').children('.box-single-checkbox').children('label.checkbox-container').children('input[type="checkbox"]').is(':checked')) {

				$(this).children('.hover-options-container').children('.box-single-checkbox').addClass('selected');
				$(this).children('.hover-options-container').addClass('selected');

			} else {

				$(this).children('.hover-options-container').children('.box-single-checkbox').removeClass('selected');
				$(this).children('.hover-options-container').removeClass('selected');

			}
		});
	}
	/*Subtheme video select*/
})
</script>
</body>
</html>