$(document).ready(function () {	
	/*Toggle between hanburger and close icon*/
	$('.navbar-toggler').click(function () {
		$(this).toggleClass('open');
	})
	/*Toggle between hanburger and close icon*/
	$('.home-testimonial-carousel').owlCarousel({
		items: 1,
		margin: 0,
		stagePadding: 0,
		loop: true,
		nav: false,
		dots: true,
		/* URLhashListener: true,
		autoplayHoverPause: true,
		startPosition: 'URLHash' */
	});
	$('.testimonial-numbers a').click(function () {
		$('.testimonial-numbers a').removeClass('active');
		$(this).addClass('active')
	});
	if ($(window).width() < 992) {
		$(window).on('scroll', function () {
			if ($(this).scrollTop() > 100) {
				$('.home-search-wrapper').addClass('homesearch-fixed');
			} else {
				$('.home-search-wrapper').removeClass('homesearch-fixed');
			}
		});
	} else {
		$(window).on('scroll', function () {
			if ($(this).scrollTop() > 325) {
				$('.home-search-wrapper').addClass('homesearch-fixed');
			} else {
				$('.home-search-wrapper').removeClass('homesearch-fixed');
			}
		});
	}
	/*Sign in mobile*/
	$('.signin-link-mobile').click(function () {
		$('.signin-flyout-mobile').fadeIn();
	});
	$('.close-mobile-signin').click(function () {
		$('.signin-flyout-mobile').fadeOut();
	})
	/*Sign in mobile*/
	/*Logged in mobile*/
	$('.mobile-profile .client-avatar').click(function () {
		$('.loggedin-flyout-mobile').fadeIn();
	});
	$('.close-mobile-loggedin').click(function () {
		$('.loggedin-flyout-mobile').fadeOut();
	})
	/*Logged in mobile*/
	/*Left padding for innerpage searchbar*/
	var searchbarLeft = $('.container').offset().left;
	$('.searchbar-top form.home-search input[type=text]').css({ 'padding-left': searchbarLeft + 15 });
	/*Left padding for innerpage searchbar*/
	$('.niceselect').niceSelect();
	$(window).on("load", function () {
		$(".nice-select .list").mCustomScrollbar({
			theme: "dark"
		});
	});
	/*All themes select*/
	$('.allthemes-filter-single .niceselect').click(function () {
		if (!$(this).hasClass('open')) {
			$('.theme-select-overlay').fadeIn();
			$('.niceselect').addClass('disabled');
			$(this).removeClass('disabled')
		}
		else {
			$('.theme-select-overlay').fadeOut();
			$('.niceselect').removeClass('disabled');
		}
	})
	$('body').click(function () {
		if ($('.niceselect').hasClass('open')) {
			$('.theme-select-overlay').fadeOut();
			$('.niceselect').removeClass('disabled');
		}
	})
	/*All themes select*/
	/*Subtheme video select*/
	if($(window).width()>991){			
		$('.block-single a').hover(function () {

			$(this).children('.hover-options-container').children('.box-single-checkbox').show();
			$(this).children('.hover-options-container').children('.box-single-share').show();

		}, function () {

			$(this).children('.hover-options-container').children('.box-single-checkbox').hide();
			$(this).children('.hover-options-container').children('.box-single-share').hide();

			if ($(this).children('.hover-options-container').children('.box-single-checkbox').children('label.checkbox-container').children('input[type="checkbox"]').is(':checked')) {

				$(this).children('.hover-options-container').children('.box-single-checkbox').addClass('selected');
				$(this).children('.hover-options-container').addClass('selected');

			} else {

				$(this).children('.hover-options-container').children('.box-single-checkbox').removeClass('selected');
				$(this).children('.hover-options-container').removeClass('selected');

			}
		});
	}
	/*Subtheme video select*/
	/*Share icon hover text*/
	$('.box-single-share .share-single').hover(function () {
		$(this).next('.share-msg-text').fadeIn();
	}, function () {
		$(this).next('.share-msg-text').fadeOut();
	});
	/*Share icon hover text*/
	/*Selected reels header counter*/
	var checkboxes = $('.box-single-checkbox input[type="checkbox"]');
	$('.select-all').click(function () {
		checkboxes.prop('checked', true);
		$('.box-single-checkbox').css({ 'display': 'block' });
		var countCheckedCheckboxes = checkboxes.filter(':checked').length;
		$('#count-checked-checkboxes').text(countCheckedCheckboxes);
		$('.hover-options-container').addClass('selected');
		$('.selectedheader-flyout').removeClass('show');
	})
	$('.deselect-all').click(function () {
		checkboxes.prop('checked', false);
		if($(window).width()>991){
			$('.box-single-checkbox').css({ 'display': 'none' });
		}
		var countCheckedCheckboxes = checkboxes.filter(':checked').length;
		$('#count-checked-checkboxes').text(countCheckedCheckboxes);
		$('.hover-options-container').removeClass('selected');
		//$('.selected-items-wrapper').fadeOut()
		$('.selectedheader-flyout').removeClass('show');
	})
	checkboxes.change(function () {
		var countCheckedCheckboxes = checkboxes.filter(':checked').length;
		if (countCheckedCheckboxes > 0) {
			$('.selected-items-wrapper').fadeIn()
		}
		else {
			$('.selected-items-wrapper').fadeOut()
		}
		$('#count-checked-checkboxes').text(countCheckedCheckboxes);
	});
	$('.close-selected-header').click(function () {
		$('.selected-items-wrapper').fadeOut()
	})
	$('.selectedheader-more').click(function(){		
		$('.selectedheader-flyout').toggleClass('show');
	})
	/*Selected reels header*/
	$("#popuptest").fancybox().trigger('click');
	/* $('.modal-close').click(function(){
		$.fancybox.close();
	}) */	
	$(window).on("load", function () {
		$('.modal-project-details').mCustomScrollbar({
			theme: "dark"
		});
	});

	$('.modal-create-btn').click(function () {
		$('.modal-addnew-project, .modal-actions').show();
		$('.modal-no-projects-msg, .modal-existing-projects, .modal-existing-actions').hide();
	});

	$('.modal-cancel-newproject').click(function () {
		$('.modal-addnew-project').hide();
		$('.modal-no-projects-msg, .modal-existing-projects, .modal-existing-actions').show();
	});

	$('#mep-search').on('keyup', function () {
		var value = $(this).val().toLowerCase();
		$('.mep-single').filter(function () {
			$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
		})
	});

	$('.mep-single').hover(function () {
		$(this).children('.hover-options-container').children('.mep-box-single-checkbox').show();
	}, function () {
		$(this).children('.hover-options-container').children('.mep-box-single-checkbox').hide();
		/*Keep checkbox visible if checked*/
		var mepcheckbox = $(this).children('.hover-options-container').children('.mep-box-single-checkbox').children('label.checkbox-container').children('input[type="checkbox"]');
		if (mepcheckbox.is(':checked')) {
			$(this).children('.hover-options-container').children('.mep-box-single-checkbox').addClass('selected');
			//$(this).children('.hover-options-container').siblings('.mep-single-overlay').addClass('selected')
		} else {
			$(this).children('.hover-options-container').children('.mep-box-single-checkbox').removeClass('selected');
			//$(this).children('.hover-options-container').siblings('.mep-single-overlay').removeClass('selected')
		}
		/*Keep checkbox visible if checked*/
	});
	/*Change overlaycolor if project selected*/
	var mepoverlay = $('.mep-single').children('.hover-options-container').children('.mep-box-single-checkbox').children('label.checkbox-container').children('input[type="checkbox"]');
	$(mepoverlay).click(function(){
		$(this).parent('.checkbox-container').parent('.mep-box-single-checkbox').parent('.hover-options-container').siblings('.mep-single-overlay').toggleClass('selected');
	});
	/*Change overlaycolor if project selected*/
	$('.emailaddress a').click(function(){
		$(this).parent('.emailaddress').remove();
	});
	/*Show Password*/
	$('.showpassword').click(function(){
		if($(this).siblings('#password').attr('type') == 'password'){
			$(this).siblings('#password').attr('type','text');
			$(this).children('img').attr("src","images/hide.svg");
		}else{
			$(this).siblings('#password').attr('type','password');
			$(this).children('img').attr("src","images/show.svg");
		}
	})
	$('.confirmpassword').click(function(){
		if($(this).siblings('#confirmpassword').attr('type') == 'password'){
			$(this).siblings('#confirmpassword').attr('type','text');			
			$(this).children('img').attr("src","images/hide.svg");
		}else{
			$(this).siblings('#confirmpassword').attr('type','password');
			$(this).children('img').attr("src","images/show.svg");
		}
	})
	/*Show Password*/
	/*Login-Register Flyout toggle*/
	$('.ff-login-link').click(function(){
		$('.ff-login').show();
		$('.ff-register,.ff-forgotpass').hide();
		$('#signin-content').find('.modal-header h3').text('Sign in');
	})
	$('.ff-register-link').click(function(){
		$('.ff-register').show();
		$('.ff-login,.ff-forgotpass').hide();
		$('#signin-content').find('.modal-header h3').text('Create an Account');
	})
	$('.ff-forgotpass-link').click(function(){
		$('.ff-forgotpass').show();
		$('.ff-login, .ff-register').hide();
		$('#signin-content').find('.modal-header h3').text('Forgot Password');
	})
	/*Login-Register Flyout toggle*/	
	/*Tooltip*/
	$('.tooltip-info').hover(function(){
		$(this).children('.tooltip-msg').fadeIn();
	},function(){
		$(this).children('.tooltip-msg').fadeOut();
	});
	$('[data-toggle="tooltip"]').tooltip()
	/*Tooltip*/
	$('.review-cart-edit').click(function(){
		$('.review-enquiry-form').hide();
		$('.edit-enquiry-form').show();
	})
	$('.save-review-cart').click(function(){
		$('.edit-enquiry-form').hide();
		$('.review-enquiry-form').show();	
	})
});




