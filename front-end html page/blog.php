<?php require_once('include/head.php') ?>
<?php require_once('include/navigation.php') ?>
<!-- Innerpage Search -->
<section class="page-container innerpage-top">
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="searchbar-top">
		<div class="home-search-wrapper">
			<form class="home-search" action="">
				<input type="text" placeholder="Search for footages and themes" name="home-search">
				<button type="submit"><img src="images/search-icon.svg" alt=""></button>
			</form>
		</div>
	</div>
</section>
<!-- Innerpage Search -->
<section class="page-container">
	<!-- Mobile Search -->
	<div class="multicolored-line multicolored-line-mobile">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="home-search-wrapper search-wrapper-mobile">
		<form class="home-search" action="">
			<input type="text" placeholder="Search for footages and themes" name="home-search">
			<button type="submit"><img src="images/search-icon.svg" alt=""></button>
		</form>
	</div>
	<!-- Mobile Search -->
	<!-- Innerpage Banner -->
	<section class="page-section mb-0">		
		<div class="innerpage-banner innerpage-banner-transparent pt-25 pb-0">
			<div class="innerpage-banner-grey-bg full-height"></div>
			<div class="container">
				<div class="row">
					<div class="col-12 no-padding-mobile">
						<div class="blog-intro-wrapper">
							<h1 class="page-title-small">Quis autem vel eum iure reprehender exercitationem ullam corporis </h1>
							<div class="blog-tagline">
								<p>Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. </p>
							</div>
							<div class="author-share-wrapper">
								<div class="author-info">
									<img src="images/blog-avatar.svg" alt="">
									<div class="author-name">
										<p class="ba-name">Gintare Kriauceliunaite</p>
										<p class="ba-date">2 January 2020</p>
									</div>									
								</div>
								<div class="blog-share">
									<a href="#/" class="bs-twitter"><img src="images/twitter-black.svg" alt=""></a>
									<a href="#/" class="bs-facebook"><img src="images/facebook-black.svg" alt=""></a>
									<a href="#/" class="bs-linkedin"><img src="images/linkedin-black.svg" alt=""></a>
								</div>
							</div>	
						</div>	
					</div>					
				</div>
			</div>			
		</div>				
	</section>
	<!-- Innerpage Banner -->
	<!-- Blog Detail Section -->
	<section class="page-section pt-50 pb-0 ptmob-25 pbmob-25">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="blog-content-wrapper">
						<p>I am currently an intern on the one-year Icon Photographic Conservation programme, working at both The National Archives and the Tate. I’ve been based at the Collection Care department at The National Archives for six months, under the supervision of photograph conservator, Jacqueline Moon. A significant part of my time here has involved working with the film collection, which includes X-rays, microfilm and still photo film, often found within paper files and records.</p>
						<h3>Surveying and understanding our film collection</h3>
						<p>Until recently, little was known about the film collection held at Kew. However, with the help of a team of hard-working volunteers, we have embarked upon a widespread survey to give us a greater understanding of the range of film items held in the collection, documenting where they can be found and, most importantly, the condition that they are in.</p>
						<h3>Deterioration of film</h3>
						<p>Monitoring the condition of our film collection is important because certain film types can deteriorate at an alarming rate. Cellulose nitrate, the earliest type of plastic used in the manufacture of film, becomes sticky as it deteriorates; it adheres itself to surrounding documents and the image is eventually destroyed. Cellulose nitrate was replaced by cellulose acetate in the 1950s, but it too was soon found to be equally unstable, becoming acidic, distorted and increasingly brittle as it ages.</p>
						<p>Some of the most deteriorated film highlighted by the survey is held within the PIN 26 files: Disability Pensions Award files from men and women injured in service during the First World War. These are fascinating documents and contain X-rays on both cellulose nitrate and cellulose acetate film, many of which have unfortunately become severely degraded and unusable in their current state.</p>
						<img src="https://via.placeholder.com/850x500.png" alt="" class="img-fluid blog-detail-banner">
						<p class="image-caption"><img src="images/pointer-arrow-small-right.svg" alt=""> Cellulose nitrate X-rays which have become sticky and adhered to each other and to a polyester sleeve. The X-ray image has now been destroyed</p>
						<div class="blog-quote">
							<p>“ Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihilet molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores aliasndis doloribus asperiores repellat."</p>
						</div>
						<h3>Saving degraded film</h3>
						<p>The discovery of the degraded film made us question whether there was anything that could be done to save these objects. For film made from cellulose nitrate it might already be too late, as the image is destroyed as the film degrades. In the case of film objects made from cellulose acetate, however, even though they might be brittle or cracked and the image masked by the distorted cellulose acetate layer, the image information is still there and can still be saved.</p>
						<p>I’ve been testing ways of separating the image layer from cellulose acetate film to see if this could be a viable option for preserving these objects. Separation, which I have been testing on non-collection items, involves immersing the film in a series of solvent baths to release the cellulose acetate base from the thin layer of gelatin which holds the image.</p>
						<p>Results have been promising and the treatment has the potential to save countless objects which would otherwise be lost, but the treatment is not without its risks. Consequently, additional testing is needed before the procedure can be carried out on film items from The National Archives’ collection.</p>
						<img src="https://via.placeholder.com/850x500.png" alt="" class="img-fluid blog-detail-banner">
						<p class="image-caption"><img src="images/pointer-arrow-small-right.svg" alt=""> Separating the cellulose acetate film base from one of the test samples</p>
						<div class="blog-detail-tags">
							<h4>Tags</h4>
							<div class="blog-tags">
								<a href="#/">Ariel</a>
								<a href="#/">German Troops</a>
								<a href="#/">Marshal Von Hindenburg</a>
								<a href="#/">Flash Intertile</a>
								<a href="#/">American Aero Cop</a>
								<a href="#/">Military Intervention</a>
								<a href="#/">MS Prisoners</a>
								<a href="#/">Bactrian Camels </a>
								<a href="#/">POWs</a>
								<a href="#/">British Soldiers</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Blog Detail Section -->
	<!-- Similar Themes -->
	<section class="page-section pt-75 mb-0 similar-section-wrapper similar-blogs">
		<div class="grey-bg"></div>
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="multicolored-line">
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
					</div>
					<div class="section-title-wrapper">
						<h2 class="section-title">
							<img src="images/section-headline.svg" alt=""> Similar Blogs
						</h2>						
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-4">
					<div class="blog-single">
						<a href="#/">
							<div class="blog-title-date">
								<h3>Blog Title Will Come Here Like This In Three Lines Maximum</h3>
								<div class="date-info">
									<span class="date-number">16</span>
									<span class="month">June</span>
									<span class="year">2020</span>
								</div>
							</div>
							<div class="blog-author">
								<div class="arrow-right-dark-small"></div>
								<p>Jordan Megyery</p>
							</div>
						</a>
						<div class="blog-tags">
							<a href="#/">film</a>
							<a href="#/">care</a>
							<a href="#/">archive</a>
						</div>
						<a href="#/">
							<div class="hover-options-container">								
								<div class="box-single-share">									
									<span class="share-single share-social" data-fancybox data-src="#sharetheme-content" data-options='{"touch" : false}'></span>
									<span class="share-msg-text share-social-text">
										Share
									</span>									
								</div>
								<img src="https://via.placeholder.com/377x212.png" alt="" class="img-fluid">
							</div>
						</a>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-4">
					<div class="blog-single">
						<a href="#/">
							<div class="blog-title-date">
								<h3>Blog Title Will Come Here Like This In Three Lines Maximum</h3>
								<div class="date-info">
									<span class="date-number">16</span>
									<span class="month">June</span>
									<span class="year">2020</span>
								</div>
							</div>
							<div class="blog-author">
								<div class="arrow-right-dark-small"></div>
								<p>Jordan Megyery</p>
							</div>
						</a>
						<div class="blog-tags">
							<a href="#/">film</a>
							<a href="#/">care</a>
							<a href="#/">archive</a>
						</div>
						<a href="#/">
							<div class="hover-options-container">								
								<div class="box-single-share">									
									<span class="share-single share-social" data-fancybox data-src="#sharetheme-content" data-options='{"touch" : false}'></span>
									<span class="share-msg-text share-social-text">
										Share
									</span>									
								</div>
								<img src="https://via.placeholder.com/377x212.png" alt="" class="img-fluid">
							</div>
						</a>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-4">
					<div class="blog-single">
						<a href="#/">
							<div class="blog-title-date">
								<h3>Blog Title Will Come Here Like This In Three Lines Maximum</h3>
								<div class="date-info">
									<span class="date-number">16</span>
									<span class="month">June</span>
									<span class="year">2020</span>
								</div>
							</div>
							<div class="blog-author">
								<div class="arrow-right-dark-small"></div>
								<p>Jordan Megyery</p>
							</div>
						</a>
						<div class="blog-tags">
							<a href="#/">film</a>
							<a href="#/">care</a>
							<a href="#/">archive</a>
						</div>
						<a href="#/">
							<div class="hover-options-container">								
								<div class="box-single-share">									
									<span class="share-single share-social" data-fancybox data-src="#sharetheme-content" data-options='{"touch" : false}'></span>
									<span class="share-msg-text share-social-text">
										Share
									</span>									
								</div>
								<img src="https://via.placeholder.com/377x212.png" alt="" class="img-fluid">
							</div>
						</a>
					</div>
				</div>				
			</div>
		</div>
	</section>						
	<!-- Similar Themes -->
</section>
<!-- Share Blog Post -->
<div style="display: none;max-width:580px;" id="sharetheme-content" class="ff-modal">
	<div class="modal-header">		
		<h3>Share Blog</h3>		
	</div>
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="ff-modal-content">
		<form class="share-reel-form">
			<div class="modal-section">			
					<div class="form-group">					
						<input type="text" class="form-control ff-field-dark linkfield" id="reelink" name="reelink" placeholder="Blog link will be shown here">
						<a href="#/" class="copylink">Copy Link</a>						
					</div>
					<div class="form-group">
						<label for="social">Social</label>
						<div class="modal-social-share">
							<a href="#/"><img src="images/twitter-black.svg" alt="Share on Twitter"></a>
							<a href="#/"><img src="images/facebook-black.svg" alt="Share on Facebook"></a>
							<a href="#/"><img src="images/linkedin-black.svg" alt="Share on Linkedin"></a>				
						</div>
					</div>
					<div class="form-group">
						<label for="sendtoemail">Send to Email Address</label>
						<input type="text" class="form-control ff-field-dark" id="sendtoemail" name="sendtoemail" required>		
						<span class="emailaddress">niraali@bokapp.design <a href="#/"><img src="images/close-black.svg" alt=""></a></span>
						<span class="emailaddress">gonzalo.c@footagefarm.com <a href="#/"><img src="images/close-black.svg" alt=""></a></span>
						<span class="emailaddress">gintare.k@footagefarm.com <a href="#/"><img src="images/close-black.svg" alt=""></a></span>
						<span class="emailaddress">mat.f@footagefarm.com <a href="#/"><img src="images/close-black.svg" alt=""></a></span>						 		
					</div>	
			</div>
			<div class="modal-content-wrapper">			
				<div class="modal-actions">
					<input type="submit" data-fancybox-close class="btn-ff btn-secondary-dark text-uppercase"  value="Cancel">
					<input type="submit" class="btn-ff btn-primary-dark text-uppercase" value="Send">
				</div>
			</div>
		</form>		
	</div>
</div>
<!-- Share Blog Post -->
<?php require_once('include/footer.php') ?>
<?php require_once('include/footer-scripts.php') ?>
<script>
$(document).ready(function(){
	/*Subtheme video select*/
	if($(window).width()>991){			
		$('.blog-single a').hover(function () {

			$(this).children('.hover-options-container').children('.box-single-checkbox').show();
			$(this).children('.hover-options-container').children('.box-single-share').show();

		}, function () {

			$(this).children('.hover-options-container').children('.box-single-checkbox').hide();
			$(this).children('.hover-options-container').children('.box-single-share').hide();

			if ($(this).children('.hover-options-container').children('.box-single-checkbox').children('label.checkbox-container').children('input[type="checkbox"]').is(':checked')) {

				$(this).children('.hover-options-container').children('.box-single-checkbox').addClass('selected');
				$(this).children('.hover-options-container').addClass('selected');

			} else {

				$(this).children('.hover-options-container').children('.box-single-checkbox').removeClass('selected');
				$(this).children('.hover-options-container').removeClass('selected');

			}
		});
	}
	/*Subtheme video select*/
})
</script>
</body>
</html>