<?php require_once('include/head.php') ?>
<?php require_once('include/navigation.php') ?>
<!-- Innerpage Search -->
<section class="page-container innerpage-top">
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="searchbar-top">
		<div class="home-search-wrapper">
			<form class="home-search" action="">
				<input type="text" placeholder="Search for footages and themes" name="home-search">
				<button type="submit"><img src="images/search-icon.svg" alt=""></button>
			</form>
		</div>
	</div>
</section>
<!-- Innerpage Search -->
<section class="page-container">
	<!-- Mobile Search -->
	<div class="multicolored-line multicolored-line-mobile">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="home-search-wrapper search-wrapper-mobile">
		<form class="home-search" action="">
			<input type="text" placeholder="Search for footages and themes" name="home-search">
			<button type="submit"><img src="images/search-icon.svg" alt=""></button>
		</form>
	</div>
	<!-- Mobile Search -->
	<section class="page-section pt-75 pb-75 ptmob-60 pbmob-60 mb-0 contact-us-section">
		<div class="container">
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 offset-lg-1 col-lg-10">
					<div class="contact-content-wrapper">
						<div class="row">
							<div class="col-12 col-sm-5">
								<h1 class="page-title-small">Contact Us</h1>
								<p class="contact-intro">The aim was to help producers to not have to struggle with high licence fees and to enable them to access and afford archival material of high quality.</p>
								<p class="contact-enquiry-title">For general enquiries and research don’t hesitate to give us a: </p>
								<div class="contact-detail">
									<img src="images/phone-black.svg" alt="">
									<p><a href="tel:+44(0)2076313773">+44(0)2076313773</a></p>
								</div>
								<div class="contact-detail">
									<img src="images/email-black.svg" alt="">
									<p>info@Footage farm.co.uk</p>
								</div>
							</div>
							<div class="col-12 col-sm-7">
								<img src="https://via.placeholder.com/600x340.png" alt="" class="img-responsive contactus-img">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</section>
<?php require_once('include/footer.php') ?>
<?php require_once('include/footer-scripts.php') ?>
</body>

</html>