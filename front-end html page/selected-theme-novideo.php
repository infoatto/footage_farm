<?php require_once('include/head.php') ?>
<?php require_once('include/navigation.php') ?>
<!-- Innerpage Search -->
<section class="page-container innerpage-top">	
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="searchbar-top">
		<div class="home-search-wrapper">
			<form class="home-search" action="">
				<input type="text" placeholder="Search for footages and themes" name="home-search">
				<button type="submit"><img src="images/search-icon.svg" alt=""></button>
			</form>
		</div>
	</div>				
</section>
<!-- Innerpage Search -->
<section class="page-container">
	<!-- Mobile Search -->
	<div class="multicolored-line multicolored-line-mobile">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="home-search-wrapper search-wrapper-mobile">
		<form class="home-search" action="">
			<input type="text" placeholder="Search for footages and themes" name="home-search">
			<button type="submit"><img src="images/search-icon.svg" alt=""></button>
		</form>
	</div>
	<!-- Mobile Search -->
	<!-- Innerpage Banner -->
	<section class="page-section mb-0">		
		<div class="innerpage-banner innerpage-banner-transparent pb-90 pt-65">
			<div class="innerpage-banner-grey-bg full-height"></div>
			<div class="container">
				<div class="row">
					<div class="col-12 col-sm-6">
						<h1 class="page-title-small">World War II</h1>
						<p class="subtheme-count">9 Sub-Themes</p>
						<a href="#/" class="btn-ff btn-ff-icon btn-transparent btn-secondary-dark text-uppercase addtofavourite">Add to favourite</a>
						<a href="#/" class="btn-ff btn-ff-icon btn-transparent btn-secondary-dark text-uppercase share">Share</a>
					</div>					
					<div class="col-12 col-sm-6">
					
					</div>					
				</div>
			</div>			
		</div>				
	</section>
	<!-- Innerpage Banner -->
	<!-- Sub-Themes -->
	<section class="page-section pt-40">
		<div class="container">			
			<div class="row">				
				<div class="col-12 col-md-6 col-lg-4">
					<div class="block-single">
						<a href="#/">
							<h3>Subtheme Name Will Come Here Like Theme Name Will Come Here Like</h3>
							<div class="block-info">								
								<div>
									<p class="block-info-title">Total Reels</p>
									<p class="block-info-content">6467</p>
								</div>
							</div>
							<img src="https://via.placeholder.com/377x212.png" alt="" class="img-fluid">
						</a>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-4">
					<div class="block-single">
						<a href="#/">
							<h3>Subtheme Name Will Come Here Like Theme Name Will Come Here Like</h3>
							<div class="block-info">								
								<div>
									<p class="block-info-title">Total Reels</p>
									<p class="block-info-content">6467</p>
								</div>
							</div>
							<img src="https://via.placeholder.com/377x212.png" alt="" class="img-fluid">
						</a>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-4">
					<div class="block-single">
						<a href="#/">
							<h3>Subtheme Name Will Come Here Like Theme Name Will Come Here Like</h3>
							<div class="block-info">								
								<div>
									<p class="block-info-title">Total Reels</p>
									<p class="block-info-content">6467</p>
								</div>
							</div>
							<img src="https://via.placeholder.com/377x212.png" alt="" class="img-fluid">
						</a>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-4">
					<div class="block-single ">
						<a href="#/">
							<h3>Subtheme Name Will Come Here Like Theme Name Will Come Here Like</h3>
							<div class="block-info">								
								<div>
									<p class="block-info-title">Total Reels</p>
									<p class="block-info-content">6467</p>
								</div>
							</div>
							<img src="https://via.placeholder.com/377x212.png" alt="" class="img-fluid">
						</a>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-4">
					<div class="block-single ">
						<a href="#/">
							<h3>Subtheme Name Will Come Here Like Theme Name Will Come Here Like</h3>
							<div class="block-info">								
								<div>
									<p class="block-info-title">Total Reels</p>
									<p class="block-info-content">6467</p>
								</div>
							</div>
							<img src="https://via.placeholder.com/377x212.png" alt="" class="img-fluid">
						</a>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-4">
					<div class="block-single ">
						<a href="#/">
							<h3>Subtheme Name Will Come Here Like Theme Name Will Come Here Like</h3>
							<div class="block-info">								
								<div>
									<p class="block-info-title">Total Reels</p>
									<p class="block-info-content">6467</p>
								</div>
							</div>
							<img src="https://via.placeholder.com/377x212.png" alt="" class="img-fluid">
						</a>
					</div>
				</div>
			</div>			
		</div>
	</section>
	<!-- Sub-Themes -->
	<!-- Similar Themes -->
	<section class="page-section pt-75 mb-0 similar-section-wrapper">
		<div class="grey-bg"></div>
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="multicolored-line">
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
					</div>
					<div class="section-title-wrapper">
						<h2 class="section-title">
							<img src="images/section-headline.svg" alt=""> Similar Themes
						</h2>						
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-4">
					<div class="block-single">
						<a href="#/">
							<h3>Theme Name Will Come Here Like Theme Name Will Come Here Like</h3>
							<div class="block-info">
								<div>
									<p class="block-info-title">Sub-themes</p>
									<p class="block-info-content">287</p>
								</div>
								<div>
									<p class="block-info-title">Total Reels</p>
									<p class="block-info-content">6467</p>
								</div>
							</div>
							<img src="https://via.placeholder.com/377x212.png" alt="" class="img-fluid">
						</a>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-4">
					<div class="block-single">
						<a href="#/">
							<h3>Theme Name Will Come Here Like Theme Name Will Come Here Like</h3>
							<div class="block-info">
								<div>
									<p class="block-info-title">Sub-themes</p>
									<p class="block-info-content">287</p>
								</div>
								<div>
									<p class="block-info-title">Total Reels</p>
									<p class="block-info-content">6467</p>
								</div>
							</div>
							<img src="https://via.placeholder.com/377x212.png" alt="" class="img-fluid">
						</a>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-4">
					<div class="block-single">
						<a href="#/">
							<h3>Theme Name Will Come Here Like Theme Name Will Come Here Like</h3>
							<div class="block-info">
								<div>
									<p class="block-info-title">Sub-themes</p>
									<p class="block-info-content">287</p>
								</div>
								<div>
									<p class="block-info-title">Total Reels</p>
									<p class="block-info-content">6467</p>
								</div>
							</div>
							<img src="https://via.placeholder.com/377x212.png" alt="" class="img-fluid">
						</a>
					</div>
				</div>				
			</div>
		</div>
	</section>						
	<!-- Similar Themes -->
</section>
<?php require_once('include/footer.php') ?>
<?php require_once('include/footer-scripts.php') ?>
</body>
</html>