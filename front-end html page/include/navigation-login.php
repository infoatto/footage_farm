<nav class="navbar navbar-expand-lg ff-navbar">
		<div class="container">
			<a class="navbar-brand" href="index-video-login.php">
				<img src="images/ff-color-logo.svg" alt="Footage Farm" id="top-logo">
			</a>
			<!-- Mobile profile container-->
			<div class="mobile-profile">
				<!-- User initials if profile picture not set -->
				<!-- <a href="#"> 
					<span class="client-initials-logo">BO</span> 
					<img src="images/down-arrow.svg" alt="">
				</a> -->
				<!-- Profile picture set -->
				<a href="#"> <img src="images/clients-initial.svg" alt="" class="client-avatar"><img src="images/down-arrow.svg" alt=""></a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
					<span class="navbar-toggler-icon"></span>
					<span class="navbar-toggler-icon"></span>
				</button>
			</div>
			<!-- Mobile profile container-->
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active">
						<a class="nav-link" href="all-themes.php">Themes</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="aboutus.php">About</a>
					</li>
					<li>
						<a href="how-we-operate.php" class="nav-link">How we operate</a>
					</li>
					<li>
						<a href="faqs.php" class="nav-link">FAQs</a>
					</li>
					<li>
						<a href="blog-listing.php" class="nav-link">Blog</a>
					</li>
					<li>
						<a href="contact.php" class="nav-link">Contact</a>
					</li>
				</ul>
				<ul class="navbar-nav ml-auto">
					<li class="nav-item dropdown signin-link-desktop">
						<!-- <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownLogin" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <span class="client-initials-logo">BO</span> BOB ODENKIRK <img src="images/down-arrow.svg" alt=""></a> -->
						<!-- User initials if profile picture not set -->
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownLogin" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <span class="client-initials-logo">BO</span> <img src="images/down-arrow.svg" alt=""></a>
						<!-- Profile picture set -->
						<!-- <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownLogin" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <img src="images/clients-initial.svg" alt="" class="client-avatar"><img src="images/down-arrow.svg" alt=""></a> -->
						<div class="dropdown-menu loggedin-flyout" aria-labelledby="navbarDropdown">						
							<a class="dropdown-item" href="myprofile.php">My Profile</a>
							<a class="dropdown-item" href="editprofile.php">Account Settings</a>							
							<a class="dropdown-item" href="#">Logout</a>
						</div>
					</li>
					<!-- <li class="nav-item signin-link-mobile">
						<a href="#/" class="nav-link">Sign In</a>
					</li> -->
					<li class="nav-item">
						<a class="enquiry btn-ff btn-ff-icon btn-tertiary-dark" href="enquiry-cart.php">Enquiry</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<!-- Signin flyout mobile -->
	<div class="signin-flyout-mobile">
		<a href="#/" class="close-mobile-signin"><img src="images/close.svg" alt=""></a>
		<div class="signin-flyout-wrapper">
			<div class="create-account">
				<h3>New Here</h3>
				<p class="signin-text">New to Footage Farm? Create an account to get started today.</p>
				<a href="#/" class="btn-ff btn-secondary-dark">Create an account</a>
			</div>
			<div class="sign-in-user">
				<h3>Registered Users</h3>
				<p class="signin-text">Have an account? Sign in now. </p>
				<a href="#/" class="btn-ff btn-primary-dark">Sign in</a>
			</div>
		</div>
	</div>
	<!-- Signin flyout mobile -->
	<!-- Logged in flyout mobile -->
	<div class="loggedin-flyout-mobile">
		<a href="#/" class="close-mobile-loggedin"><img src="images/close.svg" alt=""></a>
		<div class="signin-flyout-wrapper">
			<div class="loggedin-flyout">						
				<a class="dropdown-item" href="myprofile.php">My Profile</a>
				<a class="dropdown-item" href="editprofile.php">Account Settings</a>							
				<a class="dropdown-item" href="#">Logout</a>
			</div>
		</div>
	</div>
	<!-- Logged in flyout mobile -->