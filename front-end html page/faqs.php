<?php require_once('include/head.php') ?>
<?php require_once('include/navigation.php') ?>
<!-- Innerpage Search -->
<section class="page-container innerpage-top">
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="searchbar-top">
		<div class="home-search-wrapper">
			<form class="home-search" action="">
				<input type="text" placeholder="Search for footages and themes" name="home-search">
				<button type="submit"><img src="images/search-icon.svg" alt=""></button>
			</form>
		</div>
	</div>
</section>
<!-- Innerpage Search -->
<section class="page-container">
	<!-- Mobile Search -->
	<div class="multicolored-line multicolored-line-mobile">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="home-search-wrapper search-wrapper-mobile">
		<form class="home-search" action="">
			<input type="text" placeholder="Search for footages and themes" name="home-search">
			<button type="submit"><img src="images/search-icon.svg" alt=""></button>
		</form>
	</div>
	<!-- Mobile Search -->
	<!-- Innerpage Banner -->
	<section class="page-section mb-0">
		<div class="innerpage-banner pt-50 pb-10 ptmob-25 pbmob-25 faqs-banner">
			<div class="innerpage-banner-grey-bg full-height"></div>
			<div class="container">
				<div class="row">
					<div class="col-12 col-sm-12 col-md-12 offset-lg-1 col-lg-10">
						<h1 class="page-title-large text-uppercase">Faqs</h1>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Innerpage Banner -->
	<section class="page-section pt-50">
		<div class="container">
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 offset-lg-1 col-lg-10">
					<!--Accordion wrapper-->
					<div class="accordion" id="accordion-faq" role="tablist" aria-multiselectable="true">
						<!-- Accordion card -->
						<div class="card">
							<!-- Card header -->
							<div class="card-header" >
								<a href="#/" class="collapsed">
									<h5>
										What is public domain material? <img src="images/down-arrow.svg" alt="">
									</h5>
								</a>
							</div>
							<!-- Card body -->							
							<div class="card-body">
								<p>This is footage that is not in copyright and is not subject to the usual licensing constrictions and prices. It was produced by American government agencies, or gifted to the American people by producers thereby lifting any existing copyright.</p>
							</div>							
						</div>
						<!-- Accordion card -->
						<!-- Accordion card -->
						<div class="card">
							<!-- Card header -->
							<div class="card-header">
								<a href="#/">
									<h5>
										What is public domain material? Where does it come from? <img src="images/down-arrow.svg" alt="">
									</h5>
								</a>
							</div>
							<!-- Card body -->							
							<div class="card-body collapsed">
								<p>Our footage is largely acquired from Themes in the United States (The National Archives, The Library of Congress and other government sources) but covers worldwide events.</p>
							</div>						
						</div>
						<!-- Accordion card -->
						<!-- Accordion card -->
						<div class="card">
							<!-- Card header -->
							<div class="card-header">
								<a href="#/">
									<h5>
										What type of subjects? <img src="images/down-arrow.svg" alt="">
									</h5>
								</a>
							</div>
							<!-- Card body -->							
							<div class="card-body collapsed">
								<p>The material spans nearly 100 years (late 19th century and throughout most of the 20th) and covers all walks of life. It contains documentaries, newsreel outtakes and some early fiction.</p>
							</div>							
						</div>
						<!-- Accordion card -->
						<!-- Accordion card -->
						<div class="card">
							<!-- Card header -->
							<div class="card-header">
								<a href="#/">
									<h5>
										Can I reuse the material? <img src="images/down-arrow.svg" alt="">
									</h5>
								</a>
							</div>
							<!-- Card body -->							
							<div class="card-body collapsed">
								<p>Yes. We charge a one-time buy-out fee. Once you have bought the physical tape, it is yours to use and re-use forever, wherever, in all media and in any programme. You will not need to come back to us to renew a contractor to pay any additional fees.</p>
							</div>							
						</div>
						<!-- Accordion card -->
						<!-- Accordion card -->
						<div class="card">
							<!-- Card header -->
							<div class="card-header">
								<a href="#/">
									<h5>
										How is it priced? <img src="images/down-arrow.svg" alt="">
									</h5>
								</a>
							</div>
							<!-- Card body -->							
							<div class="card-body collapsed">
								<p>For easy pricing, our pricing structure parallels the costs at the National Archives in Washington in that our material is sold on a source-reel basis (which tend to run at 6-11 minutes. We do not charge at a per second rate like other stock Reel archives. The cost per reel diminishes according to the number you order at any one time. Duplication and stock are additional and are charged at reasonable rates.</p>
							</div>							
						</div>
						<!-- Accordion card -->
						<!-- Accordion card -->
						<div class="card">
							<!-- Card header -->
							<div class="card-header">
								<a href="#/">
									<h5>
										I already use NARA database. Why do I need Footage Farm? <img src="images/down-arrow.svg" alt="">
									</h5>
								</a>
							</div>
							<!-- Card body -->							
							<div class="card-body collapsed">
								<p>Although the National Archives in Washington DC do have a website, only a fraction of their Theme actually appears on the site. We have information about the holdings in Washington and in other Themes containing public domain material. If you contact us and tell us about the type of material you are looking for, we can send you written descriptions, or previewing files if you wish.</p>
							</div>							
						</div>
						<!-- Accordion card -->
						<!-- Accordion card -->
						<div class="card">
							<!-- Card header -->
							<div class="card-header">
								<a href="#/">
									<h5>
										Do you have a catalogue? <img src="images/down-arrow.svg" alt="">
									</h5>
								</a>
							</div>
							<!-- Card body -->							
							<div class="card-body collapsed">
								<p>No. In order to keep our costs low, we ask you to tell us about the type of material you are looking for by phone or email. Within a few hours we can come back to you with the result of our research. There is no charge for this service. When you want to view the material, we can send you mp4 files to preview via FTP at a very reasonable price.</p>
							</div>							
						</div>
						<!-- Accordion card -->
						<!-- Accordion card -->
						<div class="card">
							<!-- Card header -->
							<div class="card-header">
								<a href="#/">
									<h5>
									How long does it take to obtain material from you? <img src="images/down-arrow.svg" alt="">
									</h5>
								</a>
							</div>
							<!-- Card body -->							
							<div class="card-body collapsed">
								<p>We offer a 24-48 hour turnaround service for masters we hold in our London office. If we need to duplicate the material from Washington, the delays are similar to those you would incur if you went straight to the source.</p>
							</div>							
						</div>
						<!-- Accordion card -->
						<!-- Accordion card -->
						<div class="card">
							<!-- Card header -->
							<div class="card-header">
								<a href="#/">
									<h5>
										What format do you supply master on? <img src="images/down-arrow.svg" alt="">
									</h5>
								</a>
							</div>
							<!-- Card body -->							
							<div class="card-body collapsed">
								<p>We can supply footage in any normal industry format and standard (PAL or NTSC) including: digital QT files [ProRes422HQ; H.264; DNX185 & MXF] and in tapes: DigiBeta, BetaSP, DVCam, etc. Duplication is charged at reasonable rates.</p>
							</div>							
						</div>
						<!-- Accordion card -->
					</div>
					<!-- Accordion wrapper -->
				</div>
			</div>
		</div>
	</section>
</section>
<?php require_once('include/footer.php') ?>
<?php require_once('include/footer-scripts.php') ?>
<script>
	$(document).ready(function(){
		$('.card-header').click(function(){
			$(this).next('.card-body').toggle();
			$(this).children('a').toggleClass('collapsed')
		})
	})
</script>
</body>
</html>