<?php require_once('include/head.php') ?>
<?php require_once('include/navigation.php') ?>
<section class="page-container">
	<!-- Mobile Search -->
	<div class="multicolored-line multicolored-line-mobile">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="home-search-wrapper search-wrapper-mobile">
		<form class="home-search" action="">
			<input type="text" placeholder="Search for footages and themes" name="home-search">
			<button type="submit"><img src="images/search-icon.svg" alt=""></button>
		</form>
	</div>
	<!-- Mobile Search -->
	<!-- Homepage Banner -->
	<section class="page-section">
		<div class="section-container">
			<div class="grey-bg"></div>
			<div class="container">
				<div class="row">
					<div class="col-12">
						<h1 class="page-title-large home-title">20 YEARS<br />
							OF BEST OF FOOTAGE</h1>
						<div class="home-search-wrapper">
							<form class="home-search" action="">
								<input type="text" placeholder="Search for footages and themes" name="home-search">
								<button type="submit"><img src="images/search-icon.svg" alt=""></button>
							</form>
						</div>
					</div>
					<div class="col-12 no-padding-mobile">
						<div class="home-banner-video">
							<!-- <a href="#/">
								<img src="https://via.placeholder.com/1200x675.png" alt="" class="img-fluid">
							</a> -->
							<div class="banner-top-vimeo-wrapper">
								<iframe src="https://player.vimeo.com/video/185819357?byline=0&portrait=0" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="banner-top-vimeo">
								</iframe>
							</div>
							<div class="clearfix"></div>
							<script src="https://player.vimeo.com/api/player.js"></script>
							<div class="home-banner-video-description">
								<p>Featured Reel: Dunkirk Battlefield</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Homepage Banner -->
	<!-- Featured Themes -->
	<section class="page-section">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="multicolored-line">
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
					</div>
					<div class="section-title-wrapper">
						<h2 class="section-title section-title-half">
							<img src="images/section-headline.svg" alt=""> Featured Themes
						</h2>
						<a href="#/" class="primary-link">Browse Themes</a>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-4">
					<div class="block-single">
						<a href="#/">
							<h3>Theme Name Will Come Here Like Theme Name Will Come Here Like</h3>
							<div class="block-info">
								<div>
									<p class="block-info-title">Sub-themes</p>
									<p class="block-info-content">287</p>
								</div>
								<div>
									<p class="block-info-title">Total Reels</p>
									<p class="block-info-content">6467</p>
								</div>
							</div>
							<img src="https://via.placeholder.com/377x212.png" alt="" class="img-fluid">
						</a>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-4">
					<div class="block-single">
						<a href="#/">
							<h3>Theme Name Will Come Here Like Theme Name Will Come Here Like</h3>
							<div class="block-info">
								<div>
									<p class="block-info-title">Sub-themes</p>
									<p class="block-info-content">287</p>
								</div>
								<div>
									<p class="block-info-title">Total Reels</p>
									<p class="block-info-content">6467</p>
								</div>
							</div>
							<img src="https://via.placeholder.com/377x212.png" alt="" class="img-fluid">
						</a>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-4">
					<div class="block-single">
						<a href="#/">
							<h3>Theme Name Will Come Here Like Theme Name Will Come Here Like</h3>
							<div class="block-info">
								<div>
									<p class="block-info-title">Sub-themes</p>
									<p class="block-info-content">287</p>
								</div>
								<div>
									<p class="block-info-title">Total Reels</p>
									<p class="block-info-content">6467</p>
								</div>
							</div>
							<img src="https://via.placeholder.com/377x212.png" alt="" class="img-fluid">
						</a>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-4">
					<div class="block-single">
						<a href="#/">
							<h3>Theme Name Will Come Here Like Theme Name Will Come Here Like</h3>
							<div class="block-info">
								<div>
									<p class="block-info-title">Sub-themes</p>
									<p class="block-info-content">287</p>
								</div>
								<div>
									<p class="block-info-title">Total Reels</p>
									<p class="block-info-content">6467</p>
								</div>
							</div>
							<img src="https://via.placeholder.com/377x212.png" alt="" class="img-fluid">
						</a>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-4">
					<div class="block-single">
						<a href="#/">
							<h3>Theme Name Will Come Here Like Theme Name Will Come Here Like</h3>
							<div class="block-info">
								<div>
									<p class="block-info-title">Sub-themes</p>
									<p class="block-info-content">287</p>
								</div>
								<div>
									<p class="block-info-title">Total Reels</p>
									<p class="block-info-content">6467</p>
								</div>
							</div>
							<img src="https://via.placeholder.com/377x212.png" alt="" class="img-fluid">
						</a>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-4">
					<div class="block-single">
						<a href="#/">
							<h3>Theme Name Will Come Here Like Theme Name Will Come Here Like</h3>
							<div class="block-info">
								<div>
									<p class="block-info-title">Sub-themes</p>
									<p class="block-info-content">287</p>
								</div>
								<div>
									<p class="block-info-title">Total Reels</p>
									<p class="block-info-content">6467</p>
								</div>
							</div>
							<img src="https://via.placeholder.com/377x212.png" alt="" class="img-fluid">
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Featured Themes -->
	<!-- How We Operate -->
	<section class="page-section">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="multicolored-line">
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
					</div>
					<div class="section-title-wrapper">
						<h2 class="section-title section-title-half">
							<img src="images/section-headline.svg" alt=""> How we operate
						</h2>
						<a href="#/" class="primary-link">Read More</a>
					</div>
				</div>
				<div class="col-12 col-sm-4">
					<div class="block-single how-we-work">
						<div class="how-we-work-title">
							<h3>Research</h3>
							<img src="images/oval.svg" alt="">
						</div>
						<div class="block-info">
							<p>Submit a request to us of the Reel you want and we will respond within 24 working hours. Once we run your request through our continually updated database and we will send you a Word document with descriptions of what we have on your subject.</p>
						</div>
					</div>
				</div>
				<div class="col-12 col-sm-4">
					<div class="block-single how-we-work">
						<div class="how-we-work-title">
							<h3>Previews</h3>
							<img src="images/path.svg" alt="">
						</div>
						<div class="block-info">
							<p>To order previews, copy the descriptions of the clips you want in a Word doc marked "screener," and send to us at info@Footage farm.com. Previews are available in DVD or sent via FTP digital files and cost $10/clip.</p>
						</div>
					</div>
				</div>
				<div class="col-12 col-sm-4">
					<div class="block-single how-we-work">
						<div class="how-we-work-title">
							<h3>Masters</h3>
							<img src="images/rectangle.svg" alt="">
						</div>
						<div class="block-info">
							<p>To order masters, send us the Source Tape reference number, in and out timecodes, and title of the clip you want as it appears on the log that comes with your preview order. Let us know the format you want from the following choices: Digibeta, Betacam SP, DVCAM, Standard Def, or digital file.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- How We Operate -->
	<!-- Testimonials -->
	<section class="page-section">
		<div class="container">
			<div class="row">
				<div class="col-12 no-padding-mobile">
					<div class="home-testimonials">
						<h2 class="section-title">What Our Clients have to say <img src="images/apostrophe.svg" alt="" class="apostrophe"></h2>
						<div class="home-testimonial-carousel owl-carousel">
							<div class="testimonial-single" data-hash="one">
								<p class="testimonial-text">Just wanted to send a quick “thank you” for the excellent Reel you sent us earlier this week! The Project Excelsior material in particular was beyond belief-gorgeous color and spectacular shots. It was almost too good to believe. I'm sure our client will be thrilled!</p>
								<div class="client-name">
									<div class="arrow-right"></div>
									<p>Christopher Peterson, Clear</p>
								</div>
							</div>
							<div class="testimonial-single" data-hash="two">
								<p class="testimonial-text">Just wanted to send a quick “thank you” for the excellent Reel you sent us earlier this week! The Project Excelsior material in particular was beyond belief-gorgeous color and spectacular shots. It was almost too good to believe. I'm sure our client will be thrilled!</p>
								<div class="client-name">
									<div class="arrow-right"></div>
									<p>Christopher Peterson, Clear</p>
								</div>
							</div>
							<div class="testimonial-single" data-hash="three">
								<p class="testimonial-text">Just wanted to send a quick “thank you” for the excellent Reel you sent us earlier this week! The Project Excelsior material in particular was beyond belief-gorgeous color and spectacular shots. It was almost too good to believe. I'm sure our client will be thrilled!</p>
								<div class="client-name">
									<div class="arrow-right"></div>
									<p>Christopher Peterson, Clear</p>
								</div>
							</div>
						</div>
						<!-- <div class="testimonial-numbers">
								<a href="#one" class="active">01</a>
								<a href="#two">02</a>
								<a href="#three">03</a>
							</div> -->
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Testimonials -->
	<!-- About Us -->
	<section class="page-section">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="multicolored-line">
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
					</div>
				</div>
				<div class="col-12 col-sm-6">
					<div class="about-content-wrapper">
						<div class="section-title-wrapper">
							<h2 class="section-title section-title-half">
								<img src="images/section-headline.svg" alt="">About Us
							</h2>
						</div>
						<div class="homepage-about-content">
							<p>Footage Farm was established in 2001 by a team of experienced professionals to provide Reel master tapes of material in the Public Domain. The aim was to help producers to not have to struggle with high licence fees and to enable them to access and afford archival material of high quality.</p>
							<a href="#/" class="btn-ff btn-primary-dark">Read More</a>
						</div>
					</div>
				</div>
				<div class="col-12 col-sm-6">
					<div class="homepage-about-img">
						<img src="https://via.placeholder.com/640x530.png" alt="" class="img-fluid">
					</div>
				</div>
			</div>
			<div class="row counter">
				<div class="col-12">
					<div class="homepage-counter-border">&nbsp;</div>
				</div>
				<div class="col-12 col-sm-4">
					<div class="counter-single first">
						<h2 class="counter-number">5000</h2>
						<div class="counter-info">
							<div class="arrow-right-dark"></div>
							<p>Hours of Footage</p>
						</div>

					</div>
				</div>
				<div class="col-12 col-sm-4">
					<div class="counter-single">
						<h2 class="counter-number">24 hours</h2>
						<div class="counter-info">
							<div class="arrow-right-dark"></div>
							<p>Turnaround Service</p>
						</div>
					</div>
				</div>
				<div class="col-12 col-sm-4">
					<div class="counter-single last">
						<h2 class="counter-number">25,000</h2>
						<div class="counter-info">
							<div class="arrow-right-dark"></div>
							<p>Entries in Our Masters Database</p>
						</div>
					</div>
				</div>
				<div class="col-12">
					<div class="homepage-counter-border">&nbsp;</div>
				</div>
			</div>
		</div>
	</section>
	<!-- About Us -->
	<!-- Blog -->
	<section class="page-section">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="multicolored-line">
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
					</div>
					<div class="section-title-wrapper">
						<h2 class="section-title section-title-half">
							<img src="images/section-headline.svg" alt=""> Blog
						</h2>
						<a href="#/" class="primary-link">Read More</a>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-4">
					<div class="blog-single">
						<a href="#/">
							<div class="blog-title-date">
								<h3>Blog Title Will Come Here Like This In Three Lines Maximum</h3>
								<div class="date-info">
									<span class="date-number">16</span>
									<span class="month">June</span>
									<span class="year">2020</span>
								</div>
							</div>
							<div class="blog-author">
								<div class="arrow-right-dark-small"></div>
								<p>Jordan Megyery</p>
							</div>
						</a>
						<div class="blog-tags">
							<a href="#/">film</a>
							<a href="#/">care</a>
							<a href="#/">archive</a>
						</div>
						<a href="#/">
							<img src="https://via.placeholder.com/377x212.png" alt="" class="img-fluid">
						</a>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-4">
					<div class="blog-single">
						<a href="#/">
							<div class="blog-title-date">
								<h3>Blog Title Will Come Here Like This In Three Lines Maximum</h3>
								<div class="date-info">
									<span class="date-number">16</span>
									<span class="month">June</span>
									<span class="year">2020</span>
								</div>
							</div>
							<div class="blog-author">
								<div class="arrow-right-dark-small"></div>
								<p>Jordan Megyery</p>
							</div>
						</a>
						<div class="blog-tags">
							<a href="#/">film</a>
							<a href="#/">care</a>
							<a href="#/">archive</a>
						</div>
						<a href="#/">
							<img src="https://via.placeholder.com/377x212.png" alt="" class="img-fluid">
						</a>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-4">
					<div class="blog-single">
						<a href="#/">
							<div class="blog-title-date">
								<h3>Blog Title Will Come Here Like This In Three Lines Maximum</h3>
								<div class="date-info">
									<span class="date-number">16</span>
									<span class="month">June</span>
									<span class="year">2020</span>
								</div>
							</div>
							<div class="blog-author">
								<div class="arrow-right-dark-small"></div>
								<p>Jordan Megyery</p>
							</div>
						</a>
						<div class="blog-tags">
							<a href="#/">film</a>
							<a href="#/">care</a>
							<a href="#/">archive</a>
						</div>
						<a href="#/">
							<img src="https://via.placeholder.com/377x212.png" alt="" class="img-fluid">
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Blog -->
</section>
<?php require_once('include/footer.php') ?>
<?php require_once('include/footer-scripts.php') ?>
</body>
</html>