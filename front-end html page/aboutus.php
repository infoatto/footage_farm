<?php require_once('include/head.php') ?>
<?php require_once('include/navigation.php') ?>
<!-- Innerpage Search -->
<section class="page-container innerpage-top">
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="searchbar-top">
		<div class="home-search-wrapper">
			<form class="home-search" action="">
				<input type="text" placeholder="Search for footages and themes" name="home-search">
				<button type="submit"><img src="images/search-icon.svg" alt=""></button>
			</form>
		</div>
	</div>
</section>
<!-- Innerpage Search -->
<section class="page-container">
	<!-- Mobile Search -->
	<div class="multicolored-line multicolored-line-mobile">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="home-search-wrapper search-wrapper-mobile">
		<form class="home-search" action="">
			<input type="text" placeholder="Search for footages and themes" name="home-search">
			<button type="submit"><img src="images/search-icon.svg" alt=""></button>
		</form>
	</div>
	<!-- Mobile Search -->
	<!-- About Us Banner -->
	<section class="page-section mb-0">		
		<div class="innerpage-banner innerpage-banner-transparent pt-50 pb-75 ptmob-25 pbmob-25">
			<div class="innerpage-banner-grey-bg"></div>
			<div class="container">
				<div class="row">
					<div class="col-12 col-sm-12 col-md-12 offset-lg-1 col-lg-10">
						<h1 class="page-title-large home-title">20 YEARS<br />
							OF BEST OF FOOTAGE</h1>						
					</div>
					<div class="col-12 col-sm-12 col-md-12 offset-lg-1 col-lg-10">
						<div class="home-banner-video">
							<div class="banner-top-vimeo-wrapper banner-aboutus-video">
								<iframe src="https://player.vimeo.com/video/185819357?byline=0&portrait=0" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="banner-top-vimeo">
								</iframe>
							</div>
							<div class="clearfix"></div>
							<script src="https://player.vimeo.com/api/player.js"></script>							
						</div>
					</div>
				</div>
			</div>			
		</div>				
	</section>
	<!-- About Us Banner -->	
	<section class="page-section mb-45">
		<div class="container">
			<div class="row aboutus-info-wrapper">
				<div class="col-12 order-2 order-md-1 col-sm-5 col-md-5 offset-lg-1 col-lg-5">					
					<img src="https://via.placeholder.com/466x478.png" alt="" class="aboutus-info-img img-fluid">
				</div>
				<div class="col-12 order-1 order-md-2  col-sm-7 col-md-7 col-lg-5">		
					<div class="aboutus-info-content">
						<h1 class="page-title-small">Public Domain and Buy-out Fee</h1>	
						<p>Piublic domain film is footage which has never been, or is no longer, in copyright and is not subject to the usual licensing constrictions and prices.  It is material produced by American government agencies, or gifted to the American people by producers, thereby lifting any existing copyright.</p>
						<p>We charge a one-time buy-out fee, not a per-second rate and once you have bought the footage, it is yours to use and re-use forever, in all media and in any programme. You will not need to come back to us to renew a contract or to pay any additional fees.</p>			
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="page-section">
		<div class="container">			
			<div class="row counter">
				<div class="col-12">
					<div class="homepage-counter-border aboutus-counter-border">&nbsp;</div>
				</div>
				<div class="col-12 col-sm-4">
					<div class="counter-single first">
						<h2 class="counter-number">5000</h2>
						<div class="counter-info">
							<div class="arrow-right-dark"></div>
							<p>Hours of Footage</p>
						</div>

					</div>
				</div>
				<div class="col-12 col-sm-4">
					<div class="counter-single">
						<h2 class="counter-number">24 hours</h2>
						<div class="counter-info">
							<div class="arrow-right-dark"></div>
							<p>Turnaround Service</p>
						</div>
					</div>
				</div>
				<div class="col-12 col-sm-4">
					<div class="counter-single last">
						<h2 class="counter-number">25,000</h2>
						<div class="counter-info">
							<div class="arrow-right-dark"></div>
							<p>Entries in Our Masters Database</p>
						</div>
					</div>
				</div>
				<div class="col-12">
					<div class="homepage-counter-border aboutus-counter-border">&nbsp;</div>
				</div>
			</div>
		</div>
	</section>
	<section class="page-section about-timespan-wrapper">
		<div class="about-timespan-bg"></div>
		<img src="images/about-timespan-bg.png" alt="" class="about-timespan-bg-img-mob img-fluid">
		<img src="images/about-timespan-bg.png" alt="" class="about-timespan-bg-img">
		<div class="about-content-bg"></div>
		<div class="container">
			<div class="row about-timespan-content">				
				<div class="col-12 col-sm-12 col-md-12 offset-lg-1 col-lg-9">	
					<h1 class="page-title-large">
						OUR THEME SPAN OVER 100 YEARS
					</h1>								
					<p>From the late 19th century to the early 2000s. Whatever the subject of your programme, it is always worth enquiring if we have relevant material – for this there is no charge.  In addition to what we hold on master tape we also have extensive information about the holdings in Washington and in other collections containing public domain material. Once you decide on the content of your show we can send you written descriptions and/or previewing files.</p>					
				</div>
			</div>			
		</div>				
	</section>

	<section class="page-section">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="multicolored-line">
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
					</div>
					<div class="section-title-wrapper">
						<h2 class="section-title">
							<img src="images/section-headline.svg" alt=""> Meet the Team
						</h2>						
					</div>
				</div>
				<div class="col-12 col-sm-4">
					<div class="block-single team">
						<h3>Gonzalo Chacon</h3>
						<div class="team-email">
							<img src="images/email-black.svg" alt="">
							<p>gonzalo.c@footagefarm.com</p>
						</div>
						<img src="https://via.placeholder.com/375x282.png" alt="" class="img-fluid img-team">
					</div>
				</div>
				<div class="col-12 col-sm-4">
					<div class="block-single team">
						<h3>Gintare Kriauceliunaite</h3>
						<div class="team-email">
							<img src="images/email-black.svg" alt="">
							<p>gintare.k@footagefarm.com</p>
						</div>
						<img src="https://via.placeholder.com/375x282.png" alt="" class="img-fluid img-team">
					</div>
				</div>
				<div class="col-12 col-sm-4">
					<div class="block-single team">
						<h3>Mat Flasque</h3>
						<div class="team-email">
							<img src="images/email-black.svg" alt="">
							<p>mat.f@footagefarm.com</p>
						</div>
						<img src="https://via.placeholder.com/375x282.png" alt="" class="img-fluid img-team">
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- How We Operate -->	
</section>
<?php require_once('include/footer.php') ?>
<?php require_once('include/footer-scripts.php') ?>
</body>
</html>