<?php require_once('include/head.php') ?>
<?php require_once('include/navigation.php') ?>
<!-- Selected Reels Header -->
<section class="page-container selected-items-wrapper">
	<section class="page-section">
		<div class="black-bg"></div>
		<div class="container">
			<div class="col-sm-12">
				<div class="selected-items">
					<div class="selected-items-left">
						<div class="selected-items-count">
							<span id="count-checked-checkboxes">0</span> Reels Selected
						</div>
						<a href="#/" class="select-all">Select All</a>
						<a href="#/" class="deselect-all">Deselect All</a>						
					</div>
					<div class="selected-items-right">
						<a href="#/" class="addtofavourite-reel">
							<span>Add to Favourite</span> 
							<img src="images/favourite-white.svg" alt="">
						</a>
						<a href="#/" data-fancybox data-src="#selectprojects-content" data-options='{"touch" : false}' class="addtoproject-reel">
							<span>Add to Project</span> 
							<img src="images/project-white.svg" alt="">							
						</a>
						<a href="#/" class="selectedheader-more">
							<img src="images/more-icon.svg" alt="">
							<div class="dropdown-menu loggedin-flyout selectedheader-flyout">						
								<a class="dropdown-item select-all" href="#">Select All</a>
								<a class="dropdown-item deselect-all" href="#">Deselect All</a>							
								<a class="dropdown-item" href="#">Add to Favourite</a>
								<a class="dropdown-item" href="#">Add to Project</a>
							</div>
						</a>
						<a href="#/" class="addtocart-reel">
							<span>Add to Cart</span>
							<img src="images/cart-white.svg" alt="">
						 </a>
						<a href="#/" class="close-selected-header"><img src="images/remove.svg" alt=""></a>
					</div>					
				</div>
				
			</div>
		</div>
	</section>
</section>
<!-- Selected Reels Header -->
<!-- Innerpage Search -->
<section class="page-container innerpage-top">	
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="searchbar-top">
		<div class="home-search-wrapper">
			<form class="home-search" action="">
				<input type="text" placeholder="Search for footages and themes" name="home-search">
				<button type="submit"><img src="images/search-icon.svg" alt=""></button>
			</form>
		</div>
	</div>				
</section>
<!-- Innerpage Search -->
<section class="page-container">
	<!-- Mobile Search -->
	<div class="multicolored-line multicolored-line-mobile">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="home-search-wrapper search-wrapper-mobile">
		<form class="home-search" action="">
			<input type="text" placeholder="Search for footages and themes" name="home-search">
			<button type="submit"><img src="images/search-icon.svg" alt=""></button>
		</form>
	</div>
	<!-- Mobile Search -->
	<!-- Innerpage Banner -->
	<section class="page-section mb-0">		
		<div class="innerpage-banner innerpage-banner-transparent pt-0 pb-0">
			<div class="innerpage-banner-grey-bg"></div>
			<div class="container">
				<div class="row">
					<div class="col-12 no-padding-mobile">						
						<div class="banner-top-vimeo-wrapper">
							<iframe src="https://player.vimeo.com/video/185819357?byline=0&portrait=0" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="banner-top-vimeo">
							</iframe>
						</div>
						<div class="clearfix"></div>
						<script src="https://player.vimeo.com/api/player.js"></script>							
					</div>					
				</div>
			</div>			
		</div>				
	</section>
	<!-- Innerpage Banner -->
	<!-- Selected Video Details -->
	<section class="page-section pt-25 pb-50 mb-0 pbmob-25">
		<div class="container">
			<div class="row">				
				<div class="col-12 col-md-9 selected-video-details-wrapper">
					<div class="selected-video-details">
						<h1 class="selected-video-title">1910 - 1918 - WWI Era: Aerials; German Officers & Troops; France Fighting; Trans-Caucasia Winter</h1>
					</div>
					<div class="selected-video-request">
						<div class="svr-left">
							<a href="#/"><img src="images/project-blue.svg" alt=""></a>
							<a href="#/"><img src="images/favourite-blue.svg" alt=""></a>
							<a href="#/"><img src="images/share-blue.svg" alt=""></a>
						</div>
						<div class="svr-right">
							<a href="#/" class="btn-ff btn-ff-icon btn-secondary-dark text-uppercase request-reel-btn" data-fancybox data-src="#enquiry-master-content" data-options='{"touch" : false}'>Request a reel</a>	
							<a href="#/" class="btn-ff btn-ff-icon btn-primary-dark text-uppercase enquiry-cart-btn">Add to enquiry cart</a>	
						</div>
					</div>
					<div class="selected-video-info">
						<div class="svi-col-1">
								<div class="svi-single">
									<p><strong>Reel Number</strong> : 220659-07</p>
								</div>
								<div class="svi-single">
									<p><strong>Color</strong> : Black and White</p>
								</div>
								<div class="svi-single">
									<p><strong>Sound</strong> : SIL</p>
								</div>
						</div>
						<div class="svi-col-2">
							<div class="svi-single">
								<p><strong>Year / Date</strong> : 1910-1918 (Decade and Date)</p>
							</div>
							<div class="svi-single">
								<p><strong>Country</strong> : Germany</p>
							</div>
							<div class="svi-single">
								<p><strong>Location</strong> : Verdun, Rhine River, Coucy, Dompierre, Ottoman Empire, Trans-Caucasia</p>
							</div>	
						</div>
						<div class="svi-col-3">
							<div class="svi-single">
								<p><strong>TC Begins</strong> : 05:20:33</p>
							</div>
							<div class="svi-single">
								<p><strong>TC Ends</strong> : 05:33:03</p>
							</div>
							<div class="svi-single">
								<p><strong>Duration</strong> : 12:30</p>
							</div>	
						</div>
					</div>
					<div class="selected-video-commentary">
						<p><strong>Description</strong>: Suitcases & cameras loaded into Caproni Ca. 1 heavy bomber (?) aircraft, Ca 2318 below open cockpit. Biplane takes off; aerial views Verdun - towns, cities, rivers, roads. Slug, 2 sec. </p>
						<p><strong>05:22:07</strong> Marshal Von Hindenburg w/ other officers walking down steps. Pose. Slug, 2 sec.</p>
						<p><strong>05:22:15</strong> German troops & military equipment crossing Rhine River, includes horse drawn carts w/ Christmas trees. Troops in city, sign: Kaiser’s Kaffe on corner of street, civilian pedestrians; flags. Slug, 1 sec. 05:24:04 Flash intertitle: “On The Western Front. The flame projector...used by French troops in reprisal...” Soldier on snow-covered ground operating flame-thrower. Slug, 1 sec.</p>
						<p><strong>05:24:16</strong> Flash intertitle: “Coucy, France. The American Aero Corps...” Biplanes on field. Painting of Red Indian chief in headdress on aircraft. French pilots. Pilots w/ dog, w/ tiger cubs & mascots. Slug, 2 sec. 05:25:06 Flash intertitle: “Dompierre, France. “...ammunition cars & huge cannons are being pushed...to sweep German lines.” Large railroad artillery w/ crew riding moved past. German soldiers under barbed wire, running w/ hands raised (staged?). 05:25:33 Huge cannon or mortar w/ camouflage lowers barrel. Camouflage painted barrel of railway gun lowered. Slug, 3 sec. 05:25:51 German Officers wearing Pickelhaube / spiked dress helmets - Kaiser? French? officers. Others in soft caps & groups of unidentified officers & well-dressed women talking, some w/ children. Slug, 1 sec. 05:26:33 French? Soldiers pitching coins or playing a game like quoits. A plane flies overhead, they rush to cover artillery/gun w/camouflage/branches of trees. Barrel of gun has August painted on. 05:26:58 Soldiers firing vertical anti-aircraft gun; man up telegraph pole. Soldier in soft cap on field telephone & communication notes taken by others. 05:28:07 Troops hammering in posts & attach barbed wire for defensive fences, snow on ground. 05:28:31 Soldiers at outdoor / field service w/ priest, winter mountains. Slug, 1 sec. 05:29:01 British soldiers looking at remains of huge gun w/ exploded barrel, soldier clambering on elevating mechanism. Soldier comparing width of barrel w/ his chest looks down barrel at another soldier. Slug, 3 sec. 05:29:45 Flash intertitles: “How the Russian Troops are Turning the Turkish Flank - Cherkas infantry...are sent to fight the Turks on the snowbound wastes of Trans-Caucasia.” Pan large group of troops in snowy surroundings encampment. 05:30:16 Troops move out, officers on horseback followed by troops on foot walking on muddy mountain road. Good shot line of troops against snowy mountain background. 05:30:55 MS Prisoners of war / POWs turn & walk past building wall, r. l.; marched on snowy road l. to r. 05:31:42 Wounded put into bullock carts which move out in convoy. 05:32:27 Flash intertitle: “Transportation...solved by the use of Bactrian camels which withstand the intense cold...” Camels, wagons & soldiers on snowy village street. Good shot camels on road w/ snow covered mountain in background. WW1; 1910s; Military Inventions; War;</p>
					</div>
					<a href="#/" class="show-svc">Show more</a>
					<div class="blog-tags">
						<a href="#/">Ariel</a>
						<a href="#/">German Troops</a>
						<a href="#/">Marshal Von Hindenburg</a>
						<a href="#/">Flash Intertile</a>
						<a href="#/">American Aero Cop</a>
						<a href="#/">Military Intervention</a>
						<a href="#/">MS Prisoners</a>
						<a href="#/">Bactrian Camels </a>
						<a href="#/">POWs</a>
						<a href="#/">British Soldiers</a>
					</div>
				</div>
				<div class="col-12 col-sm-3">
					<div class="next-in-queue">
							<h3>Related Reels</h3>
							<div class="next-in-queue-videos">
								<div class="niqv-single">
									<div class="niqv-video">
										<div style="padding:75% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/185819357?byline=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>
										<div class="niqv-overlay">
											<a href="#/">
												<p>1910 - 1918 - WWI Era: Aerials; German Officers & Troops; France Fighting; Trans-Caucasia Winter</p>
											</a>
										</div>	
									</div>	
								</div>
								<div class="niqv-single">
									<div class="niqv-video">
										<div style="padding:75% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/185819357?byline=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>
										<div class="niqv-overlay">
											<a href="#/">
												<p>1910 - 1918 - WWI Era: Aerials; German Officers & Troops; France Fighting; Trans-Caucasia Winter</p>
											</a>
										</div>	
									</div>	
								</div>
								<div class="niqv-single">
									<div class="niqv-video">
										<div style="padding:75% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/185819357?byline=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>
										<div class="niqv-overlay">
											<a href="#/">
												<p>1910 - 1918 - WWI Era: Aerials; German Officers & Troops; France Fighting; Trans-Caucasia Winter</p>
											</a>
										</div>	
									</div>	
								</div>								
							</div>
					</div>		
				</div>				
			</div>
		</div>
	</section>
	<!-- Selected Video Details -->	
	<!-- Similar SubThemes -->
	<section class="page-section pt-75 mb-0 similar-section-wrapper">
		<div class="grey-bg"></div>
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="multicolored-line">
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
					</div>
					<div class="section-title-wrapper">
						<h2 class="section-title">
							<img src="images/section-headline.svg" alt=""> Similar Sub-Themes
						</h2>						
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-4">
					<div class="block-single theme-single">
						<a href="#/">
							<h3>Subtheme Name Will Come Here Like Theme Name Will Come Here Like</h3>
							<div class="block-info">
								<div>
									<p class="block-info-title">Total Reels</p>
									<p class="block-info-content">6467</p>
								</div>
							</div>
						</a>
						<a href="#/">
							<div class="hover-options-container">
								<div class="box-single-checkbox">
									<label class="checkbox-container">
										<input type="checkbox">
										<span class="checkmark"></span>
									</label>
								</div>
								<div class="box-single-share">
									<span class="share-single share-favourite addtofavourite-reel"></span>
									<span class="share-msg-text share-favourite-text">
										Add to Favourite
									</span>
									<span class="share-single share-social" data-fancybox data-src="#sharetheme-content" data-options='{"touch" : false}'></span>
									<span class="share-msg-text share-social-text">
										Share
									</span>
								</div>
								<img src="https://via.placeholder.com/377x212.png" alt="" class="img-fluid">
							</div>
						</a>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-4">
					<div class="block-single theme-single">
						<a href="#/">
							<h3>Subtheme Name Will Come Here Like Theme Name Will Come Here Like</h3>
							<div class="block-info">
								<div>
									<p class="block-info-title">Total Reels</p>
									<p class="block-info-content">6467</p>
								</div>
							</div>
						</a>
						<a href="#/">
							<div class="hover-options-container">
								<div class="box-single-checkbox">
									<label class="checkbox-container">
										<input type="checkbox">
										<span class="checkmark"></span>
									</label>
								</div>
								<div class="box-single-share">
									<span class="share-single share-favourite addtofavourite-reel"></span>
									<span class="share-msg-text share-favourite-text">
										Add to Favourite
									</span>
									<span class="share-single share-social" data-fancybox data-src="#sharetheme-content" data-options='{"touch" : false}'></span>
									<span class="share-msg-text share-social-text">
										Share
									</span>
								</div>
								<img src="https://via.placeholder.com/377x212.png" alt="" class="img-fluid">
							</div>
						</a>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-4">
					<div class="block-single theme-single">
						<a href="#/">
							<h3>Subtheme Name Will Come Here Like Theme Name Will Come Here Like</h3>
							<div class="block-info">
								<div>
									<p class="block-info-title">Total Reels</p>
									<p class="block-info-content">6467</p>
								</div>
							</div>
						</a>
						<a href="#/">
							<div class="hover-options-container">
								<div class="box-single-checkbox">
									<label class="checkbox-container">
										<input type="checkbox">
										<span class="checkmark"></span>
									</label>
								</div>
								<div class="box-single-share">
									<span class="share-single share-favourite addtofavourite-reel"></span>
									<span class="share-msg-text share-favourite-text">
										Add to Favourite
									</span>
									<span class="share-single share-social" data-fancybox data-src="#sharetheme-content" data-options='{"touch" : false}'></span>
									<span class="share-msg-text share-social-text">
										Share
									</span>
								</div>
								<img src="https://via.placeholder.com/377x212.png" alt="" class="img-fluid">
							</div>
						</a>
					</div>
				</div>				
			</div>
		</div>
	</section>						
	<!-- Similar SubThemes -->
</section>
<!-- No Projects -->
<div style="display: none;" id="noprojects-content" class="ff-modal">
	<div class="modal-header">
		<h3>Add to a Project</h3>
	</div>
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="ff-modal-content">
		<div class="modal-content-wrapper">
			<div class="modal-project-thumbnail">
				<img src="https://via.placeholder.com/360x460.png" alt="" class="modal-project-thumb">
			</div>
			<div class="modal-project-details">
				<div class="modal-create-new-project modal-section">
					<a href="#/" class="modal-create-btn">
						<img src="images/plus-black.svg" alt=""> Create New Project
					</a>
				</div>
				<div class="modal-no-projects-msg modal-section">
					<p class="title">No Projects yet!</p>
					<p class="msg">Projects that you create will show up here.</p>
				</div>
				<div class="modal-addnew-project modal-section">
					<form>
						<div class="form-group">
							<label for="nameofproject">Name of Project</label>
							<input type="text" class="form-control ff-field-dark" id="nameofproject" name="nameofproject">
						</div>
						<div class="form-group">
							<label for="projectdesc">Description (optional)</label>
							<textarea class="form-control ff-field-dark" rows="5"></textarea>
						</div>
					</form>
				</div>
			</div>
			<div class="modal-actions modal-addnew-project">
				<input type="submit" class="btn-ff btn-secondary-dark text-uppercase modal-cancel-newproject" value="Cancel">
				<input type="submit" class="btn-ff btn-primary-dark text-uppercase" value="Create">
			</div>
		</div>
	</div>
</div>
<!-- No Projects -->
<!-- Select Projects -->
<div style="display: none;" id="selectprojects-content" class="ff-modal">
	<div class="modal-header">
		<h3>Add to a Project</h3>
	</div>
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="ff-modal-content">
		<div class="modal-content-wrapper">
			<div class="modal-project-thumbnail">
				<img src="https://via.placeholder.com/360x460.png" alt="" class="modal-project-thumb">
			</div>
			<div class="modal-project-details">
				<div class="modal-create-new-project modal-section">
					<a href="#/" class="modal-create-btn">
						<img src="images/plus-black.svg" alt=""> Create New Project
					</a>
				</div>
				<div class="modal-existing-projects modal-section">
					<div class="mep-search">
						<img src="images/search-black.svg" alt="Search">
						<input type="text" placeholder="Search" id="mep-search">
					</div>
					<div class="mep-single">
						<div class="hover-options-container">
							<div class="mep-box-single-checkbox">
								<label class="checkbox-container">
									<input type="checkbox">
									<span class="checkmark"></span>
								</label>
							</div>
						</div>
						<div class="mep-single-overlay"></div>
						<img src="https://via.placeholder.com/460x82.png" alt="" class="img-fluid">
						<p class="mep-single-title"> War Montage Ref </p>
						<p class="mep-single-count">15 Reels</p>
					</div>
					<div class="mep-single">
						<div class="hover-options-container">
							<div class="mep-box-single-checkbox">
								<label class="checkbox-container">
									<input type="checkbox">
									<span class="checkmark"></span>
								</label>
							</div>
						</div>
						<div class="mep-single-overlay"></div>
						<img src="https://via.placeholder.com/460x82.png" alt="" class="img-fluid">
						<p class="mep-single-title"> Pearl Harbour </p>
						<p class="mep-single-count">15 Reels</p>
					</div>
					<div class="mep-single">
						<div class="hover-options-container">
							<div class="mep-box-single-checkbox">
								<label class="checkbox-container">
									<input type="checkbox">
									<span class="checkmark"></span>
								</label>
							</div>
						</div>
						<div class="mep-single-overlay"></div>
						<img src="https://via.placeholder.com/460x82.png" alt="" class="img-fluid">
						<p class="mep-single-title"> Dunkirk </p>
						<p class="mep-single-count">15 Reels</p>
					</div>
					<div class="mep-single">
						<div class="hover-options-container">
							<div class="mep-box-single-checkbox">
								<label class="checkbox-container">
									<input type="checkbox">
									<span class="checkmark"></span>
								</label>
							</div>
						</div>
						<div class="mep-single-overlay"></div>
						<img src="https://via.placeholder.com/460x82.png" alt="" class="img-fluid">
						<p class="mep-single-title"> War Montage Ref </p>
						<p class="mep-single-count">15 Reels</p>
					</div>
					<div class="mep-single">
						<div class="hover-options-container">
							<div class="mep-box-single-checkbox">
								<label class="checkbox-container">
									<input type="checkbox">
									<span class="checkmark"></span>
								</label>
							</div>
						</div>
						<div class="mep-single-overlay"></div>
						<img src="https://via.placeholder.com/460x82.png" alt="" class="img-fluid">
						<p class="mep-single-title"> Pearl Harbour </p>
						<p class="mep-single-count">15 Reels</p>
					</div>
					<div class="mep-single">
						<div class="hover-options-container">
							<div class="mep-box-single-checkbox">
								<label class="checkbox-container">
									<input type="checkbox">
									<span class="checkmark"></span>
								</label>
							</div>
						</div>
						<div class="mep-single-overlay"></div>
						<img src="https://via.placeholder.com/460x82.png" alt="" class="img-fluid">
						<p class="mep-single-title"> Dunkirk </p>
						<p class="mep-single-count">15 Reels</p>
					</div>
				</div>
				<!-- New Project -->
				<div class="modal-addnew-project modal-section">
					<form>
						<div class="form-group">
							<label for="nameofproject">Name of Project</label>
							<input type="text" class="form-control ff-field-dark" id="nameofproject" name="nameofproject">
						</div>
						<div class="form-group">
							<label for="projectdesc">Description (optional)</label>
							<textarea class="form-control ff-field-dark" rows="5"></textarea>
						</div>
					</form>
				</div>
			</div>
			<div class="modal-actions modal-addnew-project">
				<input type="submit" class="btn-ff btn-secondary-dark text-uppercase modal-cancel-newproject" value="Cancel">
				<input type="submit" class="btn-ff btn-primary-dark text-uppercase" value="Create">
			</div>
			<!-- New Project -->
			<div class="modal-actions modal-existing-actions">
				<input type="submit" data-fancybox-close class="btn-ff btn-secondary-dark text-uppercase modal-cancel-newproject" value="Cancel">
				<input type="submit" class="btn-ff btn-primary-dark text-uppercase" value="Save">
			</div>
		</div>
	</div>
</div>
<!-- Select Projects -->
<!-- Share Theme/Reel -->
<div style="display: none;max-width:580px;" id="sharetheme-content" class="ff-modal">
	<div class="modal-header">
		<h3>Share a Reel</h3>
	</div>
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="ff-modal-content">
		<form class="share-reel-form">
			<div class="modal-section">
				<div class="form-group">
					<input type="text" class="form-control ff-field-dark linkfield" id="reelink" name="reelink" placeholder="Theme/Reel link will be shown here">
					<a href="#/" class="copylink">Copy Link</a>
				</div>
				<div class="form-group">
					<label for="social">Social</label>
					<div class="modal-social-share">
						<a href="#/"><img src="images/twitter-black.svg" alt="Share on Twitter"></a>
						<a href="#/"><img src="images/facebook-black.svg" alt="Share on Facebook"></a>
						<a href="#/"><img src="images/linkedin-black.svg" alt="Share on Linkedin"></a>
					</div>
				</div>
				<div class="form-group">
					<label for="sendtoemail">Send to Email Address</label>
					<input type="text" class="form-control ff-field-dark" id="sendtoemail" name="sendtoemail" required>
					<span class="emailaddress">niraali@bokapp.design <a href="#/"><img src="images/close-black.svg" alt=""></a></span>
					<span class="emailaddress">gonzalo.c@footagefarm.com <a href="#/"><img src="images/close-black.svg" alt=""></a></span>
					<span class="emailaddress">gintare.k@footagefarm.com <a href="#/"><img src="images/close-black.svg" alt=""></a></span>
					<span class="emailaddress">mat.f@footagefarm.com <a href="#/"><img src="images/close-black.svg" alt=""></a></span>
				</div>
			</div>
			<div class="modal-content-wrapper">
				<div class="modal-actions">
					<input type="submit" data-fancybox-close class="btn-ff btn-secondary-dark text-uppercase" value="Cancel">
					<input type="submit" class="btn-ff btn-primary-dark text-uppercase" value="Send">
				</div>
			</div>
		</form>
	</div>
</div>
<!-- Share Theme/Reel -->
<!-- Request Enquiry Master -->
<div style="display: none;max-width:700px;" id="enquiry-master-content" class="ff-modal">
	<div class="modal-header">		
		<h3>Request Enquiry</h3>		
	</div>
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="ff-modal-content">
		<div class="modal-section">
			<form class="enquiry-master-form">
				<div class="form-group">					
					<div class="reel-title">
						<img src="https://via.placeholder.com/147x83.png" alt="">
						<h3>1910 - 1918 - WWI Era: Aerials; German Officers & Troops; France Fighting; Trans-Caucasia Winter</h3>
					</div>					
				</div>
				<div class="form-group">
					<div class="request-reel-type">
						<label for="selectreeltype">Please select the reel type</label>
						<label class="radio-container-theme"><span>Request Preview</span>
							<input type="radio" name="reeltype" required>
							<span class="radio"></span>
						</label>
						<label class="radio-container-theme"><span>Request Master</span>
							<input type="radio" name="reeltype">
							<span class="radio"></span>
						</label>
					</div>
					<label for="reeltype" class="error"></label>	
				</div>
				<div class="form-group">
					<label for="inouttime">Specify the footage's in and out time</label>
					<div class="begin-end-tc">
						<div class="tc-time begin-tc">
							<label for="begintc">Begin TC 
								<a href="#" class="tooltip-info">
									<img src="images/info.svg" alt="">
									<span class="tooltip-msg">
										Footage start time
									</span>
								</a>
							</label>
							<input type="text" class="form-control ff-field-dark" id="begintc" name="begintc" placeholder="00:00:00" required>	
						</div>
						<div class="tc-time end-tc">
							<label for="endtc">End TC 
								<a href="#" class="tooltip-info">
									<img src="images/info.svg" alt="">
									<span class="tooltip-msg">
										Footage end time
									</span>
								</a>
							</label>
							<input type="text" class="form-control ff-field-dark" id="endtc" name="endtc" placeholder="00:00:00" required>
						</div>						
					</div>	
					<div id="addmore-begin-end-tc"></div>
					<div class="tc-addmore">
						<a href="#/" class="tc-addmore-link"><img src="images/add-black.svg" alt=""> <span>Add another in and out time codes</span></a>
					</div>					 		
				</div>
				<div class="form-group">	
					<label>Do you have a file with multiple in and out time codes?</label>
					<p>Add jpegs, excel or word files here. Each file size should be under 2MB.</p>
					<div class="uploaded-files-list">
						<div class="file-single">
							<img src="images/jpg.svg" alt="" class="file-type-icon"> 
							<span>Screenshota_qe52gknknf.jpg</span>
							<a href="#/"><img src="images/close.svg" alt="" class="file-remove-icon"></a>
						</div>
						<div class="file-single">
							<img src="images/doc.svg" alt="" class="file-type-icon"> 
							<span>datasheet-with-timecodes.doc</span>
							<a href="#/"><img src="images/close.svg" alt="" class="file-remove-icon"></a>
						</div>
						<div class="file-single">
							<img src="images/xls.svg" alt="" class="file-type-icon"> 
							<span>animate-sys.xls</span>
							<a href="#/"><img src="images/close.svg" alt="" class="file-remove-icon"></a>
						</div>
					</div>
					<!-- Created using dropzone.js-->
					<div id="upload-reel-times">						
						<img src="images/upload-file.svg" alt="">
						<p>Drag and drop here or browse</p>
						<a href="#/" class="login-link">Browse</a>
					</div>
					<!-- Created using dropzone.js-->
				</div>
				<div class="form-group">					
					<div class="format-framerate">
						<div class="format formatframe">
							<label for="begintc">Format</label>
							<select class="niceselect ff-select-dark formatselect" required>
								<option value="">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
							</select>
							<label id="formatselect-error" class="error" for=""></label>
						</div>
						<div class="framerate formatframe">
							<label for="endtc">Frame Rate</label>
							<select class="niceselect ff-select-dark framerateselect" required>
								<option value="">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
							</select>	
							<label id="framerateselect-error" class="error" for=""></label>
						</div>						
					</div>											 		
				</div>
				<div class="form-group">
					<label for="codec">Codec 
						<a href="#" class="tooltip-info">
							<img src="images/info.svg" alt="">
							<span class="tooltip-msg">
								Codec type
							</span>
						</a>
					</label>
					<input type="text" class="form-control ff-field-dark" id="codec" name="codec" placeholder="QT ProRes422HQ" required>	
				</div>
				<div class="form-group">
					<label for="projectdesc">Message (optional)</label>
					<textarea class="form-control ff-field-dark" rows="5"></textarea>											
				</div>
		</div>
		<div class="modal-content-wrapper">			
			<div class="modal-actions">
				<input type="submit" data-fancybox-close class="btn-ff btn-secondary-dark text-uppercase"  value="Cancel">
				<input type="submit" class="btn-ff btn-primary-dark text-uppercase" value="Send Request">
			</div>	
			</form>		
		</div>
	</div>
</div>
<!-- Request Enquiry Master -->
<?php require_once('include/footer.php') ?>
<?php require_once('include/footer-scripts.php') ?>
<script>
	$(document).ready(function(){
		/*Notification messages*/
		$('.addtofavourite-subtheme').click(function() {
			$('.notification-message').fadeIn();
			$('.notification-modal-text').text('You saved this subtheme as your favourites!')
			setTimeout(function() {
				$('.notification-message').fadeOut();
			}, 2000);
		});
		$('.addtofavourite-reel').click(function() {
			$('.notification-message').fadeIn();
			$('.notification-modal-text').text('You saved this theme as your favourites!')
			setTimeout(function() {
				$('.notification-message').fadeOut();
			}, 2000);
		});
		$('.select-all').click(function() {
			$('.notification-message').fadeIn();
			$('.notification-modal-text').text('You selected all themes!')
			setTimeout(function() {
				$('.notification-message').fadeOut();
			}, 2000);
		});
		$('.deselect-all').click(function() {
			$('.notification-message').fadeIn();
			$('.notification-modal-text').text('You deselected all themes!')
			setTimeout(function() {
				$('.notification-message').fadeOut();
			}, 2000);
		});
		$('.addtocart-reel').click(function() {
			$('.notification-message').fadeIn();
			$('.notification-modal-text').text('5 themes have been added to the cart!')
			setTimeout(function() {
				$('.notification-message').fadeOut();
			}, 2000);
		})
		$('.share-cart').click(function() {
			$('.notification-message').fadeIn();
			$('.notification-modal-text').text('Reel added to cart!')
			setTimeout(function() {
				$('.notification-message').fadeOut();
			}, 2000);
			$('.selected-items-wrapper').fadeIn()
		})
		/*Notification messages*/
		/*Addmore Start/ End TC*/
		$('.tc-addmore-link').click(function() {
			$('#addmore-begin-end-tc').append(
				"<div class='begin-end-tc'>" +
				"<div class='tc-time begin-tc'>" +
				"<label for='begintc'>Begin TC" +
				"</label>" +
				"<input type='text' class='form-control ff-field-dark' id='begintc' name='begintc' placeholder='00:00:00'>" +
				"</div>" +
				"<div class='tc-time end-tc'>" +
				"<label for='endtc'>End TC" +
				"</label>" +
				"<input type='text' class='form-control ff-field-dark' id='endtc' name='endtc' placeholder='00:00:00'>" +
				"<a href='#/' class='remove-tc'><img src='images/close.svg'/></a>" +
				"</div>	" +
				"</div>"
			)
		});
		$('body').on('click', '.remove-tc', function() {
			$(this).closest('.begin-end-tc').remove();
		});
		/*Addmore Start/ End TC*/
		$('#upload-reel-times').dropzone({
			url: "/file/post"
		});
		$('.file-remove-icon').click(function() {
			$(this).closest('.file-single').remove();
		});
		$('.share-reel-form').validate();
		$('.enquiry-master-form').validate({
			ignore: []
		});
		$('.show-svc').click(function(){
			$('.selected-video-commentary').toggleClass('show')
		});			
		if($(window).width()>1023){
			$('.niqv-video').hover(function(){
			$(this).children('.niqv-overlay').fadeIn();
			},function(){
				$(this).children('.niqv-overlay').fadeOut();
			})
		}
		
	});
</script>
</body>
</html>