<?php // this is the function used to check wether the user is logged in or not code by shiv 
function checklogin(){
    
    if(empty($_SESSION["footage_farm_user"]) && !isset($_SESSION["footage_farm_user"])){
        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            echo json_encode(array('success'=>false,'msg'=>'redirect'));
            exit();
        }else{
            redirect('login', 'refresh');
            exit();
        }
    }
}

?>