

<!-- Innerpage Search -->
<section class="page-container innerpage-top">
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="searchbar-top">
		<div class="home-search-wrapper">
			<form class="home-search" action="">
				<input type="text" placeholder="Search for footages and themes" name="home-search">
				<button type="submit"><img src="images/search-icon.svg" alt=""></button>
			</form>
		</div>
	</div>
</section>
<!-- Innerpage Search -->
<section class="page-container">
	<!-- Mobile Search -->
	<div class="multicolored-line multicolored-line-mobile">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="home-search-wrapper search-wrapper-mobile">
		<form class="home-search" action="">
			<input type="text" placeholder="Search for footages and themes" name="home-search">
			<button type="submit"><img src="images/search-icon.svg" alt=""></button>
		</form>
	</div>
	<!-- Mobile Search -->
	<!-- Innerpage Banner -->
	<section class="page-section mb-0">
		<div class="innerpage-banner">
			<div class="innerpage-banner-grey-bg full-height"></div>
			<div class="container">
				<div class="row">
					<div class="col-12">
						<h1 class="page-title-large text-uppercase">All Themes</h1>
					</div>
					<div class="col-12">
						<div class="ff-filters allthemes-filters filters-left">
							<div class="allthemes-filter-single">
								<select name="theme_name" id="theme_id" class="niceselect theme_filter">
									<option value="" selected="selected" disabled>Themes</option>
										<?php
										foreach ($theme_data as $key => $value) {
										 ?>
										 <option value="<?=$value['theme_id'] ?>"><?=$value['theme_name'] ?></option>
										<?php 
											}
										 ?>
									<!-- <option value="Early Reel (423)">Early Reel (423)</option>
									<option value="Personalities (34)">Personalities (34)</option>
									<option value="Educational Flims (18)">Educational Flims (18)</option>
									<option value="Hungarian Revolution (45)">Hungarian Revolution (45)</option>
									<option value="Space Exploration (12)">Space Exploration (12)</option>
									<option value="Russian Revolution (3)">Russian Revolution (3)</option>
									<option value="World War I (131)">World War I (131)</option>
									<option value="World War II (343)">World War II (343)</option>
									<option value="1960’s Civil Right Movements (7)">1960’s Civil Right Movements (7)</option>
									<option value="Home Movies (81)">Home Movies (81)</option> -->
								</select>
							</div>
							<div class="allthemes-filter-single">
								<select name="country" id="country_id" class="niceselect theme_filter">
									<option value="" selected="selected" disabled>Country</option>
									<?php
									foreach ($country_list as $key => $value) {
									 ?>
									 <option value="<?=$value['country_id'] ?>"><?=$value['country_name'] ?></option>
									<?php 
										}
									 ?>
									<!-- <option value="England">England</option>
									<option value="Germany">Germany</option>
									<option value="Italy">Italy</option>
									<option value="France">France</option>
									<option value="India">India</option>
									<option value="England">England</option>
									<option value="Germany">Germany</option>
									<option value="Italy">Italy</option>
									<option value="France">France</option>
									<option value="India">India</option> -->
								</select>
							</div>
							<div class="allthemes-filter-single">
								<select name="year" id="year" class="niceselect theme_filter">
									<option value="" selected="selected" disabled>Year</option>
									<?php
									foreach ($year_data as $key => $value) {
									 ?>
									 <option value="<?=$value ?>"><?=$value ?></option>
									<?php 
										}
									 ?>
									<!-- <option value="1920">1920</option>
									<option value="1921">1921</option>
									<option value="1922">1922</option>
									<option value="1923">1923</option>
									<option value="1924">1924</option>
									<option value="1925">1925</option>
									<option value="1926">1926</option>
									<option value="1927">1927</option>
									<option value="1928">1928</option>
									<option value="1929">1929</option> -->
								</select>
							</div>
							<div class="allthemes-filter-single">
								<select name="color" id="color" class="niceselect theme_filter">
									<option value="" selected="selected" disabled>B/W Color</option>
									<?php
									foreach ($color_data as $key => $value) {
									 ?>
									 <option value="<?=$value ?>"><?=$value ?></option>
									<?php 
										}
									 ?>
									<!-- <option value="B/W">B/W</option>
									<option value="Color">Color</option> -->
								</select>
							</div>
							<div class="allthemes-filter-single">
								<select name="sound" id="sound" class="niceselect theme_filter">
									<option value="" selected="selected" disabled>Sound</option>

									<?php
									foreach ($sound_data as $key => $value) {
									 ?>
									 <option value="<?=$value ?>"><?=$value ?></option>
									<?php 
										}
									 ?>
									 
									<!-- <option value="WAV Format">WAV Format</option>
									<option value="AIFF Format">AIFF Format</option>
									<option value="MP3 Format">MP3 Format</option>
									<option value="AAC Format">AAC Format</option>
									<option value="WMA Format">WMA Format</option> -->
								</select>
							</div>
						</div>
						<div class="ff-filters allthemes-filters filters-right">
							<p>Sort By</p>
							<div class="allthemes-filter-single">
								<select name="" id="" class="niceselect">
									<option value="Latest">Latest</option>
									<option value="Most Viewed">Most Viewed</option>
									<option value="Popular">Popular</option>
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Innerpage Banner -->
	<!-- Themes -->
	<section class="page-section pt-40">
		<div class="theme-select-overlay"></div>
		<div class="container">
			<div class="row" id="themedata">
			
			</div>
		</div>
	</section>
	<!-- Themes -->
</section>

</body>

</html>

<script type="text/javascript">
$(document).ready(function(){
	var duration = $("#durationid").val();
	$("#duration").val(duration);

	var type = $("#typeid").val();
	$("#type").val(type);

	$(document).on("click",".themeview",function () {
        var themecount = $(this).attr("data-themeid");
        var csrfName   = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
	    var csrfHash   = $('.txt_csrfname').val(); // CSRF hash
		$.ajax({
			url: "<?= base_url('theme/themeview')?>",
			type: "POST",
			data:{ themeview: themecount, [csrfName]: csrfHash },
			dataType: "json",
			success: function(response){
				
			}
		}); 
	      
	});

	$(document).on("change",".theme_filter",function () {
        var theme_id = $('#theme_id').val();
        var country  = $('#country_id').val();
        var year     = $('#year').val();
        var color    = $('#color').val();
        var sound    = $('#sound').val();

        var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
	    var csrfHash = $('.txt_csrfname').val(); // CSRF hash
	    if(theme_id!=""){
		$.ajax({
			url: "<?= base_url('theme/themedata')?>",
			type: "POST",
			data:{ id: theme_id, country: country, year: year, color: color, sound: sound, [csrfName]: csrfHash },
			dataType: "json",
			success: function(response){
				if(response.success){
					
				$("#themedata").html(response.data);	
					
				}else{
				$("#themedata").html('');
				}
			}
		});

	      //window.location = "<?= base_url('theme/selected_theme/')?>"+theme_id;
	    }
	      
	});

});

$(window).on( "load", function() { 
	var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
	var csrfHash = $('.txt_csrfname').val(); // CSRF hash
		$.ajax({
			url: "<?= base_url('theme/themedata')?>",
			type: "POST",
			data:{ [csrfName]: csrfHash },
			dataType: "json",
			success: function(response){
				if(response.success){
					
				$("#themedata").html(response.data);	
					
				}else{
				$("#themedata").html('');
				}
			}
		});
		

});
</script>