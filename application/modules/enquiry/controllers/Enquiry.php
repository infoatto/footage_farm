<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Enquiry extends MX_Controller
{
	
	public function __construct(){
		parent::__construct();
		//checklogin();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('enquirymodel','',TRUE);
	}

	public function index(){
		$result = array();

		$this->load->view('header');
		$this->load->view('enquiry-empty.php',$result);
		$this->load->view('footer');

	}

	public function addEdit(){

		$theme_id = "";
		//print_r($_GET);
		$result = array();
		if(!empty($_GET['text']) && isset($_GET['text'])){
			$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
			parse_str($varr,$url_prams);
			$theme_id = $url_prams['id'];
			$condition = " theme_id ='".$theme_id."' ";
			$result['theme_data'] = $this->common->getData("tbl_themes",'*',$condition);
		}
		
		// echo "<pre>";
		// print_r($result);
		// exit;
		//echo $user_id;
		$this->load->view('main-header.php');
		$this->load->view('addEdit.php',$result);
		$this->load->view('footer.php');
	}

	public function submitForm(){
		 //print_r($_POST);die;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();

			$data['name'] = $this->input->post('name');
			$data['email_id'] = $this->input->post('email');
			$data['phone_no'] = $this->input->post('phone');
			$data['company_name'] = $this->input->post('company');
			$data['project_name'] = $this->input->post('project');
			$data['message'] = $this->input->post('projectdesc');
			$data['status'] = 'New';
			$data['updated_on'] = date("Y-m-d H:i:s");
			$data['updated_by'] = (isset($this->session->userdata('footage_farm_user')['user_id'])? $this->session->userdata('footage_farm_user')['user_id'] : "1");

			if(!empty($this->input->post('theme_id'))){
				$condition = "theme_id = '".$this->input->post('theme_id')."' ";
				$result = $this->common->updateData("tbl_themes",$data,$condition);
				if($result){
					echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
					exit;
				}
				else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while updating data.'));
					exit;
				}
			}else{
				$data['created_on'] = date("Y-m-d H:i:s");
				$data['created_by'] =  isset($this->session->userdata('footage_farm_user')['user_id'])? $this->session->userdata('footage_farm_user')['user_id'] : "1";
				$result = $this->common->insertData('tbl_enquiry_videos',$data,'1');
				$array = array(
						'user_id' => $result,
						'enquiry_status' => 'New',
						'created_by' => $result,
						'created_on' => date("Y-m-d H:i:s"),
						'updated_by' => $result,
						'updated_on' => date("Y-m-d H:i:s"),
					);
		    	
				$result1 = $this->common->insertData('tbl_general_enquiry_status_log',$array,'1');
				if(!empty($result)){
					if(!empty($_POST['reelurl']) && isset($_POST['reelurl'])){
						foreach ($_POST['reelurl'] as $key => $value) {
							$reelurl_data = array();
							$reelurl_data['enquiry_video_id'] = $result;
							$reelurl_data['url'] = $value;
							$reelurl_data['created_on'] = date("Y-m-d H:i:s");
					        $reelurl_data['created_by'] =  isset($this->session->userdata('footage_farm_user')['user_id'])? $this->session->userdata('footage_farm_user')['user_id'] : "1";
							$this->common->insertData('tbl_enquiry_video_urls',$reelurl_data,'1');
						}
					}

					echo json_encode(array('success'=>true, 'msg'=>'Record Added Successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
					exit;
				}
			}
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem While Add/Edit Data.'));
			exit;
		}
	}

}?>
