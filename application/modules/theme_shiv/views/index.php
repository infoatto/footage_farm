
<body>
	<div class="wrapper">
    
		<div class="main-panel">
			<div class="container">
				<div class="page-inner">
					<div class="col-12">
						
						<div class="row ff-title-block">
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">
							<h1 class="ff-page-title">All Themes</h1> 
						</div>
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
							<a href="<?= base_url('theme/addEdit')?>" class="btn btn-primary ff-page-title-btn ff-page-title-btn">
								Add theme
							</a>
						</div>
					</div>
					</div>					
					<div class="row">
							<div class="col-sm-12">
									<div class="card">
											<div class="card-body">
												<div class="table-responsive">
													<table id="add-row" class="display table table-striped table-hover" >
														<thead>
															<tr>
																<th>Theme ID</th>
																<th>Theme Image</th>	
																<th>Theme Name</th>
																<th>Theme Description</th>																
																<th>Main Video</th>																
																<th>Actions</th>															
															</tr>
														</thead>
														<tfoot>
															<tr>
																<th>Theme ID</th>
																<th>Theme Image</th>	
																<th>Theme Name</th>
																<th>Theme Description</th>																
																<th>Main Video</th>																
																<th>Actions</th>														
															</tr>
														</tfoot>
														<tbody>
														<?php
														// echo "<pre>";
														// print_r($theme_data);
														if(!empty($theme_data) && isset($theme_data))	{
															$cnt = 0;
															foreach ($theme_data as $key => $value) {
																	$cnt= ++$cnt?>
																	<tr>														
																		<td>#<?=$cnt?></td>
																		<td><img src="<?= FRONT_URL.'/images/theme_image/'.$value['thumbnail_image']?>"class="img-thumbnail" alt="Cinque Terre" width="100" height="100"></td>																
																		<td><?= $value['theme_name']?></td>																
																		<td><?= $value['theme_description']?></td>			
																		<td><iframe src="<?= $value['main_video']?>" height="60" width="80" title="Iframe Example"></iframe></td>													
																		<td>
																			<div class="form-button-action">
																				<a href="<?= base_url('theme/AddEdit?text='.rtrim(strtr(base64_encode("id=".$value['theme_id']), '+/', '-_'), '=').'')?>" data-toggle="tooltip" title="" class="btn btn-primary ff-page-title-btn  btn-edit" data-original-title="Edit">
																					<i class="fa fa-edit"></i>
																				</a>
																				<!-- <a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																					<i class="fa fa-times"></i>
																				</a> -->
																			</div>
																		</td>													
																	</tr>
															<?php }
														}else{?>
																	NO themes Added so far 
														<?php }?>
														</tbody>
													</table>
												</div>
											</div>
									</div>
							</div>
					</div>
				</div>
			</div>
			
		</div>						
	</div>
	<script >
		$(document).ready(function() {
			// Add Row
			$('#add-row').DataTable({
				"pageLength": 5,
			});			
		});
	</script>
</body>
</html>
