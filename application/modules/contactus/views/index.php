
<!-- Innerpage Search -->
<section class="page-container innerpage-top">
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="searchbar-top">
		<div class="home-search-wrapper">
			<form class="home-search search-form" action="">
				<input type="text" list="browsers" placeholder="Search for footages and themes" name="search" autocomplete="off">
				  <datalist id="browsers">
				
				  </datalist>
				<button type="submit"><img src="images/search-icon.svg" alt=""></button>
			</form>
		</div>
	</div>
</section>
<!-- Innerpage Search -->
<section class="page-container">
	<!-- Mobile Search -->
	<div class="multicolored-line multicolored-line-mobile">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="home-search-wrapper search-wrapper-mobile">
		<form class="home-search" action="">
			<input type="text" placeholder="Search for footages and themes" name="home-search">
			<button type="submit"><img src="images/search-icon.svg" alt=""></button>
		</form>
	</div>
	<!-- Mobile Search -->
	<section class="page-section pt-75 pb-75 ptmob-60 pbmob-60 mb-0 contact-us-section">
		<div class="container">
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 offset-lg-1 col-lg-10">
					<div class="contact-content-wrapper">
						<div class="row">
							<div class="col-12 col-sm-5">
								<h1 class="page-title-small"><?= $contactus_data[0]['heading']?></h1>
								<p class="contact-intro"><?= $contactus_data[0]['banner_heading']?></p>
								<p class="contact-enquiry-title"><?= $contactus_data[0]['general_enquiries']?></p>
								<div class="contact-detail">
									<img src="images/phone-black.svg" alt="">
									<p><a href="tel:+44(0)2076313773"><?= $contactus_data[0]['phone']?></a></p>
								</div>
								<div class="contact-detail">
									<img src="images/email-black.svg" alt="">
									<p><?= $contactus_data[0]['email']?></p>
								</div>
							</div>
							<div class="col-12 col-sm-7">
								<?php if(!empty($contactus_data[0]['video_url']) && isset($contactus_data[0]['video_url'])){?>
									<iframe src="<?= $contactus_data[0]['video_url']?>" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="banner-top-vimeo">
									</iframe>
								<?php }else{?>
											<img src="<?= base_url('images/contactus/'.$contactus_data[0]['banner_image'])?>" height="340px" width="600px" alt="banner Image">
								<?php } ?>
								<!-- <img src="https://via.placeholder.com/600x340.png" alt="" class="img-responsive contactus-img"> -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</section>
</body>

</html>