<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Contactus  extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('contactusmodel','',TRUE);
		parent::__construct();
		
	}

	public function index()
	{
		// get Contact  us details 
		$condition = "1=1 ";
		$result['contactus_data'] = $this->common->getData("tbl_contact_us",'*',$condition);

		// echo "<pre>";
		// print_r($result);
		// exit;
			
		$result['meta_description'] = $result['contactus_data'][0]['meta_description'];
		$result['meta_keywords'] = $result['contactus_data'][0]['meta_title'];
		$this->load->view('header',$result);
		$this->load->view('index',$result);
		$this->load->view('footer');
	}
}
?>
