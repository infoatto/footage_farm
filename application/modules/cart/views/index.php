<!-- Innerpage Search -->
<section class="page-container innerpage-top">
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="searchbar-top">
		<div class="home-search-wrapper">
			<form class="home-search search-form" action="">
				<input type="text" list="browsers" placeholder="Search for footages and themes" name="search" autocomplete="off">
				  <datalist id="browsers">
				
				  </datalist>
				<button type="submit"><img src="<?= base_url()?><?= base_url()?>images/search-icon.svg" alt=""></button>
			</form>
		</div>
	</div>
</section>
<!-- Innerpage Search -->
<section class="page-container">
	<!-- Mobile Search -->
	<div class="multicolored-line multicolored-line-mobile">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="home-search-wrapper search-wrapper-mobile">
		<form class="home-search" action="">
			<input type="text" placeholder="Search for footages and themes" name="home-search">
			<button type="submit"><img src="<?= base_url()?><?= base_url()?>images/search-icon.svg" alt=""></button>
		</form>
	</div>
	<!-- Mobile Search -->
	<!-- Innerpage Banner -->
	<section class="page-section mb-0 cart-banner">
		<div class="innerpage-banner innerpage-banner-transparent pt-50 pb-25">
			<div class="innerpage-banner-grey-bg full-height"></div>
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="cart-progress">
							<a href="#/" class="cart-tab current"><span>Cart</span></a><a href="#/" class="details-tab"><span>Specify Details</span></a><a href="#/" class="review-tab"><span>Review</span></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Innerpage Banner -->
	<!-- Cart Tab Content -->
	<section class="page-section pt-40" id="cart-tab-content">
		<div class="container">
			<div class="row">
				<div class="col-12" id="reel-list-cart">

				</div>
			</div>
		</div>
		<div class="cart-form-actions ">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<a href="index.php" class="btn-ff btn-secondary-dark text-uppercase">Continue Exploring</a>
						<input type="submit" id="cart-tab-next" class="btn-ff btn-primary-dark text-uppercase" value="Next">
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Cart Tab Content -->
		<!-- Details Tab Content -->
		<section class="page-section pt-40" id="details-tab-content" >
		<div class="container">
			<div class="row">
				<div class="col-12 col-sm-5 reel-list" id="detail-tab-listing">

				</div>
				<div class="col-12 col-sm-7 reel-form no-padding-mobile">
					<div class="details-enquiry-wrapper" id="cart-enquiry-wrapper">			
			
					</div>
				</div>
			</div>
		</div>

		<div class="cart-form-actions cfad-desktop" id="details-tab-next-div">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<a href="index.php" class="btn-ff btn-secondary-dark text-uppercase">Continue Exploring</a>
						<input type="submit" id="details-tab-next" class="btn-ff btn-primary-dark text-uppercase" value="Next">
					</div>
				</div>
			</div>
		</div>
		<div class="cart-form-actions cfad-mobile" id="details-tab-next-mobile-div">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<input type="submit" id="details-tab-next-mobile" class="btn-ff btn-primary-dark text-uppercase" value="Go Next">
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Details Tab Content -->
	
	<!-- Review Tab Content -->
	<section class="page-section pt-40" id="review-tab-content">
		<div class="container">
			<div class="row">
				<div class="col-12 col-sm-5 reel-list" id="review-tab-listing">
					
				</div>
				<div class="col-12 col-sm-7 reel-form no-padding-mobile">
					<div class="details-enqiry-wrapper" id="cart-review-wrapper">
						
					</div>
				</div>
			</div>
		</div>
		<div class="cart-form-actions ">
			<div class="container">
				<div class="row">
					<div class="col-12">	
						<a href="#/" class="btn-ff btn-ff-icon btn-secondary-dark text-uppercase request-reel-btn review-cart-edit review-cart-edit-mobile">Edit</a>					
						<input type="button" id="review-tab-next" class="btn-ff btn-primary-dark text-uppercase" value="Send Request">
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Review Tab Content -->
	<!-- Enquiry Sent Message -->
	<section class="page-section pt-40" id="enquiry-sent-content">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="enquiry-sent-wrapper">
						<img src="<?= base_url()?>images/success.svg" alt="">
						<p class="enquiry-success">Enquiries Successfully Sent!</p>
						<p class="enquiry-success-note">Thank you for sending the request. We will reach you shortly to proceed further</p>
						<a class="btn-ff btn-primary-dark text-uppercase" href="<?= base_url()?>">Go Back to Homepage</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Enquiry Sent Message -->
	<!-- Notification Modal -->
	<div class="notification-message">
		<div class="notification-overlay"></div>
		<div class="notification-modal">
			<div class="modal-message">
				<img src="<?= base_url()?><?= base_url()?>images/completed.svg">
				<p class="notification-modal-text"></p>
			</div>
		</div>									
	</div>
	<!-- Notification Modal -->
</section>
<script>
getCartReel();
getcartReelform(1);
$(document).ready(function() {
	$('.cart-tab').click(function(){
		if($(this).hasClass('done')){
			$('.cart-progress a').removeClass('current').removeClass('done');
			$(this).removeClass('done').addClass('current');	
			$('#cart-tab-content').show();	
			$('#details-tab-content,#review-tab-content').hide();		
		}
	});
	$('.details-tab').click(function(){
		if($(this).hasClass('done')){
			$('.cart-progress a').removeClass('current');
			$(this).removeClass('done').addClass('current');	
			$('#details-tab-content').show();	
			$('#cart-tab-content,#review-tab-content').hide();		
		}
	});
	$('#cart-tab-next').click(function() {
		$('.cart-tab').removeClass('current').addClass('done');
		$('.details-tab').addClass('current');
		$('#cart-tab-content').hide();
		$('#details-tab-content').show();
	})
	$('#details-tab-next, #details-tab-next-mobile').click(function() {
		$('.details-tab').removeClass('current').addClass('done');
		$('.review-tab').addClass('current');
		$('#details-tab-content').hide();
		$('#review-tab-content').show();
		$('#review-tab-content .details-list-single').removeClass('active');
		getCartReel();
		getcartReelform(1);
	});
	/* $('#details-tab-next-mobile').click(function() {			
		$('#details-tab-content, .cart-banner').hide();
		$('#enquiry-sent-content').show();
	});	 */

	/*Addmore Start/ End TC*/
	//  $('.tc-addmore-link-review').click(function() {				
	//  	$('#addmore-begin-end-tc-review').append(
	//  		"<div class='begin-end-tc'>" +
	//  		"<div class='tc-time begin-tc'>" +
	//  		"<label for='begintc'>Begin TC" +
	//  		"</label>" +
	//  		"<input type='text' class='form-control ff-field-light' id='begintc' name='begintc' placeholder='00:00:00'>" +
	//  		"</div>" +
	//  		"<div class='tc-time end-tc'>" +
	// 		"<label for='endtc'>End TC" +
	//  		"</label>" +
	//  		"<input type='text' class='form-control ff-field-light' id='endtc' name='endtc' placeholder='00:00:00'>" +
	//  		"<a href='#/' class='remove-tc'><img src='images/close.svg'/></a>" +
	//  		"</div>	" +
	// 	"</div>"
	//  	)
	//  });
	// $('body').on('click', '.remove-tc', function() {
	//  	$(this).closest('.begin-end-tc').remove();
	// });
	/*Addmore Start/ End TC*/
	// $('#upload-reel-times').dropzone({
	// 	url: "/file/post"
	// });
	$('.enquiry-master-form').validate({
		ignore: []
	});
	$('.details-content-wrapper .details-list-single').click(function() {
		$('.details-content-wrapper').children('.details-list-single').removeClass('active-detail');
		$(this).addClass('active-detail');
	});		
	$('.review-content-wrapper .details-list-single').click(function() {
		$('.review-content-wrapper').children('.details-list-single').removeClass('active-selected');
		$(this).addClass('active-selected');
	});
	// /*Remove cart item*/
	// $('.cls-remove').click(function(){
	// 	$(this).closest('.cart-list-single').remove();
	// 	$('.notification-message').fadeIn();		
	// 	$('.notification-modal-text').text('The reel has been removed')
	// 	setTimeout(function(){ $('.notification-message').fadeOut();	 }, 2000);
	// })

	// /*Remove cart item*/
});

// to get the added reel in cart 
function getCartReel(){
	var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
	var csrfHash = $('.txt_csrfname').val(); // CSRF hash
	$.ajax({
		url: "<?= base_url('cart/getReelList')?>",
		type: "POST",
		data:{ [csrfName]: csrfHash },
		dataType: "json",
		success: function(response){
			if(response.success){
				$('#reel-list-cart').html(response.cartListview);
				$('#detail-tab-listing').html(response.cartListviewDetail);
				$('#review-tab-listing').html(response.cartListviewDetail);


			}
		}
	});
}

$('#review-tab-next').click(function() {				
	$('#review-tab-content, .cart-banner').hide();
	var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
	var csrfHash = $('.txt_csrfname').val(); // CSRF hash
	$.ajax({
		url: "<?= base_url('cart/finalSubmissionReel')?>",
		type: "POST",
		data:{ [csrfName]: csrfHash },
		dataType: "json",
		success: function(response){
			if(response.success){
				$('#enquiry-sent-content').show();
			}
		}
	});

	
});

// to get the added reel wise form data 
function getcartReelform(reel_position,cart_id=null){
	var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
	var csrfHash = $('.txt_csrfname').val(); // CSRF hash
	$.ajax({
		url: "<?= base_url('cart/getcartReelform')?>",
		type: "POST",
		data:{reel_position,cart_id,[csrfName]: csrfHash },
		dataType: "json",
		success: function(response){
			if(response.success){
				$('#cart-enquiry-wrapper').html(response.form_html_detail);
				$('#cart-review-wrapper').html(response.form_html_review);

				if(response.next_btn_show){
					$('#details-tab-next-div').show();
					$('#details-tab-next-mobile-div').show();
				}else{
					$('#details-tab-next-div').hide();
					$('#details-tab-next-mobile-div').hide();

				}
			}else{
					$('#details-tab-next-div').hide();
					$('#details-tab-next-mobile-div').hide();

				}
		}
	});
}

// remove  cart reel  code of shiv  22-9-2020 
function deleteReel(cart_id){
	if(cart_id){
		var r = confirm("Are you want to sure delete this reel from cart");
			if (r == true) {
				var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
				var csrfHash = $('.txt_csrfname').val(); // CSRF hash
				$.ajax({
					url: "<?= base_url('cart/deleteReel')?>",
					type: "POST",
					data:{ cart_id , [csrfName]: csrfHash },
					dataType: "json",
					success: function(response){
						if(response.success){
							$('.notification-message').fadeIn();		
							$('.notification-modal-text').text(response.msg)
							setTimeout(function(){
								getCartReel();
								getcartReelform(1)
								$(this).closest('.cart-list-single').remove();
								$('.notification-message').fadeOut();	 
								}, 2000);	
						}else{
							$('.notification-message').fadeIn();		
							$('.notification-modal-text').text(response.msg)
							setTimeout(function(){
								$('.notification-message').fadeOut();	 
								}, 2000);	
						}
					}
				});
			} else {
				$('.notification-message').fadeIn();		
				$('.notification-modal-text').text('The reel safe in you cart')
				setTimeout(function(){ $('.notification-message').fadeOut();	 }, 2000);
			}
	}else{

	}

}

//remove tc  reel from form details
function removeTc(video_cart_tc_id){
	if(video_cart_tc_id){
		var r = confirm("Are you want to sure delete this TC from cart");
			if (r == true) {
				var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
				var csrfHash = $('.txt_csrfname').val(); // CSRF hash
				var reel_position = $('#reel_position').val();
				var cart_id = $('#cart_id').val();
				$.ajax({
					url: "<?= base_url('cart/deleteReelTC')?>",
					type: "POST",
					data:{ video_cart_tc_id , [csrfName]: csrfHash },
					dataType: "json",
					success: function(response){
						if(response.success){
							$('.notification-message').fadeIn();		
							$('.notification-modal-text').text(response.msg)
							setTimeout(function(){
								getCartReel();
								getcartReelform(reel_position,cart_id);
								$(this).closest('.cart-list-single').remove();
								$('.notification-message').fadeOut();	 
								}, 2000);	
						}else{
							$('.notification-message').fadeIn();		
							$('.notification-modal-text').text(response.msg)
							setTimeout(function(){
								$('.notification-message').fadeOut();	 
								}, 2000);	
						}
					}
				});
			} else {
				$('.notification-message').fadeIn();		
				$('.notification-modal-text').text('The reel safe in you cart')
				setTimeout(function(){ $('.notification-message').fadeOut();	 }, 2000);
			}
	}else{

	}

}
//remove upload file  from form details
function removeupload(video_cart_upload_id){
	if(video_cart_upload_id){
		var r = confirm("Are you want to sure delete this Upload from cart");
			if (r == true) {
				var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
				var csrfHash = $('.txt_csrfname').val(); // CSRF hash
				var reel_position = $('#reel_position').val();
				var cart_id = $('#cart_id').val();
				$.ajax({
					url: "<?= base_url('cart/deleteReelUpload')?>",
					type: "POST",
					data:{ video_cart_upload_id , [csrfName]: csrfHash },
					dataType: "json",
					success: function(response){
						if(response.success){
							$('.notification-message').fadeIn();		
							$('.notification-modal-text').text(response.msg)
							setTimeout(function(){
								getCartReel();
								getcartReelform(reel_position,cart_id);
								$(this).closest('.cart-list-single').remove();
								$('.notification-message').fadeOut();	 
								}, 2000);	
						}else{
							$('.notification-message').fadeIn();		
							$('.notification-modal-text').text(response.msg)
							setTimeout(function(){
								$('.notification-message').fadeOut();	 
								}, 2000);	
						}
					}
				});
			} else {
				$('.notification-message').fadeIn();		
				$('.notification-modal-text').text('The reel safe in you cart')
				setTimeout(function(){ $('.notification-message').fadeOut();	 }, 2000);
			}
	}else{

	}

}



</script>
</body>

</html>