
    <h3 class="cart-title">Your Cart (<?= count($cart_reels)?> items)</h3>
    <div class="cart-list-wrapper">
    <?php if(!empty($cart_reels)){
        $i=0;
        foreach ($cart_reels as $key => $value) {
            $i+=1;
            ?>
                <div class="cart-list-single" >
                    <div class="cls-image">
                        <!-- <img src="<?= base_url('images/video_image/'.$value['thumbnail_image'])?>" alt=""> -->
                        <iframe src="<?= $value['vimeo_url']?>"  frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="">
						</iframe>
                    </div>
                    <div class="cls-info">
                        <h4><?=$value['video_title'] ?> - <?= $value['country_name']?> (<?= $value['year']?>) R<?= $i?>/<?=count($cart_reels)?></h4>
                        <div class="block-info">
                            <div>
                                <p class="block-info-title">Reel Number</p>
                                <p class="block-info-content"><?= $value['reel_number']?></p>
                            </div>
                        </div>
                        <a href="Javascript:void(0)" class="cls-remove" onclick="deleteReel(<?=$value['cart_id']?>)">Remove</a>
                    </div>
                </div>
            <?php } 
            } ?>
    </div>
