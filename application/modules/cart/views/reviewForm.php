
<div class="details-enquiry-title">	
    <a href="#/" class="mobile-enquiry-nav men-back">
        <img src="images/cart-back.svg" alt="">
    </a>		
    <h3 class="details-title">Reel <?= $reel_position ?> of <?= $reel_cnt ?></h3>
    <a href="#/" class="mobile-enquiry-nav men-next">
        <img src="images/cart-forward.svg" alt="">
    </a>	
    <a href="#/" class="close-details-enquiry"><img src="images/close.svg" alt=""></a>
    <span class="saved-msg">Saved a few seconds ago</span>
</div>
<form class="enquiry-master-form details-enquiry-form review-enquiry-form">
    <div class="review-wrapper">
        <div class="cart-list-single cart-list-single-mobile">
            <div class="cls-image">
            <?php
            // echo "<pre>";  
            // print_r($check_exist_form_reel);
            // exit;
            ?>
             <iframe src="<?= $check_exist_form_reel[0]['vimeo_url']?>"  style="height:200px;width:300px" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="">
			</iframe>
            </div>
            <div class="cls-info">
                <h4><?=$check_exist_form_reel[0]['video_title'] ?> - <?= $check_exist_form_reel[0]['country_name']?> (<?= $check_exist_form_reel[0]['year']?>) R<?= $reel_position ?>/<?= $reel_cnt ?></h4>
                <div class="block-info">
                    <div>
                        <p class="block-info-title">Reel Number</p>
                        <p class="block-info-content"><?=$check_exist_form_reel[0]['reel_number']?></p>
                    </div>
                </div>
                <a href="Javascript:void(0)" class="cls-remove" onclick="deleteReel(<?=$check_exist_form_reel[0]['reel_cart_id']?>)">Remove</a>
            </div>
        </div>
        <div class="form-group">
            <div class="reel-title">
              
                <iframe src="<?= $check_exist_form_reel[0]['vimeo_url']?>"  style="height:200px;width:300px" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="">
				</iframe>
                <h4><?=$check_exist_form_reel[0]['video_title'] ?> </h4>

            </div>
        </div>
        <div class="form-group">
            <div class="request-reel-type">
                <label><span>Request Type:</span> <?= $check_exist_form_reel[0]['request_type']?> </label>
            </div>
        </div>
        <div class="form-group">
            <?php
                 $total_begin_tc = explode(",",$check_exist_form_reel[0]['begin_tc']);
                $total_end_tc = explode(",",$check_exist_form_reel[0]['end_tc']);
            foreach ($total_begin_tc as $key => $value) {?>
                <div class="mb-10">
                    <label><span>Begin TC</span>: <?= $value?></label>
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<label><span>End TC</span>: <?=$total_end_tc[$key]?></label>
                </div>
            <?php } ?>
          
          
           
            <!-- <div class="mb-10">
                <label><span>End TC</span>: 23:35:45:60</label>
            </div> -->
            <div class="mb-10">
                <label><span>In Out Time Codes</span></label>
                <?php 
                if($check_exist_form_reel[0]['uploaded_document']){ 
                    $total_uploaded_document = explode(",",$check_exist_form_reel[0]['uploaded_document']);?>
                <div class="uploaded-files-list mt-10 mb-10">
                <?php foreach ($total_uploaded_document as $key => $value) {
                    //   print_r($value);
                    if($check_exist_form_reel[0]['document_type'] == '.pdf'){?>
                        <div class="file-single">
                            <a href="<?=  base_url('images/cart_upload/'.$value)?>" target="_blank" >
                                <img src="<?= base_url()?>images/pdf.svg" alt=""    class="file-type-icon" style="height: 30px;width: 40px;">
                                <span><?= $value?></span>
                            </a>
                        </div>
                    <?php }else if($check_exist_form_reel[0]['document_type'] == '.xls'|| $check_exist_form_reel[0]['document_type'] == '.xlsx' ||  $check_exist_form_reel[0]['document_type'] == '.csv'){?>
                        <div class="file-single">
                            <a href="<?= base_url('images/cart_upload/'.$value)?>" target="_blank">
                                <img src="<?= base_url()?>images/xls.svg" alt="" class="file-type-icon">
                                <span><?= $value?></span>
                            </a>
                        </div>

                    <?php }else{?>
                        <div class="file-single">
                            <a href="<?=  base_url('images/cart_upload/'.$value)?>" target="_blank">
                                <img src="<?= base_url()?>images/jpg.svg" alt="" class="file-type-icon">
                                <span><?= $value?></span>
                            </a>
                           
                        </div>
                    <?php } ?>
                        
                    <?php } //closing of foreach
                    }?>
                </div>
            </div>
            <div class="mb-10">
                <label><span>Format</span>: <?= $check_exist_form_reel[0]['format']?></label>
            </div>
            <div class="mb-10">
                <label><span>Frame Rate</span>: <?= $check_exist_form_reel[0]['frame_rate']?></label>
            </div>
            <div class="mb-10">
                <label><span>Codec</span>: <?= $check_exist_form_reel[0]['codec'] ?></label>
            </div>
            <div class="mb-10">
                <label><span>Message</span>: <?=$check_exist_form_reel[0]['message']?></label>
            </div>
        </div>
    </div>
    <div class="edit-details">
        <a href="#/" class="btn-ff btn-ff-icon btn-secondary-dark text-uppercase request-reel-btn review-cart-edit">Edit</a>
    </div>
</form>
<form class="enquiry-master-form details-enquiry-form edit-enquiry-form" id="form-enquiry-cart-review" method="post" enctype="multipart/form-data">
    <input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden" class="txt_csrfname">
    <div class="save-detail-cart-wrapper">
        <input type="submit" class="btn-ff btn-primary-dark text-uppercase save-detail-cart" value="Save">	
    </div>			
    <input type="hidden" id="cart_id" name="cart_id" value="<?=$check_exist_form_reel[0]['reel_cart_id']?>">
    <input type="hidden" id="video_id" name="video_id" value="<?=$check_exist_form_reel[0]['reel_video_id']?>">
    <input type="hidden" id="video_cart_detail_id" name="video_cart_detail_id" value="<?=$check_exist_form_reel[0]['video_cart_detail_id']?>">
    <input type="hidden" id="reel_position" name="reel_position" value="<?=$reel_position?>">


    <div class="cart-list-single cart-list-single-mobile">
        <div class="cls-image">
          <img src="<?= base_url('images/video_image/'.$check_exist_form_reel[0]['thumbnail_image'])?>" alt="">
        </div>
        <div class="cls-info">
            <h4><?=$check_exist_form_reel[0]['video_title'] ?> - <?= $check_exist_form_reel[0]['country_name']?> (<?= $check_exist_form_reel[0]['year']?>) R<?= $reel_position ?>/<?= $reel_cnt ?></h4>
            <div class="block-info">
                <div>
                    <p class="block-info-title">Reel Number</p>
                    <p class="block-info-content"><?=$check_exist_form_reel[0]['reel_number']?></p>
                </div>
            </div>
            <a href="Javascript:void(0)" class="cls-remove" onclick="deleteReel(<?=$check_exist_form_reel[0]['reel_cart_id']?>)">Remove</a>
        </div>
    </div>
    <div class="form-group">
        <div class="reel-title  ">
            <!-- <img src="https://via.placeholder.com/147x83.png" alt=""> -->
            <img src="<?= base_url('images/video_image/'.$check_exist_form_reel[0]['thumbnail_image'])?>" alt="" style="height: 83px;width: 147px;">
            <h3><?= $check_exist_form_reel[0]['video_title']?></h3>
        </div>
    </div>
    <div class="form-group">
        <div class="request-reel-type">
            <label for="selectreeltype">Please select the reel type</label>
            <label class="radio-container-theme">
                <span class="radio-label">Request Preview</span>
                <input type="radio" name="reeltype" <?= (($check_exist_form_reel[0]['request_type']=='preview')?"checked":"") ?> value ="preview" >
                <span class="radio"></span>
            </label>
            <label class="radio-container-theme">
                <span class="radio-label">Request Master</span>
                <input type="radio" name="reeltype" <?= (($check_exist_form_reel[0]['request_type']=='master')?"checked":"") ?> value="master">
                <span class="radio"></span>
            </label>
        </div>
        <label for="reeltype" class="error"></label>
    </div>
    <div class="form-group">
        <label for="inouttime">Specify the footage's in and out time</label>
        <?php 
            if($check_exist_form_reel[0]['tc_begins']){
                $total_begin_tc = explode(",",$check_exist_form_reel[0]['begin_tc']);
                $total_end_tc = explode(",",$check_exist_form_reel[0]['end_tc']);
                $video_cart_tc_id = explode(",",$check_exist_form_reel[0]['video_cart_tc_id']);

                foreach ($total_begin_tc as $key => $value) {?>
                   <div class="begin-end-tc">
                    <div class="tc-time begin-tc">
                        <label for="begintc">Begin TC
                            <a href="#" class="tooltip-info">
                                <img src="<?= base_url()?>images/info.svg" alt="">
                                <span class="tooltip-msg">
                                    Footage start time
                                </span>
                            </a>
                        </label>
                        <input type="text" class="form-control ff-field-light" value="<?=$value?>"  id="begintc<?=$check_exist_form_reel[0]['reel_video_id']?>" name="begintc[]" placeholder="00:00:00" >
                    </div>
                    <div class="tc-time end-tc">
                        <label for="endtc">End TC
                            <a href="#" class="tooltip-info">
                                <img src="<?= base_url()?>images/info.svg" alt="">
                                <span class="tooltip-msg">
                                    Footage end time
                                </span>
                            </a>
                        </label>
                        <input type="text" class="form-control ff-field-light" value="<?=$total_end_tc[$key]?>" id="endtc<?=$check_exist_form_reel[0]['reel_video_id']?>" name="endtc[]" placeholder="00:00:00" >
                        <a href='#/' class='remove-tc'><img src='<?= base_url()?>images/close.svg'  onclick="removeTc(<?= $video_cart_tc_id[$key]?>)"> </a>
                    </div>
                </div>
               <?php  } //foreach close

                //if close  here
             }else{ ?>
                <div class="begin-end-tc">
                    <div class="tc-time begin-tc">
                        <label for="begintc">Begin TC
                            <a href="#" class="tooltip-info">
                                <img src="<?= base_url()?>images/info.svg" alt="">
                                <span class="tooltip-msg">
                                    Footage start time
                                </span>
                            </a>
                        </label>
                        <input type="text" class="form-control ff-field-light" id="begintc<?=$check_exist_form_reel[0]['reel_video_id']?>" name="begintc[]" placeholder="00:00:00" >
                    </div>
                    <div class="tc-time end-tc">
                        <label for="endtc">End TC
                            <a href="#" class="tooltip-info">
                                <img src="<?= base_url()?>images/info.svg" alt="">
                                <span class="tooltip-msg">
                                    Footage end time
                                </span>
                            </a>
                        </label>
                        <input type="text" class="form-control ff-field-light" id="endtc<?=$check_exist_form_reel[0]['reel_video_id']?>" name="endtc[]" placeholder="00:00:00" >
                    </div>
                </div>
            <?php } ?>
       
        <div id="addmore-begin-end-tc-specify-details"></div>
        <div class="form-addmore">
            <a href="javascript:void(0);" class="form-addmore-link tc-addmore-link-specify-review"><img src="<?= base_url()?>images/add-black.svg" alt=""> <span>Add another in and out time codes</span></a>
        </div>
    </div>
    <div class="form-group">
        <label>Do you have a file with multiple in and out time codes?</label>
        <p>Add jpegs, excel or word files here. Each file size should be under 2MB.</p>
        <?php 
        if($check_exist_form_reel[0]['uploaded_document']){
            $total_uploaded_document = explode(",",$check_exist_form_reel[0]['uploaded_document']);
            $video_cart_upload_id = explode(",",$check_exist_form_reel[0]['video_cart_upload_id']);
            ?>
        <div class="uploaded-files-list">
          <?php foreach ($total_uploaded_document as $key => $value) {
            //   print_r($value);
              if($check_exist_form_reel[0]['document_type'] == '.pdf'){?>
                <div class="file-single">
                    <a href="<?=  base_url('images/cart_upload/'.$value)?>" target="_blank" >
                        <img src="<?= base_url()?>images/pdf.svg" alt=""    class="file-type-icon" style="height: 30px;width: 40px;">
                        <span><?= $value?></span>
                    </a>
                    <a href="#/">

                    <img src="<?= base_url()?>images/close.svg" alt="" class="file-remove-icon" onclick="removeupload(<?= $video_cart_upload_id[$key]?>)"></a>
                    </a>
                </div>
              <?php }else if($check_exist_form_reel[0]['document_type'] == '.xls'|| $check_exist_form_reel[0]['document_type'] == '.xlsx' ||  $check_exist_form_reel[0]['document_type'] == '.csv'){?>
                <div class="file-single">
                    <a href="<?= base_url('images/cart_upload/'.$value)?>" target="_blank">
                        <img src="<?= base_url()?>images/xls.svg" alt="" class="file-type-icon">
                        <span><?= $value?></span>
                    </a>
                    <a href="#/">
                        <img src="<?= base_url()?>images/close.svg" alt="" onclick="removeupload(<?= $video_cart_upload_id[$key]?>)"  class="file-remove-icon">
                    </a>
                 </div>

              <?php }else{?>
                <div class="file-single">
                    <a href="<?=  base_url('images/cart_upload/'.$value)?>" target="_blank">
                        <img src="<?= base_url()?>images/jpg.svg" alt="" class="file-type-icon">
                        <span><?= $value?></span>
                    </a>
                    <a href="#/">
                        <img src="<?= base_url()?>images/close.svg" alt=""  onclick="removeupload(<?= $video_cart_upload_id[$key]?>)"  class="file-remove-icon">
                    </a>
                 </div>
              <?php } ?>
                
            <?php } //closing of foreach?>
        </div>
          <?php } ?>
        <!-- Created using dropzone.js-->
        <div id="upload-reel-times">
            <!-- <img src="<?= base_url()?>images/upload-file.svg" alt=""> -->
            <!-- <p>Drag and drop here or browse</p> -->
            <!-- <a href="#/" class="login-link">Browse</a> -->
            <input type="file" name="reelfiles[]" multiple  id="reelfile" >
        </div>
        <!-- Created using dropzone.js-->
    </div>
    <div class="form-group">
        <div class="format-framerate">
            <div class="format formatframe">
                <label for="begintc">Format</label>
                <?php $format = array('MP4','3GP','WMV','WEBM','WEBM','FLV','AVI','QuickTime'); ?> 
                <select class="niceselect ff-select-light formatselect" name="format" id="format<?=$check_exist_form_reel[0]['reel_video_id']?>"  >
                    <?php  $sel = '' ;
                        foreach ($format as $key => $value) {
                        $sel= ($check_exist_form_reel[0]['format']== $value)?"selected":"";
                        ?>
                     <option value="<?= $value ?>" <?= $sel;?>><?= $value ?></option>
                   <?php  } ?>
             
                </select>
                <label id="formatselect-error" class="error" for=""></label>
            </div>
            <div class="framerate formatframe">
                <label for="endtc">Frame Rate</label>
                <?php $frame_rate = array('24FPS','25FPS','30FPS','50FPS','60FPS') ?>
                <select class="niceselect ff-select-light framerateselect"  name="frame_rate" id="frame_rate<?=$check_exist_form_reel[0]['reel_video_id']?>"  >

                <?php  $sel = '' ;
                        foreach ($frame_rate as $key => $value) {
                        $sel= ($check_exist_form_reel[0]['frame_rate']== $value)?"selected":"";
                        ?>
                     <option value="<?= $value ?>" <?= $sel;?>><?= $value ?></option>
                   <?php  } ?>
               
                </select>
                <label id="framerateselect-error" class="error" for=""></label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="codec">Codec
            <a href="#" class="tooltip-info">
                <img src="<?= base_url()?>images/info.svg" alt="">
                <span class="tooltip-msg">
                    Codec type
                </span>
            </a>
        </label>
        <input type="text" class="form-control ff-field-light" id="codec<?=$check_exist_form_reel[0]['reel_video_id']?>" value="<?= $check_exist_form_reel[0]['codec']?>" name="codec" placeholder="QT ProRes422HQ" >
    </div>
    <div class="form-group">
        <label for="projectdesc">Message (optional)</label>
        <textarea class="form-control ff-field-light" name="message" id="message<?=$check_exist_form_reel[0]['reel_video_id']?>" rows="5"><?=$check_exist_form_reel[0]['message']?></textarea>
    </div>
    <div class="save-detail-cart-wrapper">
        <input type="submit" class="btn-ff btn-primary-dark text-uppercase save-detail-cart" value="Save">	
    </div>
</form>

<script>

/*Addmore Start/ End TC*/

// $(document).on("click",".tc-addmore-link-specify-review",function(){
//     alert("inside function ")

// });

$('.tc-addmore-link-specify-review').click(function() {    	
    $('#addmore-begin-end-tc-specify-details').append(
        "<div class='begin-end-tc'>" +
        "<div class='tc-time begin-tc'>" +
        "<label for='begintc'>Begin TC" +
        "</label>" +
        "<input type='text' class='form-control ff-field-light' id='begintc' name='begintc[]' placeholder='00:00:00'>" +
        "</div>" +
        "<div class='tc-time end-tc'>" +
        "<label for='endtc'>End TC" +
        "</label>" +
        "<input type='text' class='form-control ff-field-light' id='endtc' name='endtc[]' placeholder='00:00:00'>" +
        "<a href='#/' class='remove-tc'><img src='<?= base_url()?>images/close.svg'/></a>" +
        "</div>	" +
        "</div>"
    )
});
$('body').on('click', '.remove-tc', function() {
    $(this).closest('.begin-end-tc').remove();
		});


        
// use for each form submisson
$("#form-enquiry-cart-review").validate({
	ignore:[],
	rules: {
		reeltype:{required:true},
		format:{required:true},
		frame_rate:{required:true},
		codec:{required:true},
		"begintc[]":{required:true},
		"endtc[]":{required:true},
	},
	messages: {
		reeltype:{required:"Please Select Reel type of video."},
		format:{required:"Please select Format of video."},
		frame_rate:{required:"Please select Frame Rate of video."},
		codec:{required:"Please Enter Codec for this Reel."},
		"begintc[]":{required:"Please Enter Begin TC."},
		"endtc[]":{required:"Please Enter End TC."},
	},
	submitHandler: function(form) 
	{	
		var act = "<?= base_url('cart/submitForm')?>";
		var reel_position = $('#reel_position').val();
		var cart_id = $('#cart_id').val();
		$("#form-enquiry-cart-review").ajaxSubmit({
			url: act, 
			type: 'post',
			dataType: 'json',
			cache: false,
			clearForm: false,
			async:false,
			beforeSubmit : function(arr, $form, options){
				$(".btn btn-black").hide();
			},
			success: function(response) 
			{
				if(response.success){
					swal("done", response.msg, {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					}).then(
					function() {
                        // getCartReel();
                        $('#success_flag'+cart_id).show();
                        // if(next_btn_show){
                        //     $('#details-tab-next').show();
                        // }
						getcartReelform(reel_position,cart_id)
					});
				}else{	
					swal("Failed", response.msg, {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}
			}
		});
	}
});

</script>