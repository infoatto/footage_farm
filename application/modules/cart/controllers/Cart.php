<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Cart extends MX_Controller
{
	
	public function __construct(){
		parent::__construct();
		checklogin();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('cartmodel','',TRUE);
	}

	public function deleteReel(){
		// echo "<pre>";print_r($_POST);exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			$condition = "user_id = '".$this->session->userdata('footage_farm_user')[0]['user_id']."' AND  cart_id = '".$this->input->post('cart_id')."' ";
			$cart_reel_Delete = $this->common->deleteRecord("tbl_video_cart",$condition);
			$condition = "cart_id = '".$this->input->post('cart_id')."' ";
			$this->common->deleteRecord("tbl_video_cart_tc",$condition);
			$this->common->deleteRecord("tbl_video_cart_uploads",$condition);
			if(!empty($cart_reel_Delete)){
				echo json_encode(array('success'=>true, 'msg'=>'The reel has been removed'));
				exit;
			}else{
				echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
				exit;
			}

		}
	}
	public function deleteReelTC(){
		// echo "<pre>";print_r($_POST);exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			$condition = "video_cart_tc_id = '".$this->input->post('video_cart_tc_id')."' ";
			$cart_TC_Delete = $this->common->deleteRecord("tbl_video_cart_tc",$condition);
			if(!empty($cart_TC_Delete)){
				echo json_encode(array('success'=>true, 'msg'=>'The TC reel has been removed'));
				exit;
			}else{
				echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
				exit;
			}

		}
	}
	public function deleteReelUpload(){
		// echo "<pre>";print_r($_POST);exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			$condition = "video_cart_upload_id = '".$this->input->post('video_cart_upload_id')."' ";
				/* Unlink previous  imag */
	
			$condition_image =  "video_cart_upload_id = '".$this->input->post('video_cart_upload_id')."' ";
			$image =$this->common->getData("tbl_video_cart_uploads",'*',$condition_image);
			if(is_array($image) && !empty($image[0]->uploaded_document) && file_exists(DOC_ROOT_FRONT."/images/cart_upload/".$image[0]->uploaded_document))
			{
				unlink(DOC_ROOT_FRONT."/images/cart_upload/".$image[0]->uploaded_document);
			}
		
			$cart_TC_Delete = $this->common->deleteRecord("tbl_video_cart_uploads",$condition);
			if(!empty($cart_TC_Delete)){
				echo json_encode(array('success'=>true, 'msg'=>'The upload has been removed'));
				exit;
			}else{
				echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
				exit;
			}

		}
	}


	public function getReelList(){
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			$result = array();
			$condition = "user_id = ".$this->session->userdata('footage_farm_user')[0]['user_id']."  ";
			$main_table = array("tbl_video_cart as vc", array("vc.cart_id,vc.video_cart_detail_status"));
			$join_tables =  array();
			$join_tables = array(
					array("", "tbl_videos as  v", "v.video_id = vc.video_id", array("v.*")),
					array("", "tbl_video_country_location_mapping as  vclm", "vclm.video_id = vc.video_id", array()),
					array("", "tbl_country_location as  vcl", "vcl.country_location_id = vclm.country_location_id", array()),
					array("", "countries as  c", "c.country_id = vcl.country_id", array("c.country_name")),
					);
			$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("vc.cart_id" => "ASC"),"vc.video_id",null); 
			$result['cart_reels'] = $this->common->MySqlFetchRow($rs, "array");
			// echo $this->db->last_query();
			// echo "<pre>";print_r($result);exit;
			$cartListView = $this->load->view('cartListView',$result,true);
			$cartListviewDetail = $this->load->view('detailtablisting',$result,true);

			echo json_encode(array('success'=>true, 'msg'=>'Reel in cart found','cartListview'=>$cartListView,'cartListviewDetail'=>$cartListviewDetail));
			exit;
			
		}

	}

	public function index(){
		$result = array();
		$this->load->view('header');
		$this->load->view('index',$result);
		$this->load->view('footer');

	}

	public function addToFav(){
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			if(!empty($_POST['selected_reels']) && isset($_POST['selected_reels'])){
				foreach ($_POST['selected_reels'] as $key => $value) {
					$data = array();
					$data['video_id'] = $value;
					$data['user_id'] = $this->session->userdata('footage_farm_user')[0]['user_id'];
					$data['created_on'] = date("Y-m-d H:i:s");
					$data['favorite_date'] = date("Y-m-d");
					$data['created_by'] =  $this->session->userdata('footage_farm_user')[0]['user_id'];
					$condition = "user_id = '".$this->session->userdata('footage_farm_user')[0]['user_id']."' AND  video_id = '".$value."' ";
					$check_exist_fav = $this->common->getData("tbl_favourites",'*',$condition);
					if($check_exist_fav){
						$data['updated_on'] = date("Y-m-d H:i:s");
						$data['updated_by'] = $this->session->userdata('footage_farm_user')[0]['user_id'];
						$condition = "favorite_id = '".$check_exist_fav[0]['favorite_id']."' ";
						$result = $this->common->updateData("tbl_favourites",$data,$condition);
					}else{
						$data['created_on'] = date("Y-m-d H:i:s");
						$data['created_by'] =  $this->session->userdata('footage_farm_user')[0]['user_id'];
						$result = $this->common->insertData('tbl_favourites',$data,'1');
					}
				} //end of foreach 
					if(!empty($result)){
						echo json_encode(array('success'=>true, 'msg'=>'Record Added Successfully.'));
						exit;
					}else{
						echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
						exit;
					}
			}else{
				echo json_encode(array('success'=>false, 'msg'=>'Please select the reels first.'));
				exit;
			}
		

		}
	}

	public function finalSubmissionReel(){
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			$condition = "user_id = '".$this->session->userdata('footage_farm_user')[0]['user_id']."' AND  video_cart_detail_status = '1' ";
			$cart_reels = $this->common->getData("tbl_video_cart",'*',$condition);
			
		
			foreach ($cart_reels as $key => $value) {
				
				$condition = "cart_id = '".$value['cart_id']."' AND  video_id  = '".$value['video_id']."' ";
				$cart_reels_details = $this->common->getData("tbl_video_cart_detail",'*',$condition);

				foreach ($cart_reels_details as $keydetail => $valuedetail) {
					$data=array();
					$data['user_id'] =$this->session->userdata('footage_farm_user')[0]['user_id'];
					$data['video_id'] = $valuedetail['video_id'];
					$data['request_type'] = $valuedetail['request_type'];
					$data['format'] = $valuedetail['format'];
					$data['enquiry_date'] = date("Y-m-d");
					$data['frame_rate'] = $valuedetail['frame_rate'];
					$data['message'] = (!empty($valuedetail['message'])?$valuedetail['message']:" ");
					$data['codec'] = $valuedetail['codec'];
					$data['status']= 'Active';
					$data['created_on'] = date("Y-m-d H:i:s");
					$data['created_by'] =  $this->session->userdata('footage_farm_user')[0]['user_id'];
					$result = $this->common->insertData('tbl_video_enquiry_details',$data,'1');

					$array = array(
						'user_id'  => $this->session->userdata('footage_farm_user')[0]['user_id'],
						'video_id' => $valuedetail['video_id'],
						'enquiry_status' => 'New',
						'created_by' => $this->session->userdata('footage_farm_user')[0]['user_id'],
						'created_on' => date("Y-m-d H:i:s"),
						'updated_by' => $this->session->userdata('footage_farm_user')[0]['user_id'],
						'updated_on' => date("Y-m-d H:i:s"),
					);
					$result1 = $this->common->insertData('tbl_enquiry_status_log',$array,'1');

				}
				
				if($result){
					$condition = "cart_id = '".$value['cart_id']."' AND  video_id  = '".$value['video_id']."' ";
					$cart_reels_tc = $this->common->getData("tbl_video_cart_tc",'*',$condition);

					foreach ($cart_reels_tc as $keytc => $valuetc) {
						$data_tc = array();
						$data_tc['user_id'] = $this->session->userdata('footage_farm_user')[0]['user_id'];
						$data_tc['video_id'] = $valuetc['video_id'];
						$data_tc['begin_tc']= $valuetc['begin_tc'];
						$data_tc['end_tc']= $valuetc['end_tc'];
						$data_tc['status']= 'Active';
						$data_tc['created_on'] = date("Y-m-d H:i:s");
						$data_tc['created_by'] =  $this->session->userdata('footage_farm_user')[0]['user_id'];
						$result = $this->common->insertData('tbl_video_enquiry_tc',$data_tc,'1');
					}
				}

				if($result){
					$condition = "cart_id = '".$value['cart_id']."' AND  video_id  = '".$value['video_id']."' ";
					$cart_reels_upload = $this->common->getData("tbl_video_cart_uploads",'*',$condition);
					if($cart_reels_upload){
						foreach ($cart_reels_upload as $keyupload => $valueupload) {
							$data_upload  = array();
							$data_upload['user_id'] = $this->session->userdata('footage_farm_user')[0]['user_id'];
							$data_upload['video_id'] = $valueupload['video_id'];
							$data_upload['uploaded_document'] = $valueupload['uploaded_document'];
							$data_upload['document_type'] = $valueupload['document_type'];
							$data_upload['created_on'] = date("Y-m-d H:i:s");
							$data_upload['created_by'] =  $this->session->userdata('footage_farm_user')[0]['user_id'];
							$result = $this->common->insertData('tbl_video_enquiry_uploads',$data_upload,'1');
						}
					}
					
				}

				$condition = "cart_id = '".$value['cart_id']."' AND  video_id  = '".$value['video_id']."' ";
				$this->common->deleteRecord("tbl_video_cart",$condition);
				$this->common->deleteRecord("tbl_video_cart_detail",$condition);
				$this->common->deleteRecord("tbl_video_cart_tc",$condition);
				$this->common->deleteRecord("tbl_video_cart_uploads",$condition);
			} // end of foreach 
			if(!empty($result)){
				echo json_encode(array('success'=>true, 'msg'=>'Enqury recoreded successfully '));
				exit;
			}else{
				echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
				exit;
			}
		}
	}
	public function getcartReelform(){

		$condition = "user_id = ".$this->session->userdata('footage_farm_user')[0]['user_id']."  ";
		$main_table = array("tbl_video_cart as vc", array("vc.cart_id"));
		$join_tables =  array();
		$join_tables = array(
				array("", "tbl_videos as  v", "v.video_id = vc.video_id", array("v.*")),
				array("", "tbl_video_country_location_mapping as  vclm", "vclm.video_id = vc.video_id", array()),
				array("", "tbl_country_location as  vcl", "vcl.country_location_id = vclm.country_location_id", array()),
				array("", "countries as  c", "c.country_id = vcl.country_id", array("c.country_name")),
				);
		$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("vc.cart_id" => "ASC"),"vc.video_id",null); 
		$check_exist_cart = $this->common->MySqlFetchRow($rs, "array");
				
		//start check that whether the  addded video in cart has field the form  or not 
		if(!empty($_POST['cart_id']) && isset($_POST['cart_id'])){
			$condition = "vc.cart_id ='".$_POST['cart_id']."'  ";
		}else{
			$condition = "vc.cart_id ='".$check_exist_cart[0]['cart_id']."'  ";
		}
		

		$main_table = array("tbl_video_cart as vc", array("vc.cart_id as reel_cart_id"));
		$join_tables =  array();
		$join_tables = array(
				array("", "tbl_videos as  v", "v.video_id = vc.video_id", array("v.video_id as reel_video_id,v.*")),
				array("", "tbl_video_country_location_mapping as  vclm", "vclm.video_id = v.video_id", array()),
				array("", "tbl_country_location as  vcl", "vcl.country_location_id = vclm.country_location_id", array("GROUP_CONCAT( DISTINCT vcl.location_name) as location_name")),
				array("", "countries as  c", "c.country_id = vcl.country_id", array("c.country_name")),
		        array("left", "tbl_video_cart_detail as vcd","vc.cart_id = vcd.cart_id ", array("vcd.*")),
		        array("left", "tbl_video_cart_tc as  vct", "vct.video_id = vcd.video_id", array("GROUP_CONCAT( DISTINCT  vct.begin_tc) as begin_tc,GROUP_CONCAT( DISTINCT vct.end_tc) as end_tc,GROUP_CONCAT( DISTINCT vct.video_cart_tc_id) as video_cart_tc_id ")),
		        array("left", "tbl_video_cart_uploads as  vcu", "vcu.video_id = vcd.video_id", array("GROUP_CONCAT( DISTINCT  vcu.uploaded_document) as uploaded_document,GROUP_CONCAT( DISTINCT vcu.video_cart_upload_id) as video_cart_upload_id,GROUP_CONCAT( DISTINCT vcu.document_type) as document_type"))
		          );
		$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("vc.cart_id" => "ASC"),"vc.cart_id",null); 
		$check_exist_form_reel = $this->common->MySqlFetchRow($rs, "array");
		//end check that whether the  addded video in cart has field the form  or not 
		
		$result['check_exist_form_reel'] = $check_exist_form_reel;

		// echo "<pre>";
		// print_r($check_exist_form_reel);
		// exit;
		$result['reel_cnt'] = count($check_exist_cart);
		$result['reel_position'] = $_POST['reel_position'];  
		
		$condition = "user_id = '".$this->session->userdata('footage_farm_user')[0]['user_id']."' AND  video_cart_detail_status = '1' ";
		$formfilled = $this->common->getData("tbl_video_cart",'count(cart_id) as completed_form',$condition);
		
		// echo  $formfilled[0]['completed_form'];
		// echo $this->db->last_query();
		// exit;
		if($result['reel_cnt'] == $formfilled[0]['completed_form']){
			$next_btn_show = true;
		}else{
			$next_btn_show = false;

		}
		$reel_form_Detail_html = $this->load->view('detailForm',$result,true);
		$reel_form_Review_html = $this->load->view('reviewForm',$result,true);


		echo json_encode(array('success'=>true, 'msg'=>'Record Added Successfully.','form_html_detail'=>$reel_form_Detail_html,'form_html_review'=>$reel_form_Review_html,'next_btn_show'=>$next_btn_show));
		exit;
	

				
	}

	public function addToCart(){
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			if(!empty($_POST['selected_reels']) && isset($_POST['selected_reels'])){
				foreach ($_POST['selected_reels'] as $key => $value) {
					$data = array();
					$data['video_id'] = $value;
					$data['user_id'] = $this->session->userdata('footage_farm_user')[0]['user_id'];
					$data['created_on'] = date("Y-m-d H:i:s");
					$data['video_inquiry_date'] = date("Y-m-d");
					$data['video_cart_detail_complete_status'] = '0';
					$data['created_by'] =  $this->session->userdata('footage_farm_user')[0]['user_id'];
					$condition = "user_id = '".$this->session->userdata('footage_farm_user')[0]['user_id']."' AND  video_id = '".$value."'  ";
					$check_exist_cart = $this->common->getData("tbl_video_cart",'*',$condition);
					if($check_exist_cart){
						$data['updated_on'] = date("Y-m-d H:i:s");
						$data['updated_by'] = $this->session->userdata('footage_farm_user')[0]['user_id'];
						$condition = "cart_id = '".$check_exist_cart[0]['cart_id']."' ";
						$result = $this->common->updateData("tbl_video_cart",$data,$condition);
					}else{
						$data['created_on'] = date("Y-m-d H:i:s");
						$data['created_by'] =  $this->session->userdata('footage_farm_user')[0]['user_id'];
						$result = $this->common->insertData('tbl_video_cart',$data,'1');
					}
				} //end of foreach 
					if(!empty($result)){
						echo json_encode(array('success'=>true, 'msg'=>'Record Added Successfully.'));
						exit;
					}else{
						echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
						exit;
					}
			}else{
				echo json_encode(array('success'=>false, 'msg'=>'Please select the reel first.'));
				exit;
			}
		

		}
	}


	public function submitForm(){
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{	
			$uploaded_files_data = array();
			$uploaded_files_type = array();
			if(isset($_FILES) && isset($_FILES["reelfiles"]["name"])){
				for($i=0;$i<count($_FILES["reelfiles"]["name"]);$i++){
					// print_r($value);		
					$_FILES['reelfile']['name'] = $_FILES['reelfiles']['name'][$i];
					$_FILES['reelfile']['type'] = $_FILES['reelfiles']['type'][$i];
					$_FILES['reelfile']['tmp_name'] = $_FILES['reelfiles']['tmp_name'][$i];
					$_FILES['reelfile']['error'] = $_FILES['reelfiles']['error'][$i];
					$_FILES['reelfile']['size'] = $_FILES['reelfiles']['size'][$i];	

					$config = array();
					$config['upload_path'] = DOC_ROOT_FRONT."/images/cart_upload/";
					$config['max_size']    = '2000';
					$config['allowed_types'] = 'gif|jpg|png|jpeg|pdf|docx|doc|msword|xlsx|xls|csv';
					$this->load->library('upload', $config);
		
					if (!$this->upload->do_upload("reelfile")){
						$image_error = array('error' => $this->upload->display_errors());
						echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
						exit;
					}else{
						$upload_files_data = array('upload_data' => $this->upload->data());
				
						$uploaded_files_data[] = $upload_files_data['upload_data']['file_name']; 
						$uploaded_files_type[] =  $upload_files_data['upload_data']['file_ext'];
					}	
				}
			}
			$data=array();
			// for detail form submission 
			$data['cart_id'] = $this->input->post('cart_id');
			$data['video_id'] = $this->input->post('video_id');
			$data['request_type'] = $this->input->post('reeltype');
			$data['format'] = $this->input->post('format');
			$data['enquiry_date'] = date("Y-m-d");
			$data['frame_rate'] = $this->input->post('frame_rate');
			$data['message'] = (!empty($this->input->post('message'))?$this->input->post('message'):"");
			$data['codec'] = $this->input->post('codec');
			$data['status']= 'Active';


			// for update the reel detail form 
			if(!empty($_POST['video_cart_detail_id']) && isset($_POST['video_cart_detail_id'])){
				$condition = "video_cart_detail_id = '".$this->input->post('video_cart_detail_id')."' ";
				$data['updated_on'] = date("Y-m-d H:i:s");
				$data['updated_by'] = $this->session->userdata('footage_farm_user')[0]['user_id'];
				$result = $this->common->updateData("tbl_video_cart_detail",$data,$condition);

				$data_update_cart = array();
				$data_update_cart['updated_on'] = date("Y-m-d H:i:s");
				$data_update_cart['updated_by'] = $this->session->userdata('footage_farm_user')[0]['user_id'];
				$data_update_cart['video_cart_detail_status'] = '1' ;
				$condition = "cart_id = '".$this->input->post('cart_id')."' ";
				$this->common->updateData("tbl_video_cart",$data_update_cart,$condition);
				
				if($this->input->post('begintc')){
					$condition = "video_id = '".$this->input->post('video_id')."' &&  cart_id ='".$this->input->post('cart_id')."' ";
					$this->common->deleteRecord("tbl_video_cart_tc",$condition);
					foreach ($this->input->post('begintc') as $key => $value) {
						$data_tc = array();
						$data_tc['cart_id'] = $this->input->post('cart_id');
						$data_tc['video_id'] = $this->input->post('video_id');
						$data_tc['begin_tc']= $value;
						$data_tc['end_tc']= $this->input->post('endtc')[$key];
						$data_tc['status']= 'Active';
						$data_tc['created_on'] = date("Y-m-d H:i:s");
						$data_tc['created_by'] =  $this->session->userdata('footage_farm_user')[0]['user_id'];
						$result = $this->common->insertData('tbl_video_cart_tc',$data_tc,'1');
					}
				}

				if($uploaded_files_data){
					// $condition = "video_id = '".$this->input->post('video_id')."' &&  cart_id ='".$this->input->post('cart_id')."' ";
					// $this->common->deleteRecord("tbl_video_cart_uploads",$condition);
					foreach ($uploaded_files_data as $key => $value) {
						$data_upload  = array();
						$data_upload['cart_id'] = $this->input->post('cart_id');
						$data_upload['video_id'] = $this->input->post('video_id');
						$data_upload['uploaded_document'] = $value;
						$data_upload['document_type'] = $uploaded_files_type[$key];
						$data_upload['created_on'] = date("Y-m-d H:i:s");
						$data_upload['created_by'] =  $this->session->userdata('footage_farm_user')[0]['user_id'];
						$result = $this->common->insertData('tbl_video_cart_uploads',$data_upload,'1');
					}
				}

				if($result){
					echo json_encode(array('success'=>true, 'msg'=>'Record updated Successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
					exit;
				}

			}else{
				// for insert the reel detail form 
				$data['created_on'] = date("Y-m-d H:i:s");
				$data['created_by'] =  $this->session->userdata('footage_farm_user')[0]['user_id'];
				$result = $this->common->insertData('tbl_video_cart_detail',$data,'1');
				$data_update_cart = array();
				$data_update_cart['updated_on'] = date("Y-m-d H:i:s");
				$data_update_cart['updated_by'] = $this->session->userdata('footage_farm_user')[0]['user_id'];
				$data_update_cart['video_cart_detail_status'] = '1' ;
				$condition = "cart_id = '".$this->input->post('cart_id')."' ";
				$this->common->updateData("tbl_video_cart",$data_update_cart,$condition);

				if($this->input->post('begintc')){
					foreach ($this->input->post('begintc') as $key => $value) {
						$data_tc = array();
						$data_tc['cart_id'] = $this->input->post('cart_id');
						$data_tc['video_id'] = $this->input->post('video_id');
						$data_tc['begin_tc']= $value;
						$data_tc['end_tc']= $this->input->post('endtc')[$key];
						$data_tc['status']= 'Active';
						$data_tc['created_on'] = date("Y-m-d H:i:s");
						$data_tc['created_by'] =  $this->session->userdata('footage_farm_user')[0]['user_id'];
						$result = $this->common->insertData('tbl_video_cart_tc',$data_tc,'1');
					}
				}

				if($result && $uploaded_files_data){
					foreach ($uploaded_files_data as $key => $value) {
						$data_upload  = array();
						$data_upload['cart_id'] = $this->input->post('cart_id');
						$data_upload['video_id'] = $this->input->post('video_id');
						$data_upload['uploaded_document'] = $value;
						$data_upload['document_type'] = $uploaded_files_type[$key];
						$data_upload['created_on'] = date("Y-m-d H:i:s");
						$data_upload['created_by'] =  $this->session->userdata('footage_farm_user')[0]['user_id'];
						$result = $this->common->insertData('tbl_video_cart_uploads',$data_upload,'1');
					}
				}

				if($result){
					echo json_encode(array('success'=>true, 'msg'=>'Reel recorded  Successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
					exit;
				}
			}
			
		}
	}	
}?>