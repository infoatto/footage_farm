<!-- Innerpage Search -->
<section class="page-container innerpage-top">
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="searchbar-top">
		<div class="home-search-wrapper">
			<form class="home-search" action="">
				<input type="text" placeholder="Search for footages and themes" name="home-search">
				<button type="submit"><img src="<?= base_url()?><?= base_url()?>images/search-icon.svg" alt=""></button>
			</form>
		</div>
	</div>
</section>
<!-- Innerpage Search -->
<section class="page-container">
	<!-- Mobile Search -->
	<div class="multicolored-line multicolored-line-mobile">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="home-search-wrapper search-wrapper-mobile">
		<form class="home-search" action="">
			<input type="text" placeholder="Search for footages and themes" name="home-search">
			<button type="submit"><img src="<?= base_url()?><?= base_url()?>images/search-icon.svg" alt=""></button>
		</form>
	</div>
	<!-- Mobile Search -->
	<!-- Innerpage Banner -->
	<section class="page-section mb-0 cart-banner">
		<div class="innerpage-banner innerpage-banner-transparent pt-50 pb-25">
			<div class="innerpage-banner-grey-bg full-height"></div>
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="cart-progress">
							<a href="#/" class="cart-tab current"><span>Cart</span></a><a href="#/" class="details-tab"><span>Specify Details</span></a><a href="#/" class="review-tab"><span>Review</span></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Innerpage Banner -->
	<!-- Cart Tab Content -->
	<section class="page-section pt-40" id="cart-tab-content">
		<div class="container">
			<div class="row">
				<div class="col-12" id="reel-list-cart">

				</div>
			</div>
		</div>
		<div class="cart-form-actions ">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<a href="index.php" class="btn-ff btn-secondary-dark text-uppercase">Continue Exploring</a>
						<input type="submit" id="cart-tab-next" class="btn-ff btn-primary-dark text-uppercase" value="Next">
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Cart Tab Content -->
		<!-- Details Tab Content -->
		<section class="page-section pt-40" id="details-tab-content" >
		<div class="container">
			<div class="row">
				<div class="col-12 col-sm-5 reel-list" id="detail-tab-listing">

				</div>
				<div class="col-12 col-sm-7 reel-form no-padding-mobile">
					<div class="details-enquiry-wrapper" id="cart-enquiry-wrapper">
			
					</div>
				</div>
			</div>
		</div>
		<div class="cart-form-actions cfad-desktop">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<a href="index.php" class="btn-ff btn-secondary-dark text-uppercase">Continue Exploring</a>
						<input type="submit" id="details-tab-next" class="btn-ff btn-primary-dark text-uppercase" value="Next">
					</div>
				</div>
			</div>
		</div>
		<div class="cart-form-actions cfad-mobile">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<input type="submit" id="details-tab-next-mobile" class="btn-ff btn-primary-dark text-uppercase" value="Save and Go Next">
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Details Tab Content -->
	
	<!-- Review Tab Content -->
	<section class="page-section pt-40" id="review-tab-content">
		<div class="container">
			<div class="row">
				<div class="col-12 col-sm-5 reel-list">
					<h3 class="details-title">4 Reels</h3>
					<div class="review-content-wrapper">
						<div class="cart-list-single details-list-single active-selected">
							<div class="cls-image">
								<img src="https://via.placeholder.com/377x212.png" alt="">
							</div>
							<div class="cls-info">
								<h4>Know Your Enemy - Japan (1945) R3/6 Know Your Enemy - Japan (1945) R3/6 Know Your Enemy - Japan (1945) R3/6</h4>
								<div class="block-info">
									<div>
										<p class="block-info-title">Reel Number</p>
										<p class="block-info-content">376574635</p>
									</div>
									<a href="#/" class="cls-remove">Remove</a>
								</div>
							</div>
						</div>
						<div class="cart-list-single details-list-single">
							<div class="cls-image">
								<img src="https://via.placeholder.com/377x212.png" alt="">
							</div>
							<div class="cls-info">
								<h4>Know Your Enemy - Japan (1945) R3/6 Know Your Enemy - Japan (1945) R3/6 Know Your Enemy - Japan (1945) R3/6</h4>
								<div class="block-info">
									<div>
										<p class="block-info-title">Reel Number</p>
										<p class="block-info-content">376574635</p>
									</div>
									<a href="#/" class="cls-remove">Remove</a>
								</div>
							</div>
						</div>
						<div class="cart-list-single details-list-single">
							<div class="cls-image">
								<img src="https://via.placeholder.com/377x212.png" alt="">
							</div>
							<div class="cls-info">
								<h4>Know Your Enemy - Japan (1945) R3/6 Know Your Enemy - Japan (1945) R3/6 Know Your Enemy - Japan (1945) R3/6</h4>
								<div class="block-info">
									<div>
										<p class="block-info-title">Reel Number</p>
										<p class="block-info-content">376574635</p>
									</div>
									<a href="#/" class="cls-remove">Remove</a>
								</div>
							</div>
						</div>
						<div class="cart-list-single details-list-single">
							<div class="cls-image">
								<img src="https://via.placeholder.com/377x212.png" alt="">
							</div>
							<div class="cls-info">
								<h4>Know Your Enemy - Japan (1945) R3/6 Know Your Enemy - Japan (1945) R3/6 Know Your Enemy - Japan (1945) R3/6</h4>
								<div class="block-info">
									<div>
										<p class="block-info-title">Reel Number</p>
										<p class="block-info-content">376574635</p>
									</div>
									<a href="#/" class="cls-remove">Remove</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 col-sm-7 reel-form no-padding-mobile">
					<div class="details-enqiry-wrapper" id="cart-review-wrapper">
						<div class="details-enquiry-title">	
							<a href="#/" class="mobile-enquiry-nav men-back">
								<img src="images/cart-back.svg" alt="">
							</a>		
							<h3 class="details-title">Reel 1 of 4</h3>
							<a href="#/" class="mobile-enquiry-nav men-next">
								<img src="images/cart-forward.svg" alt="">
							</a>	
							<a href="#/" class="close-details-enquiry"><img src="images/close.svg" alt=""></a>
							<span class="saved-msg">Saved a few seconds ago</span>
						</div>
						<form class="enquiry-master-form details-enquiry-form review-enquiry-form">
							<div class="review-wrapper">
								<div class="cart-list-single cart-list-single-mobile">
									<div class="cls-image">
										<img src="https://via.placeholder.com/377x212.png" alt="">
									</div>
									<div class="cls-info">
										<h4>Know Your Enemy - Japan (1945) R3/6</h4>
										<div class="block-info">
											<div>
												<p class="block-info-title">Reel Number</p>
												<p class="block-info-content">376574635</p>
											</div>
										</div>
										<a href="#/" class="cls-remove">Remove</a>
									</div>
								</div>
								<div class="form-group">
									<div class="reel-title">
										<img src="https://via.placeholder.com/147x83.png" alt="">
										<h3>1910 - 1918 - WWI Era: Aerials; German Officers & Troops; France Fighting; Trans-Caucasia Winter</h3>
									</div>
								</div>
								<div class="form-group">
									<div class="request-reel-type">
										<label><span>Request Type:</span> Preview</label>
									</div>
								</div>
								<div class="form-group">
									<div class="mb-10">
										<label><span>Begin TC</span>: 23:35:45:60</label>
									</div>
									<div class="mb-10">
										<label><span>End TC</span>: 23:35:45:60</label>
									</div>
									<div class="mb-10">
										<label><span>In Out Time Codes</span></label>
										<div class="uploaded-files-list mt-10 mb-10">
											<div class="file-single">
												<img src="images/jpg.svg" alt="" class="file-type-icon">
												<span>Screenshota_qe52gknknf.jpg</span>
											</div>
											<div class="file-single">
												<img src="images/doc.svg" alt="" class="file-type-icon">
												<span>datasheet-with-timecodes.doc</span>
											</div>
											<div class="file-single">
												<img src="images/xls.svg" alt="" class="file-type-icon">
												<span>animate-sys.xls</span>
											</div>
										</div>
									</div>
									<div class="mb-10">
										<label><span>Format</span>: Format Type</label>
									</div>
									<div class="mb-10">
										<label><span>Frame Rate</span>: Frame Rate Speed</label>
									</div>
									<div class="mb-10">
										<label><span>Codec</span>: QT ProRes422HQ</label>
									</div>
									<div class="mb-10">
										<label><span>Message</span>: Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</label>
									</div>
								</div>
							</div>
							<div class="edit-details">
								<a href="#/" class="btn-ff btn-ff-icon btn-secondary-dark text-uppercase request-reel-btn review-cart-edit">Edit</a>
							</div>
						</form>
						<form class="enquiry-master-form details-enquiry-form edit-enquiry-form">
							<div class="save-detail-cart-wrapper edit-detail-cart-wrapper">
								<input type="submit" class="btn-ff btn-primary-dark text-uppercase save-detail-cart save-review-cart" value="Save">	
							</div>													
							<div class="cart-list-single cart-list-single-mobile">
								<div class="cls-image">
									<img src="https://via.placeholder.com/377x212.png" alt="">
								</div>
								<div class="cls-info">
									<h4>Know Your Enemy - Japan (1945) R3/6</h4>
									<div class="block-info">
										<div>
											<p class="block-info-title">Reel Number</p>
											<p class="block-info-content">376574635</p>
										</div>
									</div>
									<a href="#/" class="cls-remove">Remove</a>
								</div>
							</div>
							<div class="form-group">
								<div class="reel-title">
									<img src="https://via.placeholder.com/147x83.png" alt="">
									<h3>1910 - 1918 - WWI Era: Aerials; German Officers & Troops; France Fighting; Trans-Caucasia Winter</h3>
								</div>
							</div>
							<div class="form-group">
								<div class="request-reel-type">
									<label for="selectreeltype">Please select the reel type</label>
									<label class="radio-container-theme">
										<span class="radio-label">Request Preview</span>
										<input type="radio" name="reeltype" required>
										<span class="radio"></span>
									</label>
									<label class="radio-container-theme">
										<span class="radio-label">Request Master</span>
										<input type="radio" name="reeltype">
										<span class="radio"></span>
									</label>
								</div>
								<label for="reeltype" class="error"></label>
							</div>
							<div class="form-group">
								<label for="inouttime">Specify the footage's in and out time</label>
								<div class="begin-end-tc">
									<div class="tc-time begin-tc">
										<label for="begintc">Begin TC
											<a href="#" class="tooltip-info">
												<img src="images/info.svg" alt="">
												<span class="tooltip-msg">
													Footage start time
												</span>
											</a>
										</label>
										<input type="text" class="form-control ff-field-light" id="begintc" name="begintc" placeholder="00:00:00" required>
									</div>
									<div class="tc-time end-tc">
										<label for="endtc">End TC
											<a href="#" class="tooltip-info">
												<img src="images/info.svg" alt="">
												<span class="tooltip-msg">
													Footage end time
												</span>
											</a>
										</label>
										<input type="text" class="form-control ff-field-light" id="endtc" name="endtc" placeholder="00:00:00" required>
									</div>
								</div>
								<div id="addmore-begin-end-tc"></div>
								<div class="form-addmore">
									<a href="#/" class="form-addmore-link tc-addmore-link"><img src="images/add-black.svg" alt=""> <span>Add another in and out time codes</span></a>
								</div>
							</div>
							<div class="form-group">
								<label>Do you have a file with multiple in and out time codes?</label>
								<p>Add jpegs, excel or word files here. Each file size should be under 2MB.</p>
								<div class="uploaded-files-list">
									<div class="file-single">
										<img src="images/jpg.svg" alt="" class="file-type-icon">
										<span>Screenshota_qe52gknknf.jpg</span>
										<a href="#/"><img src="images/close.svg" alt="" class="file-remove-icon"></a>
									</div>
									<div class="file-single">
										<img src="images/doc.svg" alt="" class="file-type-icon">
										<span>datasheet-with-timecodes.doc</span>
										<a href="#/"><img src="images/close.svg" alt="" class="file-remove-icon"></a>
									</div>
									<div class="file-single">
										<img src="images/xls.svg" alt="" class="file-type-icon">
										<span>animate-sys.xls</span>
										<a href="#/"><img src="images/close.svg" alt="" class="file-remove-icon"></a>
									</div>
								</div>
								<!-- Created using dropzone.js-->
								<div id="upload-reel-times">
									<img src="images/upload-file.svg" alt="">
									<p>Drag and drop here or browse</p>
									<a href="#/" class="login-link">Browse</a>
								</div>
								<!-- Created using dropzone.js-->
							</div>
							<div class="form-group">
								<div class="format-framerate">
									<div class="format formatframe">
										<label for="begintc">Format</label>
										<select class="niceselect ff-select-light formatselect" required>
											<option value="">Placeholder text</option>
											<option value="test">Placeholder text</option>
											<option value="test">Placeholder text</option>
											<option value="test">Placeholder text</option>
											
										</select>
										<label id="formatselect-error" class="error" for=""></label>
									</div>
									<div class="framerate formatframe">
										<label for="endtc">Frame Rate</label>
										<select class="niceselect ff-select-light framerateselect" required>
											<option value="">Placeholder text</option>
											<option value="test">Placeholder text</option>
											<option value="test">Placeholder text</option>
											
										</select>
										<label id="framerateselect-error" class="error" for=""></label>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="codec">Codec
									<a href="#" class="tooltip-info">
										<img src="images/info.svg" alt="">
										<span class="tooltip-msg">
											Codec type
										</span>
									</a>
								</label>
								<input type="text" class="form-control ff-field-light" id="codec" name="codec" placeholder="QT ProRes422HQ" required>
							</div>
							<div class="form-group">
								<label for="projectdesc">Message (optional)</label>
								<textarea class="form-control ff-field-light" rows="5"></textarea>
							</div>
							<div class="save-detail-cart-wrapper edit-detail-cart-wrapper">
								<input type="submit" class="btn-ff btn-primary-dark text-uppercase save-detail-cart save-review-cart" value="Save">	
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="cart-form-actions ">
			<div class="container">
				<div class="row">
					<div class="col-12">	
						<a href="#/" class="btn-ff btn-ff-icon btn-secondary-dark text-uppercase request-reel-btn review-cart-edit review-cart-edit-mobile">Edit</a>					
						<input type="submit" id="review-tab-next" class="btn-ff btn-primary-dark text-uppercase" value="Send Request">
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Review Tab Content -->
	<!-- Enquiry Sent Message -->
	<section class="page-section pt-40" id="enquiry-sent-content">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="enquiry-sent-wrapper">
						<img src="<?= base_url()?><?= base_url()?>images/success.svg" alt="">
						<p class="enquiry-success">Enquiries Successfully Sent!</p>
						<p class="enquiry-success-note">Thank you for sending the request. We will reach you shortly to proceed further</p>
						<a class="btn-ff btn-primary-dark text-uppercase" href="index.php">Go Back to Homepage</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Enquiry Sent Message -->
	<!-- Notification Modal -->
	<div class="notification-message">
		<div class="notification-overlay"></div>
		<div class="notification-modal">
			<div class="modal-message">
				<img src="<?= base_url()?><?= base_url()?>images/completed.svg">
				<p class="notification-modal-text"></p>
			</div>
		</div>									
	</div>
	<!-- Notification Modal -->
</section>
<script>
getCartReel();
getcartReelform(1);
$(document).ready(function() {
	$('.cart-tab').click(function(){
		if($(this).hasClass('done')){
			$('.cart-progress a').removeClass('current').removeClass('done');
			$(this).removeClass('done').addClass('current');	
			$('#cart-tab-content').show();	
			$('#details-tab-content,#review-tab-content').hide();		
		}
	});
	$('.details-tab').click(function(){
		if($(this).hasClass('done')){
			$('.cart-progress a').removeClass('current');
			$(this).removeClass('done').addClass('current');	
			$('#details-tab-content').show();	
			$('#cart-tab-content,#review-tab-content').hide();		
		}
	});
	$('#cart-tab-next').click(function() {
		$('.cart-tab').removeClass('current').addClass('done');
		$('.details-tab').addClass('current');
		$('#cart-tab-content').hide();
		$('#details-tab-content').show();
	})
	$('#details-tab-next, #details-tab-next-mobile').click(function() {
		$('.details-tab').removeClass('current').addClass('done');
		$('.review-tab').addClass('current');
		$('#details-tab-content').hide();
		$('#review-tab-content').show();
		$('#review-tab-content .details-list-single').removeClass('active');
	});
	/* $('#details-tab-next-mobile').click(function() {			
		$('#details-tab-content, .cart-banner').hide();
		$('#enquiry-sent-content').show();
	});	 */
	$('#review-tab-next').click(function() {			
		$('#review-tab-content, .cart-banner').hide();
		$('#enquiry-sent-content').show();
	});
	/*Addmore Start/ End TC*/
	// $('.tc-addmore-link').click(function() {
	// 	alert("inside");
	// 	$('#addmore-begin-end-tc').append(
	// 		"<div class='begin-end-tc'>" +
	// 		"<div class='tc-time begin-tc'>" +
	// 		"<label for='begintc'>Begin TC" +
	// 		"</label>" +
	// 		"<input type='text' class='form-control ff-field-light' id='begintc' name='begintc' placeholder='00:00:00'>" +
	// 		"</div>" +
	// 		"<div class='tc-time end-tc'>" +
	// 		"<label for='endtc'>End TC" +
	// 		"</label>" +
	// 		"<input type='text' class='form-control ff-field-light' id='endtc' name='endtc' placeholder='00:00:00'>" +
	// 		"<a href='#/' class='remove-tc'><img src='images/close.svg'/></a>" +
	// 		"</div>	" +
	// 		"</div>"
	// 	)
	// });
	// $('body').on('click', '.remove-tc', function() {
	// 	$(this).closest('.begin-end-tc').remove();
	// });
	/*Addmore Start/ End TC*/
	// $('#upload-reel-times').dropzone({
	// 	url: "/file/post"
	// });
	$('.enquiry-master-form').validate({
		ignore: []
	});
	$('.details-content-wrapper .details-list-single').click(function() {
		$('.details-content-wrapper').children('.details-list-single').removeClass('active-detail');
		$(this).addClass('active-detail');
	});		
	$('.review-content-wrapper .details-list-single').click(function() {
		$('.review-content-wrapper').children('.details-list-single').removeClass('active-selected');
		$(this).addClass('active-selected');
	});
	// /*Remove cart item*/
	// $('.cls-remove').click(function(){
	// 	$(this).closest('.cart-list-single').remove();
	// 	$('.notification-message').fadeIn();		
	// 	$('.notification-modal-text').text('The reel has been removed')
	// 	setTimeout(function(){ $('.notification-message').fadeOut();	 }, 2000);
	// })

	// /*Remove cart item*/
});

// to get the added reel in cart 
function getCartReel(){
	var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
	var csrfHash = $('.txt_csrfname').val(); // CSRF hash
	$.ajax({
		url: "<?= base_url('cart/getReelList')?>",
		type: "POST",
		data:{ [csrfName]: csrfHash },
		dataType: "json",
		success: function(response){
			if(response.success){
				$('#reel-list-cart').html(response.cartListview);
				$('#detail-tab-listing').html(response.cartListviewDetail);

			}
		}
	});
}
// to get the added reel wise form data 
function getcartReelform(reel_position,cart_id=null){
	var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
	var csrfHash = $('.txt_csrfname').val(); // CSRF hash
	
	
	$.ajax({
		url: "<?= base_url('cart/getcartReelform')?>",
		type: "POST",
		data:{reel_position,cart_id,[csrfName]: csrfHash },
		dataType: "json",
		success: function(response){
			if(response.success){
				$('#cart-enquiry-wrapper').html(response.form_html_detail);
			}
		}
	});
}

// remove  cart reel  code of shiv  22-9-2020 
function deleteReel(cart_id){
	if(cart_id){
		var r = confirm("Are you want to sure delete this reel from cart");
			if (r == true) {
				var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
				var csrfHash = $('.txt_csrfname').val(); // CSRF hash
				$.ajax({
					url: "<?= base_url('cart/deleteReel')?>",
					type: "POST",
					data:{ cart_id , [csrfName]: csrfHash },
					dataType: "json",
					success: function(response){
						if(response.success){
							$('.notification-message').fadeIn();		
							$('.notification-modal-text').text(response.msg)
							setTimeout(function(){
								getCartReel();
								getcartReelform(1)
								$(this).closest('.cart-list-single').remove();
								$('.notification-message').fadeOut();	 
								}, 2000);	
						}else{
							$('.notification-message').fadeIn();		
							$('.notification-modal-text').text(response.msg)
							setTimeout(function(){
								$('.notification-message').fadeOut();	 
								}, 2000);	
						}
					}
				});
			} else {
				$('.notification-message').fadeIn();		
				$('.notification-modal-text').text('The reel safe in you cart')
				setTimeout(function(){ $('.notification-message').fadeOut();	 }, 2000);
			}
	}else{

	}

}

//remove tc  reel from form details
function removeTc(video_cart_tc_id){
	if(video_cart_tc_id){
		var r = confirm("Are you want to sure delete this TC from cart");
			if (r == true) {
				var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
				var csrfHash = $('.txt_csrfname').val(); // CSRF hash
				var reel_position = $('#reel_position').val();
				var cart_id = $('#cart_id').val();
				$.ajax({
					url: "<?= base_url('cart/deleteReelTC')?>",
					type: "POST",
					data:{ video_cart_tc_id , [csrfName]: csrfHash },
					dataType: "json",
					success: function(response){
						if(response.success){
							$('.notification-message').fadeIn();		
							$('.notification-modal-text').text(response.msg)
							setTimeout(function(){
								getCartReel();
								getcartReelform(reel_position,cart_id);
								$(this).closest('.cart-list-single').remove();
								$('.notification-message').fadeOut();	 
								}, 2000);	
						}else{
							$('.notification-message').fadeIn();		
							$('.notification-modal-text').text(response.msg)
							setTimeout(function(){
								$('.notification-message').fadeOut();	 
								}, 2000);	
						}
					}
				});
			} else {
				$('.notification-message').fadeIn();		
				$('.notification-modal-text').text('The reel safe in you cart')
				setTimeout(function(){ $('.notification-message').fadeOut();	 }, 2000);
			}
	}else{

	}

}
//remove upload file  from form details
function removeupload(video_cart_upload_id){
	if(video_cart_upload_id){
		var r = confirm("Are you want to sure delete this Upload from cart");
			if (r == true) {
				var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
				var csrfHash = $('.txt_csrfname').val(); // CSRF hash
				var reel_position = $('#reel_position').val();
				var cart_id = $('#cart_id').val();
				$.ajax({
					url: "<?= base_url('cart/deleteReelUpload')?>",
					type: "POST",
					data:{ video_cart_upload_id , [csrfName]: csrfHash },
					dataType: "json",
					success: function(response){
						if(response.success){
							$('.notification-message').fadeIn();		
							$('.notification-modal-text').text(response.msg)
							setTimeout(function(){
								getCartReel();
								getcartReelform(reel_position,cart_id);
								$(this).closest('.cart-list-single').remove();
								$('.notification-message').fadeOut();	 
								}, 2000);	
						}else{
							$('.notification-message').fadeIn();		
							$('.notification-modal-text').text(response.msg)
							setTimeout(function(){
								$('.notification-message').fadeOut();	 
								}, 2000);	
						}
					}
				});
			} else {
				$('.notification-message').fadeIn();		
				$('.notification-modal-text').text('The reel safe in you cart')
				setTimeout(function(){ $('.notification-message').fadeOut();	 }, 2000);
			}
	}else{

	}

}



</script>
</body>

</html>