
<h3 class="details-title"> <?= count($cart_reels)?>  Reels</h3>
<div class="details-content-wrapper">
    <?php if(!empty($cart_reels)){
        $i=0;
        foreach ($cart_reels as $key => $value) {
            // print_r($value);
            $i+=1;
            ?>
            <div class="cart-list-single details-list-single  <?= (($key == 0)?"active-detail":"")?>" position ="<?=$i?>" cart_id="<?= $value['cart_id']?>" onclick="getcartReelform(<?=$i?>,<?= $value['cart_id']?>)" >
                <div class="cls-image">
                    <!-- <img src="https://via.placeholder.com/377x212.png" alt=""> -->
                    <img src="<?= base_url('images/video_image/'.$value['thumbnail_image'])?>" alt="">																				
                </div>
                <div class="cls-info">
                    <h4><?=$value['video_title'] ?> - <?= $value['country_name']?> (<?= $value['year']?>) R<?= $i?>/<?=count($cart_reels)?></h4>
                    <div class="block-info">
                        <div>
                            <p class="block-info-title">Reel Number</p>
                            <p class="block-info-content"><?= $value['reel_number']?></p>
                        </div>
                        <a href="Javascript:void(0)" class="cls-remove" onclick="deleteReel(<?=$value['cart_id']?>)">Remove</a>
                    </div>
                </div>
                  <img src="<?= base_url()?>images/completed.svg" id="success_flag<?= $value['cart_id'] ?>" alt=""  style="display:<?=($value['video_cart_detail_status'] == 1)?"block":"none"?>" class="detail-complete">
            </div>
        <?php } 
        } ?>

</div>
