<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Reels extends MX_Controller
{
	
	public function __construct(){
		parent::__construct();
		//checklogin();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('reelsmodel','',TRUE);
	}

	// public function _remap($method){
	//   switch ($method) {
	//      case 'addToFav' : 
	//       $this->addToFav();// call about_me() function.
	//      break;
	//      default :
	//       $this->index();
	//      break;
	// 	}
	// }

	function _remap($method_name){
		if(!method_exists($this, $method_name)){
			$this->index();
		}else{
			$this->{$method_name}();
		}
	}

	public function index(){
		$id = $this->uri->segment(2);
		$result = array();

		$condition = "sb.sub_theme_id = $id ";
		$main_table = array("tbl_videos as t", array("t.*"));
		$join_tables =  array();
		$join_tables = array(
		          array("", "tbl_video_theme_subtheme_mapping as  sb", "t.video_id = sb.video_id", array("sb.sub_theme_id")),
		          );

		$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("t.video_id" => "DESC"),"",null); 
		$featured_theme_data = $this->common->MySqlFetchRow($rs, "array");

		foreach ($featured_theme_data as $key => $value) {
		$condition = "vt.video_id = '".$value['video_id']."' ";
		$main_table = array("tbl_video_tag_mapping as vt", array());
		$join_tables =  array();
		$join_tables = array(
				  array("", "tbl_tags as  tg", "tg.tag_id = vt.tag_id", array("tg.tag_id, tg.tag_name")),
		          );

		$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array(),"",null); 
		$tag = $this->common->MySqlFetchRow($rs, "array");
		$featured_theme_data[$key]['tag'] = $tag;
		}

	
		$where = array('sub_theme_id' => $id);
		$result['subtheme'] = $this->common->getData("tbl_sub_themes",'sub_theme_id, theme_id, sub_theme_name',$where);

		$theme_id = $result['subtheme'][0]['theme_id'];
		$condition = "sub_theme_id != $id AND theme_id = $theme_id";
		$sub_themes = $this->common->getData("tbl_sub_themes",'*',$condition);
		if (!empty($sub_themes)) {
			foreach ($sub_themes as $key => $value) {
			$condition = "1=1 AND sub_theme_id = ".$value['sub_theme_id'];
			$total_reel = $this->common->getData("tbl_video_theme_subtheme_mapping",'count(video_id) as total_reel',$condition,"","","video_id");
			$sub_themes[$key]['total_reals'] =count($total_reel);
		}
			
		}
		
		$result['reel_data']  = $featured_theme_data;
		$result['reel_total']  = count($featured_theme_data);
		$result['similar_subtheme']  = $sub_themes;
						
		//echo "<pre>";print_r($result);exit;

		$this->load->view('header');
		$this->load->view('index',$result);
		$this->load->view('footer');

	}

	public function addToFav(){

		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			if(!empty($_POST['selected_reels']) && isset($_POST['selected_reels'])){
				foreach ($_POST['selected_reels'] as $key => $value) {
					$data = array();
					$data['video_id'] = $value;
					$data['user_id'] = $this->session->userdata('footage_farm_user')[0]['user_id'];
					$data['created_on'] = date("Y-m-d H:i:s");
					$data['favorite_date'] = date("Y-m-d");
					$data['created_by'] =  $this->session->userdata('footage_farm_user')[0]['user_id'];
					$condition = "user_id = '".$this->session->userdata('footage_farm_user')[0]['user_id']."' AND  video_id = '".$value."' ";
					$check_exist_fav = $this->common->getData("tbl_favourites",'*',$condition);
					if($check_exist_fav){
						$data['updated_on'] = date("Y-m-d H:i:s");
						$data['updated_by'] = $this->session->userdata('footage_farm_user')[0]['user_id'];
						$condition = "favorite_id = '".$check_exist_fav[0]['favorite_id']."' ";
						$result = $this->common->updateData("tbl_favourites",$data,$condition);
					}else{
						$data['created_on'] = date("Y-m-d H:i:s");
						$data['created_by'] =  $this->session->userdata('footage_farm_user')[0]['user_id'];
						$result = $this->common->insertData('tbl_favourites',$data,'1');
					}
				} //end of foreach 
					if(!empty($result)){
						echo json_encode(array('success'=>true, 'msg'=>'Record Added Successfully.'));
						exit;
					}else{
						echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
						exit;
					}
			}else{
				echo json_encode(array('success'=>false, 'msg'=>'Please select the reels first.'));
				exit;
			}
		

		}
	}

	public function addToCart(){
		// print_r($_POST['selected_reels']);
		// exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			if(!empty($_POST['selected_reels']) && isset($_POST['selected_reels'])){
				foreach ($_POST['selected_reels'] as $key => $value) {
					$data = array();
					$data['video_id'] = $value;
					$data['user_id'] = $this->session->userdata('footage_farm_user')[0]['user_id'];
					$data['created_on'] = date("Y-m-d H:i:s");
					$data['video_inquiry_date'] = date("Y-m-d");
					$data['created_by'] =  $this->session->userdata('footage_farm_user')[0]['user_id'];
					// $condition = "user_id = '".$this->session->userdata('footage_farm_user')[0]['user_id']."' AND  video_id = '".$value."' ";
					$condition = "user_id = '".$this->session->userdata('footage_farm_user')[0]['user_id']."' AND  video_id = '".$value."' ";
					$check_exist_cart = $this->common->getData("tbl_video_cart",'*',$condition);
					if($check_exist_cart){
						$data['updated_on'] = date("Y-m-d H:i:s");
						$data['updated_by'] = $this->session->userdata('footage_farm_user')[0]['user_id'];
						$condition = "cart_id = '".$check_exist_cart[0]['cart_id']."' ";
						$result = $this->common->updateData("tbl_video_cart",$data,$condition);
					}else{
						$data['created_on'] = date("Y-m-d H:i:s");
						$data['created_by'] =  $this->session->userdata('footage_farm_user')[0]['user_id'];
						$result = $this->common->insertData('tbl_video_cart',$data,'1');
					}
				} //end of foreach 
					if(!empty($result)){
						echo json_encode(array('success'=>true, 'msg'=>'Record Added Successfully.'));
						exit;
					}else{
						echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
						exit;
					}
			}else{
				echo json_encode(array('success'=>false, 'msg'=>'Please select the reel first.'));
				exit;
			}
		

		}
	}

}
?>