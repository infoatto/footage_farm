<!-- Selected Reels Header -->
<section class="page-container selected-items-wrapper">
	<section class="page-section">
		<div class="black-bg"></div>
		<div class="container">
			<div class="col-sm-12">
				<div class="selected-items">
					<div class="selected-items-left">
						<div class="selected-items-count">
							<span id="count-checked-checkboxes">0</span> Themes Selected
						</div>
						<a href="#/" class="select-all">Select All</a>
						<a href="#/" class="deselect-all">Deselect All</a>
					</div>
					<div class="selected-items-right">
						<?php if(!$this->session->userdata('footage_farm_user')) { ?>
							<a href="#/" class="addtofavourite-reel" data-fancybox="" data-src="#signin-content" data-options="{&quot;touch&quot; : false}">
						<?php }else{?>
							<a href="Javascript:void(0)" class="addtofavourite-reel" onclick="addToFav('multiple')">
						<?php }?>
							<span>Add to Favourite</span>
							<img src="<?= base_url()?>images/favourite-white.svg" alt="">
						</a>
						<?php if($this->session->userdata('footage_farm_user')) { ?>
						<a href="#/" data-fancybox data-src="#selectprojects-content" data-options='{"touch" : false}' class="addtoproject-reel">
						<?php }else{?>
							<a href="#/" class="addtofavourite-reel" data-fancybox="" data-src="#signin-content" data-options="{&quot;touch&quot; : false}">
						<?php } ?>
							<span>Add to Project</span>
							<img src="<?= base_url()?>images/project-white.svg" alt="">
						</a>
						<a href="#/" class="selectedheader-more">
							<img src="<?= base_url()?>images/more-icon.svg" alt="">
							<div class="dropdown-menu loggedin-flyout selectedheader-flyout">
								<a class="dropdown-item select-all" href="#">Select All</a>
								<a class="dropdown-item deselect-all" href="#">Deselect All</a>
								<?php if($this->session->userdata('footage_farm_user')) {?>
									<a href="Javascript:void(0)" class="addtofavourite-reel" onclick="addToFav('multiple')">
								<?php }else{?>
									<a href="#/" class="dropdown-item" data-fancybox="" data-src="#signin-content" data-options="{&quot;touch&quot; : false}">
								<?php } ?>	Add to Favourite</a>

									<?php if($this->session->userdata('footage_farm_user')) { ?>
										<a href="#/" data-fancybox data-src="#selectprojects-content" data-options='{"touch" : false}' class="dropdown-item">
									<?php }else{?>
										<a href="#/" class="dropdown-item" data-fancybox="" data-src="#signin-content" data-options="{&quot;touch&quot; : false}">
									<?php } ?>	Add to Project</a>
							</div>
						</a>


						<?php if($this->session->userdata('footage_farm_user')) { ?>
							<a href="#/" class="addtocart-reel" onclick="addToCart('multiple')">
						<?php }else{?>
							<a href="#/" class="" data-fancybox="" data-src="#signin-content" data-options="{&quot;touch&quot; : false}">
						<?php } ?>
							<span>Add to Cart</span>
							<img src="<?= base_url()?>images/cart-white.svg" alt="">
						</a>
						<a href="#/" class="close-selected-header"><img src="<?= base_url()?>images/remove.svg" alt=""></a>
					</div>
				</div>

			</div>
		</div>
	</section>
</section>
<!-- Selected Reels Header -->
<!-- Innerpage Search -->
<section class="page-container innerpage-top">	
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="searchbar-top">
		<div class="home-search-wrapper">
			<form class="home-search search-form" action="">
				<input type="text" list="browsers" placeholder="Search for footages and themes" name="search" autocomplete="off">
				  <datalist id="browsers">
				    
				  </datalist>
				<button type="submit"><img src="<?= base_url('images/search-icon.svg')?>" alt=""></button>
			</form>
		</div>
	</div>				
</section>
<!-- Innerpage Search -->
<section class="page-container">
	<!-- Mobile Search -->
	<div class="multicolored-line multicolored-line-mobile">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="home-search-wrapper search-wrapper-mobile">
		<form class="home-search" action="">
			<input type="text" placeholder="Search for footages and themes" name="home-search">
			<button type="submit"><img src="<?= base_url('images/search-icon.svg')?>" alt=""></button>
		</form>
	</div>
	<!-- Mobile Search -->
	<!-- Innerpage Banner -->
	<section class="page-section mb-0">		
		<div class="innerpage-banner innerpage-banner-transparent pb-90 pt-65">
			<div class="innerpage-banner-grey-bg full-height"></div>
			<div class="container">
				<div class="row">
					<div class="col-12 col-sm-6">
						<h1 class="page-title-small"><?=(!empty($subtheme))?$subtheme[0]['sub_theme_name']:'';?></h1>
						<p class="subtheme-count"><?=(!empty($reel_data))?$reel_total:'';?> Reels</p>
						
							<?php if($this->session->userdata('footage_farm_user')) {?>
								<a href="Javascript:void(0)" class="btn-ff btn-ff-icon btn-transparent btn-secondary-dark text-uppercase addtofavourite" onclick="addToFavSubTheme(<?=$subtheme[0]['sub_theme_id']?>)">
							<?php }else{?>
								<a href="#/" class="btn-ff btn-ff-icon btn-transparent btn-secondary-dark text-uppercase addtofavourite" data-fancybox="" data-src="#signin-content" data-options="{&quot;touch&quot; : false}">
							<?php } ?>
								Add to favourite </a>


						<!-- <a href="#/" class="btn-ff btn-ff-icon btn-transparent btn-secondary-dark text-uppercase addtofavourite addtofavourite-subtheme" href="javascript:;">Add to favourite</a> -->
						<a href="#/" class="btn-ff btn-ff-icon btn-transparent btn-secondary-dark text-uppercase share reelshare" data-fancybox data-src="#sharetheme-content" data-url="<?= base_url('reels/'.$subtheme[0]['sub_theme_id'])?>" data-options='{"touch" : false}'>Share</a>
					</div>					
					<div class="col-12 col-sm-6">
					
					</div>					
				</div>
			</div>			
		</div>				
	</section>
	<!-- Innerpage Banner -->
	<!-- Reels Listing -->
	<section class="page-section pt-40">
		<div class="container">
			<form action="">
				<div class="row">
				<?php
			if(!empty($reel_data)){
				foreach ($reel_data as $key => $value) {?>

				<div class="col-12 col-md-6 col-lg-4">
						<div class="block-single ">
							<a href="<?= base_url('reel_detail/'.$value['video_id'].'/'.$subtheme[0]['sub_theme_id'])?>">
								<h3><?= $value['video_title']?></h3>
								<div class="block-info">								
									<div>
										<p class="block-info-title">Reel Number</p>
										<p class="block-info-content"><?= $value['reel_number']?></p>
									</div>
								</div>
							</a>
							<div class="blog-tags">
							<?php 
							foreach ($value['tag'] as $tagkey => $tagvalue) { ?>
								<a href="#/"><?= $tagvalue['tag_name']?></a>
							<?php } ?>
								<!-- <a href="#/">film</a>
								<a href="#/">care</a>
								<a href="#/">archive</a> -->
							</div>
							<a href="#/">
								<div class="hover-options-container">
									<div class="box-single-checkbox">
										<label class="checkbox-container">
											<input type="checkbox" class="cls_checkbox"  value="<?= $value['video_id']?>" name="reels[]"id="">
											<span class="checkmark"></span>
										</label>
									</div>
									<div class="box-single-share">
										<span class="share-single share-favourite addtofavourite-reel" onclick="addToFav('single',<?=$value['video_id']?>)"></span>
									
										<span class="share-msg-text share-favourite-text">
											Add to Favourite
										</span>
										<span class="share-single share-social reelshare" data-fancybox data-src="#sharetheme-content" data-url="<?= base_url('reel_detail/'.$value['video_id'].'/'.$subtheme[0]['sub_theme_id'])?>" data-options='{"touch" : false}'></span>	
										<span class="share-msg-text share-social-text">
											Share 
										</span>									
										<span class="share-single share-project" data-fancybox data-src="#noprojects-content" data-options='{"touch" : false}'></span>
										<span class="share-msg-text share-project-text">
											Add to Project
										</span>										
										<!-- <span class="share-single share-cart" data-fancybox data-src="#enquiry-master-content" data-options='{"touch" : false}'></span> -->
										<span class="share-single share-cart"  onclick="addToCart('single',<?=$value['video_id']?>)"></span>
										<span class="share-msg-text share-cart-text">
											Add to Cart
										</span>
									</div>
									<iframe src="<?= $value['vimeo_url']?>?byline=0&portrait=0" class="listing-video" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
								</div>
							</a>
						</div>
					</div>

				<?php } } ?>

				</div>
			</form>
		</div>
	</section>
	<!-- Reels Listing -->
	<!-- Similar Themes -->
	<section class="page-section pt-75 mb-0 similar-section-wrapper">
		<div class="grey-bg"></div>
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="multicolored-line">
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
					</div>
					<div class="section-title-wrapper">
						<h2 class="section-title">
							<img src="<?= base_url()?>images/section-headline.svg" alt=""> Similar Sub-Themes
						</h2>						
					</div>
				</div>
				<?php
			    if(!empty($similar_subtheme)){
				foreach ($similar_subtheme as $key => $value) { ?>

				<div class="col-12 col-md-6 col-lg-4">
					<div class="block-single">
						<a href="<?= base_url('reels/'.$value['sub_theme_id'])?>">
							<h3><?= $value['sub_theme_name']?></h3>
							<div class="block-info">								
								<div>
									<p class="block-info-title">Total Reels</p>
									<p class="block-info-content"><?= $value['total_reals']?></p>
								</div>
							</div>
							<img src="<?= base_url('images/sub_theme_image/'.$value['sub_thumbnail_image'])?>" alt="<?= $value['sub_thumbnail_image']?>" class="img-fluid">
						</a>
					</div>
				</div>
				<?php } } ?>

							
			</div>
		</div>
	</section>						
	<!-- Similar Themes -->
</section>
<script>

	getProject();
	// add to fav shiv code on 22-9-20202
	function addToFav(fav_type,video_id){
		var selected_reels = [];
			if(fav_type == 'multiple'){
			var selected_reels = $('.cls_checkbox:checked').map(function () {
			return this.value;
			}).get();
		}else{
			selected_reels.push(video_id);
		}
		console.log(selected_reels);
		var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
		var csrfHash = $('.txt_csrfname').val(); // CSRF hash
		if(selected_reels){
				$.ajax({
					url: "<?= base_url('reels/addToFav')?>",
					type: "POST",
					data:{ selected_reels , [csrfName]: csrfHash },
					dataType: "json",
					success: function(response){
						if(response.success){
							$('.notification-message').fadeIn();		
							$('.notification-modal-text').text('You saved this reel as your favourites!')
							setTimeout(function(){
								$('.notification-message').fadeOut();	 
								}, 2000);	
						}else{
							$('.notification-message').fadeIn();		
							$('.notification-modal-text').text(response.msg)
							setTimeout(function(){
								$('.notification-message').fadeOut();	 
								}, 2000);	
						}
					}
				});
			}
	}

	
	// add to fav shiv code on 22-10-20202
	function addToFavSubTheme(subtheme_id){
		var selected_subthemes = [];
			selected_subthemes.push(subtheme_id);
		console.log(selected_subthemes);
		var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
		var csrfHash = $('.txt_csrfname').val(); // CSRF hash
		if(selected_subthemes){
				$.ajax({
					url: "<?= base_url('subtheme/addToFav')?>",
					type: "POST",
					data:{ selected_subthemes , [csrfName]: csrfHash },
					dataType: "json",
					success: function(response){
						if(response.success){
							$('.notification-message').fadeIn();		
							$('.notification-modal-text').text('You saved this subtheme as your favourites!')
							setTimeout(function(){
								$('.notification-message').fadeOut();	 
								}, 2000);	
						}else{
							$('.notification-message').fadeIn();		
							$('.notification-modal-text').text(response.msg)
							setTimeout(function(){
								$('.notification-message').fadeOut();	 
								}, 2000);	
						}
					}
				});
			}
	}

	// add to cart shiv code on 22-9-2020
	function addToCart(fav_type,video_id){
		var selected_reels = [];
			if(fav_type == 'multiple'){
			var selected_reels = $('.cls_checkbox:checked').map(function () {
			return this.value;
			}).get();
		}else{
			selected_reels.push(video_id);
		}
		console.log(selected_reels);
		var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
		var csrfHash = $('.txt_csrfname').val(); // CSRF hash
		if(selected_reels){
				$.ajax({
					url: "<?= base_url('reels/addToCart')?>",
					type: "POST",
					data:{ selected_reels , [csrfName]: csrfHash },
					dataType: "json",
					success: function(response){
						if(response.success){
							$('.notification-message').fadeIn();		
							if(selected_reels.length > 1){
								$('.notification-modal-text').text(selected_reels.length + ' Reels have been added to the cart!')
							}else{
								$('.notification-modal-text').text(selected_reels.length + ' Reel have been added to the cart!')
							}
							setTimeout(function(){ $('.notification-message').fadeOut();	 }, 2000);
						}else{
							$('.notification-message').fadeIn();		
							$('.notification-modal-text').text(response.msg)
							setTimeout(function(){
								$('.notification-message').fadeOut();	 
								}, 2000);	
						}
					}
				});
			}
	}
	// get project listing code by ashutosh n made changes by shiv on  1-10-2020
	function getProject() {
		var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
		var csrfHash = $('.txt_csrfname').val(); // CSRF hash
		$.ajax({
			url: "<?= base_url('project/projectdata')?>",
			type: "POST",
			data:{ [csrfName]: csrfHash },
			dataType: "json",
			success: function(response){
				if(response.success){
					$("#projectshow").html(response.data);		
				}else{
					$("#projectshow").html('');
				}
				
				$('.mep-single').hover(function () {
					$(this).children('.hover-options-container').children('.mep-box-single-checkbox').show();
				}, function () {
					$(this).children('.hover-options-container').children('.mep-box-single-checkbox').hide();
					/*Keep checkbox visible if checked*/
					var mepcheckbox = $(this).children('.hover-options-container').children('.mep-box-single-checkbox').children('label.checkbox-container').children('input[type="checkbox"]');
					if (mepcheckbox.is(':checked')) {
						$(this).children('.hover-options-container').children('.mep-box-single-checkbox').addClass('selected');
						//$(this).children('.hover-options-container').siblings('.mep-single-overlay').addClass('selected')
					} else {
						$(this).children('.hover-options-container').children('.mep-box-single-checkbox').removeClass('selected');
						//$(this).children('.hover-options-container').siblings('.mep-single-overlay').removeClass('selected')
					}
					/*Keep checkbox visible if checked*/
				});


			}
		});
	}

	// use for project form submit
	$("#form-project").validate({
		ignore:[],
		rules: {
			// nameofproject:{required:true},
			// projectdesc/:{required:true},
		},
		messages: {
			// nameofproject:{required:"Please Enter Project Name."},
			// projectdesc:{required:"Please Enter Description."},
		},
		submitHandler: function(form) 
		{	
			var myCheckboxes = new Array();
				$("input[name='reels[]']:checked").each(function() {
				myCheckboxes.push($(this).val());
				});
			//var pagedata = { "keyword": myCheckboxes};
			
			var act = "<?= base_url('project/submitForm')?>";
			$("#form-project").ajaxSubmit({
				url: act, 
				type: 'post',
				data: { reel: myCheckboxes },
				dataType: 'json',
				cache: false,
				clearForm: false,
				async:false,
				beforeSubmit : function(arr, $form, options){
					$(".btn btn-black").hide();
				},
				success: function(response) 
				{
					if(response.success){
						getProject();
						swal("done", response.msg, {
							icon : "success",
							buttons: {        			
								confirm: {
									className : 'btn btn-success'
								}
							},
						}).then(
						function() {

							// $("#selectprojects-content").modal('show');
						});
					}else{	
						swal("Failed", response.msg, {
							icon : "error",
							buttons: {        			
								confirm: {
									className : 'btn btn-danger'
								}
							},
						});
					}
				}
			});
		}
	});


</script>