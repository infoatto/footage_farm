<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Faq extends MX_Controller
{
	
	public function __construct(){
		parent::__construct();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('faqmodel','',TRUE);
	}

	public function index(){
		$result = array();
		$condition = array('status'=>'Active');
		$result['faq_data'] = $this->common->getData("tbl_faqs",'*',$condition);
		$result['meta_description'] = "faq description ";
		$result['meta_keywords'] = "faq keywords";
		$this->load->view('header',$result);
		$this->load->view('index.php',$result);
		$this->load->view('footer');

	}

}?>