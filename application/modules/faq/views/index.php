
<!-- Innerpage Search -->
<section class="page-container innerpage-top">
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="searchbar-top">
		<div class="home-search-wrapper">
			<form class="home-search search-form" action="">
				<input type="text" list="browsers" placeholder="Search for footages and themes" name="search" autocomplete="off">
				  <datalist id="browsers">
				    
				  </datalist>
				<button type="submit"><img src="images/search-icon.svg" alt=""></button>
			</form>
		</div>
	</div>
</section>
<!-- Innerpage Search -->
<section class="page-container">
	<!-- Mobile Search -->
	<div class="multicolored-line multicolored-line-mobile">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="home-search-wrapper search-wrapper-mobile">
		<form class="home-search" action="">
			<input type="text" placeholder="Search for footages and themes" name="home-search">
			<button type="submit"><img src="images/search-icon.svg" alt=""></button>
		</form>
	</div>
	<!-- Mobile Search -->
	<!-- Innerpage Banner -->
	<section class="page-section mb-0">
		<div class="innerpage-banner pt-50 pb-10 ptmob-25 pbmob-25 faqs-banner">
			<div class="innerpage-banner-grey-bg full-height"></div>
			<div class="container">
				<div class="row">
					<div class="col-12 col-sm-12 col-md-12 offset-lg-1 col-lg-10">
						<h1 class="page-title-large text-uppercase">Faqs</h1>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Innerpage Banner -->
	<section class="page-section pt-50">
		<div class="container">
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 offset-lg-1 col-lg-10">
					<!--Accordion wrapper-->
					<div class="accordion" id="accordion-faq" role="tablist" aria-multiselectable="true">
						<?php if(!empty($faq_data)){
							foreach ($faq_data as $key => $value) {?>
								<!-- Accordion card -->
								<div class="card">
									<!-- Card header -->
									<div class="card-header" >
										<a href="#/" class=" <?= ($key == 0 ?"collapsed":"") ?>">
											<h5>
												<?= $value['question']?> <img src="images/down-arrow.svg" alt="">
											</h5>
										</a>
									</div>
									<!-- Card body -->							
									<div class="card-body <?= ($key != 0 ?"collapsed":"") ?>" >
										<p><?= $value['answer']?></p> 
									</div>							
								</div>
								<!-- Accordion card -->
						<?php } }?>
					</div>
					<!-- Accordion wrapper -->
				</div>
			</div>
		</div>
	</section>
</section>
<script>
	$(document).ready(function(){
		$('.card-header').click(function(){
			$(this).next('.card-body').toggle();
			$(this).children('a').toggleClass('collapsed')
		})
	})
</script>
</body>
</html>