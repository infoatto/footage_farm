<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Home  extends MX_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('homemodel','',TRUE);
		parent::__construct();
		
	}

	public function cookies(){
		$cookie_name = "footage_farm";
		$cookie_value = "footage_farm";
		setcookie($cookie_name, $cookie_value, time() + (86400 * 30 + 360), "/");
	}


	public function index()
	{
		// get banner image/video
		$condition = "1=1 AND banner_id = 1 ";
		$result['banner_data'] = $this->common->getData("tbl_banner",'*',$condition);

		//get featured themes
		$condition = "1=1 AND t.featured_theme = 'Yes' AND t.status= 'Active' ";
		$main_table = array("tbl_themes as t", array("t.*"));
		$join_tables =  array();
		$join_tables = array(
							array("", "tbl_sub_themes as  sb", "t.theme_id = sb.theme_id", array("count(sb.sub_theme_id) as totol_sub_themes")),
							);
	
		$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("t.theme_id" => "DESC"),"t.theme_id",null); 
		 // fetch query
		$featured_theme_data = $this->common->MySqlFetchRow($rs, "array"); // fetch result

		foreach ($featured_theme_data as $key => $value) {
			$condition = "1=1 AND theme_id = ".$value['theme_id'];
			$total_reel = $this->common->getData("tbl_video_theme_subtheme_mapping",'count(video_id) as total_reel',$condition,"","","video_id");
			$featured_theme_data[$key]['total_reals'] =count($total_reel) ;
		}
	
		$result['featured_theme_data']  = $featured_theme_data;
		
		//get testimonial 
		$condition = "1=1 AND status = 'Active'  ";
		$result['testimonial_data'] = $this->common->getDataLimit("tbl_testimonials",'*',$condition,"testimonial_id","desc",'3');

		// get about us 
		$condition = "1=1";
		$result['about_us'] = $this->common->getData("tbl_about_us",'*',$condition);

		//get blog data
		$condition = "1=1 AND b.status= 'Active' ";
		$main_table = array("tbl_blogs as b", array("b.*"));
		$join_tables =  array();
		$join_tables = array(
							array("", "tbl_blog_tag_mapping as  bt", "bt.blog_id = b.blog_id"),
							array("", "tbl_blog_tags as  t", "t.blog_tag_id = bt.tag_id", array("group_concat(t.blog_tag_name) as tag_name,group_concat(t.blog_tag_id) as tag_id")),
							array("", "tbl_users as  u", "u.user_id = b.author_id", array("user_name as author_name")));
	
		$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("b.blog_id" => "DESC"),"b.blog_id",null); 
		 // fetch query
		 $result['blog_data']= $this->common->MySqlFetchRow($rs, "array"); // fetch result
 
		 //get how we operate
		$condition = "1=1 AND status = 'Active'  ";
		$result['operate_data'] = $this->common->getDataLimit("tbl_how_we_operate",'*',$condition,"how_we_operate_id","ASC",'3');

		// echo "<pre>";
		// print_r($result);
		// exit;
		
		$result['meta_description'] = "home page banner";
		$result['meta_keywords'] = " keywords,shiv,admin";


		$this->load->view('header',$result);
		$this->load->view('index',$result);
		$this->load->view('footer');
	}

}
?>