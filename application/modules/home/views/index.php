
<section class="page-container">
	<!-- Mobile Search -->
	<div class="multicolored-line multicolored-line-mobile">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="home-search-wrapper search-wrapper-mobile">
		<form class="home-search" action="">
			<input type="text" placeholder="Search for footages and themes" name="home-search">
			<button type="submit"><img src="<?= base_url()?>assets/images/search-icon.svg" alt=""></button>
		</form>
	</div>
	<!-- Mobile Search -->
	<!-- Homepage Banner -->
	<section class="page-section">
		<div class="section-container <?= (($banner_data[0]['withoutall'] == '1')?"no-video":"") ?>">
			<div class="grey-bg"></div>
			<div class="container">
				<div class="row">
					<div class="col-12">
						<h1 class="page-title-large home-title"><?= ($banner_data[0]['home_heading'])?></h1>
						<div class="home-search-wrapper">
							<form class="home-search search-form" action="">
								<input type="text" list="browsers" placeholder="Search for footages and themes" name="search" autocomplete="off">
								  <datalist id="browsers">
								    <!-- <option value="Edge"> -->
								  </datalist>
								<button type="submit"><img src="<?= base_url()?>assets/images/search-icon.svg" alt=""></button>
							</form>
						</div>
					</div>

					<?php if($banner_data[0]['withoutall'] != '1'){?>
						<div class="col-12 no-padding-mobile">
							<div class="home-banner-video">
								<div class="banner-top-vimeo-wrapper">
									<?php if(!empty($banner_data[0]['vimeo_link']) && isset($banner_data[0]['vimeo_link'])){?>
										<iframe src="<?= $banner_data[0]['vimeo_link']?>" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="banner-top-vimeo">
										</iframe>
									<?php }else{?>
											<img  style="width: 1170px;height: 720px;object-fit: cover;" src="<?= base_url('images/banner_image/'.$banner_data[0]['thumbnail_image'])?>" alt="banner Image">
									<?php } ?>
								</div>
								<div class="clearfix"></div>
									<script src="https://player.vimeo.com/api/player.js"></script>
								<div class="home-banner-video-description">
									<?php
										if(!empty($banner_data[0]['banner_title']) && isset($banner_data[0]['banner_title']) && $banner_data[0]['vimeo_link']){?>
											<p>Featured Reel:  <?= $banner_data[0]['banner_title']?></p>
										<?php }								
									?>
								</div>
							</div>
						</div>
					<?php }?>
				</div>
			</div>
		</div>
	</section>
	<!-- Homepage Banner -->
	<!-- Featured Themes -->
	<section class="page-section">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="multicolored-line">
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
					</div>
					<div class="section-title-wrapper">
						<h2 class="section-title section-title-half">
							<img src="<?= base_url()?>assets/images/section-headline.svg" alt=""> Featured Themes
						</h2>
						<a href="<?= base_url('theme')?>" class="primary-link">Browse Themes</a>
					</div>
				</div>
				
				<?php
					if(!empty($featured_theme_data[0])){
						foreach ($featured_theme_data as $key => $value) {?>
							<div class="col-12 col-md-6 col-lg-4">
								<div class="block-single themeview" data-themeid="<?= $value['theme_id']?>">
									<a href="<?= base_url('subtheme/'.$value['theme_id'])?>">
										<h3><?= $value['theme_name']?></h3>
										<div class="block-info">
											<div>
												<p class="block-info-title">Sub-themes</p>
												<p class="block-info-content"><?= $value['totol_sub_themes']?></p>
											</div>
											<div>
												<p class="block-info-title">Total Reels</p>
												<p class="block-info-content"><?= $value['total_reals']?></p>
											</div>
										</div>
										<img src="<?= base_url('images/theme_image/'.$value['thumbnail_image'])?>" alt="<?= $value['theme_name']?>" class="img-fluid">
									</a>
								</div>
							</div>	
						<?php }
					} ?>
			</div>
		</div>
	</section>
	<!-- Featured Themes -->
	<!-- How We Operate -->
	<section class="page-section">
		<div class="container">
		<?php if(!empty($operate_data) && isset($operate_data)){?>
			<div class="row">
				<div class="col-12">
					<div class="multicolored-line">
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
					</div>
					<div class="section-title-wrapper">
						<h2 class="section-title section-title-half">
							<img src="<?= base_url()?>assets/images/section-headline.svg" alt=""> How we operate
						</h2>
						<a href="<?= base_url('how_we_operate')?>" class="primary-link">Read More</a>
					</div>
				</div>
				
				
				<?php if($operate_data){?>
					<div class="col-12 col-sm-4">
						<div class="block-single how-we-work">
							<div class="how-we-work-title">
								<h3><?= $operate_data[0]['title_1']?></h3>
								<img src="<?= base_url()?>images/how_we_work/<?= $operate_data[0]['thumbnail_image_1']?>" alt="">
							</div>
							<div class="block-info">
								<p><?= $operate_data[0]['home_description_1']?></p>
							</div>
						</div>
					</div>

					<div class="col-12 col-sm-4">
						<div class="block-single how-we-work">
							<div class="how-we-work-title">
								<h3><?= $operate_data[0]['title_2']?></h3>
								<img src="<?= base_url()?>images/how_we_work/<?= $operate_data[0]['thumbnail_image_2']?>" alt="">
							</div>
							<div class="block-info">
								<p><?= $operate_data[0]['home_description_2']?></p>
							</div>
						</div>
					</div>
					<div class="col-12 col-sm-4">
						<div class="block-single how-we-work">
							<div class="how-we-work-title">
								<h3><?= $operate_data[0]['title_3']?></h3>
								<img src="<?= base_url()?>images/how_we_work/<?= $operate_data[0]['thumbnail_image_3']?>" alt="">
							</div>
							<div class="block-info">
								<p><?= $operate_data[0]['home_description_3']?></p>
							</div>
						</div>
					</div>
				<?php }?>
			</div>
			<?php } ?>
		</div>
	</section>
	<!-- How We Operate -->
	<!-- Testimonials -->
	<section class="page-section">
		<div class="container">
		<?php if(!empty($testimonial_data)){?>
			<div class="row">
				<div class="col-12 no-padding-mobile">
					<div class="home-testimonials">
						<h2 class="section-title">What Our Clients have to say <img src="<?= base_url()?>assets/images/apostrophe.svg" alt="" class="apostrophe"></h2>
							<div class="home-testimonial-carousel owl-carousel">
								<?php  foreach ($testimonial_data as $key => $value) {?>
										<div class="testimonial-single" data-hash="<?=$key?>">
											<p class="testimonial-text"><?= $value['testimonial_text']?></p>
											<div class="client-name">
												<div class="arrow-right"></div>
												<p><?= $value['testimonial_name'].' , '.$value['designation_company']?></p>
											</div>
										</div>
								<?php }?>
							</div>	
					</div>
				</div>
			</div>
			<?php }?>
		</div>
	</section>
	<!-- Testimonials -->
	<!-- About Us -->
	<section class="page-section">
		<div class="container">
			<?php  if(!empty($about_us[0]) && isset($about_us[0])){?>
				<div class="row">
					<div class="col-12">
						<div class="multicolored-line">
							<div></div>
							<div></div>
							<div></div>
							<div></div>
							<div></div>
							<div></div>
							<div></div>
						</div>
					</div>
					<div class="col-12 col-sm-6">
						<div class="about-content-wrapper">
							<div class="section-title-wrapper">
								<h2 class="section-title section-title-half">
									<img src="<?= base_url()?>assets/images/section-headline.svg" alt="">About Us
								</h2>
							</div>
							<div class="homepage-about-content">
								<p><?= $about_us[0]['home_description'];?></p>
								<a href="<?= base_url('aboutus')?>" class="btn-ff btn-primary-dark">Read More</a>
							</div>
						</div>
					</div>
					<div class="col-12 col-sm-6">
						<div class="homepage-about-img">
							<img src="<?= base_url('images/aboutus/'.$about_us[0]['home_banner_image'])?>" alt="" class="img-fluid">
						</div>
					</div>
				</div>
				<div class="row counter">
					<div class="col-12">
						<div class="homepage-counter-border">&nbsp;</div>
					</div>
					<div class="col-12 col-sm-4">
						<div class="counter-single first">
							<h2 class="counter-number"><?= $about_us[0]['stat_one_no'];?></h2>
							<div class="counter-info">
								<div class="arrow-right-dark"></div>
								<p><?= $about_us[0]['stat_one_desc'];?></p>
							</div>
						</div>
					</div>
					<div class="col-12 col-sm-4">
						<div class="counter-single">
							<h2 class="counter-number"><?= $about_us[0]['stat_two_no'];?></h2>
							<div class="counter-info">
								<div class="arrow-right-dark"></div>
								<p><?= $about_us[0]['stat_two_desc'];?></p>
							</div>
						</div>
					</div>
					<div class="col-12 col-sm-4">
						<div class="counter-single last">
							<h2 class="counter-number"><?= $about_us[0]['stat_three_no'];?></h2>
							<div class="counter-info">
								<div class="arrow-right-dark"></div>
								<p><?= $about_us[0]['stat_three_desc'];?></p>
							</div>
						</div>
					</div>
					<div class="col-12">
						<div class="homepage-counter-border">&nbsp;</div>
					</div>
				</div>
			<?php } ?>
		</div>
	</section>
	<!-- About Us -->
	<!-- Blog -->
	<section class="page-section">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="multicolored-line">
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
					</div>
					<div class="section-title-wrapper">
						<h2 class="section-title section-title-half">
							<img src="<?= base_url()?>assets/images/section-headline.svg" alt=""> Blog
						</h2>
						<a href="<?= base_url('blog')?>" class="primary-link">Read More</a>
					</div>
				</div>
				
				<?php if(!empty($blog_data)){
					foreach ($blog_data as $key => $value) {?>
				<div class="col-12 col-md-6 col-lg-4">
					<div class="blog-single">
						<a href="<?= base_url('blog_detail/'.$value['blog_slug'])?>">
							<div class="blog-title-date">
								<h3><?= $value['blog_title']?></h3>
								<div class="date-info">
								<?php $date_convert = date('Y-M-d',strtotime($value['blog_date']));
									$date_convert_array = explode('-',$date_convert); ?>
									<span class="date-number"><?= $date_convert_array[2]?></span>
									<span class="month"><?= $date_convert_array[1]?></span>
									<span class="year"><?= $date_convert_array[0]?></span>
								</div>
							</div>
							<div class="blog-author">
								<div class="arrow-right-dark-small"></div>
								<p><?= $value['author_name']?></p>
							</div>
						</a>
						<div class="blog-tags">
					<?php $all_tags = explode(",",$value['tag_name']);
					$all_tag_id = explode(",",$value['tag_id']);
						foreach ($all_tags as $tagkey => $tagvalue) {?>
							<a href="<?= base_url('blog/'.$all_tag_id[$tagkey])?>"><?= $tagvalue?></a>
						<?php }?>
						</div>
						<a href="<?= base_url('blog_detail/'.$value['blog_slug'])?>">
							<img src="<?= base_url('images/blog_main_image/'.$value['thumbnail_image']);?>" alt="" class="img-fluid">
						</a>
					</div>
				</div>
				<?php } }  ?>

			</div>
		</div>
	</section>
	<!-- Blog -->
</section>
</body>
</html>

<script type="text/javascript">
$(document).ready(function(){

	$(document).on("click",".themeview",function () {
        var themecount = $(this).attr("data-themeid");
        var csrfName   = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
	    var csrfHash   = $('.txt_csrfname').val(); // CSRF hash
		$.ajax({
			url: "<?= base_url('theme/themeview')?>",
			type: "POST",
			data:{ themeview: themecount, [csrfName]: csrfHash },
			dataType: "json",
			success: function(response){
				
			}
		}); 
	      
	});

});


</script>