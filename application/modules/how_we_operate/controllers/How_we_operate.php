<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class How_we_operate extends MX_Controller
{
	
	public function __construct(){
		parent::__construct();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('how_we_operatemodel','',TRUE);
	}

	public function index(){
		$result = array();
		$condition = array('status'=>'Active');
		$result['operate_data'] = $this->common->getData("tbl_how_we_operate",'*',$condition);
		$result['meta_description'] = $result['operate_data'][0]['meta_description'];
		$result['meta_keywords'] = $result['operate_data'][0]['meta_title'];
		$this->load->view('header',$result);
		$this->load->view('index.php',$result);
		$this->load->view('footer');

	}

}?>