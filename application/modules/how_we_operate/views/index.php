
<section class="page-container innerpage-top">
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="searchbar-top">
		<div class="home-search-wrapper">
			<form class="home-search search-form" action="">
				<input type="text" list="browsers" placeholder="Search for footages and themes" name="search" autocomplete="off">
				  <datalist id="browsers">
				    
				  </datalist>
				<button type="submit"><img src="images/search-icon.svg" alt=""></button>
			</form>
		</div>
	</div>
</section>
<!-- Innerpage Search -->
<section class="page-container">
	<!-- Mobile Search -->
	<div class="multicolored-line multicolored-line-mobile">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="home-search-wrapper search-wrapper-mobile">
		<form class="home-search" action="">
			<input type="text" placeholder="Search for footages and themes" name="home-search">
			<button type="submit"><img src="images/search-icon.svg" alt=""></button>
		</form>
	</div>
	<!-- Mobile Search -->
	<!-- Innerpage Banner -->
	<section class="page-section mb-0">
		<div class="innerpage-banner pt-50 pb-50 ptmob-25 pbmob-25 how-we-work-banner">
			<div class="innerpage-banner-grey-bg full-height"></div>
			<div class="container">
				<div class="row">
					<div class="col-12 offset-sm-1 col-sm-10">
						<h1 class="page-title-large text-uppercase">How we operate</h1>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Innerpage Banner -->
	<!-- How we work content -->
	<section class="page-section pt-40 mb-0">		
		<div class="container">
			<div class="row">
				<div class="col-12 offset-sm-1 col-sm-10">
					<div class="howwework-content-wrapper">
						<?= $operate_data[0]['short_description'];?>
					</div>
					<div class="row pointer-section">
						<div class="col-12 col-sm-4 col-lg-3">
							<h2 class="hww-title"><img src="<?= base_url('images/how_we_work/'.$operate_data[0]['thumbnail_image_1'])?>" alt=""> <span><?= $operate_data[0]['title_1']?></span></h2>
						</div>
						<div class="col-12 col-sm-8 col-lg-9">
							<div class="hww-pointers">
								<div class="hww-pointer-single ">
									<div class="pointer-content">
										<?= $operate_data[0]['main_description_1']?>
									</div> 
								</div>
							</div>
						</div>
					</div>
					<div class="row pointer-section">
						<div class="col-12 col-sm-4 col-lg-3">
							<h2 class="hww-title"><img src="<?= base_url('images/how_we_work/'.$operate_data[0]['thumbnail_image_2'])?>" alt=""> <span><?= $operate_data[0]['title_2']?></span></h2>
						</div>
						<div class="col-12 col-sm-8 col-lg-9">
							<div class="hww-pointers">
								<div class="hww-pointer-single ">
									<div class="pointer-content">
										<?= $operate_data[0]['main_description_2']?>
									</div> 
								</div>
							</div>
						</div>
					</div>
					<div class="row pointer-section">
						<div class="col-12 col-sm-4 col-lg-3">
							<h2 class="hww-title"><img src="<?= base_url('images/how_we_work/'.$operate_data[0]['thumbnail_image_3'])?>" alt=""> <span><?= $operate_data[0]['title_3']?></span></h2>
						</div>
						<div class="col-12 col-sm-8 col-lg-9">
							<div class="hww-pointers">
								<div class="hww-pointer-single ">
									<div class="pointer-content">
										<?= $operate_data[0]['main_description_3']?>
									</div> 
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 offset-sm-2 col-sm-8"></div>
			</div>
		</div>
	</section>
	<!--<section class="page-section pt-75 mb-0 similar-section-wrapper">
		<div class="grey-bg"></div>
		<div class="container">
			<div class="row">
				<div class="col-12 offset-sm-2 col-sm-8">
					<div class="hww-bottom-content">
						<?= $operate_data[0]['description']?>
					</div>
				</div>
			</div>
		</div>
	</section>-->
	<!-- How we work content -->
</section>
<!-- Share Blog Post -->
<div style="display: none;max-width:580px;" id="sharetheme-content" class="ff-modal">
	<div class="modal-header">
		<h3>Share Blog</h3>
	</div>
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="ff-modal-content">
		<form class="share-reel-form">
			<div class="modal-section">
				<div class="form-group">
					<input type="text" class="form-control ff-field-dark linkfield" id="reelink" name="reelink" placeholder="Blog link will be shown here">
					<a href="#/" class="copylink">Copy Link</a>
				</div>
				<div class="form-group">
					<label for="social">Social</label>
					<div class="modal-social-share">
						<a href="#/"><img src="images/twitter-black.svg" alt="Share on Twitter"></a>
						<a href="#/"><img src="images/facebook-black.svg" alt="Share on Facebook"></a>
						<a href="#/"><img src="images/linkedin-black.svg" alt="Share on Linkedin"></a>
					</div>
				</div>
				<div class="form-group">
					<label for="sendtoemail">Send to Email Address</label>
					<input type="text" class="form-control ff-field-dark" id="sendtoemail" name="sendtoemail" required>
					<span class="emailaddress">niraali@bokapp.design <a href="#/"><img src="images/close-black.svg" alt=""></a></span>
					<span class="emailaddress">gonzalo.c@footagefarm.com <a href="#/"><img src="images/close-black.svg" alt=""></a></span>
					<span class="emailaddress">gintare.k@footagefarm.com <a href="#/"><img src="images/close-black.svg" alt=""></a></span>
					<span class="emailaddress">mat.f@footagefarm.com <a href="#/"><img src="images/close-black.svg" alt=""></a></span>
				</div>
			</div>
			<div class="modal-content-wrapper">
				<div class="modal-actions">
					<input type="submit" data-fancybox-close class="btn-ff btn-secondary-dark text-uppercase" value="Cancel">
					<input type="submit" class="btn-ff btn-primary-dark text-uppercase" value="Send">
				</div>
			</div>
		</form>
	</div>
</div>
<!-- Share Blog Post -->
<script>
	$(document).ready(function() {
		/*Subtheme video select*/
		if ($(window).width() > 991) {
			$('.blog-single a').hover(function() {

				$(this).children('.hover-options-container').children('.box-single-checkbox').show();
				$(this).children('.hover-options-container').children('.box-single-share').show();

			}, function() {

				$(this).children('.hover-options-container').children('.box-single-checkbox').hide();
				$(this).children('.hover-options-container').children('.box-single-share').hide();

				if ($(this).children('.hover-options-container').children('.box-single-checkbox').children('label.checkbox-container').children('input[type="checkbox"]').is(':checked')) {

					$(this).children('.hover-options-container').children('.box-single-checkbox').addClass('selected');
					$(this).children('.hover-options-container').addClass('selected');

				} else {

					$(this).children('.hover-options-container').children('.box-single-checkbox').removeClass('selected');
					$(this).children('.hover-options-container').removeClass('selected');

				}
			});
		}
		/*Subtheme video select*/
	})
</script>
</body>

</html>