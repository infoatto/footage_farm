<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Register extends MX_Controller
{
	
	public function __construct(){
		parent::__construct();
		$this->load->model('common_model/common_model','common',TRUE);
		//$this->load->model('registermodel','',TRUE);
		$this->load->library('email');
	}

	public function index(){
		$result = array();
		$condition = "1=1";
		$result['users'] = $this->common->getData("tbl_users",'*',$condition);
		 echo "<pre>";print_r($result);exit;
	}

	public function submitForm(){
		 //print_r($_POST);exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();
			$condition = "email_id = '".$this->input->post('email')."' ";
			if($this->input->post('id') && $this->input->post('id') > 0){
				$condition .= " AND  id != ".$this->input->post('id')." ";
			}			
			$check_name = $this->common->getData("tbl_users",'*',$condition);

			if(!empty($check_name[0]['user_id'])){
				echo json_encode(array("success"=>false, 'msg'=>'User Email Already Present!'));
				exit;
			}

			$data['role_id'] = '5';
			$data['user_type'] = 'customer';
			$data['user_name'] = $this->input->post('first_name');
			$data['email_id'] = $this->input->post('email');
			$data['phone'] = $this->input->post('phone');
			$data['password'] = md5($this->input->post('user_password'));
			$data['is_email_verified'] = 'No';
			$data['status'] = 'Active';
			$data['updated_on'] = date("Y-m-d H:i:s");
			
			if(!empty($this->input->post('id'))){
				$condition = "id = '".$this->input->post('id')."' ";
				$result = $this->common->updateData("tbl_users",$data,$condition);
				if($result){

					echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
					exit;
				}
				else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while updating data.'));
					exit;
				}
			}else{
				$data['created_on'] = date("Y-m-d H:i:s");

				$this->email->set_mailtype('html');
		        
		        $this->email->from(FROM_MAIL, 'Footage-farm');
		        
		        $this->email->to($data['email_id']);
		        $this->email->subject('Footage Farm Email Verified');
		        $msg = "Hello " . $data['user_name'] . ",<br><br>As per your request your registration has been create.<br>
		        <p>Please click this link to verify your account:<a href='". base_url(). "register/verify?email=".$data['email_id']."'>Click here</a></p>";
		        
		        $content = '<!DOCTYPE HTML>' . '<head>' . '<meta http-equiv="content-type" content="text/html">' . '<title>Email notification</title>' . '</head>' . '<body>' . '<div class="message">&nbsp;</div>' . '<div class="message">' . $msg . '</div>' . '<div class="message">&nbsp;</div>' . '<div class="message">&nbsp;</div>' . '<div class="message">&nbsp;</div>' . '<div>Thanks</div>' . '<div>Footage Farm Support Team</div>' . '<div>&nbsp;</div>' . '<div class="logo">&nbsp;</div>' . '</body>';
		        $this->email->message($content);
		        $this->email->send();
				
				$result = $this->common->insertData('tbl_users',$data,'1');
				if(!empty($result)){
					echo json_encode(array('success'=>true, 'msg'=>'User Registration verification mail send Please verify email.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
					exit;
				}
			}
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem While Add/Edit Data.'));
			exit;
		}
	}

	public function verify() {
		
	    $data = array('is_email_verified' => 'Yes');
	    $condition = "email_id = '".$_GET['email']."' ";
		$result = $this->common->updateData("tbl_users",$data,$condition);
		if($result){
			redirect(base_url());
				}
	}

	public function forgotpassword() {
        $username = isset($_POST['email']) ? $_POST['email'] : "";
        if ($username != "") {
            $data = array();
            $this->load->helper(array('url', 'form'));
            $this->load->library('user_agent');
            $this->load->library('email');
            $config['charset'] = 'utf-8';
	        $config['wordwrap'] = TRUE;
	        $config['mailtype'] = 'html';
	        $this->email->initialize($config);
            
            $user_email = $this->check_valid_email_('tbl_users', 'email_id', $username);
            if ($user_email) {
            	echo json_encode(array('success'=>true, 'msg'=>'Your password has been reset and emailed to you.'));
					exit;
            } else {
                echo json_encode(array('success'=>false, 'msg'=>'This email id is not registered with us.'));
                exit;
            }
            
        } else {
        	echo json_encode(array('success'=>false, 'msg'=>'This email id is not registered with us.'));
			exit;
        }
    }

    function check_valid_email_($table, $email_field, $email_value) {
        $result1 = $this->db->query("select user_id,email_id,user_name from tbl_users where email_id = '$email_value'");
        if ($result1->num_rows() > 0) {
            $res = $result1->result();
            $user = $res[0];
  			
            if ($user->email_id != '') {
                $this->resetpassword($user);
            }

            return $user->email_id;
        } else {

            return false;
        }
    }

    function resetpassword($user) {
        $this->load->helper('string');
        //$password = random_string('alnum', 6);
        $passname = $user->user_name;
        $password = substr($passname, 0, 4).rand(100, 999);

        $this->db->where('user_id', $user->user_id);
        $this->db->update('tbl_users', array('password' => md5($password)));

        //Send email using codeigniter
        $sendmail = FROM_MAIL;
        $this->email->from('$sendmail', 'Footage-farm');

        $mails = explode(',', $user->email_id);

        $this->email->to($mails);
        $this->email->subject('Footage Farm Password Reset');
        $msg = "Hello " . $user->user_name . ",<br><br>As per your request your password has been reset and your new temporary password is: <b>" . $password . "</b><br>Click  <a href='http://footage.webshowcase-india.com' target=new>here </a>to login with your temporary password then go to My Account and create your new password.";
        // Change image src to your site specific settings
        $content = '<!DOCTYPE HTML>' . '<head>' . '<meta http-equiv="content-type" content="text/html">' . '<title>Email notification</title>' . '</head>' . '<body>' . '<div class="message">&nbsp;</div>' . '<div class="message">' . $msg . '</div>' . '<div class="message">&nbsp;</div>' . '<div class="message">&nbsp;</div>' . '<div class="message">&nbsp;</div>' . '<div>Thanks</div>' . '<div>Footage Farm Support Team</div>' . '<div>&nbsp;</div>' . '<div class="logo">&nbsp;</div>' . '</body>';
        $this->email->message($content);
        $this->email->send();
        //var_dump($this->email->print_debugger());
        //echo $this->email->print_debugger();
    }

	public function logout(){
		$this->session->unset_userdata('footage_farm_user');
		$this->session->unset_userdata('access_token');
		redirect(base_url()); 

	}

	public function sharelink(){
		// print_r($_POST);exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();
			$reelink = $this->input->post('reelink');
			$email = $this->input->post('sendtoemail');
		
			$this->email->set_mailtype('html');
	        $this->email->from(FROM_MAIL, 'Footage-farm');
	        
	        $mails = explode(',', $email);
            $this->email->to($mails);
	        $this->email->subject('Footage Farm Shared Reel');
	        $msg = "Hello,<br><br>This is mail form footage farm.<br>You friend " . $this->session->userdata('footage_farm_user')[0]['user_name'] . ".shared the link to see the footage farm video.<br>
	        <p>Please click the link to see the shared reel footage:<a href='".$reelink."'>Click here</a></p>";
	        // print_r($msg);die;
	        $content = '<!DOCTYPE HTML>' . '<head>' . '<meta http-equiv="content-type" content="text/html">' . '<title>Shared Reel</title>' . '</head>' . '<body>' . '<div class="message">&nbsp;</div>' . '<div class="message">' . $msg . '</div>' . '<div class="message">&nbsp;</div>' . '<div class="message">&nbsp;</div>' . '<div class="message">&nbsp;</div>' . '<div>Thanks</div>' . '<div>Footage Farm Support Team</div>' . '<div>&nbsp;</div>' . '<div class="logo">&nbsp;</div>' . '</body>';
	        $this->email->message($content);
	        $result = $this->email->send();
			if($result){
				echo json_encode(array('success'=>true, 'msg'=>'Share link mail send.'));
				exit;
			}else{
				echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
				exit;
			}
			
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem While Add/Edit Data.'));
			exit;
		}
	}
		
}
?>