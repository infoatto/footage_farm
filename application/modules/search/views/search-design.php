<?php
if(!empty($reel_data)){
	foreach ($reel_data as $key => $value) { ?>

	<div class="col-12 col-md-6 col-lg-4">
			<div class="block-single ">
				<a href="#/">
					<h3><?= $value['video_title']?></h3>
					<div class="block-info">								
						<div>
							<p class="block-info-title">Reel Number</p>
							<p class="block-info-content"><?= $value['reel_number']?></p>
						</div>
					</div>
				</a>
				<div class="blog-tags">
					<a href="#/">film</a>
					<a href="#/">care</a>
					<a href="#/">archive</a>
				</div>
				<a href="#/">
					<div class="hover-options-container">
						<div class="box-single-checkbox">
							<label class="checkbox-container">
								<input type="checkbox" class="cls_checkbox"  value="<?= $value['video_id']?>" name="reels[]"id="">
								<span class="checkmark"></span>
							</label>
						</div>
						<div class="box-single-share">
							<span class="share-single share-favourite addtofavourite-reel" onclick="addToFav('single',<?=$value['video_id']?>)"></span>
						
							<span class="share-msg-text share-favourite-text">
								Add to Favourite
							</span>
							<span class="share-single share-social" data-fancybox data-src="#sharetheme-content" data-options='{"touch" : false}'></span>	
							<span class="share-msg-text share-social-text">
								Share 
							</span>									
							<span class="share-single share-project" data-fancybox data-src="#noprojects-content" data-options='{"touch" : false}'></span>
							<span class="share-msg-text share-project-text">
								Add to Project
							</span>										
							
							<span class="share-single share-cart"  onclick="addToCart('single',<?=$value['video_id']?>)"></span>
							<span class="share-msg-text share-cart-text">
								Add to Cart
							</span>
						</div>
						<iframe src="<?= $value['vimeo_url']?>?byline=0&portrait=0" class="listing-video" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
					</div>
				</a>
			</div>
		</div>

	<?php } } ?>