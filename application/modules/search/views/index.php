<!-- Selected Reels Header -->
<section class="page-container selected-items-wrapper">
	<section class="page-section">
		<div class="black-bg"></div>
		<div class="container">
			<div class="col-sm-12">
				<div class="selected-items">
					<div class="selected-items-left">
						<div class="selected-items-count">
							<span id="count-checked-checkboxes">0</span> Themes Selected
						</div>
						<a href="#/" class="select-all">Select All</a>
						<a href="#/" class="deselect-all">Deselect All</a>
					</div>
					<div class="selected-items-right">
						<a href="#/" class="addtofavourite-reel">
							<span>Add to Favourite</span>
							<img src="images/favourite-white.svg" alt="">
						</a>
						<a href="#/" data-fancybox data-src="#selectprojects-content" data-options='{"touch" : false}' class="addtoproject-reel">
							<span>Add to Project</span>
							<img src="images/project-white.svg" alt="">
						</a>
						<a href="#/" class="selectedheader-more">
							<img src="images/more-icon.svg" alt="">
							<div class="dropdown-menu loggedin-flyout selectedheader-flyout">
								<a class="dropdown-item select-all" href="#">Select All</a>
								<a class="dropdown-item deselect-all" href="#">Deselect All</a>
								<a class="dropdown-item" href="#">Add to Favourite</a>
								<a class="dropdown-item" href="#">Add to Project</a>
							</div>
						</a>
						<a href="#/" class="addtocart-reel">
							<span>Add to Cart</span>
							<img src="images/cart-white.svg" alt="">
						</a>
						<a href="#/" class="close-selected-header"><img src="images/remove.svg" alt=""></a>
					</div>
				</div>

			</div>
		</div>
	</section>
</section>
<!-- Selected Reels Header -->
<!-- Innerpage Search -->
<section class="page-container innerpage-top">
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="searchbar-top">
		<div class="home-search-wrapper">
			<form class="home-search search-form" action="">
				<input type="text" list="browsers" placeholder="Search for footages and themes" name="search" autocomplete="off">
				  <datalist id="browsers">
				    
				  </datalist>
				<button type="submit"><img src="images/search-icon.svg" alt=""></button>
			</form>
		</div>
	</div>
</section>
<!-- Innerpage Search -->
<section class="page-container">
	<!-- Mobile Search -->
	<div class="multicolored-line multicolored-line-mobile">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="home-search-wrapper search-wrapper-mobile">
		<form class="home-search" action="">
			<input type="text" placeholder="Search for footages and themes" name="home-search">
			<button type="submit"><img src="images/search-icon.svg" alt=""></button>
		</form>
	</div>
	<!-- Mobile Search -->
	<!-- Innerpage Banner -->
	<section class="page-section mb-0">
		<div class="innerpage-banner">
			<div class="innerpage-banner-grey-bg full-height"></div>
			<div class="container">
				<div class="row">
					<div class="col-12">
						<h1 class="page-title-large text-uppercase"><?=$keywords;?></h1>
					</div>
					<div class="col-12">
						<div class="ff-filters allthemes-filters filters-left">
							<div class="allthemes-filter-single">
								<select name="theme_name" id="theme_id" class="niceselect theme_filter">
									<option value="">Themes</option>
									<?php
										foreach ($theme_data as $key => $value) {
										 ?>
										 <option value="<?=$value['theme_id'] ?>"><?=$value['theme_name'] ?></option>
										<?php 
											}
										 ?>
								</select>
							</div>
							<div class="allthemes-filter-single">
								<select name="country" id="country_id" class="niceselect theme_filter">
									<option value="">Country</option>
									<?php
									foreach ($country_list as $key => $value) {
									 ?>
									 <option value="<?=$value['country_id'] ?>"><?=$value['country_name'] ?></option>
									<?php 
										}
									 ?>
								</select>
							</div>
							<div class="allthemes-filter-single">
								<select name="year" id="year" class="niceselect theme_filter">
									<option value="">Year</option>
									<?php
									foreach ($year_data as $key => $value) {
									 ?>
									 <option value="<?=$value ?>"><?=$value ?></option>
									<?php 
										}
									 ?>
								</select>
							</div>
							<div class="allthemes-filter-single">
								<select name="color" id="color" class="niceselect theme_filter">
									<option value="">B/W Color</option>
									<?php
									foreach ($color_data as $key => $value) {
									 ?>
									 <option value="<?=$value ?>"><?=$value ?></option>
									<?php 
										}
									 ?>
								</select>
							</div>
							<div class="allthemes-filter-single">
								<select name="sound" id="sound" class="niceselect theme_filter">
									<option value="">Sound</option>
									<?php
									foreach ($sound_data as $key => $value) {
									 ?>
									 <option value="<?=$value ?>"><?=$value ?></option>
									<?php 
										}
									 ?>
								</select>
							</div>
						</div>
						<div class="ff-filters allthemes-filters filters-right">
							<p>Sort By</p>
							<div class="allthemes-filter-single">
								<select name="" id="" class="niceselect">
									<option value="Latest">Latest</option>
									<option value="Most Viewed">Most Viewed</option>
									<option value="Popular">Popular</option>
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Innerpage Banner -->
	<!-- Themes -->
	<section class="page-section pt-40">
		<div class="theme-select-overlay"></div>
		<div class="container">
			<div class="row" id="searchdata">				
				
			</div>
		</div>
	</section>
	<!-- Themes -->
</section>
<!-- No Projects -->
<div style="display: none;" id="noprojects-content" class="ff-modal">
	<div class="modal-header">
		<h3>Add to a Project</h3>
	</div>
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="ff-modal-content">
		<div class="modal-content-wrapper">
			<div class="modal-project-thumbnail">
				<img src="https://via.placeholder.com/360x460.png" alt="" class="modal-project-thumb">
			</div>
			<div class="modal-project-details">
				<div class="modal-create-new-project modal-section">
					<a href="#/" class="modal-create-btn">
						<img src="images/plus-black.svg" alt=""> Create New Project
					</a>
				</div>
				<div class="modal-no-projects-msg modal-section">
					<p class="title">No Projects yet!</p>
					<p class="msg">Projects that you create will show up here.</p>
				</div>
				<div class="modal-addnew-project modal-section">
					<form>
						<div class="form-group">
							<label for="nameofproject">Name of Project</label>
							<input type="text" class="form-control ff-field-dark" id="nameofproject" name="nameofproject">
						</div>
						<div class="form-group">
							<label for="projectdesc">Description (optional)</label>
							<textarea class="form-control ff-field-dark" rows="5"></textarea>
						</div>
					</form>
				</div>
			</div>
			<div class="modal-actions modal-addnew-project">
				<input type="submit" class="btn-ff btn-secondary-dark text-uppercase modal-cancel-newproject" value="Cancel">
				<input type="submit" class="btn-ff btn-primary-dark text-uppercase" value="Create">
			</div>
		</div>
	</div>
</div>
<!-- No Projects -->
<!-- Select Projects -->
<div style="display: none;" id="selectprojects-content" class="ff-modal">
	<div class="modal-header">
		<h3>Add to a Project</h3>
	</div>
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="ff-modal-content">
		<div class="modal-content-wrapper">
			<div class="modal-project-thumbnail">
				<img src="https://via.placeholder.com/360x460.png" alt="" class="modal-project-thumb">
			</div>
			<div class="modal-project-details">
				<div class="modal-create-new-project modal-section">
					<a href="#/" class="modal-create-btn">
						<img src="images/plus-black.svg" alt=""> Create New Project
					</a>
				</div>
				<div class="modal-existing-projects modal-section">
					<div class="mep-search">
						<img src="images/search-black.svg" alt="Search">
						<input type="text" placeholder="Search" id="mep-search">
					</div>
					<div class="mep-single">
						<div class="hover-options-container">
							<div class="mep-box-single-checkbox">
								<label class="checkbox-container">
									<input type="checkbox">
									<span class="checkmark"></span>
								</label>
							</div>
						</div>
						<div class="mep-single-overlay"></div>
						<img src="https://via.placeholder.com/460x82.png" alt="" class="img-fluid">
						<p class="mep-single-title"> War Montage Ref </p>
						<p class="mep-single-count">15 Reels</p>
					</div>
					<div class="mep-single">
						<div class="hover-options-container">
							<div class="mep-box-single-checkbox">
								<label class="checkbox-container">
									<input type="checkbox">
									<span class="checkmark"></span>
								</label>
							</div>
						</div>
						<div class="mep-single-overlay"></div>
						<img src="https://via.placeholder.com/460x82.png" alt="" class="img-fluid">
						<p class="mep-single-title"> Pearl Harbour </p>
						<p class="mep-single-count">15 Reels</p>
					</div>
					<div class="mep-single">
						<div class="hover-options-container">
							<div class="mep-box-single-checkbox">
								<label class="checkbox-container">
									<input type="checkbox">
									<span class="checkmark"></span>
								</label>
							</div>
						</div>
						<div class="mep-single-overlay"></div>
						<img src="https://via.placeholder.com/460x82.png" alt="" class="img-fluid">
						<p class="mep-single-title"> Dunkirk </p>
						<p class="mep-single-count">15 Reels</p>
					</div>
					<div class="mep-single">
						<div class="hover-options-container">
							<div class="mep-box-single-checkbox">
								<label class="checkbox-container">
									<input type="checkbox">
									<span class="checkmark"></span>
								</label>
							</div>
						</div>
						<div class="mep-single-overlay"></div>
						<img src="https://via.placeholder.com/460x82.png" alt="" class="img-fluid">
						<p class="mep-single-title"> War Montage Ref </p>
						<p class="mep-single-count">15 Reels</p>
					</div>
					<div class="mep-single">
						<div class="hover-options-container">
							<div class="mep-box-single-checkbox">
								<label class="checkbox-container">
									<input type="checkbox">
									<span class="checkmark"></span>
								</label>
							</div>
						</div>
						<div class="mep-single-overlay"></div>
						<img src="https://via.placeholder.com/460x82.png" alt="" class="img-fluid">
						<p class="mep-single-title"> Pearl Harbour </p>
						<p class="mep-single-count">15 Reels</p>
					</div>
					<div class="mep-single">
						<div class="hover-options-container">
							<div class="mep-box-single-checkbox">
								<label class="checkbox-container">
									<input type="checkbox">
									<span class="checkmark"></span>
								</label>
							</div>
						</div>
						<div class="mep-single-overlay"></div>
						<img src="https://via.placeholder.com/460x82.png" alt="" class="img-fluid">
						<p class="mep-single-title"> Dunkirk </p>
						<p class="mep-single-count">15 Reels</p>
					</div>
				</div>
				<!-- New Project -->
				<div class="modal-addnew-project modal-section">
					<form>
						<div class="form-group">
							<label for="nameofproject">Name of Project</label>
							<input type="text" class="form-control ff-field-dark" id="nameofproject" name="nameofproject">
						</div>
						<div class="form-group">
							<label for="projectdesc">Description (optional)</label>
							<textarea class="form-control ff-field-dark" rows="5"></textarea>
						</div>
					</form>
				</div>
			</div>
			<div class="modal-actions modal-addnew-project">
				<input type="submit" class="btn-ff btn-secondary-dark text-uppercase modal-cancel-newproject" value="Cancel">
				<input type="submit" class="btn-ff btn-primary-dark text-uppercase" value="Create">
			</div>
			<!-- New Project -->
			<div class="modal-actions modal-existing-actions">
				<input type="submit" data-fancybox-close class="btn-ff btn-secondary-dark text-uppercase modal-cancel-newproject" value="Cancel">
				<input type="submit" class="btn-ff btn-primary-dark text-uppercase" value="Save">
			</div>
		</div>
	</div>
</div>
<!-- Select Projects -->
<!-- Share Theme/Reel -->
<div style="display: none;max-width:580px;" id="sharetheme-content" class="ff-modal">
	<div class="modal-header">
		<h3>Share a Reel</h3>
	</div>
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="ff-modal-content">
		<form class="share-reel-form">
			<div class="modal-section">
				<div class="form-group">
					<input type="text" class="form-control ff-field-dark linkfield" id="reelink" name="reelink" placeholder="Theme/Reel link will be shown here">
					<a href="#/" class="copylink">Copy Link</a>
				</div>
				<div class="form-group">
					<label for="social">Social</label>
					<div class="modal-social-share">
						<a href="#/"><img src="images/twitter-black.svg" alt="Share on Twitter"></a>
						<a href="#/"><img src="images/facebook-black.svg" alt="Share on Facebook"></a>
						<a href="#/"><img src="images/linkedin-black.svg" alt="Share on Linkedin"></a>
					</div>
				</div>
				<div class="form-group">
					<label for="sendtoemail">Send to Email Address</label>
					<input type="text" class="form-control ff-field-dark" id="sendtoemail" name="sendtoemail" required>
					<span class="emailaddress">niraali@bokapp.design <a href="#/"><img src="images/close-black.svg" alt=""></a></span>
					<span class="emailaddress">gonzalo.c@footagefarm.com <a href="#/"><img src="images/close-black.svg" alt=""></a></span>
					<span class="emailaddress">gintare.k@footagefarm.com <a href="#/"><img src="images/close-black.svg" alt=""></a></span>
					<span class="emailaddress">mat.f@footagefarm.com <a href="#/"><img src="images/close-black.svg" alt=""></a></span>
				</div>
			</div>
			<div class="modal-content-wrapper">
				<div class="modal-actions">
					<input type="submit" data-fancybox-close class="btn-ff btn-secondary-dark text-uppercase" value="Cancel">
					<input type="submit" class="btn-ff btn-primary-dark text-uppercase" value="Send">
				</div>
			</div>
		</form>
	</div>
</div>
<!-- Share Theme/Reel -->
<!-- Request Enquiry Master -->
<div style="display: none;max-width:700px;" id="enquiry-master-content" class="ff-modal">
	<div class="modal-header">
		<h3>Request Enquiry</h3>
	</div>
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="ff-modal-content">
		<form class="enquiry-master-form">
			<div class="modal-section">

				<div class="form-group">
					<div class="reel-title">
						<img src="https://via.placeholder.com/147x83.png" alt="">
						<h3>1910 - 1918 - WWI Era: Aerials; German Officers & Troops; France Fighting; Trans-Caucasia Winter</h3>
					</div>
				</div>
				<div class="form-group">
					<div class="request-reel-type">
						<label for="selectreeltype">Please select the reel type</label>
						<label class="radio-container-theme">
							<span class="radio-label">Request Preview</span>
							<input type="radio" name="reeltype" required>
							<span class="radio"></span>
						</label>
						<label class="radio-container-theme">
							<span class="radio-label">Request Master</span>
							<input type="radio" name="reeltype">
							<span class="radio"></span>
						</label>
					</div>
					<label for="reeltype" class="error"></label>
				</div>
				<div class="form-group">
					<label for="inouttime">Specify the footage's in and out time</label>
					<div class="begin-end-tc">
						<div class="tc-time begin-tc">
							<label for="begintc">Begin TC
								<a href="#" class="tooltip-info">
									<img src="images/info.svg" alt="">
									<span class="tooltip-msg">
										Footage start time
									</span>
								</a>
							</label>
							<input type="text" class="form-control ff-field-dark" id="begintc" name="begintc" placeholder="00:00:00" required>
						</div>
						<div class="tc-time end-tc">
							<label for="endtc">End TC
								<a href="#" class="tooltip-info">
									<img src="images/info.svg" alt="">
									<span class="tooltip-msg">
										Footage end time
									</span>
								</a>
							</label>
							<input type="text" class="form-control ff-field-dark" id="endtc" name="endtc" placeholder="00:00:00" required>
						</div>
					</div>
					<div id="addmore-begin-end-tc"></div>
					<div class="form-addmore">
						<a href="#/" class="form-addmore-link tc-addmore-link"><img src="images/add-black.svg" alt=""> <span>Add another in and out time codes</span></a>
					</div>
				</div>
				<div class="form-group">
					<label>Do you have a file with multiple in and out time codes?</label>
					<p>Add jpegs, excel or word files here. Each file size should be under 2MB.</p>
					<div class="uploaded-files-list">
						<div class="file-single">
							<img src="images/jpg.svg" alt="" class="file-type-icon">
							<span>Screenshota_qe52gknknf.jpg</span>
							<a href="#/"><img src="images/close.svg" alt="" class="file-remove-icon"></a>
						</div>
						<div class="file-single">
							<img src="images/doc.svg" alt="" class="file-type-icon">
							<span>datasheet-with-timecodes.doc</span>
							<a href="#/"><img src="images/close.svg" alt="" class="file-remove-icon"></a>
						</div>
						<div class="file-single">
							<img src="images/xls.svg" alt="" class="file-type-icon">
							<span>animate-sys.xls</span>
							<a href="#/"><img src="images/close.svg" alt="" class="file-remove-icon"></a>
						</div>
					</div>
					<!-- Created using dropzone.js-->
					<div id="upload-reel-times">
						<img src="images/upload-file.svg" alt="">
						<p>Drag and drop here or browse</p>
						<a href="#/" class="login-link">Browse</a>
					</div>
					<!-- Created using dropzone.js-->
				</div>
				<div class="form-group">
					<div class="format-framerate">
						<div class="format formatframe">
							<label for="begintc">Format</label>
							<select class="niceselect ff-select-dark formatselect" required>
								<option value="">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
							</select>
							<label id="formatselect-error" class="error" for=""></label>
						</div>
						<div class="framerate formatframe">
							<label for="endtc">Frame Rate</label>
							<select class="niceselect ff-select-dark framerateselect" required>
								<option value="">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
							</select>
							<label id="framerateselect-error" class="error" for=""></label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="codec">Codec
						<a href="#" class="tooltip-info">
							<img src="images/info.svg" alt="">
							<span class="tooltip-msg">
								Codec type
							</span>
						</a>
					</label>
					<input type="text" class="form-control ff-field-dark" id="codec" name="codec" placeholder="QT ProRes422HQ" required>
				</div>
				<div class="form-group">
					<label for="projectdesc">Message (optional)</label>
					<textarea class="form-control ff-field-dark" rows="5"></textarea>
				</div>
			</div>
			<div class="modal-content-wrapper">
				<div class="modal-actions">
					<input type="submit" data-fancybox-close class="btn-ff btn-secondary-dark text-uppercase" value="Cancel">
					<input type="submit" class="btn-ff btn-primary-dark text-uppercase" value="Send Request">
				</div>
			</div>
		</form>
	</div>
</div>
<!-- Request Enquiry Master -->
<script type="text/javascript">
	$(document).ready(function() {
		getSearch();

	$(document).on("change",".theme_filter",function () {
		var key = "<?=$keywords;?>";
        var theme_id = $('#theme_id').val();
        var country  = $('#country_id').val();
        var year     = $('#year').val();
        var color    = $('#color').val();
        var sound    = $('#sound').val();
        
        var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
	    var csrfHash = $('.txt_csrfname').val(); // CSRF hash
		$.ajax({
			url: "<?= base_url('search/searchdata')?>",
			type: "POST",
			data:{ key, theme_id, country, year, color, sound, [csrfName]: csrfHash },
			dataType: "json",
			success: function(response){
				if(response.success){
					
				$("#searchdata").html(response.data);	
					
				}else{
				$("#searchdata").html('');
				}
				
			}
		});
	      
	    });

						
	});

	function getSearch() {

		var key = "<?=$keywords;?>";

		var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
		var csrfHash = $('.txt_csrfname').val(); // CSRF hash
		$.ajax({
			url: "<?= base_url('search/searchdata')?>",
			type: "POST",
			data:{ key, [csrfName]: csrfHash },
			dataType: "json",
			success: function(response){
				if(response.success){
					$("#searchdata").html(response.data);		
				}else{
					$("#searchdata").html('');
				}

			}
		});
	}
</script>

<script>
	$(document).ready(function() {
		/*Notification messages*/
		$('.addtofavourite-subtheme').click(function() {
			$('.notification-message').fadeIn();
			$('.notification-modal-text').text('You saved this subtheme as your favourites!')
			setTimeout(function() {
				$('.notification-message').fadeOut();
			}, 2000);
		});
		$('.addtofavourite-reel').click(function() {
			$('.notification-message').fadeIn();
			$('.notification-modal-text').text('You saved this theme as your favourites!')
			setTimeout(function() {
				$('.notification-message').fadeOut();
			}, 2000);
		});
		$('.select-all').click(function() {
			$('.notification-message').fadeIn();
			$('.notification-modal-text').text('You selected all themes!')
			setTimeout(function() {
				$('.notification-message').fadeOut();
			}, 2000);
		});
		$('.deselect-all').click(function() {
			$('.notification-message').fadeIn();
			$('.notification-modal-text').text('You deselected all themes!')
			setTimeout(function() {
				$('.notification-message').fadeOut();
			}, 2000);
		});
		$('.addtocart-reel').click(function() {
			$('.notification-message').fadeIn();
			$('.notification-modal-text').text('5 themes have been added to the cart!')
			setTimeout(function() {
				$('.notification-message').fadeOut();
			}, 2000);
		})
		$('.share-cart').click(function() {
			$('.notification-message').fadeIn();
			$('.notification-modal-text').text('Reel added to cart!')
			setTimeout(function() {
				$('.notification-message').fadeOut();
			}, 2000);
			$('.selected-items-wrapper').fadeIn()
		})
		/*Notification messages*/
		/*Addmore Start/ End TC*/
		$('.tc-addmore-link').click(function() {
			$('#addmore-begin-end-tc').append(
				"<div class='begin-end-tc'>" +
				"<div class='tc-time begin-tc'>" +
				"<label for='begintc'>Begin TC" +
				"</label>" +
				"<input type='text' class='form-control ff-field-dark' id='begintc' name='begintc' placeholder='00:00:00'>" +
				"</div>" +
				"<div class='tc-time end-tc'>" +
				"<label for='endtc'>End TC" +
				"</label>" +
				"<input type='text' class='form-control ff-field-dark' id='endtc' name='endtc' placeholder='00:00:00'>" +
				"<a href='#/' class='remove-tc'><img src='images/close.svg'/></a>" +
				"</div>	" +
				"</div>"
			)
		});
		$('body').on('click', '.remove-tc', function() {
			$(this).closest('.begin-end-tc').remove();
		});
		/*Addmore Start/ End TC*/
		$('#upload-reel-times').dropzone({
			url: "/file/post"
		});
		$('.file-remove-icon').click(function() {
			$(this).closest('.file-single').remove();
		});
		$('.share-reel-form').validate();
		$('.enquiry-master-form').validate({
			ignore: []
		});

	});
</script>