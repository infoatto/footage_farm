<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Search extends MX_Controller
{
	
	public function __construct(){
		parent::__construct();
		//checklogin();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('searchmodel','',TRUE);
	}

	public function index(){
		
		$result = array();
		
		$result['keywords']  = $_GET['key'];

		$where = array('status'=>'Active');
		$result['theme_data'] = $this->common->getData("tbl_themes",'theme_id, theme_name',$where);

		$where1 = "1=1";
		$result['country_list'] = $this->common->getData("tbl_country",'*',$where1);

		$where2 = "1=1";
		$year = $this->common->getData("tbl_videos",'year',$where2);
		foreach ($year as $key => $value) {
		 $year1[] = $value['year'];
			}
		$result['year_data'] = array_unique($year1);

		$color = $this->common->getData("tbl_videos",'colour_id','');
		foreach ($color as $key => $value) {
		 $color1[] = $value['colour_id'];
			}
		$result['color_data'] = array_unique($color1);
		
		$sound = $this->common->getData("tbl_videos",'sound_id','');
		foreach ($sound as $key => $value) {
		 $sound1[] = $value['sound_id'];
			}
		$result['sound_data'] = array_unique($sound1);

		//echo "<pre>";print_r($result['featured_theme_data']);exit;

		$this->load->view('header');
		$this->load->view('index.php',$result);
		$this->load->view('footer');

	}

	public function searchdata(){
		$result = array();
		$key = $_POST['key'];

		$condition = "(th.theme_name LIKE '%$key%' OR st.sub_theme_name LIKE '%$key%' OR t.video_title LIKE '%$key%') ";

			if (isset($_POST['theme_id']) && $_POST['theme_id']!='') {
				$theme_id = $this->input->post('theme_id');
				$condition .= " AND th.theme_id = $theme_id";
			}
		    if (isset($_POST['country']) && $_POST['country']!='') {
				$country = $this->input->post('country');
				$condition .= " AND tcl.country_id = $country";
			}
			if (isset($_POST['year']) && $_POST['year']!='') {
				$year = $this->input->post('year');
				$condition .= " AND t.year = '$year'";
			}
			if (isset($_POST['color']) && $_POST['color']!='') {
				$color = $this->input->post('color');
				$condition .= " AND t.colour_id = '$color'";
			}
			if (isset($_POST['sound']) && $_POST['sound']!='') {
				$sound = $this->input->post('sound');
				$condition .= " AND t.sound_id = '$sound'";
			}
		
		$main_table = array("tbl_videos as t", array("t.*"));
		$join_tables =  array();
		$join_tables = array(
		          array("", "tbl_video_theme_subtheme_mapping as  sb", "t.video_id = sb.video_id", array("sb.sub_theme_id")),
		          array("", "tbl_themes as  th", "th.theme_id = sb.theme_id", array()),
		          array("", "tbl_sub_themes as  st", "st.sub_theme_id = sb.sub_theme_id", array()),
		         
		          );

		$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("t.video_id" => "DESC"),"",null); 
		$featured_theme_data = $this->common->MySqlFetchRow($rs, "array");

		$result['reel_data']  = $featured_theme_data;

		$themeresponse = $this->load->view('search-design.php',$result,true);	
		
		if (!empty($themeresponse)) {
			print_r(json_encode(array('success'=>true, 'data'=> $themeresponse)));
			exit;
		}else{
			print_r(json_encode(array('success'=>false, 'data'=> $themeresponse)));
			exit;
			}
		

	}

	public function listdata(){
		$result = array();
		$keyword = $_POST['key'];

		$condition = "theme_name LIKE '%$keyword%'";
		$theme = $this->common->getData("tbl_themes",'theme_name',$condition);
		$themedata = array();
		if ($theme) {
		foreach ($theme as $key => $value) {
			$themedata[] = $value['theme_name'];
		}	
		}
		

		$condition = "sub_theme_name LIKE '%$keyword%' ";
		$stheme = $this->common->getData("tbl_sub_themes",'sub_theme_name',$condition);
		$subtheme = array();
		if ($stheme) {
		foreach ($stheme as $key => $value) {
			$subtheme[] = $value['sub_theme_name'];
		}
		}
		
		$condition = "video_title LIKE '%$keyword%' ";
		$videodata = $this->common->getData("tbl_videos",'video_title',$condition);
		$video = array();
		if ($stheme) {
		foreach ($videodata as $key => $value) {
			$video[] = $value['video_title'];
		}
		}

		$condition = "nicename LIKE '%$keyword%' ";
		$countrydata = $this->common->getData("tbl_country",'country_name',$condition);
		
		$country = array();
		if ($countrydata) {
		foreach ($countrydata as $key => $value) {
			$country[] = $value['country_name'];
		}
		}

		$data = array_merge($themedata,$subtheme,$video,$country);
		
		if (!empty($data)) {
			print_r(json_encode($data));
			exit;
		}
		

	}

	

}?>