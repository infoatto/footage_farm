<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Google_login extends MX_Controller
{
	
	public function __construct(){
		parent::__construct();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('google_login_model','',TRUE);
		
		if($this->session->userdata('footage_farm_user'))
			redirect(base_url().'home/');  
	}

	public function login()
	 {
	  include_once APPPATH . "libraries/vendor/autoload.php";

	  $data = array();
	  $google_client = new Google_Client();
	  $google_client->setClientId('93305526721-8s7quffh5q90p4hjaqg7gij6cie56p9n.apps.googleusercontent.com'); //Define your ClientID
	  $google_client->setClientSecret('ghqTxb2VPtQe7GZSEDT-OQEI'); //Define your Client Secret Key
	  $google_client->setRedirectUri('http://footage.webshowcase-india.com/google_login/login'); //Define your Redirect Uri
	  $google_client->addScope('email');
	  $google_client->addScope('profile');
	  if(isset($_GET["code"]))
	  {
	   $token = $google_client->fetchAccessTokenWithAuthCode($_GET["code"]);
	   if(!isset($token["error"]))
	   {
	    $google_client->setAccessToken($token['access_token']);
	    $this->session->set_userdata('access_token', $token['access_token']);
	    $google_service = new Google_Service_Oauth2($google_client);
	    $data = $google_service->userinfo->get();
	    $current_datetime = date('Y-m-d H:i:s');
	    if($this->google_login_model->Is_already_register($data['id']))
	    {

	    $condition = "login_oauth_uid = '".$data['id']."' ";
		$result = $this->common->getData('tbl_users','*',$condition);
	     //update data
	     $user_data = array(
	      'role_id' => '5',
	      'user_name' => $data['given_name'],
	      //'last_name'  => $data['family_name'],
	      'email_id' => $data['email'],
	      //'profile_picture'=> $data['picture'],
	      'updated_on' => $current_datetime
	     );

	     $this->google_login_model->Update_user_data($user_data, $data['id']);
	    }
	    else
	    {
	     //insert data
	     $user_data = array(
	      'login_oauth_uid' => $data['id'],
	      'role_id' => '5',
	      'user_name'  => $data['given_name'],
	      //'last_name'   => $data['family_name'],
	      'email_id'  => $data['email'],
	      //'profile_picture' => $data['picture'],
	      'created_on'  => $current_datetime
	     );

	     $this->google_login_model->Insert_user_data($user_data);

	    $condition = "login_oauth_uid = '".$data['id']."' ";
		$result = $this->common->getData('tbl_users','*',$condition);

	    }
	    //$this->session->set_userdata('user_data', $user_data);
	    $this->session->set_userdata('footage_farm_user', $result);
		$this->session->set_flashdata('success_msg', 'Login Successful');  
			redirect(base_url().'home');
	   }
	  }
	  /*$login_button = '';
	  if(!$this->session->userdata('access_token'))
	  {
	   $login_button = '<a href="'.$google_client->createAuthUrl().'"><img src="'.base_url().'asset/sign-in-with-google.png" /></a>';
	   $data['login_button'] = $login_button;
	   $this->load->view('google_login', $data);
	  }
	  else
	  {
	   $this->load->view('google_login', $data);
	  }*/

	 }

	 function logout()
	 {
	  $this->session->unset_userdata('access_token');
	  $this->session->unset_userdata('footage_farm_user');

	  redirect('google_login/login');
	 }

}?>