<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends MX_Controller
{
	
	public function __construct(){
		parent::__construct();
		$this->load->library('form_validation'); 
		$this->load->model('loginmodel','',TRUE);
		$this->load->model('common_model/common_model','common',TRUE);
		if($this->session->userdata('footage_farm_user'))
			redirect(base_url().'home/');  
	}

	public function index(){
		$this->load->view('head.php');
		$this->load->view('index.php');
		$this->load->view('footer.php');
	}

	public function form_validate(){
			$username = $this->input->post('username'); 
			$password = $this->input->post('password'); 
			$condition = "(user_name = '".$username."' OR email_id = '".$username."') AND password = md5('".$password."') ";
			$result = $this->common->getData('tbl_users','*',$condition);
			if($result){

				if ($result[0]['is_email_verified']!='Yes') {
					echo json_encode(array('success'=>false, 'msg'=>'Please Verify your email account to login.'));
                    exit;	
				}else{
				   $session_data = array('footage_farm_user'=>$result);  
				   $this->session->set_userdata($session_data);  
				   $this->session->set_flashdata('success_msg', 'Login Successful');  
				   //redirect(base_url().'home');
				   echo json_encode(array('success'=>true, 'msg'=>'Login Successful.'));
                   exit;
				}
				  
			}else{
				echo json_encode(array('success'=>false, 'msg'=>'Username and Password not match!.'));
                exit;  
                 
			} 

	}

}?>