<!DOCTYPE html>
<html lang="en">

<body class="login">
	<div class="wrapper wrapper-login">
	<!-- <form class="" id="form-login" method="post" enctype="multipart/form-data"> -->
	<?php $attributes = array('class' => '', 'id' => 'form-login','name' => 'form-login');
            echo form_open(base_url("login/form_validate"),$attributes);?>
		<div class="container container-login animated fadeIn">
			<h3 class="text-center">Sign In To Admin</h3>
			<div class="login-form">
				<div class="form-group">
					<label for="username" class="placeholder"><b>Username</b></label>
					<input id="username" name="username" type="text" class="form-control" >
					<span class="text-danger"><?php echo form_error("username"); ?></span> 
				</div>
				<div class="form-group">
					<label for="password" class="placeholder"><b>Password</b></label>
					<a href="#" class="link float-right">Forget Password ?</a>
					<div class="position-relative">
						<input id="password" name="password" type="password"  class="form-control" >
						<span class="text-danger"><?php echo form_error("password"); ?></span> 
						<div class="show-password">
							<i class="icon-eye"></i>
						</div>
					</div>
				</div>
				<div class="form-group form-action-d-flex mb-3">
					<div class="custom-control custom-checkbox">
						<input type="checkbox" class="custom-control-input" id="rememberme">
						<label class="custom-control-label m-0" for="rememberme">Remember Me</label>
					</div>
					<!-- <a href="#" class="btn btn-primary col-md-5 float-right mt-3 mt-sm-0 fw-bold">Sign In</a> -->
					<button type="submit" class="btn btn-primary col-md-5 float-right mt-3 mt-sm-0 fw-bold">Sign In</button>
				</div>
				<?php echo '<label class="text-danger">'.$this->session->flashdata("error").'</label>';   ?>
				<!-- <div class="login-account">
					<span class="msg">Don't have an account yet ?</span>
					<a href="#" id="show-signup" class="link">Sign Up</a>
				</div> -->
			</div>
		</div>
		<?php echo form_close();?>
	</div>
	
</body>
</html>