<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Subtheme extends MX_Controller
{
	
	public function __construct(){
		parent::__construct();
		//checklogin();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('subthememodel','',TRUE);
	}

	// public function _remap($method){
	//   switch ($method) {
	//      case 'about' : 
	//       $this->about_me();// call about_me() function.
	//      break;
	//      default :
	//       $this->index();
	//      break;
	// }
	// }

	function _remap($method_name){
		if(!method_exists($this, $method_name)){
			$this->index();
		}else{
			$this->{$method_name}();
		}
	}
	
	public function index(){
		$id = $this->uri->segment(2);
		$result = array();
	
		$condition = "t.theme_id = $id ";
		$main_table = array("tbl_themes as t", array("t.theme_name,t.main_video as theme_main_video"));
		$join_tables =  array();
		$join_tables = array(
		          array("", "tbl_sub_themes as  sb", "t.theme_id = sb.theme_id", array("sb.*")),
		          );
		$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("t.theme_id" => "DESC"),"",null);
		$sub_themes = $this->common->MySqlFetchRow($rs, "array");

		foreach ($sub_themes as $key => $value) {
			$condition = "1=1 AND sub_theme_id = ".$value['sub_theme_id'];
			$total_reel = $this->common->getData("tbl_video_theme_subtheme_mapping",'count(video_id) as total_reel',$condition,"","","video_id");
			$sub_themes[$key]['total_reals'] =count($total_reel);
		}

		$where = "theme_id = $id";
		$similar_theme = $this->common->getData("tbl_similar_theme_mapping",'similar_theme_id',$where);
		$aaa = array();
		if (!empty($similar_theme)) {
		foreach ($similar_theme as $key => $theme) {
		    $idd = $theme['similar_theme_id'];
			$condition = "t.theme_id = $idd  AND t.status = 'Active' ";
			$main_table = array("tbl_themes as t", array("t.*"));
			$join_tables =  array();
			$join_tables = array(
			          array("", "tbl_sub_themes as  sb", "t.theme_id = sb.theme_id", array("count(sb.sub_theme_id) as totol_sub_themes")),
			          );

			$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("t.theme_id" => "DESC"),"t.theme_id",null); 
			 // fetch query
			$similar_theme_data = $this->common->MySqlFetchRow($rs, "assoc"); // fetch result
			if (!empty($similar_theme_data['theme_id'])) {
			$condition = "1=1 AND theme_id = ".$similar_theme_data['theme_id'];
			$total_reel = $this->common->getData("tbl_video_theme_subtheme_mapping",'count(video_id) as total_reel',$condition,"","","video_id");
			$similar_theme_data['total_reals'] =@count($total_reel);
			$aaa[] = $similar_theme_data;
			}
			
			
		}
		}

		$result['sub_themes'] = $sub_themes; 
		$result['count_themes'] = count($sub_themes);
		$result['similar_theme'] = $aaa;
				
		// echo "<pre>";print_r($result);exit;

		$this->load->view('header');
		$this->load->view('index.php',$result);
		$this->load->view('footer');

	}

	
	public function addToFav(){

		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			if(!empty($_POST['selected_subthemes']) && isset($_POST['selected_subthemes'])){
				foreach ($_POST['selected_subthemes'] as $key => $value) {
					$data = array();
					$data['subtheme_id'] = $value;
					$data['user_id'] = $this->session->userdata('footage_farm_user')[0]['user_id'];
					$data['created_on'] = date("Y-m-d H:i:s");
					$data['favorite_date'] = date("Y-m-d");
					$data['created_by'] =  $this->session->userdata('footage_farm_user')[0]['user_id'];
					$condition = "user_id = '".$this->session->userdata('footage_farm_user')[0]['user_id']."' AND  subtheme_id = '".$value."' ";
					$check_exist_fav = $this->common->getData("tbl_favourites",'*',$condition);
					if($check_exist_fav){
						$data['updated_on'] = date("Y-m-d H:i:s");
						$data['updated_by'] = $this->session->userdata('footage_farm_user')[0]['user_id'];
						$condition = "favorite_id = '".$check_exist_fav[0]['favorite_id']."' ";
						$result = $this->common->updateData("tbl_favourites",$data,$condition);
					}else{
						$data['created_on'] = date("Y-m-d H:i:s");
						$data['created_by'] =  $this->session->userdata('footage_farm_user')[0]['user_id'];
						$result = $this->common->insertData('tbl_favourites',$data,'1');
					}
				} //end of foreach 
					if(!empty($result)){
						echo json_encode(array('success'=>true, 'msg'=>'Record Added Successfully.'));
						exit;
					}else{
						echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
						exit;
					}
			}else{
				echo json_encode(array('success'=>false, 'msg'=>'Please select the reels first.'));
				exit;
			}
		

		}
	}

}
?>