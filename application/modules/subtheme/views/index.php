<!-- Selected subthemes Header -->
<section class="page-container selected-items-wrapper">
	<section class="page-section">
		<div class="black-bg"></div>
		<div class="container">
			<div class="col-sm-12">
				<div class="selected-items">
					<div class="selected-items-left">
						<div class="selected-items-count">
							<span id="count-checked-checkboxes">0</span> Themes Selected
						</div>
						<a href="#/" class="select-all">Select All</a>
						<a href="#/" class="deselect-all">Deselect All</a>
					</div>
					<div class="selected-items-right">
						<?php if(!$this->session->userdata('footage_farm_user')) { ?>
							<a href="#/" class="addtofavourite-reel" data-fancybox="" data-src="#signin-content" data-options="{&quot;touch&quot; : false}">
						<?php }else{?>
							<a href="Javascript:void(0)" class="addtofavourite-reel" onclick="addToFav('multiple')">
						<?php }?>
							<span>Add to Favourite</span>
							<img src="<?= base_url()?>images/favourite-white.svg" alt="">
						</a>
						
						<a href="#/" class="selectedheader-more">
							<img src="<?= base_url()?>images/more-icon.svg" alt="">
							<div class="dropdown-menu loggedin-flyout selectedheader-flyout">
								<a class="dropdown-item select-all" href="#">Select All</a>
								<a class="dropdown-item deselect-all" href="#">Deselect All</a>
								<?php if($this->session->userdata('footage_farm_user')) {?>
									<a href="Javascript:void(0)" class="addtofavourite-reel" onclick="addToFav('multiple')"></a>
								<?php }else{?>
									<a href="#/" class="dropdown-item" data-fancybox="" data-src="#signin-content" data-options="{&quot;touch&quot; : false}">
								<?php } ?>	Add to Favourite</a>
							</div>
						<a href="#/" class="close-selected-header"><img src="<?= base_url()?>images/remove.svg" alt=""></a>
						</a>

					</div>
				</div>

			</div>
		</div>
	</section>
</section>
<!-- Selected subthemes Header -->
<!-- Innerpage Search -->
<section class="page-container innerpage-top">
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="searchbar-top">
		<div class="home-search-wrapper">
			<form class="home-search search-form" action="">
				<input type="text" list="browsers" placeholder="Search for footages and themes" name="search" autocomplete="off">
				  <datalist id="browsers">
				    
				  </datalist>
				<button type="submit"><img src="<?= base_url()?>images/search-icon.svg" alt=""></button>
			</form>
		</div>
	</div>
</section>
<!-- Innerpage Search -->
<section class="page-container">
	<!-- Mobile Search -->
	<div class="multicolored-line multicolored-line-mobile">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="home-search-wrapper search-wrapper-mobile">
		<form class="home-search" action="">
			<input type="text" placeholder="Search for footages and themes" name="home-search">
			<button type="submit"><img src="<?= base_url()?>images/search-icon.svg" alt=""></button>
		</form>
	</div>
	<!-- Mobile Search -->
	<!-- Innerpage Banner -->
	<section class="page-section mb-0">
		<div class="innerpage-banner innerpage-banner-transparent pb-0">
			<div class="innerpage-banner-grey-bg"></div>
			<div class="container">
				<div class="row">
					<div class="col-12 col-sm-6">
						<h1 class="page-title-small"><?=(!empty($sub_themes))?$sub_themes[0]['theme_name']:'';?></h1>
						<p class="subtheme-count"><?=(!empty($sub_themes))?$count_themes:'';?> Sub-Themes</p>
							<?php if($this->session->userdata('footage_farm_user')) {?>
								<a href="Javascript:void(0)" class="btn-ff btn-ff-icon btn-transparent btn-secondary-dark text-uppercase addtofavourite" onclick="addToFavTheme(<?=$sub_themes[0]['theme_id']?>)">
							<?php }else{?>
								<a href="#/" class="btn-ff btn-ff-icon btn-transparent btn-secondary-dark text-uppercase addtofavourite" data-fancybox="" data-src="#signin-content" data-options="{&quot;touch&quot; : false}">
							<?php } ?>
								Add to favourite </a>
						<!-- <a href="#/" class="btn-ff btn-ff-icon btn-transparent btn-secondary-dark text-uppercase addtofavourite">Add to favourite</a> -->
						<a href="#/" class="btn-ff btn-ff-icon btn-transparent btn-secondary-dark text-uppercase share reelshare" data-fancybox data-src="#sharetheme-content" data-url="<?= base_url('subtheme/'.$sub_themes[0]['theme_id'])?>" data-options='{"touch" : false}'>Share</a>
					</div>
					<div class="col-12 col-sm-6">
						<div style="padding:75% 0 0 0;position:relative;"><iframe src="<?= $sub_themes[0]['theme_main_video']?>" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>
						<script src="https://player.vimeo.com/api/player.js"></script>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Innerpage Banner -->
	<!-- Sub-Themes -->
	<section class="page-section pt-40">
		<div class="container">
			<div class="row">
				<?php
			if(!empty($sub_themes)){
				foreach ($sub_themes as $key => $value) { ?>

				<div class="col-12 col-md-6 col-lg-4">
					<div class="block-single theme-single">
						<a href="<?= base_url('reels/'.$value['sub_theme_id'])?>">
							<h3><?= $value['sub_theme_name']?></h3>
							<div class="block-info">
								<div>
									<p class="block-info-title">Total Reels</p>
									<p class="block-info-content"><?= $value['total_reals']?></p>
								</div>
							</div>
						</a>
						<a href="javascript:void(0)">
							<div class="hover-options-container">
								<!-- <div class="box-single-checkbox">
									<label class="checkbox-container">
										<input type="checkbox">
										<span class="checkmark"></span>
									</label>
								</div> -->
								<div class="box-single-checkbox">
										<label class="checkbox-container">
											<input type="checkbox" class="cls_checkbox"  value="<?= $value['sub_theme_id']?>" name="sub_theme_id[]"id="">
											<span class="checkmark"></span>
										</label>
									</div>
								<div class="box-single-share">
									<!-- <span class="share-single share-favourite addtofavourite-reel"></span> -->
									<span class="share-single share-favourite addtofavourite-reel" onclick="addToFav('single',<?=$value['sub_theme_id']?>)"></span>
									<span class="share-msg-text share-favourite-text">
										Add to Favourite
									</span>
									<span class="share-single share-social reelshare" data-fancybox data-src="#sharetheme-content" data-url="<?= base_url('reels/'.$value['sub_theme_id'])?>" data-options='{"touch" : false}'></span>
									<span class="share-msg-text share-social-text">
										Share
									</span>
								</div>
								<img src="<?= base_url('images/sub_theme_image/'.$value['sub_thumbnail_image'])?>" alt="<?= $value['sub_thumbnail_image']?>" class="img-fluid">
							</div>
						</a>
					</div>
				</div>

				<?php } } ?>
			</div>
		</div>
	</section>
	<!-- Sub-Themes -->
	<!-- Similar Themes -->
	<section class="page-section pt-75 mb-0 similar-section-wrapper">
		<div class="grey-bg"></div>
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="multicolored-line">
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
					</div>
				<?php
				if(!empty($similar_theme)){ ?>
					<div class="section-title-wrapper">
						<h2 class="section-title">
							<img src="<?= base_url()?>images/section-headline.svg" alt=""> Similar Themes
						</h2>
					</div>
				</div>
				<?php
				foreach ($similar_theme as $key => $value) { ?>
				<div class="col-12 col-md-6 col-lg-4">
					<div class="block-single">
						<a href="<?= base_url('subtheme/'.$value['theme_id'])?>">
							<h3><?= $value['theme_name']?></h3>
							<div class="block-info">
								<div>
									<p class="block-info-title">Sub-themes</p>
									<p class="block-info-content"><?= $value['totol_sub_themes']?></p>
								</div>
								<div>
									<p class="block-info-title">Total Reels</p>
									<p class="block-info-content"><?= $value['total_reals']?></p>
								</div>
							</div>
							<img src="<?= base_url('images/theme_image/'.$value['thumbnail_image'])?>" alt="<?= $value['theme_name']?>" class="img-fluid">
						</a>
					</div>
				</div>
				<?php } } ?>

			</div>
		</div>
	</section>
	<!-- Similar Themes -->
</section>

<script>
	// add to fav shiv code on 22-9-20202
	function addToFav(fav_type,subtheme_id){
		var selected_subthemes = [];
			if(fav_type == 'multiple'){
			var selected_subthemes = $('.cls_checkbox:checked').map(function () {
			return this.value;
			}).get();
		}else{
			selected_subthemes.push(subtheme_id);
		}
		console.log(selected_subthemes);
		var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
		var csrfHash = $('.txt_csrfname').val(); // CSRF hash
		if(selected_subthemes){
				$.ajax({
					url: "<?= base_url('subtheme/addToFav')?>",
					type: "POST",
					data:{ selected_subthemes , [csrfName]: csrfHash },
					dataType: "json",
					success: function(response){
						if(response.success){
							$('.notification-message').fadeIn();		
							$('.notification-modal-text').text('You saved subthemes as your favourites!')
							setTimeout(function(){
								$('.notification-message').fadeOut();	 
								}, 2000);	
						}else{
							$('.notification-message').fadeIn();		
							$('.notification-modal-text').text(response.msg)
							setTimeout(function(){
								$('.notification-message').fadeOut();	 
								}, 2000);	
						}
					}
				});
			}
	}

		// add to fav shiv code on 22-10-20202
	function addToFavTheme(theme_id){
		var selected_themes = [];
			selected_themes.push(theme_id);
		console.log(selected_themes);
		var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
		var csrfHash = $('.txt_csrfname').val(); // CSRF hash
		if(selected_themes){
				$.ajax({
					url: "<?= base_url('theme/addToFav')?>",
					type: "POST",
					data:{ selected_themes , [csrfName]: csrfHash },
					dataType: "json",
					success: function(response){
						if(response.success){
							$('.notification-message').fadeIn();		
							$('.notification-modal-text').text('You saved this reel as your favourites!')
							setTimeout(function(){
								$('.notification-message').fadeOut();	 
								}, 2000);	
						}else{
							$('.notification-message').fadeIn();		
							$('.notification-modal-text').text(response.msg)
							setTimeout(function(){
								$('.notification-message').fadeOut();	 
								}, 2000);	
						}
					}
				});
			}
	}

	$(document).ready(function() {
		/*Notification messages*/
		$('.addtofavourite-reel').click(function() {
			$('.notification-message').fadeIn();
			$('.notification-modal-text').text('You saved this subtheme as your favourites!')
			setTimeout(function() {
				$('.notification-message').fadeOut();
			}, 2000);
		});
		$('.select-all').click(function() {
			$('.notification-message').fadeIn();
			$('.notification-modal-text').text('You selected all themes!')
			setTimeout(function() {
				$('.notification-message').fadeOut();
			}, 2000);
		});
		$('.deselect-all').click(function() {
			$('.notification-message').fadeIn();
			$('.notification-modal-text').text('You deselected all themes!')
			setTimeout(function() {
				$('.notification-message').fadeOut();
			}, 2000);
		});
		/*Notification messages*/	
	});
</script>