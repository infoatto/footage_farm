
<div class="wrapper">
	<div class="main-panel">
		<div class="container">
			<div class="page-inner">
				<div class="col-12">
					<div class="row ff-title-block">
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">
							<h1 class="ff-page-title">New Tag</h1> 
						</div>
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
							<a href="#" class="btn btn-light btn-border ff-page-title-btn">
								Cancel
							</a>
						</div>
					</div>
				</div>					
				<div class="row">
					<div class="col-sm-12">
						<div class="card">
							<div class="card-body">
								<form class="" id="form-tag" method="post" enctype="multipart/form-data">
								
									<input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden">
									<input name="tag_id" id="tag_id" value="<?= (!empty($tag_data[0]['tag_id'])?$tag_data[0]['tag_id']:"") ?>" type="hidden">
								
											<div class="form-group">
												<label for="tag_name">Tag Name</label>
												<input type="text" class="form-control" id="tag_name" name="tag_name" value="<?= (!empty($tag_data[0]['tag_name'])?$tag_data[0]['tag_name']:"") ?>" placeholder="eg. Bryan Schreier">	
											</div>
										
											<div class="form-group">
												<label for="tag_description">Tag Description</label>
												<textarea class="form-control" id="tag_description" name="tag_description" rows="5" placeholder="eg. We offer a 24-48 hour turnaround service for masters we hold in our London office. If we need to duplicate the material from Washington, the delays are similar to those you would incur if you went straight to the source." spellcheck="false"><?= (!empty($tag_data[0]['tag_description'])?$tag_data[0]['tag_description']:"") ?></textarea>	
											</div>
										
											<div class="form-check">
												<label>Activate Tag</label><br>
												<div class="switch-field">
														<?php $active = 'checked';
															 $in_active= '';
														if(!empty($tag_data)){
															if($tag_data[0]['status']=='Active'){
																$active = "checked";
															}else{
																$in_active = "checked";
															} 
														}?>
													<input type="radio" id="activate-tag-three" name="tag-status" value="Active" <?= $active?>/>
													<label for="activate-tag-three">Yes</label>
													<input type="radio" id="activate-tag-four" name="tag-status" value="In-active"  <?= $in_active?>/>
													<label for="activate-tag-four">No</label>
												</div>
											</div>	
											<div class="form-group">
												<button  type="submit" class="btn btn-black"><?=(!empty($tag_data)?"Update":"Create")?></button>
												<a href="<?= base_url('tag')?>" class="btn btn-cancel btn-border">Cancel</a>
											</div>
									</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>						
</div>

<script>
$(document).ready(function(){
});


// use for form submit
var vRules = 
{
	tag_name:{required:true},
	// designation_company:{required:true},
	tag_description:{required:true}
};
var vMessages = 
{
	tag_name:{required:"Please Enter Tag Name."},
	// designation_company:{required:"Please designation company Name."},
	tag_description:{required:"Please Enter Tag description."},
};

$("#form-tag").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{	
		var act = "<?= base_url('tag/submitform')?>";
		$("#form-tag").ajaxSubmit({
			url: act, 
			type: 'post',
			dataType: 'json',
			cache: false,
			clearForm: false,
			async:false,
			beforeSubmit : function(arr, $form, options){
				// $(".btn-primary").hide();
			},
			success: function(response) 
			{
				if(response.success){
					swal("done", response.msg, {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					}).then(
					function() {
						window.location = "<?= base_url('tag')?>";
						// $this->router->fetch_class().'/'.$this->router->fetch_method() ?>";
					});
				}else{	
					swal("Failed", response.msg, {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}
			}
		});
	}
});


</script>