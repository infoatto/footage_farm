<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Theme extends MX_Controller
{
	
	public function __construct(){
		parent::__construct();
		//checklogin();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('thememodel','',TRUE);
	}

	public function index(){
		
		$result = array();

		$condition = "1=1 AND t.status = 'Active' ";
		$main_table = array("tbl_themes as t", array("t.*"));
		$join_tables =  array();
		$join_tables = array(
		          array("", "tbl_sub_themes as  sb", "t.theme_id = sb.theme_id", array("count(sb.sub_theme_id) as totol_sub_themes")),
		          );

		$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("t.theme_id" => "DESC"),"t.theme_id",null); 
		 // fetch query
		$featured_theme_data = $this->common->MySqlFetchRow($rs, "array"); // fetch result

		foreach ($featured_theme_data as $key => $value) {
		  $condition = "1=1 AND theme_id = ".$value['theme_id'];
		  $total_reel = $this->common->getData("tbl_video_theme_subtheme_mapping",'count(video_id) as total_reel',$condition,"","","video_id");
		  $featured_theme_data[$key]['total_reals'] =count($total_reel) ;
		}

		$result['featured_theme_data']  = $featured_theme_data;

		$where = array('status'=>'Active');
		$result['theme_data'] = $this->common->getData("tbl_themes",'theme_id, theme_name',$where);

		$where1 = "1=1";
		$result['country_list'] = $this->common->getData("tbl_country",'*',$where1);

		$where2 = "1=1";
		$year = $this->common->getData("tbl_videos",'year',$where2);
		foreach ($year as $key => $value) {
		 $year1[] = $value['year'];
			}
		$result['year_data'] = array_unique($year1);

		$color = $this->common->getData("tbl_videos",'colour_id','');
		foreach ($color as $key => $value) {
		 $color1[] = $value['colour_id'];
			}
		$result['color_data'] = array_unique($color1);
		
		$sound = $this->common->getData("tbl_videos",'sound_id','');
		foreach ($sound as $key => $value) {
		 $sound1[] = $value['sound_id'];
			}
		$result['sound_data'] = array_unique($sound1);					
		//echo "<pre>";print_r($result['featured_theme_data']);exit;


		
		$result['meta_description'] = "this is theme page";
		$result['meta_keywords'] = " keywords,theme,admin";

		
		$this->load->view('header',$result);
		$this->load->view('index.php',$result);
		$this->load->view('footer');

	}

	public function themedata(){
		$result = array();
		$condition = "1=1 AND t.status = 'Active' ";
		if (isset($_POST['id']) && $_POST['id']!='') {
			$id = $this->input->post('id');
			$condition .= " AND t.theme_id = $id";
		}

		$oderby = array("t.theme_id" => "DESC");
		if (isset($_POST['view']) && $_POST['view']!='') {
			$view = $this->input->post('view');
			if ($view=='Alphabetical') {
				$oderby = array("t.theme_name" => "ASC");
			}
			if ($view=='Mostviewed') {
				$oderby = array("t.view_count" => "DESC");
			}
			if ($view=='DESC') {
				$oderby = array("t.updated_on" => "DESC");
			}
			
		}
		
		$main_table = array("tbl_themes as t", array("t.*"));
		$join_tables =  array();
		$join_tables = array(
		          array("", "tbl_sub_themes as  sb", "t.theme_id = sb.theme_id", array("count(sb.sub_theme_id) as totol_sub_themes")),
		          );

		$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, $oderby,"t.theme_id",null); 
		 // fetch query
		$featured_theme_data = $this->common->MySqlFetchRow($rs, "array"); // fetch result

		foreach ($featured_theme_data as $key => $value) {
		  $condition = "1=1 AND tm.theme_id = ".$value['theme_id'];
		    if (isset($_POST['country']) && $_POST['country']!='') {
				$country = $this->input->post('country');
				$condition .= " AND tcl.country_id = $country";
			}
			if (isset($_POST['year']) && $_POST['year']!='') {
				$year = $this->input->post('year');
				$condition .= " AND tv.year = '$year'";
			}
			if (isset($_POST['color']) && $_POST['color']!='') {
				$color = $this->input->post('color');
				$condition .= " AND tv.colour_id = '$color'";
			}
			if (isset($_POST['sound']) && $_POST['sound']!='') {
				$sound = $this->input->post('sound');
				$condition .= " AND tv.sound_id = '$sound'";
			}

		  $main_table = array("tbl_video_theme_subtheme_mapping as tm", array("tm.*"));
		  $join_tables =  array();
		  $join_tables = array(
		          array("", "tbl_videos as  tv", "tm.video_id = tv.video_id", array()),
		          array("", "tbl_video_country_location_mapping as  tvc", "tvc.video_id = tv.video_id", array()),
		          array("", "tbl_country_location as tcl", "tcl.country_location_id = tvc.country_location_id", array()),
		          );

		$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("tm.video_theme_id" => "DESC"),"tm.video_theme_id",null); 
		 // fetch query
		$total_reel = $this->common->MySqlFetchRow($rs, "array"); // fetch result

		// echo "<pre>";
		// print_r($total_reel);
		// exit;
		  $featured_theme_data[$key]['total_reals'] =count($total_reel);
		}

		$result['featured_theme_data'] = $featured_theme_data;		
		$themeresponse = $this->load->view('theme-design',$result,true);	
		
		if (!empty($themeresponse)) {
			print_r(json_encode(array('success'=>true, 'data'=> $themeresponse)));
			exit;
		}else{
			print_r(json_encode(array('success'=>false, 'data'=> $themeresponse)));
			exit;
			}
		

	}

	public function themeview(){
		$id = $this->input->post('themeview');
		$sql = "SELECT view_count FROM tbl_themes WHERE theme_id = $id";
		$res = $this->db->query($sql);
		$result = $res->row_array();
		$count = ($result['view_count']+1);

		$update = $this->common->updateData("tbl_themes",array('view_count'=>$count), array('theme_id'=>$id));
		if ($update) {
			echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
				exit;
		}
	}

	public function addEdit(){

		$theme_id = "";
		//print_r($_GET);
		$result = array();
		if(!empty($_GET['text']) && isset($_GET['text'])){
			$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
			parse_str($varr,$url_prams);
			$theme_id = $url_prams['id'];
			$condition = " theme_id ='".$theme_id."' ";
			$result['theme_data'] = $this->common->getData("tbl_themes",'*',$condition);
		}
		
		// echo "<pre>";
		// print_r($result);
		// exit;
		//echo $user_id;
		$this->load->view('main-header.php');
		$this->load->view('addEdit.php',$result);
		$this->load->view('footer.php');
	}

	function submitformtesting(){
		// codeigniter form validation 
		// $this->form_validation->set_rules('themename', 'Themename', 'trim|required|alpha');
		// $this->form_validation->set_rules('mainvideo', 'Main video', 'trim|required');
		// $this->form_validation->set_rules('meta_title', 'Meta title', 'trim|required');
		// $this->form_validation->set_rules('meta_description', 'Meta description', 'trim|required');
		// $this->form_validation->set_rules('mainvideo', 'Main video', 'trim|required');

		// if($this->form_validation->run()) {  
		// 	// true  if validation property sectifice properly
		// 	$themename = $this->input->post('themename');  
		// 	$mainvideo = $this->input->post('mainvideo'); 
		// 	$themename = $this->input->post('meta_title');  
		// 	$mainvideo = $this->input->post('meta_description'); 
		// 	$themename = $this->input->post('mainvideo');  
		// 	$mainvideo = $this->input->post('meta_title'); 
		// }else{  
		// 	// if validation is not proper
		// 	$this->index();  
		// } 
	}

	public function addToFav(){

		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			if(!empty($_POST['selected_themes']) && isset($_POST['selected_themes'])){
				foreach ($_POST['selected_themes'] as $key => $value) {
					$data = array();
					$data['theme_id'] = $value;
					$data['user_id'] = $this->session->userdata('footage_farm_user')[0]['user_id'];
					$data['created_on'] = date("Y-m-d H:i:s");
					$data['favorite_date'] = date("Y-m-d");
					$data['created_by'] =  $this->session->userdata('footage_farm_user')[0]['user_id'];
					$condition = "user_id = '".$this->session->userdata('footage_farm_user')[0]['user_id']."' AND  theme_id = '".$value."' ";
					$check_exist_fav = $this->common->getData("tbl_favourites",'*',$condition);
					if($check_exist_fav){
						$data['updated_on'] = date("Y-m-d H:i:s");
						$data['updated_by'] = $this->session->userdata('footage_farm_user')[0]['user_id'];
						$condition = "favorite_id = '".$check_exist_fav[0]['favorite_id']."' ";
						$result = $this->common->updateData("tbl_favourites",$data,$condition);
					}else{
						$data['created_on'] = date("Y-m-d H:i:s");
						$data['created_by'] =  $this->session->userdata('footage_farm_user')[0]['user_id'];
						$result = $this->common->insertData('tbl_favourites',$data,'1');
					}
				} //end of foreach 
					if(!empty($result)){
						echo json_encode(array('success'=>true, 'msg'=>'Record Added Successfully.'));
						exit;
					}else{
						echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
						exit;
					}
			}else{
				echo json_encode(array('success'=>false, 'msg'=>'Please select the reels first.'));
				exit;
			}
		

		}
	}



	public function submitForm(){
		// print_r($_POST);
		// print_r($_FILES);
		// exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();
			$condition = "theme_name = '".$this->input->post('themename')."' ";
			if($this->input->post('theme_id') && $this->input->post('theme_id') > 0){
				$condition .= " AND  theme_id != ".$this->input->post('theme_id')." ";
			}			
			$check_name = $this->common->getData("tbl_themes",'*',$condition);

			if(!empty($check_name[0]->theme_id)){
				echo json_encode(array("success"=>false, 'msg'=>'Theme Name Already Present!'));
				exit;
			}
			
			$thumnail_value = "";
			if(isset($_FILES) && isset($_FILES["themefile"]["name"])){
				 $config = array();
				$config['upload_path'] = DOC_ROOT_FRONT."/images/theme_image/";
				$config['max_size']    = '0';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['min_width']            = 1000;
                $config['min_height']           = 1000;
				// $config['allowed_types'] = '*';
				// $config['file_name']     = md5(uniqid("100_ID", true));
				$config['file_name']     = $_FILES["themefile"]["name"];
				$this->load->library('upload', $config);
				if (!$this->upload->do_upload("themefile")){
					$image_error = array('error' => $this->upload->display_errors());
					echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
					exit;
				}else{
					$image_data = array('upload_data' => $this->upload->data());
					$thumnail_value = $image_data['upload_data']['file_name']; 
					$this->setCompresseredImage($image_data['upload_data']['file_name']);

				}	
				/* Unlink previous category image */
				if(!empty($this->input->post('theme_id'))){	
					$condition_image = " theme_id = ".$this->input->post('theme_id');
					$image =$this->common->getData("tbl_themes",'*',$condition_image);
					if(is_array($image) && !empty($image[0]->thumbnail_image) && file_exists(DOC_ROOT_FRONT."/images/theme_image/".$image[0]->thumbnail_image))
					{
						unlink(DOC_ROOT_FRONT."/images/theme_image/".$image[0]->thumbnail_image);
					}
				}
			}else{
				$thumnail_value = $this->input->post('pre_themefile_name');
			}

			$data['thumbnail_image'] = $thumnail_value;
			$data['theme_name'] = $this->input->post('themename');
			$data['featured_theme'] = $this->input->post('featured-theme');
			$data['status'] = $this->input->post('theme-status');
			$data['main_video'] = $this->input->post('mainvideo');
			$data['theme_description'] = $this->input->post('theme_description');
			$data['meta_title'] = $this->input->post('meta_title');
			$data['meta_description'] = $this->input->post('meta_description');
			$data['updated_on'] = date("Y-m-d H:i:s");
			$data['updated_by'] = $this->session->userdata('footage_farm_admin')[0]['user_id'];
			if(!empty($this->input->post('theme_id'))){
				// print_r($data);
				$condition = "theme_id = '".$this->input->post('theme_id')."' ";
				$result = $this->common->updateData("tbl_themes",$data,$condition);
				if($result){
					echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
					exit;
				}
				else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while updating data.'));
					exit;
				}
			}else{
				$data['created_on'] = date("Y-m-d H:i:s");
				$data['created_by'] =  $this->session->userdata('footage_farm_admin')[0]['user_id'];
				$result = $this->common->insertData('tbl_themes',$data,'1');
				if(!empty($result)){
					echo json_encode(array('success'=>true, 'msg'=>'Record Added Successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
					exit;
				}
			}
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem While Add/Edit Data.'));
			exit;
		}
	}
	public function setCompresseredImage($image_name){
		$config1 = array();
		$this->load->library('image_lib');
		$config1['image_library'] = 'gd2';
		$config1['source_image'] = DOC_ROOT_FRONT."/images/theme_image/".$image_name;
		$config1['maintain_ratio'] = TRUE;
		$config1['quality'] = '100%';
		$config1['width'] = 1000;
		$config1['height'] = 1000;
		$config1['new_image'] = DOC_ROOT_FRONT."/images/theme_image/".$image_name;
		$this->image_lib->initialize($config1);
		$this->image_lib->resize();
		$this->image_lib->clear();
		}

	
		public function selected_theme($id){
		$result = array();
	
		$condition = "t.theme_id = $id ";
		$main_table = array("tbl_themes as t", array("t.theme_name"));
		$join_tables =  array();
		$join_tables = array(
		          array("", "tbl_sub_themes as  sb", "t.theme_id = sb.theme_id", array("sb.*")),
		          );
		$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("t.theme_id" => "DESC"),"",null);
		$sub_themes = $this->common->MySqlFetchRow($rs, "array");

		foreach ($sub_themes as $key => $value) {
			$condition = "1=1 AND sub_theme_id = ".$value['sub_theme_id'];
			$total_reel = $this->common->getData("tbl_video_theme_subtheme_mapping",'count(video_id) as total_reel',$condition,"","","video_id");
			$sub_themes[$key]['total_reals'] =count($total_reel);
		}

		$result['sub_themes'] = $sub_themes; 
		$result['count_themes'] = count($sub_themes);
				
		//echo "<pre>";print_r($result);exit;

		$this->load->view('header');
		$this->load->view('selected-theme.php',$result);
		$this->load->view('footer');

	}

}
?>