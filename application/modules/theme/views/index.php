
<!-- Selected Reels Header -->
<section class="page-container selected-items-wrapper">
	<section class="page-section">
		<div class="black-bg"></div>
		<div class="container">
			<div class="col-sm-12">
				<div class="selected-items">
					<div class="selected-items-left">
						<div class="selected-items-count">
							<span id="count-checked-checkboxes">0</span> Themes Selected
						</div>
						<a href="#/" class="select-all">Select All</a>
						<a href="#/" class="deselect-all">Deselect All</a>
					</div>
					<div class="selected-items-right">
						<?php if(!$this->session->userdata('footage_farm_user')) { ?>
							<a href="#/" class="addtofavourite-reel" data-fancybox="" data-src="#signin-content" data-options="{&quot;touch&quot; : false}">
						<?php }else{?>
							<a href="Javascript:void(0)" class="addtofavourite-reel" onclick="addToFav('multiple')">
						<?php }?>
							<span>Add to Favourite</span>
							<img src="<?= base_url()?>images/favourite-white.svg" alt="">
						</a>
					
						<a href="#/" class="selectedheader-more">
							<img src="<?= base_url()?>images/more-icon.svg" alt="">
							<div class="dropdown-menu loggedin-flyout selectedheader-flyout">
								<a class="dropdown-item select-all" href="#">Select All</a>
								<a class="dropdown-item deselect-all" href="#">Deselect All</a>
								<?php if($this->session->userdata('footage_farm_user')) {?>
									<a href="Javascript:void(0)" class="addtofavourite-reel" onclick="addToFav('multiple')">
								<?php }else{?>
									<a href="#/" class="dropdown-item" data-fancybox="" data-src="#signin-content" data-options="{&quot;touch&quot; : false}">
								<?php } ?>	Add to Favourite</a>
							</div>
						</a>
						<a href="#/" class="close-selected-header"><img src="<?= base_url()?>images/remove.svg" alt=""></a>
					</div>
				</div>

			</div>
		</div>
	</section>
</section>
<!-- Selected Reels Header -->	
<!-- Innerpage Search -->
<section class="page-container innerpage-top">
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="searchbar-top">
		<div class="home-search-wrapper">
			<form class="home-search search-form" action="">
				<input type="text" list="browsers" placeholder="Search for footages and themes" name="search" autocomplete="off">
				  <datalist id="browsers">
				    
				  </datalist>
				<button type="submit"><img src="images/search-icon.svg" alt=""></button>
			</form>
		</div>
	</div>
</section>
<!-- Innerpage Search -->
<section class="page-container">
	<!-- Mobile Search -->
	<div class="multicolored-line multicolored-line-mobile">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="home-search-wrapper search-wrapper-mobile">
		<form class="home-search" action="">
			<input type="text" placeholder="Search for footages and themes" name="home-search">
			<button type="submit"><img src="images/search-icon.svg" alt=""></button>
		</form>
	</div>
	<!-- Mobile Search -->
	<!-- Innerpage Banner -->
	<section class="page-section mb-0">
		<div class="innerpage-banner">
			<div class="innerpage-banner-grey-bg full-height"></div>
			<div class="container">
				<div class="row">
					<div class="col-12">
						<h1 class="page-title-large text-uppercase">All Themes</h1>
					</div>
					<div class="col-12">
						<div class="ff-filters allthemes-filters filters-left">
							<div class="allthemes-filter-single">
								<select name="theme_name" id="theme_id" class="niceselect theme_filter">
									<option value="">Themes</option>
										<?php
										foreach ($theme_data as $key => $value) {
										 ?>
										 <option value="<?=$value['theme_id'] ?>"><?=$value['theme_name'] ?></option>
										<?php 
											}
										 ?>
								
								</select>
							</div>
							<div class="allthemes-filter-single">
								<select name="country" id="country_id" class="niceselect theme_filter">
									<option value="" >Country</option>
									<?php
									foreach ($country_list as $key => $value) {
									 ?>
									 <option value="<?=$value['country_id'] ?>"><?=$value['country_name'] ?></option>
									<?php 
										}
									 ?>
								
								</select>
							</div>
							<div class="allthemes-filter-single">
								<select name="year" id="year" class="niceselect theme_filter">
									<option value="" >Year</option>
									<?php
									foreach ($year_data as $key => $value) {
									 ?>
									 <option value="<?=$value ?>"><?=$value ?></option>
									<?php 
										}
									 ?>
									
								</select>
							</div>
							<div class="allthemes-filter-single">
								<select name="color" id="color" class="niceselect theme_filter">
									<option value="" >B/W Color</option>
									<?php
									foreach ($color_data as $key => $value) {
									 ?>
									 <option value="<?=$value ?>"><?=$value ?></option>
									<?php 
										}
									 ?>
									
								</select>
							</div>
							<div class="allthemes-filter-single">
								<select name="sound" id="sound" class="niceselect theme_filter">
									<option value="" >Sound</option>

									<?php
									foreach ($sound_data as $key => $value) {
									 ?>
									 <option value="<?=$value ?>"><?=$value ?></option>
									<?php 
										}
									 ?>
									 
								
								</select>
							</div>
						</div>
						<div class="ff-filters allthemes-filters filters-right">
							<p>Sort By</p>
							<div class="allthemes-filter-single">
								<select name="view" id="view" class="niceselect theme_filter">
									<option value="Latest">Latest</option>
									<option value="Alphabetical">Alphabetical</option>
									<option value="Mostviewed">Popular</option>
									<option value="DESC">Recently Modified</option>
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Innerpage Banner -->
	<!-- Themes -->
	<section class="page-section pt-40">
		<div class="theme-select-overlay"></div>
		<div class="container">
			<div class="row" id="themedata">
			
			</div>
		</div>
	</section>
	<!-- Themes -->
</section>

</body>

</html>

<script type="text/javascript">
$(document).ready(function(){
	var duration = $("#durationid").val();
	$("#duration").val(duration);

	var type = $("#typeid").val();
	$("#type").val(type);

	$(document).on("click",".themeview",function () {
        var themecount = $(this).attr("data-themeid");
        var csrfName   = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
	    var csrfHash   = $('.txt_csrfname').val(); // CSRF hash
		$.ajax({
			url: "<?= base_url('theme/themeview')?>",
			type: "POST",
			data:{ themeview: themecount, [csrfName]: csrfHash },
			dataType: "json",
			success: function(response){
				
			}
		}); 
	      
	});

	$(document).on("change",".theme_filter",function () {
        var theme_id = $('#theme_id').val();
        var country  = $('#country_id').val();
        var year     = $('#year').val();
        var color    = $('#color').val();
        var sound    = $('#sound').val();
        var view     = $('#view').val();

        var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
	    var csrfHash = $('.txt_csrfname').val(); // CSRF hash
		$.ajax({
			url: "<?= base_url('theme/themedata')?>",
			type: "POST",
			data:{ id: theme_id, country: country, year: year, color: color, sound: sound, view: view, [csrfName]: csrfHash },
			dataType: "json",
			success: function(response){
				if(response.success){
					
				$("#themedata").html(response.data);	
					
				}else{
				$("#themedata").html('');
				}
			}
		});

	      //window.location = "<?= base_url('theme/selected_theme/')?>"+theme_id;
	    
	      
	});

});

$(window).on( "load", function() { 
	var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
	var csrfHash = $('.txt_csrfname').val(); // CSRF hash
		$.ajax({
			url: "<?= base_url('theme/themedata')?>",
			type: "POST",
			data:{ [csrfName]: csrfHash },
			dataType: "json",
			success: function(response){
				if(response.success){
					
				$("#themedata").html(response.data);	
					
				}else{
				$("#themedata").html('');
				}
			}
		});
		

});

	// add to fav shiv code on 22-10-20202
	function addToFav(fav_type,video_id){
		var selected_themes = [];
			if(fav_type == 'multiple'){
			var selected_themes = $('.cls_checkbox:checked').map(function () {
			return this.value;
			}).get();
		}else{
			selected_themes.push(video_id);
		}
		console.log(selected_themes);
		var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
		var csrfHash = $('.txt_csrfname').val(); // CSRF hash
		if(selected_themes){
				$.ajax({
					url: "<?= base_url('theme/addToFav')?>",
					type: "POST",
					data:{ selected_themes , [csrfName]: csrfHash },
					dataType: "json",
					success: function(response){
						if(response.success){
							$('.notification-message').fadeIn();		
							$('.notification-modal-text').text('You saved this reel as your favourites!')
							setTimeout(function(){
								$('.notification-message').fadeOut();	 
								}, 2000);	
						}else{
							$('.notification-message').fadeIn();		
							$('.notification-modal-text').text(response.msg)
							setTimeout(function(){
								$('.notification-message').fadeOut();	 
								}, 2000);	
						}
					}
				});
			}
	}
</script>