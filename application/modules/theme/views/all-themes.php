<!-- Selected Reels Header -->
<section class="page-container selected-items-wrapper">
	<section class="page-section">
		<div class="black-bg"></div>
		<div class="container">
			<div class="col-sm-12">
				<div class="selected-items">
					<div class="selected-items-left">
						<div class="selected-items-count">
							<span id="count-checked-checkboxes">0</span> Themes Selected
						</div>
						<a href="#/" class="select-all">Select All</a>
						<a href="#/" class="deselect-all">Deselect All</a>
					</div>
					<div class="selected-items-right">
						<a href="#/" class="addtofavourite-reel">
							<span>Add to Favourite</span>
							<img src="images/favourite-white.svg" alt="">
						</a>
						<a href="#/" data-fancybox data-src="#selectprojects-content" data-options='{"touch" : false}' class="addtoproject-reel">
							<span>Add to Project</span>
							<img src="images/project-white.svg" alt="">
						</a>
						<a href="#/" class="selectedheader-more">
							<img src="images/more-icon.svg" alt="">
							<div class="dropdown-menu loggedin-flyout selectedheader-flyout">
								<a class="dropdown-item select-all" href="#">Select All</a>
								<a class="dropdown-item deselect-all" href="#">Deselect All</a>
								<a class="dropdown-item" href="#">Add to Favourite</a>
								<a class="dropdown-item" href="#">Add to Project</a>
							</div>
						</a>
						<a href="#/" class="addtocart-reel">
							<span>Add to Cart</span>
							<img src="images/cart-white.svg" alt="">
						</a>
						<a href="#/" class="close-selected-header"><img src="images/remove.svg" alt=""></a>
					</div>
				</div>

			</div>
		</div>
	</section>
</section>
<!-- Selected Reels Header -->
<!-- Innerpage Search -->
<section class="page-container innerpage-top">
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="searchbar-top">
		<div class="home-search-wrapper">
			<form class="home-search" action="">
				<input type="text" placeholder="Search for footages and themes" name="home-search">
				<button type="submit"><img src="<?=FRONT_URL?>/images/theme_image/search-icon.svg" alt=""></button>
			</form>
		</div>
	</div>
</section>
<!-- Innerpage Search -->
<section class="page-container">
	<!-- Mobile Search -->
	<div class="multicolored-line multicolored-line-mobile">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="home-search-wrapper search-wrapper-mobile">
		<form class="home-search" action="">
			<input type="text" placeholder="Search for footages and themes" name="home-search">
			<button type="submit"><img src="images/search-icon.svg" alt=""></button>
		</form>
	</div>
	<!-- Mobile Search -->
	<!-- Innerpage Banner -->
	<section class="page-section mb-0">
		<div class="innerpage-banner">
			<div class="innerpage-banner-grey-bg full-height"></div>
			<div class="container">
				<div class="row">
					<div class="col-12">
						<h1 class="page-title-large text-uppercase">All Themes</h1>
					</div>
					<div class="col-12">
						<div class="ff-filters allthemes-filters filters-left">
							<div class="allthemes-filter-single">
								<select name="" id="" class="niceselect">
									<option value="">Themes</option>
									<option value="Early Reel (423)">Early Reel (423)</option>
									<option value="Personalities (34)">Personalities (34)</option>
									<option value="Educational Flims (18)">Educational Flims (18)</option>
									<option value="Hungarian Revolution (45)">Hungarian Revolution (45)</option>
									<option value="Space Exploration (12)">Space Exploration (12)</option>
									<option value="Russian Revolution (3)">Russian Revolution (3)</option>
									<option value="World War I (131)">World War I (131)</option>
									<option value="World War II (343)">World War II (343)</option>
									<option value="1960’s Civil Right Movements (7)">1960’s Civil Right Movements (7)</option>
									<option value="Home Movies (81)">Home Movies (81)</option>
								</select>
							</div>
							<div class="allthemes-filter-single">
								<select name="" id="" class="niceselect">
									<option value="">Country</option>
									<option value="England">England</option>
									<option value="Germany">Germany</option>
									<option value="Italy">Italy</option>
									<option value="France">France</option>
									<option value="India">India</option>
									<option value="England">England</option>
									<option value="Germany">Germany</option>
									<option value="Italy">Italy</option>
									<option value="France">France</option>
									<option value="India">India</option>
								</select>
							</div>
							<div class="allthemes-filter-single">
								<select name="" id="" class="niceselect">
									<option value="">Year</option>
									<option value="1920">1920</option>
									<option value="1921">1921</option>
									<option value="1922">1922</option>
									<option value="1923">1923</option>
									<option value="1924">1924</option>
									<option value="1925">1925</option>
									<option value="1926">1926</option>
									<option value="1927">1927</option>
									<option value="1928">1928</option>
									<option value="1929">1929</option>
								</select>
							</div>
							<div class="allthemes-filter-single">
								<select name="" id="" class="niceselect">
									<option value="">B/W Color</option>
									<option value="B/W">B/W</option>
									<option value="Color">Color</option>
								</select>
							</div>
							<div class="allthemes-filter-single">
								<select name="" id="" class="niceselect">
									<option value="">Sound</option>
									<option value="WAV Format">WAV Format</option>
									<option value="AIFF Format">AIFF Format</option>
									<option value="MP3 Format">MP3 Format</option>
									<option value="AAC Format">AAC Format</option>
									<option value="WMA Format">WMA Format</option>
								</select>
							</div>
						</div>
						<div class="ff-filters allthemes-filters filters-right">
							<p>Sort By</p>
							<div class="allthemes-filter-single">
								<select name="" id="" class="niceselect">
									<option value="Latest">Latest</option>
									<option value="Most Viewed">Most Viewed</option>
									<option value="Popular">Popular</option>
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Innerpage Banner -->
	<!-- Themes -->
	<section class="page-section pt-40">
		<div class="theme-select-overlay"></div>
		<div class="container">
			<div class="row">				
				<div class="col-12 col-md-6 col-lg-4">
					<div class="block-single theme-single">
						<a href="#/">
							<h3>Theme Name Will Come Here Like Theme Name Will Come Here Like</h3>
							<div class="block-info">
								<div>
									<p class="block-info-title">Sub-themes</p>
									<p class="block-info-content">287</p>
								</div>
								<div>
									<p class="block-info-title">Total Reels</p>
									<p class="block-info-content">6467</p>
								</div>
							</div>							
						</a>
						<a href="#/">
							<div class="hover-options-container">
								<div class="box-single-checkbox">
									<label class="checkbox-container">
										<input type="checkbox">
										<span class="checkmark"></span>
									</label>
								</div>
								<div class="box-single-share">
									<span class="share-single share-favourite addtofavourite-reel"></span>
									<span class="share-msg-text share-favourite-text">
										Add to Favourite
									</span>
									<span class="share-single share-social" data-fancybox data-src="#sharetheme-content" data-options='{"touch" : false}'></span>
									<span class="share-msg-text share-social-text">
										Share
									</span>									
								</div>
								<img src="https://via.placeholder.com/377x212.png" alt="" class="img-fluid">
							</div>
						</a>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-4">
					<div class="block-single theme-single">
						<a href="#/">
							<h3>Theme Name Will Come Here Like Theme Name Will Come Here Like</h3>
							<div class="block-info">
								<div>
									<p class="block-info-title">Sub-themes</p>
									<p class="block-info-content">287</p>
								</div>
								<div>
									<p class="block-info-title">Total Reels</p>
									<p class="block-info-content">6467</p>
								</div>
							</div>							
						</a>
						<a href="#/">
							<div class="hover-options-container">
								<div class="box-single-checkbox">
									<label class="checkbox-container">
										<input type="checkbox">
										<span class="checkmark"></span>
									</label>
								</div>
								<div class="box-single-share">
									<span class="share-single share-favourite addtofavourite-reel"></span>
									<span class="share-msg-text share-favourite-text">
										Add to Favourite
									</span>
									<span class="share-single share-social" data-fancybox data-src="#sharetheme-content" data-options='{"touch" : false}'></span>
									<span class="share-msg-text share-social-text">
										Share
									</span>									
								</div>
								<img src="https://via.placeholder.com/377x212.png" alt="" class="img-fluid">
							</div>
						</a>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-4">
					<div class="block-single theme-single">
						<a href="#/">
							<h3>Theme Name Will Come Here Like Theme Name Will Come Here Like</h3>
							<div class="block-info">
								<div>
									<p class="block-info-title">Sub-themes</p>
									<p class="block-info-content">287</p>
								</div>
								<div>
									<p class="block-info-title">Total Reels</p>
									<p class="block-info-content">6467</p>
								</div>
							</div>							
						</a>
						<a href="#/">
							<div class="hover-options-container">
								<div class="box-single-checkbox">
									<label class="checkbox-container">
										<input type="checkbox">
										<span class="checkmark"></span>
									</label>
								</div>
								<div class="box-single-share">
									<span class="share-single share-favourite addtofavourite-reel"></span>
									<span class="share-msg-text share-favourite-text">
										Add to Favourite
									</span>
									<span class="share-single share-social" data-fancybox data-src="#sharetheme-content" data-options='{"touch" : false}'></span>
									<span class="share-msg-text share-social-text">
										Share
									</span>									
								</div>
								<img src="https://via.placeholder.com/377x212.png" alt="" class="img-fluid">
							</div>
						</a>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-4">
					<div class="block-single theme-single">
						<a href="#/">
							<h3>Theme Name Will Come Here Like Theme Name Will Come Here Like</h3>
							<div class="block-info">
								<div>
									<p class="block-info-title">Sub-themes</p>
									<p class="block-info-content">287</p>
								</div>
								<div>
									<p class="block-info-title">Total Reels</p>
									<p class="block-info-content">6467</p>
								</div>
							</div>							
						</a>
						<a href="#/">
							<div class="hover-options-container">
								<div class="box-single-checkbox">
									<label class="checkbox-container">
										<input type="checkbox">
										<span class="checkmark"></span>
									</label>
								</div>
								<div class="box-single-share">
									<span class="share-single share-favourite addtofavourite-reel"></span>
									<span class="share-msg-text share-favourite-text">
										Add to Favourite
									</span>
									<span class="share-single share-social" data-fancybox data-src="#sharetheme-content" data-options='{"touch" : false}'></span>
									<span class="share-msg-text share-social-text">
										Share
									</span>									
								</div>
								<img src="https://via.placeholder.com/377x212.png" alt="" class="img-fluid">
							</div>
						</a>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-4">
					<div class="block-single theme-single">
						<a href="#/">
							<h3>Theme Name Will Come Here Like Theme Name Will Come Here Like</h3>
							<div class="block-info">
								<div>
									<p class="block-info-title">Sub-themes</p>
									<p class="block-info-content">287</p>
								</div>
								<div>
									<p class="block-info-title">Total Reels</p>
									<p class="block-info-content">6467</p>
								</div>
							</div>							
						</a>
						<a href="#/">
							<div class="hover-options-container">
								<div class="box-single-checkbox">
									<label class="checkbox-container">
										<input type="checkbox">
										<span class="checkmark"></span>
									</label>
								</div>
								<div class="box-single-share">
									<span class="share-single share-favourite addtofavourite-reel"></span>
									<span class="share-msg-text share-favourite-text">
										Add to Favourite
									</span>
									<span class="share-single share-social" data-fancybox data-src="#sharetheme-content" data-options='{"touch" : false}'></span>
									<span class="share-msg-text share-social-text">
										Share
									</span>									
								</div>
								<img src="https://via.placeholder.com/377x212.png" alt="" class="img-fluid">
							</div>
						</a>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-4">
					<div class="block-single theme-single">
						<a href="#/">
							<h3>Theme Name Will Come Here Like Theme Name Will Come Here Like</h3>
							<div class="block-info">
								<div>
									<p class="block-info-title">Sub-themes</p>
									<p class="block-info-content">287</p>
								</div>
								<div>
									<p class="block-info-title">Total Reels</p>
									<p class="block-info-content">6467</p>
								</div>
							</div>							
						</a>
						<a href="#/">
							<div class="hover-options-container">
								<div class="box-single-checkbox">
									<label class="checkbox-container">
										<input type="checkbox">
										<span class="checkmark"></span>
									</label>
								</div>
								<div class="box-single-share">
									<span class="share-single share-favourite addtofavourite-reel"></span>
									<span class="share-msg-text share-favourite-text">
										Add to Favourite
									</span>
									<span class="share-single share-social" data-fancybox data-src="#sharetheme-content" data-options='{"touch" : false}'></span>
									<span class="share-msg-text share-social-text">
										Share
									</span>									
								</div>
								<img src="https://via.placeholder.com/377x212.png" alt="" class="img-fluid">
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Themes -->
</section>
<!-- No Projects -->
<div style="display: none;" id="noprojects-content" class="ff-modal">
	<div class="modal-header">
		<h3>Add to a Project</h3>
	</div>
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="ff-modal-content">
		<div class="modal-content-wrapper">
			<div class="modal-project-thumbnail">
				<img src="https://via.placeholder.com/360x460.png" alt="" class="modal-project-thumb">
			</div>
			<div class="modal-project-details">
				<div class="modal-create-new-project modal-section">
					<a href="#/" class="modal-create-btn">
						<img src="images/plus-black.svg" alt=""> Create New Project
					</a>
				</div>
				<div class="modal-no-projects-msg modal-section">
					<p class="title">No Projects yet!</p>
					<p class="msg">Projects that you create will show up here.</p>
				</div>
				<div class="modal-addnew-project modal-section">
					<form>
						<div class="form-group">
							<label for="nameofproject">Name of Project</label>
							<input type="text" class="form-control ff-field-dark" id="nameofproject" name="nameofproject">
						</div>
						<div class="form-group">
							<label for="projectdesc">Description (optional)</label>
							<textarea class="form-control ff-field-dark" rows="5"></textarea>
						</div>
					</form>
				</div>
			</div>
			<div class="modal-actions modal-addnew-project">
				<input type="submit" class="btn-ff btn-secondary-dark text-uppercase modal-cancel-newproject" value="Cancel">
				<input type="submit" class="btn-ff btn-primary-dark text-uppercase" value="Create">
			</div>
		</div>
	</div>
</div>
<!-- No Projects -->
<!-- Select Projects -->
<div style="display: none;" id="selectprojects-content" class="ff-modal">
	<div class="modal-header">
		<h3>Add to a Project</h3>
	</div>
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="ff-modal-content">
		<div class="modal-content-wrapper">
			<div class="modal-project-thumbnail">
				<img src="https://via.placeholder.com/360x460.png" alt="" class="modal-project-thumb">
			</div>
			<div class="modal-project-details">
				<div class="modal-create-new-project modal-section">
					<a href="#/" class="modal-create-btn">
						<img src="images/plus-black.svg" alt=""> Create New Project
					</a>
				</div>
				<div class="modal-existing-projects modal-section">
					<div class="mep-search">
						<img src="images/search-black.svg" alt="Search">
						<input type="text" placeholder="Search" id="mep-search">
					</div>
					<div class="mep-single">
						<div class="hover-options-container">
							<div class="mep-box-single-checkbox">
								<label class="checkbox-container">
									<input type="checkbox">
									<span class="checkmark"></span>
								</label>
							</div>
						</div>
						<div class="mep-single-overlay"></div>
						<img src="https://via.placeholder.com/460x82.png" alt="" class="img-fluid">
						<p class="mep-single-title"> War Montage Ref </p>
						<p class="mep-single-count">15 Reels</p>
					</div>
					<div class="mep-single">
						<div class="hover-options-container">
							<div class="mep-box-single-checkbox">
								<label class="checkbox-container">
									<input type="checkbox">
									<span class="checkmark"></span>
								</label>
							</div>
						</div>
						<div class="mep-single-overlay"></div>
						<img src="https://via.placeholder.com/460x82.png" alt="" class="img-fluid">
						<p class="mep-single-title"> Pearl Harbour </p>
						<p class="mep-single-count">15 Reels</p>
					</div>
					<div class="mep-single">
						<div class="hover-options-container">
							<div class="mep-box-single-checkbox">
								<label class="checkbox-container">
									<input type="checkbox">
									<span class="checkmark"></span>
								</label>
							</div>
						</div>
						<div class="mep-single-overlay"></div>
						<img src="https://via.placeholder.com/460x82.png" alt="" class="img-fluid">
						<p class="mep-single-title"> Dunkirk </p>
						<p class="mep-single-count">15 Reels</p>
					</div>
					<div class="mep-single">
						<div class="hover-options-container">
							<div class="mep-box-single-checkbox">
								<label class="checkbox-container">
									<input type="checkbox">
									<span class="checkmark"></span>
								</label>
							</div>
						</div>
						<div class="mep-single-overlay"></div>
						<img src="https://via.placeholder.com/460x82.png" alt="" class="img-fluid">
						<p class="mep-single-title"> War Montage Ref </p>
						<p class="mep-single-count">15 Reels</p>
					</div>
					<div class="mep-single">
						<div class="hover-options-container">
							<div class="mep-box-single-checkbox">
								<label class="checkbox-container">
									<input type="checkbox">
									<span class="checkmark"></span>
								</label>
							</div>
						</div>
						<div class="mep-single-overlay"></div>
						<img src="https://via.placeholder.com/460x82.png" alt="" class="img-fluid">
						<p class="mep-single-title"> Pearl Harbour </p>
						<p class="mep-single-count">15 Reels</p>
					</div>
					<div class="mep-single">
						<div class="hover-options-container">
							<div class="mep-box-single-checkbox">
								<label class="checkbox-container">
									<input type="checkbox">
									<span class="checkmark"></span>
								</label>
							</div>
						</div>
						<div class="mep-single-overlay"></div>
						<img src="https://via.placeholder.com/460x82.png" alt="" class="img-fluid">
						<p class="mep-single-title"> Dunkirk </p>
						<p class="mep-single-count">15 Reels</p>
					</div>
				</div>
				<!-- New Project -->
				<div class="modal-addnew-project modal-section">
					<form>
						<div class="form-group">
							<label for="nameofproject">Name of Project</label>
							<input type="text" class="form-control ff-field-dark" id="nameofproject" name="nameofproject">
						</div>
						<div class="form-group">
							<label for="projectdesc">Description (optional)</label>
							<textarea class="form-control ff-field-dark" rows="5"></textarea>
						</div>
					</form>
				</div>
			</div>
			<div class="modal-actions modal-addnew-project">
				<input type="submit" class="btn-ff btn-secondary-dark text-uppercase modal-cancel-newproject" value="Cancel">
				<input type="submit" class="btn-ff btn-primary-dark text-uppercase" value="Create">
			</div>
			<!-- New Project -->
			<div class="modal-actions modal-existing-actions">
				<input type="submit" data-fancybox-close class="btn-ff btn-secondary-dark text-uppercase modal-cancel-newproject" value="Cancel">
				<input type="submit" class="btn-ff btn-primary-dark text-uppercase" value="Save">
			</div>
		</div>
	</div>
</div>
<!-- Select Projects -->
<!-- Share Theme/Reel -->
<div style="display: none;max-width:580px;" id="sharetheme-content" class="ff-modal">
	<div class="modal-header">
		<h3>Share a Reel</h3>
	</div>
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="ff-modal-content">
		<form class="share-reel-form">
			<div class="modal-section">
				<div class="form-group">
					<input type="text" class="form-control ff-field-dark linkfield" id="reelink" name="reelink" placeholder="Theme/Reel link will be shown here">
					<a href="#/" class="copylink">Copy Link</a>
				</div>
				<div class="form-group">
					<label for="social">Social</label>
					<div class="modal-social-share">
						<a href="#/"><img src="images/twitter-black.svg" alt="Share on Twitter"></a>
						<a href="#/"><img src="images/facebook-black.svg" alt="Share on Facebook"></a>
						<a href="#/"><img src="images/linkedin-black.svg" alt="Share on Linkedin"></a>
					</div>
				</div>
				<div class="form-group">
					<label for="sendtoemail">Send to Email Address</label>
					<input type="text" class="form-control ff-field-dark" id="sendtoemail" name="sendtoemail" required>
					<span class="emailaddress">niraali@bokapp.design <a href="#/"><img src="images/close-black.svg" alt=""></a></span>
					<span class="emailaddress">gonzalo.c@footagefarm.com <a href="#/"><img src="images/close-black.svg" alt=""></a></span>
					<span class="emailaddress">gintare.k@footagefarm.com <a href="#/"><img src="images/close-black.svg" alt=""></a></span>
					<span class="emailaddress">mat.f@footagefarm.com <a href="#/"><img src="images/close-black.svg" alt=""></a></span>
				</div>
			</div>
			<div class="modal-content-wrapper">
				<div class="modal-actions">
					<input type="submit" data-fancybox-close class="btn-ff btn-secondary-dark text-uppercase" value="Cancel">
					<input type="submit" class="btn-ff btn-primary-dark text-uppercase" value="Send">
				</div>
			</div>
		</form>
	</div>
</div>
<!-- Share Theme/Reel -->
<!-- Request Enquiry Master -->
<div style="display: none;max-width:700px;" id="enquiry-master-content" class="ff-modal">
	<div class="modal-header">
		<h3>Request Enquiry</h3>
	</div>
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="ff-modal-content">
		<form class="enquiry-master-form">
			<div class="modal-section">

				<div class="form-group">
					<div class="reel-title">
						<img src="https://via.placeholder.com/147x83.png" alt="">
						<h3>1910 - 1918 - WWI Era: Aerials; German Officers & Troops; France Fighting; Trans-Caucasia Winter</h3>
					</div>
				</div>
				<div class="form-group">
					<div class="request-reel-type">
						<label for="selectreeltype">Please select the reel type</label>
						<label class="radio-container-theme">
							<span class="radio-label">Request Preview</span>
							<input type="radio" name="reeltype" required>
							<span class="radio"></span>
						</label>
						<label class="radio-container-theme">
							<span class="radio-label">Request Master</span>
							<input type="radio" name="reeltype">
							<span class="radio"></span>
						</label>
					</div>
					<label for="reeltype" class="error"></label>
				</div>
				<div class="form-group">
					<label for="inouttime">Specify the footage's in and out time</label>
					<div class="begin-end-tc">
						<div class="tc-time begin-tc">
							<label for="begintc">Begin TC
								<a href="#" class="tooltip-info">
									<img src="images/info.svg" alt="">
									<span class="tooltip-msg">
										Footage start time
									</span>
								</a>
							</label>
							<input type="text" class="form-control ff-field-dark" id="begintc" name="begintc" placeholder="00:00:00" required>
						</div>
						<div class="tc-time end-tc">
							<label for="endtc">End TC
								<a href="#" class="tooltip-info">
									<img src="images/info.svg" alt="">
									<span class="tooltip-msg">
										Footage end time
									</span>
								</a>
							</label>
							<input type="text" class="form-control ff-field-dark" id="endtc" name="endtc" placeholder="00:00:00" required>
						</div>
					</div>
					<div id="addmore-begin-end-tc"></div>
					<div class="form-addmore">
						<a href="#/" class="form-addmore-link tc-addmore-link"><img src="images/add-black.svg" alt=""> <span>Add another in and out time codes</span></a>
					</div>
				</div>
				<div class="form-group">
					<label>Do you have a file with multiple in and out time codes?</label>
					<p>Add jpegs, excel or word files here. Each file size should be under 2MB.</p>
					<div class="uploaded-files-list">
						<div class="file-single">
							<img src="images/jpg.svg" alt="" class="file-type-icon">
							<span>Screenshota_qe52gknknf.jpg</span>
							<a href="#/"><img src="images/close.svg" alt="" class="file-remove-icon"></a>
						</div>
						<div class="file-single">
							<img src="images/doc.svg" alt="" class="file-type-icon">
							<span>datasheet-with-timecodes.doc</span>
							<a href="#/"><img src="images/close.svg" alt="" class="file-remove-icon"></a>
						</div>
						<div class="file-single">
							<img src="images/xls.svg" alt="" class="file-type-icon">
							<span>animate-sys.xls</span>
							<a href="#/"><img src="images/close.svg" alt="" class="file-remove-icon"></a>
						</div>
					</div>
					<!-- Created using dropzone.js-->
					<div id="upload-reel-times">
						<img src="images/upload-file.svg" alt="">
						<p>Drag and drop here or browse</p>
						<a href="#/" class="login-link">Browse</a>
					</div>
					<!-- Created using dropzone.js-->
				</div>
				<div class="form-group">
					<div class="format-framerate">
						<div class="format formatframe">
							<label for="begintc">Format</label>
							<select class="niceselect ff-select-dark formatselect" required>
								<option value="">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
							</select>
							<label id="formatselect-error" class="error" for=""></label>
						</div>
						<div class="framerate formatframe">
							<label for="endtc">Frame Rate</label>
							<select class="niceselect ff-select-dark framerateselect" required>
								<option value="">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
							</select>
							<label id="framerateselect-error" class="error" for=""></label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="codec">Codec
						<a href="#" class="tooltip-info">
							<img src="images/info.svg" alt="">
							<span class="tooltip-msg">
								Codec type
							</span>
						</a>
					</label>
					<input type="text" class="form-control ff-field-dark" id="codec" name="codec" placeholder="QT ProRes422HQ" required>
				</div>
				<div class="form-group">
					<label for="projectdesc">Message (optional)</label>
					<textarea class="form-control ff-field-dark" rows="5"></textarea>
				</div>
			</div>
			<div class="modal-content-wrapper">
				<div class="modal-actions">
					<input type="submit" data-fancybox-close class="btn-ff btn-secondary-dark text-uppercase" value="Cancel">
					<input type="submit" class="btn-ff btn-primary-dark text-uppercase" value="Send Request">
				</div>
			</div>
		</form>
	</div>
</div>
<!-- Request Enquiry Master -->