
<?php
if(!empty($featured_theme_data)){
foreach ($featured_theme_data as $key => $value) {
	if ($value['total_reals'] > 0) { ?>
<div class="col-12 col-md-6 col-lg-4">
	<div class="block-single theme-single themeview" data-themeid="<?= $value['theme_id']?>">
		<a href="<?= base_url('subtheme/'.$value['theme_id'])?>">
			<h3><?= $value['theme_name']?></h3>
			<div class="block-info">
				<div>
					<p class="block-info-title">Sub-themes</p>
					<p class="block-info-content"><?= $value['totol_sub_themes']?></p>
				</div>
				<div>
					<p class="block-info-title">Total Reels</p>
					<p class="block-info-content"><?= $value['total_reals']?></p>
				</div>
			</div>							
		</a>
		<a href="javascript:void(0)">
			<div class="hover-options-container">
				<div class="box-single-checkbox">
					<label class="checkbox-container">
						<!-- <input type="checkbox"> -->
						<input type="checkbox" class="cls_checkbox"  value="<?= $value['theme_id']?>" name="reels[]"id="">
						<span class="checkmark"></span>
					</label>
				</div>
				<div class="box-single-share">
					<!-- <span class="share-single share-favourite addtofavourite-reel"></span>
					<span class="share-msg-text share-favourite-text">
						Add to Favourite
					</span> -->
					<span class="share-single share-favourite addtofavourite-reel" onclick="addToFav('single',<?=$value['theme_id']?>)"></span>			
					<span class="share-msg-text share-favourite-text">
						Add to Favourite
					</span>
					<span class="share-single share-social reelshare" data-fancybox data-src="#sharetheme-content" data-url="<?= base_url('subtheme/'.$value['theme_id'])?>" data-options='{"touch" : false}'></span>
					<span class="share-msg-text share-social-text">
						Share
					</span>									
				</div>
				<img src="<?= base_url('images/theme_image/'.$value['thumbnail_image'])?>" alt="<?= $value['theme_name']?>" class="img-fluid">
			</div>
		</a>
	</div>
</div>

<?php } }
	} ?>

<script>
	$(document).ready(function() {
		/*Notification messages*/
		$('.addtofavourite-reel').click(function() {
			$('.notification-message').fadeIn();
			$('.notification-modal-text').text('You saved this subtheme as your favourites!')
			setTimeout(function() {
				$('.notification-message').fadeOut();
			}, 2000);
		});
		$('.select-all').click(function() {
			$('.notification-message').fadeIn();
			$('.notification-modal-text').text('You selected all themes!')
			setTimeout(function() {
				$('.notification-message').fadeOut();
			}, 2000);
		});
		$('.deselect-all').click(function() {
			$('.notification-message').fadeIn();
			$('.notification-modal-text').text('You deselected all themes!')
			setTimeout(function() {
				$('.notification-message').fadeOut();
			}, 2000);
		});
		/*Notification messages*/	
	});
</script>