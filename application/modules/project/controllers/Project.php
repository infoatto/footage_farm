<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Project extends MX_Controller
{
	
	public function __construct(){
		parent::__construct();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('projectmodel','',TRUE);
	}

	public function index(){

	}

	public function submitForm(){
		// print_r($_POST);exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();
		if ($_POST['submit']=='Create') {
			if(empty($this->input->post('nameofproject'))){
				echo json_encode(array('success'=>false, 'msg'=>'Please Enter project Name first'));
				exit;
			}

			$condition = '';
			$condition = "project_name = '".trim($this->input->post('nameofproject'))."' && user_id = ".$this->session->userdata('footage_farm_user')[0]['user_id']." ";

			$project_exist  = $this->common->getData("tbl_projects",'*',$condition);
			
			if($project_exist){
				echo json_encode(array('success'=>false, 'msg'=>'Project name already exist please create diffrent one'));
				exit;
			}

			$data['project_name'] = trim($this->input->post('nameofproject'));
			$data['project_description'] = $this->input->post('projectdesc');
			$data['project_date'] = date("Y-m-d");
			$data['user_id'] = @$this->session->userdata('footage_farm_user')[0]['user_id'];
			$data['shared_by_user_id'] = @$this->session->userdata('footage_farm_user')[0]['user_id'];
			$data['status'] = 'Active';
			$data['updated_on'] = date("Y-m-d H:i:s");
			$data['updated_by'] = @$this->session->userdata('footage_farm_user')[0]['user_id'];
			$data['created_on'] = date("Y-m-d H:i:s");
			$data['created_by'] = @$this->session->userdata('footage_farm_user')[0]['user_id'];
			$result = $this->common->insertData('tbl_projects',$data,'1');
			$project_id = $result;
			// foreach ($_POST['reel'] as $key => $value) {
			// 	$data_reel = array();
			// 	$data_reel['project_id'] = $project_id ;
			// 	$data_reel['reel_id'] = $value ;
			// 	$result_data_reel = $this->common->insertData('tbl_project_reel',$data_reel,'1');
			// }
			if(!empty($result)){
				echo json_encode(array('success'=>true, 'msg'=>'Project created successfully'));
				exit;
			}else{
				echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
				exit;
			}
		
		}else{
			foreach ($_POST['projectcheck'] as $key => $value) {
				foreach ($_POST['reel'] as $key1 => $reel) {
					$data_reel = array();
					$where = array('project_id'=>$value, 'reel_id'=>$reel);
					$getreel = $this->common->getData(" tbl_project_reel",'*',$where);
					if (!empty($getreel)) {
							$data_reel['updated_on'] = date("Y-m-d H:i:s");
							$result_data_reel = $this->common->updateData("tbl_project_reel",$data_reel,$where);
						}else{
							$data_reel['project_id'] = $value ;
							$data_reel['reel_id'] = $reel ;
							$data_reel['created_on'] = date("Y-m-d H:i:s");
							$result_data_reel = $this->common->insertData('tbl_project_reel',$data_reel,'1');	
					}
				}
			}
			if(!empty($result_data_reel)){
				echo json_encode(array('success'=>true, 'msg'=>'Record Added Successfully.'));
				exit;
			}else{
				echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
				exit;
			}

		}

		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem While Add/Edit Data.'));
			exit;
		}
	}

	public function projectdata(){

		$result = array();
		$condition = "1=1 AND p.status = 'Active' ";
		$main_table = array("tbl_projects as p", array("p.*"));
		$join_tables =  array();
		$join_tables = array(
		          array("left", "tbl_project_reel as  pr", "p.project_id = pr.project_id", array("count(pr.project_reel_id) as totol_project_reel")),
		          );

		$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("p.project_id" => "DESC"),"p.project_id",null); 
		 // fetch query
		$project_data = $this->common->MySqlFetchRow($rs, "array"); // fetch result
		//echo '<pre>';print_r($project_data);die;
		$result['project_data'] = $project_data;		
		$projectsponse = $this->load->view('project-design',$result,true);	
		
		if (!empty($projectsponse)) {
			print_r(json_encode(array('success'=>true, 'data'=> $projectsponse)));
			exit;
		}else{
			print_r(json_encode(array('success'=>false, 'data'=> $projectsponse)));
			exit;
			}

	}

	public function submitRename(){
		// print_r($_POST);exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();
			if(empty($this->input->post('nameofproject'))){
				echo json_encode(array('success'=>false, 'msg'=>'Please Enter project Name first'));
				exit;
			}

			$condition = "project_name = '".trim($this->input->post('nameofproject'))."' && user_id = ".$this->session->userdata('footage_farm_user')[0]['user_id']." && project_id != '".$this->input->post('id')."' ";

			$project_exist  = $this->common->getData("tbl_projects",'*',$condition);
			
			if($project_exist){
				echo json_encode(array('success'=>false, 'msg'=>'Project name already exist please create diffrent one'));
				exit;
			}

			$data['project_name'] = trim($this->input->post('nameofproject'));
			$data['updated_on'] = date("Y-m-d H:i:s");
			$condition = "project_id = '".$this->input->post('id')."' ";
			$result = $this->common->updateData("tbl_projects",$data,$condition);
			if($result){
				echo json_encode(array('success'=>true, 'msg'=>'Project Rename successfully'));
				exit;
			}else{
				echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
				exit;
			}

		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem While Add/Edit Data.'));
			exit;
		}
	}
		
}
?>