<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog  extends MX_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('blogmodel','',TRUE);
		parent::__construct();
		
	}

	function _remap($method_name){
		if(!method_exists($this, $method_name)){
			$this->index();
		}else{
		$this->{$method_name}();
		}
	}

	public function index()
	{
		// get about us 
		$condition = "1=1 AND status = 'Active' ";
		$result['blog_categoy'] = $this->common->getData("tbl_blog_tags",'*',$condition);
		// $blog_name = $this->uri->segment(2);
		if($this->uri->segment(2)){
			// $condition = "1=1 AND status = 'Active' AND blog_tag_name = '".$this->uri->segment(2)."' ";
			
			// $blog_id = $this->common->getData("tbl_blog_tags",'blog_tag_id',$condition);
			// $result['blog_id'] = $blog_id[0]['blog_tag_id'];
			$result['blog_id'] = $this->uri->segment(2);
		}else{
			$result['blog_id'] = 'null';	
		}

		//get blog data
		// $condition = "1=1 AND b.status= 'Active' ";
		// $main_table = array("tbl_blogs as b", array("b.*"));
		// $join_tables =  array();
		// $join_tables = array(
		// 					array("", "tbl_blog_tag_mapping as  bt", "bt.blog_id = b.blog_id"),
		// 					array("", "tbl_blog_tags as  t", "t.blog_tag_id = bt.tag_id", array("group_concat(t.blog_tag_name) as tag_name,group_concat(t.blog_tag_id) as tag_id")),
		// 					array("", "tbl_users as  u", "u.user_id = b.author_id", array("user_name as author_name")));
	
		// $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("b.blog_id" => "DESC"),"b.blog_id",null); 
		//  // fetch query
		//  $result['blog_data']= $this->common->MySqlFetchRow($rs, "array"); // fetch result
			// echo $this->db->last_query
		// echo "<pre>";
		// print_r($result);
		// exit;

		$result['meta_description'] = "blog description ";
		$result['meta_keywords'] = "blog keywords";
		$this->load->view('header',$result);
		$this->load->view('index',$result);
		$this->load->view('footer');
	}

	public function blogData(){
		// print_r($_POST);
		// exit;
		//get blog data
		$condition = "1=1 AND b.status= 'Active' ";
		$oderby = array("b.created_by" => "ASC");
		if (isset($_POST['id']) && $_POST['id']!='') {
			$id = $this->input->post('id');
			$condition .= " AND t.blog_tag_id = $id";
		}
		if (isset($_POST['view']) && $_POST['view']!='') {
			$view = $this->input->post('view');
			if ($view=='DESC') {
				$oderby = array("b.blog_id" => "DESC");
			}
			if ($view=='Mostviewed') {
				$oderby = array("b.blog_view" => "DESC");
			}
			
		}

		$main_table = array("tbl_blogs as b", array("b.*"));
		$join_tables =  array();
		$join_tables = array(
							array("", "tbl_blog_tag_mapping as  bt", "bt.blog_id = b.blog_id"),
							array("", "tbl_blog_tags as  t", "t.blog_tag_id = bt.tag_id", array("group_concat(t.blog_tag_name) as tag_name,group_concat(t.blog_tag_id) as tag_id")),
							array("", "tbl_users as  u", "u.user_id = b.author_id", array("user_name as author_name")));

		$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, $oderby,"b.blog_id",null); 
		// fetch query
		$result['blog_data']= $this->common->MySqlFetchRow($rs, "array"); // fetch result
			// echo $this->db->last_query
		//echo "<pre>";print_r($result);exit;

		$blogresponse = $this->load->view('blog-design.php',$result,true);
		if (!empty($blogresponse)) {
			print_r(json_encode(array('success'=>true, 'data'=> $blogresponse)));
			exit;
		}else{
			print_r(json_encode(array('success'=>false, 'data'=> $blogresponse)));
			exit;
			}

	}

	public function blogview(){
		$id = $this->input->post('blog_id');
		$sql = "SELECT blog_view FROM tbl_blogs WHERE blog_id = $id";
		$res = $this->db->query($sql);
		$result = $res->row_array();
		$count = ($result['blog_view']+1);

		$update = $this->common->updateData("tbl_blogs",array('blog_view'=>$count), array('blog_id'=>$id));
		if ($update) {
			echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
				exit;
		}
	}

}

?>