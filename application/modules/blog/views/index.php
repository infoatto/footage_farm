
<!-- Innerpage Search -->
<section class="page-container innerpage-top">
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="searchbar-top">
		<div class="home-search-wrapper">
			<form class="home-search search-form" action="">
				<input type="text" list="browsers" placeholder="Search for footages and themes" name="search" autocomplete="off">
				  <datalist id="browsers">
				    
				  </datalist>
				<button type="submit"><img src="images/search-icon.svg" alt=""></button>
			</form>
		</div>
	</div>
</section>
<!-- Innerpage Search -->
<section class="page-container">
	<!-- Mobile Search -->
	<div class="multicolored-line multicolored-line-mobile">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="home-search-wrapper search-wrapper-mobile">
		<form class="home-search" action="">
			<input type="text" placeholder="Search for footages and themes" name="home-search">
			<button type="submit"><img src="images/search-icon.svg" alt=""></button>
		</form>
	</div>
	<!-- Mobile Search -->
	<!-- Innerpage Banner -->
	<section class="page-section mb-0">
		<div class="innerpage-banner pbmob-25">
			<div class="innerpage-banner-grey-bg full-height"></div>
			<div class="container">
				<div class="row">
					<div class="col-12">
						<h1 class="page-title-large text-uppercase">Blog</h1>
					</div>
					<div class="col-12">
						<div class="ff-filters allthemes-filters filters-left">
							<div class="allthemes-filter-single">
								<select name="blog" id="blog_id" class="niceselect blog_filter">
									<option value="">Category</option>
								<?php
									foreach ($blog_categoy as $key => $value) {?>
										<option value="<?= $value['blog_tag_id']?>"><?= $value['blog_tag_name']?></option>
									<?php }
								?> 
								</select>
							</div>
						</div>
						<div class="ff-filters allthemes-filters filters-right blog-filter-right">
							<p>Sort By</p>
							<div class="allthemes-filter-single">
								<select name="mview" id="mview" class="niceselect blog_filter">
									<option value="All">All</option>
									<option value="Mostviewed">Most Popular</option>
									<option value="DESC">Recently Added</option>
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Innerpage Banner -->
	<!-- Blog Listing -->
	<section class="page-section pt-40">
		<div class="theme-select-overlay"></div>
		<div class="container">
			<div class="row" id="blogdata">
			
			</div>
		</div>
	</section>
	<!-- Blog Listing -->
</section>
<!-- Share Blog Post -->
<div style="display: none;max-width:580px;" id="sharetheme-content" class="ff-modal">
	<div class="modal-header">		
		<h3>Share Blog</h3>		
	</div>
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="ff-modal-content">
		<form class="share-reel-form">
			<div class="modal-section">			
					<div class="form-group">					
						<input type="text" class="form-control ff-field-dark linkfield" id="reelink" name="reelink" placeholder="Blog link will be shown here">
						<a href="#/" class="copylink">Copy Link</a>						
					</div>
					<div class="form-group">
						<label for="social">Social</label>
						<div class="modal-social-share">
							<a href="#/"><img src="images/twitter-black.svg" alt="Share on Twitter"></a>
							<a href="#/"><img src="images/facebook-black.svg" alt="Share on Facebook"></a>
							<a href="#/"><img src="images/linkedin-black.svg" alt="Share on Linkedin"></a>				
						</div>
					</div>
					<div class="form-group">
						<label for="sendtoemail">Send to Email Address</label>
						<input type="text" class="form-control ff-field-dark" id="sendtoemail" name="sendtoemail" required>		
						<span class="emailaddress">niraali@bokapp.design <a href="#/"><img src="images/close-black.svg" alt=""></a></span>
						<span class="emailaddress">gonzalo.c@footagefarm.com <a href="#/"><img src="images/close-black.svg" alt=""></a></span>
						<span class="emailaddress">gintare.k@footagefarm.com <a href="#/"><img src="images/close-black.svg" alt=""></a></span>
						<span class="emailaddress">mat.f@footagefarm.com <a href="#/"><img src="images/close-black.svg" alt=""></a></span>						 		
					</div>	
			</div>
			<div class="modal-content-wrapper">			
				<div class="modal-actions">
					<input type="submit" data-fancybox-close class="btn-ff btn-secondary-dark text-uppercase"  value="Cancel">
					<input type="submit" class="btn-ff btn-primary-dark text-uppercase" value="Send">
				</div>
			</div>
		</form>		
	</div>
</div>
<!-- Share Blog Post -->
<script type="text/javascript">

getblogData();
$(document).ready(function(){
	$(document).on("click",".blogview",function () {
        var blogid   = $(this).attr("data-blogid");
        var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
	    var csrfHash = $('.txt_csrfname').val(); // CSRF hash
		$.ajax({
			url: "<?= base_url('blog/blogview')?>",
			type: "POST",
			data:{ blog_id: blogid, [csrfName]: csrfHash },
			dataType: "json",
			success: function(response){
				
			}
		}); 
	      
	});
});

function getblogData(){
	var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
	var csrfHash = $('.txt_csrfname').val(); // CSRF hash
		$.ajax({
			url: "<?= base_url('blog/blogData')?>",
			type: "POST",
			data:{ [csrfName]: csrfHash },
			dataType: "json",
			success: function(response){
				if(response.success){
					
				$("#blogdata").html(response.data);	
					
				}else{
				$("#blogdata").html('');
				}
			}
		});

}

	 $(document).on("change",".blog_filter",function () {
	     var blog_id = $('#blog_id').val();
	     var mview = $('#mview').val();

	    var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
	    var csrfHash = $('.txt_csrfname').val(); // CSRF hash
		$.ajax({
			url: "<?= base_url('blog/blogData')?>",
			type: "POST",
			data:{ id: blog_id, view: mview, [csrfName]: csrfHash },
			dataType: "json",
			success: function(response){
				if(response.success){
					
				$("#blogdata").html(response.data);	
					
				}else{
				$("#blogdata").html('');
				}
			}
		});
	      
	 });

</script>

<script>
$(document).ready(function(){
	/*Subtheme video select*/
	if($(window).width()>991){			
		$('.blog-single a').hover(function () {

			$(this).children('.hover-options-container').children('.box-single-checkbox').show();
			$(this).children('.hover-options-container').children('.box-single-share').show();

		}, function () {

			$(this).children('.hover-options-container').children('.box-single-checkbox').hide();
			$(this).children('.hover-options-container').children('.box-single-share').hide();

			if ($(this).children('.hover-options-container').children('.box-single-checkbox').children('label.checkbox-container').children('input[type="checkbox"]').is(':checked')) {

				$(this).children('.hover-options-container').children('.box-single-checkbox').addClass('selected');
				$(this).children('.hover-options-container').addClass('selected');

			} else {

				$(this).children('.hover-options-container').children('.box-single-checkbox').removeClass('selected');
				$(this).children('.hover-options-container').removeClass('selected');

			}
		});
	}
	/*Subtheme video select*/
})
</script>
</body>
</html>