<?php if(!empty($blog_data)){
	foreach ($blog_data as $key => $value) {?>
<div class="col-12 col-md-6 col-lg-4">
	<div class="blog-single blogview" data-blogid="<?= $value['blog_id']?>">
		<a href="<?= base_url('blog_detail/'.$value['blog_slug'])?>">
			<div class="blog-title-date">
				<h3><?= $value['blog_title']?></h3>
				<div class="date-info">
				<?php $date_convert = date('Y-M-d',strtotime($value['blog_date']));
					$date_convert_array = explode('-',$date_convert); ?>
					<span class="date-number"><?= $date_convert_array[2]?></span>
					<span class="month"><?= $date_convert_array[1]?></span>
					<span class="year"><?= $date_convert_array[0]?></span>
				</div>
			</div>
			<div class="blog-author">
				<div class="arrow-right-dark-small"></div>
				<p><?= $value['author_name']?></p>
			</div>
		</a>
		<div class="blog-tags">
	<?php $all_tags = explode(",",$value['tag_name']);
		$all_tag_id = explode(",",$value['tag_id']);
		foreach ($all_tags as $tagkey => $tagvalue) {?>
			<a href="<?= base_url('blog/'.$all_tag_id[$tagkey])?>"><?= $tagvalue?></a>
		<?php }?>
		</div>
		<a href="<?= base_url('blog_detail/'.$value['blog_slug'])?>">
			<img src="<?= base_url('images/blog_main_image/'.$value['thumbnail_image']);?>" alt="" class="img-fluid">
		</a>
	</div>
</div>
<?php } }  ?>