
<!-- Innerpage Search -->
<section class="page-container innerpage-top">
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="searchbar-top">
		<div class="home-search-wrapper">
			<form class="home-search" action="">
				<input type="text" placeholder="Search for footages and themes" name="home-search">
				<button type="submit"><img src="images/search-icon.svg" alt=""></button>
			</form>
		</div>
	</div>
</section>
<!-- Innerpage Search -->
<section class="page-container">
	<!-- Mobile Search -->
	<div class="multicolored-line multicolored-line-mobile">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="home-search-wrapper search-wrapper-mobile">
		<form class="home-search" action="">
			<input type="text" placeholder="Search for footages and themes" name="home-search">
			<button type="submit"><img src="images/search-icon.svg" alt=""></button>
		</form>
	</div>
	<!-- Mobile Search -->
	<!-- Innerpage Banner -->
	<section class="page-section mb-0">
		<div class="innerpage-banner innerpage-banner-transparent pt-50 pb-0">
			<div class="innerpage-banner-grey-bg full-height"></div>
			<div class="container">
				<div class="row">
					<div class="col-12 no-padding-mobile">													
						<h1 class="page-title-large pp-title text-uppercase">Terms and Conditions</h1>	
						<div class="profile-tabs-wrapper editprofile-tabs-wrapper">
							<div class="tabs">
								<ul class="tabs__items">
									<li><a href="#/" class="tnc-link active">Terms & Conditions</a></li>
									<li><a href="#/" class="pp-link">Privacy Policy</a></li>	
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Innerpage Banner -->
	<!-- Terms & Conditions Section -->
	<section class="page-section pt-50 pb-50 ptmob-25 pbmob-25" id="terms-conditions">
		<div class="container">
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 offset-lg-2 col-lg-8">
					<div class="tnc-wrapper">
						<?= $term_conditions[0]['terms_conditions']; ?>
						<div class="info-msg-wrapper">
							<img src="images/informartion.svg" alt="">
							<span>This policy was last modified on <?= date("M, d Y",strtotime($term_conditions[0]['created_on']))?></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Terms & Conditions Section -->
	<!-- Privacy Policy Section -->
	<section class="page-section pt-50 pb-50 ptmob-25 pbmob-25" id="privacy-policy">
		<div class="container">		
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 offset-lg-2 col-lg-8">
				<div class="tnc-wrapper">
						<?= $privacy_policy[0]['privacy_policy']?>
						<div class="info-msg-wrapper">
							<img src="images/informartion.svg" alt="">
							<span>This policy was last modified on <?= date("M, d Y",strtotime($privacy_policy[0]['created_on']))?></span>
						</div>
					</div>
				</div>
			</div>			
		</div>
	</section>
	<!-- Privacy Policy Section -->
</section>
<script>
	$(document).ready(function() {
		$('.tnc-link').click(function() {
			$('#privacy-policy').hide();
			$('#terms-conditions').show();
			$('.pp-title').text('Terms & Conditions');
		})
		$('.pp-link').click(function() {
			$('#terms-conditions').hide();
			$('#privacy-policy').show();
			$('.pp-title').text('Privacy Policy');
		})
		$('.tabs__items li a').click(function() {
			$('.tabs__items li a').removeClass('active');
			$(this).addClass('active');
		})	
	});
</script>
</body>

</html>