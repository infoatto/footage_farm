<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Terms_condition  extends MX_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('terms_conditionmodel','',TRUE);
		parent::__construct();
		
	}

	public function index()
	{
		 
		$condition = "1=1 ";
		$result['term_conditions'] = $this->common->getData("tbl_terms_conditions",'*',$condition);
		$result['privacy_policy'] = $this->common->getData("tbl_privacy_policy",'*',$condition);

		// echo "<pre>";
		// print_r($result);
		// exit;	
		$this->load->view('header',$result);
		$this->load->view('index',$result);
		$this->load->view('footer');
	}
}?>