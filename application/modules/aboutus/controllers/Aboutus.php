<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aboutus  extends MX_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('aboutusmodel','',TRUE);
		parent::__construct();
		
	}

	public function index()
	{
		// get banner for about us 
		$condition = "1=1 AND banner_id = 1 ";
		$result['banner_data'] = $this->common->getData("tbl_banner",'*',$condition);

		// get about us team members
		$condition = "1=1 AND status = 'Active'";
		$result['team_members'] = $this->common->getDataLimit("tbl_meet_team",'*',$condition,"meetteam_id","desc",'3');

		 //get about us details
		$condition = "1=1 ";
		$result['aboutus_data'] = $this->common->getData("tbl_about_us",'*',$condition);

		$result['meta_description'] = $result['aboutus_data'][0]['meta_description'];
		$result['meta_keywords'] = $result['aboutus_data'][0]['meta_title'];

		// echo "<pre>";
		// print_r($result);
		// exit;	
		$this->load->view('header',$result);
		$this->load->view('index',$result);
		$this->load->view('footer');
	}
}
?>