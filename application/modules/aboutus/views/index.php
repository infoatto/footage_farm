
<!-- Innerpage Search -->
<section class="page-container innerpage-top">
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="searchbar-top">
		<div class="home-search-wrapper">
			<form class="home-search search-form" action="">
				<input type="text" list="browsers" placeholder="Search for footages and themes" name="search" autocomplete="off">
				  <datalist id="browsers">
				
				  </datalist>
				<button type="submit"><img src="images/search-icon.svg" alt=""></button>
			</form>
		</div>
	</div>
</section>
<!-- Innerpage Search -->
<section class="page-container">
	<!-- Mobile Search -->
	<div class="multicolored-line multicolored-line-mobile">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="home-search-wrapper search-wrapper-mobile">
		<form class="home-search" action="">
			<input type="text" placeholder="Search for footages and themes" name="home-search">
			<button type="submit"><img src="<?= base_url()?>assets/images/search-icon.svg" alt=""></button>
		</form>
	</div>
	<!-- Mobile Search -->
	<!-- About Us Banner -->
	<section class="page-section mb-0">		
		<div class="innerpage-banner innerpage-banner-transparent pt-50 pb-75 ptmob-25 pbmob-25">
			<div class="innerpage-banner-grey-bg"></div>
			<div class="container">
				<div class="row">
					<div class="col-12 col-sm-12 col-md-12 offset-lg-1 col-lg-10">
						<h1 class="page-title-large home-title"><?= $aboutus_data[0]['banner_heading']?></h1>						
					</div>
				
					<div class="col-12 col-sm-12 col-md-12 offset-lg-1 col-lg-10">
						<div class="home-banner-video">
							<div class="banner-top-vimeo-wrapper banner-aboutus-video">
								<!-- <iframe src="https://player.vimeo.com/video/185819357?byline=0&portrait=0" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="banner-top-vimeo">
								</iframe> -->
								<?php if(!empty($aboutus_data[0]['vimeo_link']) && isset($aboutus_data[0]['vimeo_link'])){?>
									<iframe src="<?= $aboutus_data[0]['vimeo_link']?>" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="banner-top-vimeo">
									</iframe>
								<?php }else{?>
											<img src="<?= base_url('images/aboutus/'.$aboutus_data[0]['banner_image'])?>" alt="banner Image">
								<?php } ?>
							</div>
							<div class="clearfix"></div>
							<script src="https://player.vimeo.com/api/player.js"></script>							
						</div>
					</div>
				
				</div>
			</div>			
		</div>				
	</section>
	<!-- About Us Banner -->	
	<section class="page-section mb-45">
		<div class="container">
			<div class="row aboutus-info-wrapper">
				<div class="col-12 order-2 order-md-1 col-sm-5 col-md-5 offset-lg-1 col-lg-5">					
					<img src="<?= (!empty($aboutus_data[0]['section1_image'])?base_url('images/aboutus/'.$aboutus_data[0]['section1_image']):"https://via.placeholder.com/466x478.png")?>" alt="" class="aboutus-info-img img-fluid">
				</div>
				<div class="col-12 order-1 order-md-2  col-sm-7 col-md-7 col-lg-5">		
					<div class="aboutus-info-content">
						<h1 class="page-title-small"><?= $aboutus_data[0]['section1_heading']?></h1>	
						<p><?= $aboutus_data[0]['section1_paragraph']?></p>			
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="page-section">
		<div class="container">			
			<div class="row counter">
				<div class="col-12">
					<div class="homepage-counter-border aboutus-counter-border">&nbsp;</div>
				</div>
				<div class="col-12 col-sm-4">
					<div class="counter-single first">
						<h2 class="counter-number"><?= $aboutus_data[0]['stat_one_no']?></h2>
						<div class="counter-info">
							<div class="arrow-right-dark"></div>
							<p><?= $aboutus_data[0]['stat_one_desc']?></p>
						</div>

					</div>
				</div>
				<div class="col-12 col-sm-4">
					<div class="counter-single">
						<h2 class="counter-number"><?= $aboutus_data[0]['stat_two_no']?></h2>
						<div class="counter-info">
							<div class="arrow-right-dark"></div>
							<p><?= $aboutus_data[0]['stat_two_desc']?></p>
						</div>
					</div>
				</div>
				<div class="col-12 col-sm-4">
					<div class="counter-single last">
						<h2 class="counter-number"><?= $aboutus_data[0]['stat_three_no']?></h2>
						<div class="counter-info">
							<div class="arrow-right-dark"></div>
							<p><?= $aboutus_data[0]['stat_three_desc']?></p>
						</div>
					</div>
				</div>
				<div class="col-12">
					<div class="homepage-counter-border aboutus-counter-border">&nbsp;</div>
				</div>
			</div>
		</div>
	</section>
	<section class="page-section about-timespan-wrapper">
		<div class="about-timespan-bg"></div>
		<img src="<?= base_url('images/aboutus/'.$aboutus_data[0]['section2_image'])?>" alt="" class="about-timespan-bg-img-mob img-fluid">
		<img src="<?= base_url('images/aboutus/'.$aboutus_data[0]['section2_image'])?>" alt="" class="about-timespan-bg-img">
		<div class="about-content-bg"></div>
		<div class="container">
			<div class="row about-timespan-content">				
				<div class="col-12 col-sm-12 col-md-12 offset-lg-1 col-lg-9">	
					<h1 class="page-title-large">
						<?= $aboutus_data[0]['section2_heading']?>
					</h1>								
					<p><?= $aboutus_data[0]['section2_paragraph']?></p>					
				</div>
			</div>			
		</div>				
	</section>

<?php if(!empty($team_members) && isset($team_members)) {?>
	<section class="page-section">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="multicolored-line">
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
					</div>
					<div class="section-title-wrapper">
						<h2 class="section-title">
							<img src="images/section-headline.svg" alt=""> Meet the Team
						</h2>						
					</div>
				</div>
					<?php foreach ($team_members as $key => $value) {?>
						<div class="col-12 col-sm-4">
							<div class="block-single team">
								<h3><?= $value['member_heading']?></h3>
								<div class="team-email">
									<img src="images/email-black.svg" alt="">
									<p><?= $value['member_email']?></p>
								</div>
								<img src="<?= (!empty($value['thumbnail_image'])?base_url('images/team_member/'.$value['thumbnail_image']):"https://via.placeholder.com/375x282.png")?>" alt="" class="img-fluid img-team">
							</div>
						</div>
					<?php }?>
			</div>
		</div>
	</section>
<?php }?>
	<!-- How We Operate -->	
</section>
</body>
</html>