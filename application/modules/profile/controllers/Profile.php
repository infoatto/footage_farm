<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Profile extends MX_Controller
{
	
	public function __construct(){
		parent::__construct();
		checklogin();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('profilemodel','',TRUE);
		$this->load->model('video/videomodel','',TRUE);

	}

	public function index(){

		// echo "<pre>";
		// print_r($this->session->userdata('footage_farm_user'));
		// exit;
		$result = array();
		$condition = "1=1 AND user_id = ".$this->session->userdata('footage_farm_user')[0]['user_id']." ";
		$result['profile_data'] = $this->common->getData("tbl_users",'*',$condition);
		//get  enum value
		$result['roles'] = $this->common->getData("tbl_roles","*");


		$condition = "1=1 AND status = 'Active' ";
		$result['blog_categoy'] = $this->common->getData("tbl_blog_tags",'*',$condition);

		//get fav data
		// $condition = "user_id = ".$this->session->userdata('footage_farm_user')[0]['user_id']." ";
		// $main_table = array("tbl_favourites as f", array("f.video_id"));
		// $join_tables =  array();
		// $join_tables = array(
		// 					array("", "tbl_videos as  v", "v.video_id = f.video_id", array("v.*")),
		// 					array("left", "tbl_video_tag_mapping as  tm", "tm.video_id = v.video_id", array()),
		// 					array("left", "tbl_tags as  t", "t.tag_id = tm.tag_id", array("group_concat(t.tag_name) as tag_name")));
	
		// $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("f.video_id" => "DESC"),"f.video_id",null); 
		//  // fetch query
		//  $result['fav_reel_details']= $this->common->MySqlFetchRow($rs, "array"); // fetch result

		// //  get project name accourding to user wise

		// $condition = "user_id = ".$this->session->userdata('footage_farm_user')[0]['user_id']." ";
		// $main_table = array("tbl_projects as p", array("p.project_id, p.project_name, p.project_date "));
		// $join_tables =  array();
		// $join_tables = array(
		// 					array("", "tbl_project_reel as pr", "pr.project_id = p.project_id", array()),
		// 					array("", "tbl_videos as  v", "v.video_id = pr.reel_id", array("v.vimeo_url, v.thumbnail_image"))
		// 				);
	
		// $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("p.project_id" => "DESC"),"p.project_id",null); 
		//  // fetch query
		// $result['project_detail']= $this->common->MySqlFetchRow($rs, "array"); // fetch result


		$condition = "cd.enquiry_status IN ('Purchased','Closed') AND cd.user_id = ".$this->session->userdata('footage_farm_user')[0]['user_id']." ";
		$main_table = array("tbl_video_enquiry_details as cd", array("cd.*"));
		$join_tables =  array();
		$join_tables = array(
							array("", "tbl_video_enquiry_tc as  ct", "ct.video_id = cd.video_id", array("begin_tc,end_tc")),
							array("", "tbl_videos as v", "v.video_id = cd.video_id", array("v.video_title,v.reel_number")),
						);
	
		$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("cd.video_enquiry_detail_id" => "DESC"),"",null); 
		 // fetch query
		$result['Purchase_data']= $this->common->MySqlFetchRow($rs, "array"); // fetch result
		//echo "<pre>";print_r($result['Purchase_data']);exit;


		
		$this->load->view('header.php');
		$this->load->view('myprofile',$result);
		$this->load->view('footer.php');
	}

	public function project_detail(){
		$result = array();
		if(!empty($_GET['text']) && isset($_GET['text'])){
			$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
			parse_str($varr,$url_prams);
			$project_id = $url_prams['id'];

		$where = "project_id = '".$project_id."' ";
		$result['project_data'] = $this->common->getData("tbl_projects",'project_id, project_name, project_description',$where);

		$condition = "1=1 AND p.project_id = '".$project_id."' ";
		$main_table = array("tbl_project_reel as p", array("p.project_reel_id"));
		$join_tables =  array();
		$join_tables = array(
		          array("", "tbl_videos as v", "v.video_id = p.reel_id", array("v.*")),
		          array("", "tbl_video_tag_mapping as  vt", "vt.video_id = v.video_id"),
				  array("", "tbl_tags as  t", "t.tag_id = vt.tag_id", array("group_concat(t.tag_name) as tag_name,group_concat(t.tag_id) as tag_id")),
		          );

		$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("p.project_reel_id" => "DESC"),"p.project_reel_id",null); 
		 // fetch query
		$project_data = $this->common->MySqlFetchRow($rs, "array"); // fetch result

		$result['total_reals']  = count($project_data);
		$result['video_data']  = $project_data;
		}
		
		//echo "<pre>";print_r($result);exit;
		$this->load->view('header.php');
		$this->load->view('selected-project',$result);
		$this->load->view('footer.php');
	}

	public function project_detail_reel(){
		$result = array();

		$order_by = 'v.created_by';
		$order = 'ASC';
		if($_POST['type'] == 'All'){
			$order_by = "v.created_by";
			$order = "ASC";
		}elseif ($_POST['type'] == 'DESC') {
			$order_by = "v.updated_on";
			$order = "desc";
		}elseif ($_POST['type'] == 'Alphabetical') {
			$order_by = "v.video_title";
			$order = "ASC";
		}

		$project_id = $_POST['projectid'];
		$condition = "1=1 AND p.project_id = '".$project_id."' ";
		$main_table = array("tbl_project_reel as p", array("p.project_reel_id"));
		$join_tables =  array();
		$join_tables = array(
		          array("", "tbl_videos as v", "v.video_id = p.reel_id", array("v.*")),
		          array("", "tbl_video_tag_mapping as  vt", "vt.video_id = v.video_id"),
				  array("", "tbl_tags as  t", "t.tag_id = vt.tag_id", array("group_concat(t.tag_name) as tag_name,group_concat(t.tag_id) as tag_id")),
		          );

		$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array($order_by => $order),"p.project_reel_id",null); 
		 // fetch query
		$project_data = $this->common->MySqlFetchRow($rs, "array"); // fetch result
		$result['video_data']  = $project_data;		
		$projectsponse = $this->load->view('project-reel',$result,true);	
		
		if (!empty($projectsponse)) {
			print_r(json_encode(array('success'=>true, 'data'=> $projectsponse)));
			exit;
		}else{
			print_r(json_encode(array('success'=>false, 'data'=> $projectsponse)));
			exit;
			}
		
		
	}

	public function addEdit(){

		$user_id = "";
		//print_r($_GET);
		$result = array();
		if(!empty($_GET['text']) && isset($_GET['text'])){
			$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
			parse_str($varr,$url_prams);
			$user_id = $url_prams['id'];
			$condition = " user_id ='".$user_id."' ";
			$result['theme_data'] = $this->common->getData("tbl_users",'*',$condition);
		}
		
		// echo "<pre>";
		// print_r($result);
		// exit;
		//echo $user_id;
		$this->load->view('main-header.php');
		$this->load->view('addEdit.php',$result);
		$this->load->view('footer.php');
	}
		
	public function submitFormMember(){

		// print_r($_POST);
		// exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();
			$condition =" role_id = ".$_POST['role_id']." ";
			$get_usertype = $this->common->getData("tbl_roles","*",$condition);
			$data['user_type'] = $get_usertype[0]['role_name'];
			$data['role_id'] = $this->input->post('role_id');
			$data['user_name'] = $this->input->post('member_username');
			$data['email_id'] = $this->input->post('member_email');
			$data['password'] = md5('123456');
			
			$data['created_on'] = date("Y-m-d H:i:s");
			$data['created_by'] = $this->session->userdata('footage_farm_user')[0]['user_id'];

			$result = $this->common->insertData('tbl_users',$data,'1');
			if($result){

				$this->send_mail( $get_usertype[0]['role_name'],$this->input->post('member_username'),$this->input->post('member_email'));
				echo json_encode(array('success'=>true, 'msg'=>'Record inserted Successfully.'));
				exit;
			}else{
				echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
				exit;
			}
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem While Add/Edit Data.'));
			exit;
		}
	}

	public function send_mail($role_type,$username,$email_id){
		$result = array();
		$result['user_type']= $role_type;
		$result['username']=  $username;
		$result['email_id']= $email_id ;
		$result['password']= '123456' ;
	
		$member_mail_hmtl = $this->load->view('member-mail',$result,true);
		$this->email->clear();
		$this->email->from(FROM_EMAIL); // change it to yours
		$this->email->to($email_id); // change it to yours
		$this->email->subject("Enrollement in Footage farm");
		$this->email->message($member_mail_hmtl);
		$this->email->send();

	}


	public function submitForm(){
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();
			$condition = "email_id = '".$this->input->post('useremail')."' ";
			if($this->input->post('user_id') && $this->input->post('user_id') > 0){
				$condition .= " AND  user_id != ".$this->input->post('user_id')." ";
			}			
			$check_name = $this->common->getData("tbl_users",'*',$condition);

			if(!empty($check_name[0]->user_id)){
				echo json_encode(array("success"=>false, 'msg'=>'Email Already Allocated to Another user!'));
				exit;
			}
			
			$thumnail_value = "";
			if(isset($_FILES) && isset($_FILES["profile"]["name"])){
				 $config = array();
				$config['upload_path'] = DOC_ROOT_FRONT."/images/profile_image/";
				$config['max_size']    = '0';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				// $config['allowed_types'] = '*';
				// $config['file_name']     = md5(uniqid("100_ID", true));
				$config['file_name']     = $_FILES["profile"]["name"];
				$this->load->library('upload', $config);
				if (!$this->upload->do_upload("profile")){
					$image_error = array('error' => $this->upload->display_errors());
					echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
					exit;
				}else{
					$image_data = array('upload_data' => $this->upload->data());
					$thumnail_value = $image_data['upload_data']['file_name']; 
				}	
				/* Unlink previous category image */
				if(!empty($this->input->post('user_id'))){	
					$condition_image = " user_id = ".$this->input->post('user_id');
					$image =$this->common->getData("tbl_users",'*',$condition_image);
					if(is_array($image) && !empty($image[0]->profile_photo) && file_exists(DOC_ROOT_FRONT."/images/profile_image/".$image[0]->profile_photo))
					{
						unlink(DOC_ROOT_FRONT."/images/profile_image/".$image[0]->profile_photo);
					}
				}
			}else{
				$thumnail_value = $this->input->post('pre_profile_name');
			}

			$data['profile_photo'] = $thumnail_value;
			$data['user_name'] = $this->input->post('username');
			$data['email_id'] = $this->input->post('useremail');
			$data['phone'] = $this->input->post('userphone');
			//$data['dob'] = (!empty($this->input->post('dob'))) ? date("Y-m-d", strtotime($this->input->post('dob'))) : '';
			//$data['gender'] = $this->input->post('gender');
			$data['updated_on'] = date("Y-m-d H:i:s");

			if(!empty($this->input->post('currentpass'))){
				if($this->input->post('currentpass') == $this->input->post('newpass')){
					$data['password'] = md5($this->input->post('newpass'));
				}else{
					echo json_encode(array("success"=>false, 'msg'=>'Please make sure both password is same !'));
					exit;
				}
			}

			$data['updated_by'] = $this->session->userdata('footage_farm_user')[0]['user_id'];
			if(!empty($this->input->post('user_id'))){
				// print_r($data);
				$condition = "user_id = '".$this->input->post('user_id')."' ";
				$result = $this->common->updateData("tbl_users",$data,$condition);
				$result = $this->common->getData('tbl_users','*',$condition);
				if($result){
					$session_data = array('footage_farm_user'=>$result);  
					$this->session->set_userdata($session_data);  
					echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
					exit;
				}
				else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while updating data.'));
					exit;
				}
			}
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem While Add/Edit Data.'));
			exit;
		}
	}

	public function changepassword(){
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();
			$condition = "password = '".md5($this->input->post('currentpassword'))."' ";
			if($this->input->post('user_id') && $this->input->post('user_id') > 0){
				$condition .= " AND  user_id = ".$this->input->post('user_id')." ";
			}			
			$check_name = $this->common->getData("tbl_users",'*',$condition);
			if(empty($check_name[0]['user_id'])){
				echo json_encode(array("success"=>false, 'msg'=>'Current Password did not match'));
				exit;
			}

			if($this->input->post('password') !== $this->input->post('confirmpassword') ){
				echo json_encode(array("success"=>false, 'msg'=>'New Password and Confirm Password did not match'));
				exit;
			}

			$data['password'] = md5($this->input->post('password'));
			$data['updated_on'] = date("Y-m-d H:i:s");
			$data['updated_by'] = $this->session->userdata('footage_farm_user')[0]['user_id'];
			if(!empty($this->input->post('user_id'))){

				$condition = "user_id = '".$this->input->post('user_id')."' ";
				$result = $this->common->updateData("tbl_users",$data,$condition);
				if($result){ 
					echo json_encode(array('success'=>true, 'msg'=>'Password Updated Successfully.'));
					exit;
				}
				else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while updating data.'));
					exit;
				}
			}
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem While Add/Edit Data.'));
			exit;
		}
	}

	function submitform_security(){
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();
			if(!empty($this->input->post('currentpass'))){
				if($this->input->post('currentpass') == $this->input->post('newpass')){
					$data['password'] = md5($this->input->post('newpass'));
				}else{
					echo json_encode(array("success"=>false, 'msg'=>'Please make sure both password is same !'));
					exit;
				}
			}

			$data['updated_by'] = $this->session->userdata('footage_farm_user')[0]['user_id'];
			if(!empty($this->input->post('user_id'))){
				// print_r($data);
				$condition = "user_id = '".$this->input->post('user_id')."' ";
				$result = $this->common->updateData("tbl_users",$data,$condition);
				if($result){
					echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while updating data.'));
					exit;
				}
			}
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem While Add/Edit Data.'));
			exit;
		}
	}

	function getMemberslisting(){
		$result['roles'] = $this->common->getData("tbl_roles","*");
		//get total number of roles user available
		$condition = " user_type != ('customer')";
		$result['roles_users'] = $this->common->getData("tbl_users","*",$condition);
		$getMemberhtml=$this->load->view('members_view',$result,true);
		if($getMemberhtml){
			echo json_encode(array('success'=>true, 'html'=>$getMemberhtml));
			exit;
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'No Member found'));
			exit;
		}
	}

	function changeUserStatus(){

		$data = array();
		$condition =" role_id = ".$_POST['role_id']." ";
		$get_usertype = $this->common->getData("tbl_roles","*",$condition);
		$data['user_type'] = $get_usertype[0]['role_name'];
		$data['role_id'] = $this->input->post('role_id');
	
		$data['updated_on'] = date("Y-m-d H:i:s");
		$data['updated_by'] = $this->session->userdata('footage_farm_user')[0]['user_id'];
		$condition = "user_id = '".$this->input->post('user_id')."' ";
		$result = $this->common->updateData("tbl_users",$data,$condition);
		if($result){
			echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
			exit;
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem while updating data.'));
			exit;
		}
	}

	function deleteUser(){
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

			$condition="user_id = '".$_POST['user_id']."' ";
			$result = $this->common->deleteRecord('tbl_users',$condition);
			if($result){
				echo json_encode(array('success'=>true, 'msg'=>'User deleted Successfully.'));
				exit;
			}else{
				echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
				exit;
			}
		}
	}

	public function editprofile(){
		$result = array();
		$condition = "1=1 AND user_id = ".$this->session->userdata('footage_farm_user')[0]['user_id']." ";
		$result['profile_data'] = $this->common->getData("tbl_users",'*',$condition);
		//get  enum value
		$result['roles'] = $this->common->getData("tbl_roles","*");
		
		$this->load->view('header.php');
		$this->load->view('editprofile',$result);
		$this->load->view('footer.php');
	}

	public function getfavourite(){
		// fetch data for all favourite added by the user shiv code on  6-10-2020
		$condition = "user_id = ".$this->session->userdata('footage_farm_user')[0]['user_id']." ";
		
		if($_POST['type'] == 'all'){
			$condition .= "AND 1=1";
		}elseif ($_POST['type'] == 'theme') {
			$condition .= "AND theme_id  IS NOT NULL ";
		}elseif ($_POST['type'] == 'subtheme') {
			$condition .= "AND subtheme_id  IS NOT NULL ";
		}else{
			//  for reel/video 
			$condition .= "AND video_id  IS NOT NULL ";
		}
		
		$favourite_data = $this->common->getData("tbl_favourites",'*',$condition);
		// print_r($favourite_data);
		$result = array();
		$htmlfav = '';
		if($favourite_data){
			foreach ($favourite_data as $key => $value) {
				if(!empty($value['video_id']) && isset($value['video_id'])){
					$condition = "video_id = ".$value['video_id']." ";
					$result['fav_data'][$key]=$this->common->getData("tbl_videos",'*',$condition);
	
				}else if(!empty($value['subtheme_id']) && isset($value['subtheme_id'])){
	
					$condition = "sb.sub_theme_id = ".$value['subtheme_id']." ";
					$main_table = array("tbl_sub_themes as sb", array("sb.*"));
					$join_tables =  array();
					$join_tables = array(
							array("", "tbl_video_theme_subtheme_mapping as  tsm", "tsm.sub_theme_id = sb.sub_theme_id", array("count(video_id) as total_reel")),
							);
					$rs = $this->common->JoinFetch($main_table, $join_tables, $condition,"","",null);
					$result['fav_data'][$key] = $this->common->MySqlFetchRow($rs, "array");
	
				}else if(!empty($value['theme_id']) && isset($value['theme_id'])) {
	
					$condition = "1=1 AND t.theme_id =".$value['theme_id']." ";
					$main_table = array("tbl_themes as t", array("t.*"));
					$join_tables =  array();
					$join_tables = array(
							  array("", "tbl_sub_themes as  sb", "t.theme_id = sb.theme_id", array("count(sb.sub_theme_id) as totol_sub_themes")),
							  );
			
					$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("t.theme_id" => "DESC"),"t.theme_id",null); 
					 // fetch query
					$theme_data = $this->common->MySqlFetchRow($rs, "array"); // fetch result
				
					foreach ($theme_data as $keycnt => $valuecnt) {
					  $condition = "1=1 AND theme_id = ".$valuecnt['theme_id'];
					  $total_reel = $this->common->getData("tbl_video_theme_subtheme_mapping",'count(video_id) as total_reel',$condition,"","","video_id");
					  $theme_data[$keycnt]['total_reals'] =count($total_reel) ;
					}
					$result['fav_data'][$key]  = $theme_data;
				}
			}
			$htmlfav= $this->load->view('favourite',$result,true);
		}else{
			$result['fav_data'][0] = '';
		} 
			echo json_encode(array('success'=>true, 'htmlfav'=>$htmlfav,"fav_count"=>'Favourite ('.(!empty($result['fav_data'][0])?count($result['fav_data']):'0').')'));
			exit;
	}
	public function getProject_section(){
		// fetch data for all favourite added by the user shiv code on  23-10-2020
		
		$order_by = '';
		$order = '';
		if($_POST['type'] == 'all'){
			$order_by = "p.created_by";
			$order = "ASC";
		}elseif ($_POST['type'] == 'DESC') {
			$order_by = "p.updated_on";
			$order = "desc";
		}elseif ($_POST['type'] == 'Alphabetical') {
			$order_by = "p.project_name";
			$order = "ASC";
		}
		
		//  get project name accourding to user wise
		$result = array();
		$condition = "user_id = ".$this->session->userdata('footage_farm_user')[0]['user_id']." ";
		$main_table = array("tbl_projects as p", array("p.project_id, p.project_name, p.project_date "));
		$join_tables =  array();
		$join_tables = array(
							array("left", "tbl_project_reel as pr", "pr.project_id = p.project_id", array()),
							array("", "tbl_videos as  v", "v.video_id = pr.reel_id", array("v.vimeo_url, v.thumbnail_image"))
						);
		$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array($order_by => $order),"p.project_id",null); 
		 // fetch query
		$result['project_detail'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result
		
		// echo $this->db->last_query();
		// exit;
		$htmlproject = '';
		if($result['project_detail']){
			$htmlproject= $this->load->view('project_listing',$result,true);
		}
			echo json_encode(array('success'=>true, 'htmlproject'=>$htmlproject,"project_count"=>'Project ('.count($result['project_detail']).')'));
			exit;
	}

	public function delete_row(){
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

			$condition = "project_id = '".$_POST['id']."' ";
			$result = $this->common->deleteRecord('tbl_projects',$condition);
					  $this->common->deleteRecord('tbl_project_reel',$condition);
					  
			if($result){
				echo json_encode(array('success'=>true, 'msg'=>'Record deleted Successfully.'));
				exit;
			}else{
				echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
				exit;
			}
		}

	}
	
}?>