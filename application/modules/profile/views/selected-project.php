<!-- Selected Reels Header -->
<section class="page-container selected-items-wrapper">
	<section class="page-section">
		<div class="black-bg"></div>
		<div class="container">
			<div class="col-sm-12">
				<div class="selected-items">
					<div class="selected-items-left">
						<div class="selected-items-count">
							<span id="count-checked-checkboxes">0</span> Themes Selected
						</div>
						<a href="#/" class="select-all">Select All</a>
						<a href="#/" class="deselect-all">Deselect All</a>
					</div>
					<div class="selected-items-right">
					
						<?php if($this->session->userdata('footage_farm_user')) { ?>
						<a href="#/" data-fancybox data-src="#selectprojects-content" data-options='{"touch" : false}' class="addtoproject-reel">
						<?php }else{?>
							<a href="#/" class="addtofavourite-reel" data-fancybox="" data-src="#signin-content" data-options="{&quot;touch&quot; : false}">
						<?php } ?>
							<span>Add to Project</span>
							<img src="<?= base_url()?>images/project-white.svg" alt="">
						</a>
						<a href="#/" class="selectedheader-more">
							<img src="<?= base_url()?>images/more-icon.svg" alt="">
							<div class="dropdown-menu loggedin-flyout selectedheader-flyout">
								<a class="dropdown-item select-all" href="#">Select All</a>
								<a class="dropdown-item deselect-all" href="#">Deselect All</a>
								<?php if($this->session->userdata('footage_farm_user')) {?>
									<a href="Javascript:void(0)" class="addtofavourite-reel" onclick="addToFav('multiple')">
								<?php }else{?>
									<a href="#/" class="dropdown-item" data-fancybox="" data-src="#signin-content" data-options="{&quot;touch&quot; : false}">
								<?php } ?>	Add to Favourite</a>

									<?php if($this->session->userdata('footage_farm_user')) { ?>
										<a href="#/" data-fancybox data-src="#selectprojects-content" data-options='{"touch" : false}' class="dropdown-item">
									<?php }else{?>
										<a href="#/" class="dropdown-item" data-fancybox="" data-src="#signin-content" data-options="{&quot;touch&quot; : false}">
									<?php } ?>	Add to Project</a>
							</div>
						</a>


						<?php if($this->session->userdata('footage_farm_user')) { ?>
							<a href="#/" class="addtocart-reel" onclick="addToCart('multiple')">
						<?php }else{?>
							<a href="#/" class="" data-fancybox="" data-src="#signin-content" data-options="{&quot;touch&quot; : false}">
						<?php } ?>
							<span>Add to Cart</span>
							<img src="<?= base_url()?>images/cart-white.svg" alt="">
						</a>
						<a href="#/" class="close-selected-header"><img src="<?= base_url()?>images/remove.svg" alt=""></a>
					</div>
				</div>

			</div>
		</div>
	</section>
</section>
<!-- Selected Reels Header -->
<!-- Innerpage Search -->
<section class="page-container innerpage-top">
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="searchbar-top">
		<div class="home-search-wrapper">
			<form class="home-search" action="">
				<input type="text" placeholder="Search for footages and themes" name="home-search">
				<button type="submit"><img src="<?=base_url()?>images/search-icon.svg" alt=""></button>
			</form>
		</div>
	</div>
</section>
<!-- Innerpage Search -->
<section class="page-container">
	<!-- Mobile Search -->
	<div class="multicolored-line multicolored-line-mobile">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="home-search-wrapper search-wrapper-mobile">
		<form class="home-search" action="">
			<input type="text" placeholder="Search for footages and themes" name="home-search">
			<button type="submit"><img src="<?=base_url()?>images/search-icon.svg" alt=""></button>
		</form>
	</div>
	<!-- Mobile Search -->
	<!-- Innerpage Banner -->
	<section class="page-section mb-0">
		<div class="innerpage-banner innerpage-banner-transparent pt-50 pb-0">
			<div class="innerpage-banner-grey-bg full-height"></div>
			<div class="container">
				<div class="row">
					<div class="col-12 no-padding-mobile">
						<div class="profile-top-wrapper selected-project-top-wrapper">
							<a href="<?= base_url('profile')?>" class="backtoprofile"><img src="<?=base_url()?>images/back.svg" alt=""></a>
							<h1 class="page-title-small"><?=(!empty($project_data))?$project_data[0]['project_name']:'';?></h1>
						</div>
						<p class="project-reel-count"><?=(!empty($total_reals))?$total_reals:'';?> Reels</p>
						<div class="selected-project-description">
							<p><?=(!empty($project_data))?$project_data[0]['project_description']:'';?></p>
						</div>
						<div class="project-share-wrapper">
							<a href="javascript:void(0)" onclick="delete_row(<?= $project_data[0]['project_id']?>)" data-toggle="tooltip" data-placement="top" title="Delete"><img src="<?=base_url()?>images/delete-blue.svg" alt=""></a>
							<a href="#/" data-fancybox data-src="#createnewproject-content" data-options='{"touch" : false}' data-toggle="tooltip" data-placement="top" title="Rename"><img src="<?=base_url()?>images/edit-blue.svg" alt=""></a>

							<a href="#/" class="reelshare" data-toggle="tooltip" data-placement="top" title="Share" data-fancybox data-src="#sharetheme-content" data-url="<?=base_url('profile/project_detail?text='.rtrim(strtr(base64_encode("id=".$project_data[0]['project_id']), '+/', '-_'), '=').'')?>" data-options='{"touch" : false}' ><img src="<?=base_url()?>images/share-blue.svg" alt=""></a>
							<div class="ff-filters allthemes-filters filters-right selected-project-filter">
								<p>Sort By</p>
								<div class="allthemes-filter-single">
									<select name="" id="" class="niceselect" onchange="project_detail(this.value)" >
										<option value="All">All</option>
										<option value="Alphabetical">Alphabetical</option>
										<option value="DESC">Recently Modified</option>
									</select>
								</div>
							</div>
						<!-- 	<a href="#/" class="project-detail-addnew" data-fancybox data-src="#createnewproject-content" data-options='{"touch" : false}'><img src="<?=base_url()?>images/add-filled-blue.svg" alt=""></a> -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Innerpage Banner -->
	<!-- Selected Project Reels -->
	<section class="page-section pt-50 pb-50 ptmob-25 pbmob-25 selected-project">
		<div class="container">
			<!-- Favourites No Reels Message -->
			<!-- <div class="col-12">
				<div class="no-favourite-reels text-center">
					<p>Your favorite Reels will be listed here.</p>
					<a href="index.php" class="btn-ff btn-primary-dark text-uppercase">EXPLORE FOOTAGES</a>
				</div>
			</div> -->
			<!-- Favourites No Reels Message -->
			<!-- Favourites Reel Listing -->
			<div class="row" id="projectreel">
			
			</div>
			<!-- Favourites Reel Listing -->
			<!-- Favourites No Reels Message -->

			<!-- Favourites No Reels Message -->
		</div>
	</section>
	<!-- Selected Project Reels -->
</section>

<!-- Share Theme/Reel -->
<div style="display: none;max-width:580px;" id="sharetheme-content" class="ff-modal">
	<div class="modal-header">
		<h3>Share a Reel</h3>
	</div>
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="ff-modal-content">
		<form class="share-reel-form">
			<div class="modal-section">
				<div class="form-group">
					<input type="text" class="form-control ff-field-dark linkfield" id="reelink" name="reelink" placeholder="Theme/Reel link will be shown here">
					<a href="#/" class="copylink">Copy Link</a>
				</div>
				<div class="form-group">
					<label for="social">Social</label>
					<div class="modal-social-share">
						<a href="#/"><img src="<?=base_url()?>images/twitter-black.svg" alt="Share on Twitter"></a>
						<a href="#/"><img src="<?=base_url()?>images/facebook-black.svg" alt="Share on Facebook"></a>
						<a href="#/"><img src="<?=base_url()?>images/linkedin-black.svg" alt="Share on Linkedin"></a>
					</div>
				</div>
				<div class="form-group">
					<label for="sendtoemail">Send to Email Address</label>
					<input type="text" class="form-control ff-field-dark" id="sendtoemail" name="sendtoemail" required>
					<span class="emailaddress">niraali@bokapp.design <a href="#/"><img src="<?=base_url()?>images/close-black.svg" alt=""></a></span>
					<span class="emailaddress">gonzalo.c@footagefarm.com <a href="#/"><img src="<?=base_url()?>images/close-black.svg" alt=""></a></span>
					<span class="emailaddress">gintare.k@footagefarm.com <a href="#/"><img src="<?=base_url()?>images/close-black.svg" alt=""></a></span>
					<span class="emailaddress">mat.f@footagefarm.com <a href="#/"><img src="<?=base_url()?>images/close-black.svg" alt=""></a></span>
				</div>
			</div>
			<div class="modal-content-wrapper">
				<div class="modal-actions">
					<input type="submit" data-fancybox-close class="btn-ff btn-secondary-dark text-uppercase" value="Cancel">
					<input type="submit" class="btn-ff btn-primary-dark text-uppercase" value="Send">
				</div>
			</div>
		</form>
	</div>
</div>
<!-- Share Theme/Reel -->
<!-- Request Enquiry Master -->
<div style="display: none;max-width:700px;" id="enquiry-master-content" class="ff-modal">
	<div class="modal-header">
		<h3>Request Enquiry</h3>
	</div>
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="ff-modal-content">
		<form class="enquiry-master-form">
			<div class="modal-section">

				<div class="form-group">
					<div class="reel-title">
						<img src="https://via.placeholder.com/147x83.png" alt="">
						<h3>1910 - 1918 - WWI Era: Aerials; German Officers & Troops; France Fighting; Trans-Caucasia Winter</h3>
					</div>
				</div>
				<div class="form-group">
					<div class="request-reel-type">
						<label for="selectreeltype">Please select the reel type</label>
						<label class="radio-container-theme">
							<span class="radio-label">Request Preview</span>
							<input type="radio" name="reeltype" required>
							<span class="radio"></span>
						</label>
						<label class="radio-container-theme">
							<span class="radio-label">Request Master</span>
							<input type="radio" name="reeltype">
							<span class="radio"></span>
						</label>
					</div>
					<label for="reeltype" class="error"></label>
				</div>
				<div class="form-group">
					<label for="inouttime">Specify the footage's in and out time</label>
					<div class="begin-end-tc">
						<div class="tc-time begin-tc">
							<label for="begintc">Begin TC
								<a href="#" class="tooltip-info">
									<img src="<?=base_url()?>images/info.svg" alt="">
									<span class="tooltip-msg">
										Footage start time
									</span>
								</a>
							</label>
							<input type="text" class="form-control ff-field-dark" id="begintc" name="begintc" placeholder="00:00:00" required>
						</div>
						<div class="tc-time end-tc">
							<label for="endtc">End TC
								<a href="#" class="tooltip-info">
									<img src="<?=base_url()?>images/info.svg" alt="">
									<span class="tooltip-msg">
										Footage end time
									</span>
								</a>
							</label>
							<input type="text" class="form-control ff-field-dark" id="endtc" name="endtc" placeholder="00:00:00" required>
						</div>
					</div>
					<div id="addmore-begin-end-tc"></div>
					<div class="form-addmore">
						<a href="#/" class="form-addmore-link tc-addmore-link"><img src="<?=base_url()?>images/add-black.svg" alt=""> <span>Add another in and out time codes</span></a>
					</div>
				</div>
				<div class="form-group">
					<label>Do you have a file with multiple in and out time codes?</label>
					<p>Add jpegs, excel or word files here. Each file size should be under 2MB.</p>
					<div class="uploaded-files-list">
						<div class="file-single">
							<img src="<?=base_url()?>images/jpg.svg" alt="" class="file-type-icon">
							<span>Screenshota_qe52gknknf.jpg</span>
							<a href="#/"><img src="<?=base_url()?>images/close.svg" alt="" class="file-remove-icon"></a>
						</div>
						<div class="file-single">
							<img src="<?=base_url()?>images/doc.svg" alt="" class="file-type-icon">
							<span>datasheet-with-timecodes.doc</span>
							<a href="#/"><img src="<?=base_url()?>images/close.svg" alt="" class="file-remove-icon"></a>
						</div>
						<div class="file-single">
							<img src="<?=base_url()?>images/xls.svg" alt="" class="file-type-icon">
							<span>animate-sys.xls</span>
							<a href="#/"><img src="<?=base_url()?>images/close.svg" alt="" class="file-remove-icon"></a>
						</div>
					</div>
					<!-- Created using dropzone.js-->
					<div id="upload-reel-times">
						<img src="<?=base_url()?>images/upload-file.svg" alt="">
						<p>Drag and drop here or browse</p>
						<a href="#/" class="login-link">Browse</a>
					</div>
					<!-- Created using dropzone.js-->
				</div>
				<div class="form-group">
					<div class="format-framerate">
						<div class="format formatframe">
							<label for="begintc">Format</label>
							<select class="niceselect ff-select-dark formatselect" required>
								<option value="">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
							</select>
							<label id="formatselect-error" class="error" for=""></label>
						</div>
						<div class="framerate formatframe">
							<label for="endtc">Frame Rate</label>
							<select class="niceselect ff-select-dark framerateselect" required>
								<option value="">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
							</select>
							<label id="framerateselect-error" class="error" for=""></label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="codec">Codec
						<a href="#" class="tooltip-info">
							<img src="<?=base_url()?>images/info.svg" alt="">
							<span class="tooltip-msg">
								Codec type
							</span>
						</a>
					</label>
					<input type="text" class="form-control ff-field-dark" id="codec" name="codec" placeholder="QT ProRes422HQ" required>
				</div>
				<div class="form-group">
					<label for="projectdesc">Message (optional)</label>
					<textarea class="form-control ff-field-dark" rows="5"></textarea>
				</div>
			</div>
			<div class="modal-content-wrapper">
				<div class="modal-actions">
					<input type="submit" data-fancybox-close class="btn-ff btn-secondary-dark text-uppercase" value="Cancel">
					<input type="submit" class="btn-ff btn-primary-dark text-uppercase" value="Send Request">
				</div>
			</div>
		</form>
	</div>
</div>
<!-- Request Enquiry Master -->

<!-- Create New Project -->
<div style="display: none;max-width:580px;" id="createnewproject-content" class="ff-modal">
	<div class="modal-header">
		<h3>Rename Project</h3>
	</div>
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="ff-modal-content">
		<!-- <form class="createnewproject-form"> -->
		<form class="" id="form-project-rename" method="post" enctype="multipart/form-data">
			<input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden" class="txt_csrfname">

			<div class="modal-section">
				<input type="hidden" class="form-control ff-field-dark" id="id" name="id" value="<?=(!empty($project_data))?$project_data[0]['project_id']:'';?>">
				<div class="form-group">
					<label for="nameofproject">Name of Project</label>
					<input type="text" class="form-control ff-field-dark" id="nameofproject" name="nameofproject" value="<?=(!empty($project_data))?$project_data[0]['project_name']:'';?>">
				</div>
			</div>
			<div class="modal-content-wrapper">
				<div class="modal-actions">
					<input type="button" class="btn-ff btn-secondary-dark text-uppercase modal-cancel-newproject" value="Cancel">
				<input type="submit" class="btn-ff btn-primary-dark text-uppercase" name="submit" value="Create">
				</div>
			</div>
		</form>
	</div>
</div>
<!-- Create New Project -->


<script>


function delete_row(id){
	//alert(id);
		var csrfName = "<?= $this->security->get_csrf_token_name()?>"; // Value specified in $config['csrf_token_name']
		var csrfHash = "<?= $this->security->get_csrf_hash()?>"; // CSRF hash
		if(id ){
			var r = confirm("Are you sure want to delete this Project");
			if (r == true) {
				$.ajax({
					url: "<?= base_url('profile/delete_row')?>",
					data:{id,[csrfName]: csrfHash },
					dataType: "json",
					type: "POST",
					success: function(response){
					
						if(response.success){
							swal("done", response.msg, {
								icon : "success",
								buttons: {        			
									confirm: {
										className : 'btn btn-success'
									}
								},
							}).then(
							function() {
								window.location.href= "<?=  base_url('profile')?>";
							});
						}else{	
							swal("Failed", response.msg, {
								icon : "error",
								buttons: {        			
									confirm: {
										className : 'btn btn-danger'
									}
								},
							});
						}
					}
				});
			}
		}


	}
project_detail('All');	
getProject();
	// add to fav shiv code on 22-9-20202
function addToFav(fav_type,video_id){
	var selected_reels = [];
		if(fav_type == 'multiple'){
		var selected_reels = $('.cls_checkbox:checked').map(function () {
		return this.value;
		}).get();
	}else{
		selected_reels.push(video_id);
	}
	console.log(selected_reels);
	var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
	var csrfHash = $('.txt_csrfname').val(); // CSRF hash
	if(selected_reels){
			$.ajax({
				url: "<?= base_url('reels/addToFav')?>",
				type: "POST",
				data:{ selected_reels , [csrfName]: csrfHash },
				dataType: "json",
				success: function(response){
					if(response.success){
						$('.notification-message').fadeIn();		
						$('.notification-modal-text').text('You saved this reel as your favourites!')
						setTimeout(function(){
							$('.notification-message').fadeOut();	 
							}, 2000);	
					}else{
						$('.notification-message').fadeIn();		
						$('.notification-modal-text').text(response.msg)
						setTimeout(function(){
							$('.notification-message').fadeOut();	 
							}, 2000);	
					}
				}
			});
		}
}
// add to cart shiv code on 22-9-2020
function addToCart(fav_type,video_id){
	var selected_reels = [];
		if(fav_type == 'multiple'){
		var selected_reels = $('.cls_checkbox:checked').map(function () {
		return this.value;
		}).get();
	}else{
		selected_reels.push(video_id);
	}
	console.log(selected_reels);
	var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
	var csrfHash = $('.txt_csrfname').val(); // CSRF hash
	if(selected_reels){
			$.ajax({
				url: "<?= base_url('reels/addToCart')?>",
				type: "POST",
				data:{ selected_reels , [csrfName]: csrfHash },
				dataType: "json",
				success: function(response){
					if(response.success){
						$('.notification-message').fadeIn();		
						if(selected_reels.length > 1){
							$('.notification-modal-text').text(selected_reels.length + ' Reels have been added to the cart!')
						}else{
							$('.notification-modal-text').text(selected_reels.length + ' Reel have been added to the cart!')
						}
						setTimeout(function(){ $('.notification-message').fadeOut();	 }, 2000);
					}else{
						$('.notification-message').fadeIn();		
						$('.notification-modal-text').text(response.msg)
						setTimeout(function(){
							$('.notification-message').fadeOut();	 
							}, 2000);	
					}
				}
			});
		}
}

function project_detail(type=null) {
	var projectid = "<?= $project_data[0]['project_id']?>";
	var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
	var csrfHash = $('.txt_csrfname').val(); // CSRF hash
	$.ajax({
		url: "<?= base_url('profile/project_detail_reel')?>",
		type: "POST",
		data:{ type, projectid, [csrfName]: csrfHash },
		dataType: "json",
		success: function(response){
			if(response.success){
				$("#projectreel").html(response.data);		
			}else{
				$("#projectreel").html('');
			}
		}
	});
}

// get project listing code by ashutosh n made changes by shiv on  1-10-2020
function getProject() {
	var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
	var csrfHash = $('.txt_csrfname').val(); // CSRF hash
	$.ajax({
		url: "<?= base_url('project/projectdata')?>",
		type: "POST",
		data:{ [csrfName]: csrfHash },
		dataType: "json",
		success: function(response){
			if(response.success){
				$("#projectshow").html(response.data);		
			}else{
				$("#projectshow").html('');
			}
			
			$('.mep-single').hover(function () {
				$(this).children('.hover-options-container').children('.mep-box-single-checkbox').show();
			}, function () {
				$(this).children('.hover-options-container').children('.mep-box-single-checkbox').hide();
				/*Keep checkbox visible if checked*/
				var mepcheckbox = $(this).children('.hover-options-container').children('.mep-box-single-checkbox').children('label.checkbox-container').children('input[type="checkbox"]');
				if (mepcheckbox.is(':checked')) {
					$(this).children('.hover-options-container').children('.mep-box-single-checkbox').addClass('selected');
					//$(this).children('.hover-options-container').siblings('.mep-single-overlay').addClass('selected')
				} else {
					$(this).children('.hover-options-container').children('.mep-box-single-checkbox').removeClass('selected');
					//$(this).children('.hover-options-container').siblings('.mep-single-overlay').removeClass('selected')
				}
				/*Keep checkbox visible if checked*/
			});


		}
	});
}

// use for project form submit
$("#form-project").validate({
	ignore:[],
	rules: {
		// nameofproject:{required:true},
		// projectdesc/:{required:true},
	},
	messages: {
		// nameofproject:{required:"Please Enter Project Name."},
		// projectdesc:{required:"Please Enter Description."},
	},
	submitHandler: function(form) 
	{	
		var myCheckboxes = new Array();
			$("input[name='reels[]']:checked").each(function() {
			myCheckboxes.push($(this).val());
			});
		//var pagedata = { "keyword": myCheckboxes};
		
		var act = "<?= base_url('project/submitForm')?>";
		$("#form-project").ajaxSubmit({
			url: act, 
			type: 'post',
			data: { reel: myCheckboxes },
			dataType: 'json',
			cache: false,
			clearForm: false,
			async:false,
			beforeSubmit : function(arr, $form, options){
				$(".btn btn-black").hide();
			},
			success: function(response) 
			{
				if(response.success){
					getProject();
					swal("done", response.msg, {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					}).then(
					function() {

						// $("#selectprojects-content").modal('show');
					});
				}else{	
					swal("Failed", response.msg, {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}
			}
		});
	}
});

$("#form-project-rename").validate({
	ignore:[],
	submitHandler: function(form) 
	{	

		var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
		var csrfHash = $('.txt_csrfname').val(); // CSRF hash
		var act = "<?= base_url('project/submitRename')?>";
		$("#form-project-rename").ajaxSubmit({
			url: act, 
			type: 'post',
			data: { [csrfName]: csrfHash  },
			dataType: 'json',
			cache: false,
			clearForm: false,
			async:false,
			beforeSubmit : function(arr, $form, options){
				$(".btn btn-black").hide();
			},
			success: function(response) 
			{
				if(response.success){
					$.fancybox.close();
					$('.notification-message').fadeIn();		
					$('.notification-modal-text').text(response.msg)
					setTimeout(function(){
						$('.notification-message').fadeOut();
						location.reload();	 
						}, 2000);	
					// swal("done", response.msg, {
					// 	icon : "success",
					// 	buttons: {        			
					// 		confirm: {
					// 			className : 'btn btn-success'
					// 		}
					// 	},
					// }).then(
					// function() {
					// });
				}else{	
					swal("Failed", response.msg, {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}
			}
		});
	}
});

$(document).ready(function() {
	$('.profile-purchase-history-link').click(function() {
		$('#profile-favourites,#profile-projects').hide();
		$('#profile-purchase-history').show();
	})
	$('.profile-favourite-link').click(function() {
		$('#profile-purchase-history,#profile-projects').hide();
		$('#profile-favourites').show();
	})
	$('.profile-projects-link').click(function() {
		$('#profile-favourites,#profile-purchase-history').hide();
		$('#profile-projects').show();
	})
	$('.tabs__items li a').click(function() {
		$('.tabs__items li a').removeClass('active');
		$(this).addClass('active');
	})
	/*Favourites section scripts*/
	/*Notification messages*/
	$('.addtofavourite-subtheme').click(function() {
		$('.notification-message').fadeIn();
		$('.notification-modal-text').text('You saved this subtheme as your favourites!')
		setTimeout(function() {
			$('.notification-message').fadeOut();
		}, 2000);
	});
	$('.addtofavourite-reel').click(function() {
		$('.notification-message').fadeIn();
		$('.notification-modal-text').text('You saved this reel as your favourites!')
		setTimeout(function() {
			$('.notification-message').fadeOut();
		}, 2000);
	});
	$('.select-all').click(function() {
		$('.notification-message').fadeIn();
		$('.notification-modal-text').text('You selected all reels!')
		setTimeout(function() {
			$('.notification-message').fadeOut();
		}, 2000);
	});
	$('.deselect-all').click(function() {
		$('.notification-message').fadeIn();
		$('.notification-modal-text').text('You deselected all reels!')
		setTimeout(function() {
			$('.notification-message').fadeOut();
		}, 2000);
	});
	$('.addtocart-reel').click(function() {
		$('.notification-message').fadeIn();
		$('.notification-modal-text').text('5 reels have been added to the cart!')
		setTimeout(function() {
			$('.notification-message').fadeOut();
		}, 2000);
	});
	$('.share-cart').click(function() {
		$('.notification-message').fadeIn();
		$('.notification-modal-text').text('Reel added to cart!')
		setTimeout(function() {
			$('.notification-message').fadeOut();
		}, 2000);
		$('.selected-items-wrapper').fadeIn()
	})
	/*Notification messages*/
	/*Addmore Start/ End TC*/
	$('.tc-addmore-link').click(function() {
		$('#addmore-begin-end-tc').append(
			"<div class='begin-end-tc'>" +
			"<div class='tc-time begin-tc'>" +
			"<label for='begintc'>Begin TC" +
			"</label>" +
			"<input type='text' class='form-control ff-field-dark' id='begintc' name='begintc' placeholder='00:00:00'>" +
			"</div>" +
			"<div class='tc-time end-tc'>" +
			"<label for='endtc'>End TC" +
			"</label>" +
			"<input type='text' class='form-control ff-field-dark' id='endtc' name='endtc' placeholder='00:00:00'>" +
			"<a href='#/' class='remove-tc'><img src='images/close.svg'/></a>" +
			"</div>	" +
			"</div>"
		)
	});
	$('body').on('click', '.remove-tc', function() {
		$(this).closest('.begin-end-tc').remove();
	});
	/*Addmore Start/ End TC*/
	$('#upload-reel-times').dropzone({
		url: "/file/post"
	});
	$('.file-remove-icon').click(function() {
		$(this).closest('.file-single').remove();
	});
	$('.share-reel-form').validate();
	$('.enquiry-master-form').validate({
		ignore: []
	});
	/*Favourites section scripts*/
});
</script>