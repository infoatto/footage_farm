<body>
	<div class="wrapper">
		<div class="main-panel">
			<div class="container">
				<div class="page-inner">
					<div class="col-12">
						<div class="row ff-title-block">
							<div class="col-12 col-sm-12 col-md-6 col-lg-6">
									<h1 class="ff-page-title">My Profile</h1> 
							</div>	
							<div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
									<a href="#/" onclick="goBack()" class="btn btn-light btn-border ff-page-title-btn">
											Back
									</a>
							</div>						
						</div>
					</div>					
					<div class="row">
							<div class="col-sm-12">
									<div class="card">
											<div class="card-body">
												<ul class="nav nav-pills nav-secondary" id="pills-tab" role="tablist">
													<li class="nav-item">
														<a class="nav-link active" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="true">Profile</a>
													</li>
													<li class="nav-item">
														<a class="nav-link" id="pills-security-tab" data-toggle="pill" href="#pills-security" role="tab" aria-controls="pills-security" aria-selected="false">Security</a>
													</li>
													<li class="nav-item">
														<a class="nav-link" id="pills-team-tab" data-toggle="pill" href="#pills-team" role="tab" aria-controls="pills-team" aria-selected="false" onclick="getMemberslisting()">Team Members</a>
													</li>												
												</ul>
												<div class="tab-content mt-2 mb-3" id="pills-tabContent">
													
														<div class="tab-pane fade show active" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
															<form class="" id="form-personal-info" method="post" enctype="multipart/form-data">
																<input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden">
																<input name="user_id" value="<?= (!empty($profile_data[0]['user_id'])?$profile_data[0]['user_id']:"") ?>" type="hidden">
															
																<div class="row mt-3">
																	<div class="col-md-12">
																		<div>
																			<span class="h2">Personal Info</span>
																		</div>																
																		<div class="form-group">
																		<?php $profile_image = (!empty($this->session->userdata('footage_farm_user')[0]['profile_photo']))? FRONT_URL.'/images/profile_image/'.$this->session->userdata('footage_farm_user')[0]['profile_photo']:"https://via.placeholder.com/50" ?>		
																			<div class="avatar avatar-xl" style="width: 8rem;height: 8rem;">
																				<img src="<?= $profile_image?>" alt="..." class="avatar-img rounded-circle">
																				<?php $req= '';
																						if(empty($profile_data[0]['profile_photo'])){
																							$req ='required';
																						}else{?>
																						<br>
																							<input name="pre_profile_name" id="pre_profile_name" value="<?= (!empty($profile_data[0]['profile_photo'])?$profile_data[0]['profile_photo']:"") ?>" type="hidden">
																						<?php }?>
																						
																						<input type="file" class="form-control-file" id="profile" name="profile" <?= $req;?> >
																			</div>	
																		</div>
																	</div>
																	<div class="col-md-4">																
																		<div class="form-group">
																			<label for="username">Name</label>
																			<input type="text" class="form-control" id="username" name="username"  value="<?= (!empty($profile_data[0]['user_name'])?$profile_data[0]['user_name']:"") ?>"placeholder="Jimmy Spencer">	
																		</div>
																		<div class="form-group">
																			<label for="email">Email</label>
																			<input type="email" class="form-control" id="email" name="email" value="<?= (!empty($profile_data[0]['email_id'])?$profile_data[0]['email_id']:"") ?>" placeholder="jimmy-spenser@hotmail.com">	
																		</div>																
																	</div>
																	<div class="col-md-4">
																		<div class="form-group">
																			<label for="gender">Gender</label>
																			<input type="text" class="form-control" id="gender" name="gender" value="<?= (!empty($profile_data[0]['gender'])?$profile_data[0]['gender']:"") ?>" placeholder="Male">	
																		</div>
																		<div class="form-group">
																			<label for="phone">Phone</label>
																			<input type="text" class="form-control" id="phone" name="phone" value="<?= (!empty($profile_data[0]['phone'])?$profile_data[0]['phone']:"") ?>" placeholder="+91- 123-456-7890">	
																		</div>		
																	</div>
																	<div class="col-md-4">
																		<div class="form-group">
																			<label for="dob">Date of Birth</label>
																			<input type="text"  value="<?= (!empty($profile_data[0]['dob'])?date('d-m-Y',strtotime($profile_data[0]['dob'])):"") ?>" class="form-control" id="dob" name="dob" placeholder="04 March,1981 ">	
																		</div>																
																	</div>
																	<div class="col-sm-12">
																		<div class="form-group">
																				<button class="btn btn-black" type="submit">Save</button>
																				<button class="btn btn-cancel btn-border" onclick="goBack()">Cancel</button>
																			</div>
																	</div>																	
																</div>
															</form>														
														</div>
													
														<div class="tab-pane fade" id="pills-security" role="tabpanel" aria-labelledby="pills-security-tab">
															<form class="" id="form-personal-security" method="post" enctype="multipart/form-data">
																<input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden">
																<input name="user_id" value="<?= (!empty($profile_data[0]['user_id'])?$profile_data[0]['user_id']:"") ?>" type="hidden">
												
																<div class="row mt-3">
																	<div class="col-md-12">
																		<div>
																			<span class="h2">Security</span>
																		</div>	
																	</div>
																	<div class="col-md-4">																
																		<div class="form-group">
																			<label for="currentpass">Current Password</label>
																			<input type="password"  name="currentpass"  class="form-control" id="currentpass" placeholder="Enter current password">	
																		</div>
																		<div class="form-group">
																			<label for="newpass">New Password</label>
																			<input type="password"  name="newpass" class="form-control" id="newpass" placeholder="Enter new password">	
																		</div>																
																	</div>															
																	<div class="col-sm-12">
																		<div class="form-group">
																				<button class="btn btn-black" type="submit">Save</button>
																				<button class="btn btn-cancel btn-border" onclick="goBack()">Cancel</button>
																			</div>
																	</div>																	
																</div>
															</form>
														</div>

													<div class="tab-pane fade" id="pills-team" role="tabpanel" aria-labelledby="pills-team-tab">
														<form class="" id="form-personal-info" method="post" enctype="multipart/form-data">
															<input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden">
															<input name="user_id" value="<?= (!empty($profile_data[0]['user_id'])?$profile_data[0]['user_id']:"") ?>" type="hidden">
															<div class="row mt-3">
																<div class="col-md-12">
																	<div>
																		<span class="h2">Collaborators</span>
																	</div>	
																</div>
																</div>
																<div class="row mt-3" id="dynamic_members">									
																																		
																</div>
																<div class="row mt-3">
																	
																	<div class="col-sm-12">																	
																		<a data-fancybox="" data-src="#add-team-member" href="javascript:;" class="btn btn-primary ml-2">
																				<i class="fas fa-plus"></i> 
																				Add Team Member					
																		</a>																	
																	</div>														
																	<div class="col-sm-12">
																		<div class="form-group">
																			<button class="btn btn-black">Save</button>
																			<button class="btn btn-cancel btn-border">Cancel</button>
																		</div>
																	</div>																	
																</div>
														</form>
													</div>	
													
													
												</div>
											</div>
									</div>
							</div>
					</div>
				</div>
			</div>			
		</div>						
	</div>
	<!-- Add Team Member -->
	<div style="display: none;" id="add-team-member" class="ff-popup">
		<div class="ff-popup-title"><span class="h2">Invite Collaborator</span></div>		
		<div class="card">
		<form class="" id="form-personal-member" method="post" enctype="multipart/form-data">
			<input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden">
			<div class="card-body">
				<div class="form-group">
					<label for="selectrole">Select Role</label>
					<select class="form-control" name="role_id" id="role_id">
						<option value="">Select Roles Type</option>
						<?php 
							foreach ($roles as $key => $value) {?>
								<option value="<?=$value['role_id']?>"><?= $value['role_name']?></option>
						<?php }?>
					</select> 
				</div>
				<div class="form-group">
					<label for="collabemail">User-Name</label>
					<input type="input" class="form-control" id="member_username" name="member_username" placeholder="Enter Username">
				</div>
				<div class="form-group">
					<label for="collabemail">Email</label>
					<input type="email" class="form-control" id="member_email" name="member_email" placeholder="Enter email id">
				</div>
				<div class="form-group">
					<button class="btn btn-black" type="submit">Save</button>
					<a href="#/" class="btn btn-cancel btn-border" onclick="goBack()">Cancel</a>
				</div>
			</div>
		</form>
		</div>
	</div>
	<!-- Add Team Member -->
	
<script >
	$(document).ready(function() {
		
		$('#user-projects').DataTable({
			"pageLength": 5,
		});	
		
		$('#user-enquiry').DataTable({
			"pageLength": 5,
		});			
	});

	function goBack() {
		window.history.back()
	}



	
// use for form submit
var vRules = 
{
	username:{required:true},
	email:{required:true},
	gender:{required:true},
	dob:{required:true},
	phone:{required:true},

};
var vMessages = 
{
	username:{required:"Please Enter Username Name."},
	gender:{required:"Please Enter Gender ."},
	email:{required:"Please Enter Email."},
	dob:{required:"Please Select DOB."},
	phone:{required:"Please Enter Phone Number"},

};

$("#form-personal-info").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{	
		var act = "<?= base_url('profile/submitform')?>";
		$("#form-personal-info").ajaxSubmit({
			url: act, 
			type: 'post',
			dataType: 'json',
			cache: false,
			clearForm: false,
			async:false,
			beforeSubmit : function(arr, $form, options){
				// $(".btn-primary").hide();
			},
			success: function(response) 
			{
				if(response.success){
					swal("done", response.msg, {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					}).then(
					function() {
						window.location = "<?= base_url('profile')?>";
						// $this->router->fetch_class().'/'.$this->router->fetch_method() ?>";
					});
				}else{	
					swal("Failed", response.msg, {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}
			}
		});
	}
});
	
// use for form submit member
var vRules = 
{
	member_username:{required:true},
	member_email:{required:true},
	role_id:{required:true}
	

};
var vMessages = 
{
	member_username:{required:"Please Enter Username Name."},
	member_email:{required:"Please Enter Email."},
	role_id:{required:"Please Select User Type."}

};

$("#form-personal-member").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{	
		var act = "<?= base_url('profile/submitFormMember')?>";
		$("#form-personal-member").ajaxSubmit({
			url: act, 
			type: 'post',
			dataType: 'json',
			cache: false,
			clearForm: false,
			async:false,
			beforeSubmit : function(arr, $form, options){
				// $(".btn-primary").hide();
			},
			success: function(response) 
			{

				$("#form-personal-member").hide();
				$("#add-team-member").hide();
				if(response.success){
					swal("done", response.msg, {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					}).then(
					function() {
						getMemberslisting()
					});
				}else{	
					swal("Failed", response.msg, {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}
			}
		});
	}
});

var vRules = 
{
	currentpass:{required:true},
	newpass:{required:true},

	

};
var vMessages = 
{
	currentpass:{required:"Please Enter Current Password."},
	newpass:{required:"Please Enter New Password."},

};


$("#form-personal-security").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{	
		var act = "<?= base_url('profile/submitform_security')?>";
		$("#form-personal-security").ajaxSubmit({
			url: act, 
			type: 'post',
			dataType: 'json',
			cache: false,
			clearForm: false,
			async:false,
			beforeSubmit : function(arr, $form, options){
				// $(".btn-primary").hide();
			},
			success: function(response) 
			{
				if(response.success){
					swal("done", response.msg, {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					}).then(
					function() {
						window.location = "<?= base_url('home/logOut')?>";
						// $this->router->fetch_class().'/'.$this->router->fetch_method() ?>";
					});
				}else{	
					swal("Failed", response.msg, {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}
			}
		});
	}
});

function getMemberslisting(){
	var csrfName = "<?= $this->security->get_csrf_token_name()?>"; // Value specified in $config['csrf_token_name']
	var csrfHash = "<?= $this->security->get_csrf_hash()?>"; // CSRF hash
  	$.ajax({
		url: "<?= base_url('profile/getMemberslisting')?>",
		data:{[csrfName]: csrfHash },
		dataType: "json",
		type: "POST",
		success: function(res){
			$("#dynamic_members").html(res.html);
			// console.log(res.html);
		}
	});	
}

function changeUserStatus(user_id,role_id){
	var csrfName = "<?= $this->security->get_csrf_token_name()?>"; // Value specified in $config['csrf_token_name']
	var csrfHash = "<?= $this->security->get_csrf_hash()?>"; // CSRF hash
	var r = confirm("Are you sure want change the role of this user");
	if (r == true) {
		$.ajax({
			url: "<?= base_url('profile/changeUserStatus')?>",
			data:{[csrfName]: csrfHash ,user_id,role_id},
			dataType: "json",
			type: "POST",
			success: function(response){

				if(response.success){
					swal("done", response.msg, {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					})
				}else{	
					swal("Failed", response.msg, {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}
			}
		});
	}

}
function deleteUser(user_id){
	var csrfName = "<?= $this->security->get_csrf_token_name()?>"; // Value specified in $config['csrf_token_name']
	var csrfHash = "<?= $this->security->get_csrf_hash()?>"; // CSRF hash
	var r = confirm("Are you sure want to delete this user");
	if (r == true) {
		$.ajax({
			url: "<?= base_url('profile/deleteUser')?>",
			data:{[csrfName]: csrfHash ,user_id},
			dataType: "json",
			type: "POST",
			success: function(response){
				getMemberslisting();
				if(response.success){
					swal("done", response.msg, {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					})
				}else{	
					swal("Failed", response.msg, {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}
			}
		});
	}

}

$("#dob").datepicker({
	format: 'DD-MM-YYYY',
});
</script>
</body>
</html>
