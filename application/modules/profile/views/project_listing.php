<?php
			if(!empty($project_detail)){?>
			<div class="row">
			<?php foreach ($project_detail as $key => $value) {
				$date_convert = date('Y-F-jS',strtotime($value['project_date']));
	            $date_convert_array = explode('-',$date_convert);
			 ?>	
				<div class="col-12 col-md-6 col-lg-4">
					<div class="block-single project-single">
						<a href="<?=base_url('profile/project_detail?text='.rtrim(strtr(base64_encode("id=".$value['project_id']), '+/', '-_'), '=').'')?>">
							<h3><?= $value['project_name']; ?></h3>
							<div class="block-info">
								<div>
									<p class="block-info-title">Updated on</p>
									<p class="block-info-content"><?= $date_convert_array[2].'  '.$date_convert_array[1].'  '.$date_convert_array[0] ?></p>
								</div>
							</div>							
						</a>						
						<a href="#/">
							<div class="hover-options-container">	
								<div class="box-single-share delete_project" onclick="delete_row(<?=$value['project_id']?>)" >									
									<span class="share-single share-social delete-item"></span>
									<span class="share-msg-text share-social-text">
										Delete
									</span>									
								</div>
								<img src="<?= ($value['thumbnail_image']!='')?base_url('images/video_image/'.$value['thumbnail_image']):'images/newproject.jpg'; ?>" alt="" class="img-fluid">
							</div>
						</a>
					</div>
				</div>
			<?php } ?>
			</div>
			<?php } ?>