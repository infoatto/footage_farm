<!-- Innerpage Search -->
<section class="page-container innerpage-top">
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="searchbar-top">
		<div class="home-search-wrapper">
			<form class="home-search search-form" action="">
				<input type="text" list="browsers" placeholder="Search for footages and themes" name="search" autocomplete="off">
				  <datalist id="browsers">
				    
				  </datalist>
				<button type="submit"><img src="images/search-icon.svg" alt=""></button>
			</form>
		</div>
	</div>
</section>
<!-- Innerpage Search -->
<section class="page-container">
	<!-- Mobile Search -->
	<div class="multicolored-line multicolored-line-mobile">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="home-search-wrapper search-wrapper-mobile">
		<form class="home-search" action="">
			<input type="text" placeholder="Search for footages and themes" name="home-search">
			<button type="submit"><img src="images/search-icon.svg" alt=""></button>
		</form>
	</div>
	<!-- Mobile Search -->
	<!-- Innerpage Banner -->
	<section class="page-section mb-0">
		<div class="innerpage-banner innerpage-banner-transparent pt-50 ptmob-25 pb-0">
			<div class="innerpage-banner-grey-bg full-height"></div>
			<div class="container">
				<div class="row">
					<div class="col-12 no-padding-mobile">
						<div class="profile-top-wrapper">							
							<h1 class="page-title-large text-uppercase">Account Settings</h1>							
						</div>
						<div class="profile-tabs-wrapper editprofile-tabs-wrapper">
							<div class="tabs">
								<ul class="tabs__items">
									<li><a href="#/" class="edit-profile-link active">Edit Profile</a></li>
									<li><a href="#/" class="change-password-link">Change Password</a></li>	
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Innerpage Banner -->
	<!-- Edit Profile Section -->
	<section class="page-section pt-50 pb-50 ptmob-25 pbmob-25" id="edit-profile">
		<div class="container">
			<div class="row">
				<div class="col-12 offset-sm-2 col-sm-6">
					<div class="edit-profile-wrapper">
						<?php 
						$profile = $profile_data[0];
						 ?>
						<form class="" id="form-profile" method="post" enctype="multipart/form-data">
							<input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden" class="txt_csrfname">
							<input name="user_id" id="user_id" value="<?= (!empty($profile['user_id'])?$profile['user_id']:"") ?>" type="hidden">

							<input type="hidden" class="form-control ff-field-dark" name="pre_profile_name" value="<?= (!empty($profile['profile_photo'])?$profile['profile_photo']:"") ?>">
							<div class="form-group">
								<div class="profile-pic-wrapper">
									<img src="<?= (!empty($profile['profile_photo'])?base_url('images/profile_image/'.$profile['profile_photo']):"https://via.placeholder.com/100x100.png") ?>" alt="" class="edit-profilepic">
									
									<div class="changepic-btn">
										<input type="button" class="btn-ff btn-secondary-dark text-uppercase imgupload-btn" value="Update Photo">
										<input type="file" id="imgupload" name="profile" style="display:none"/> 
										<p>Add a jpeg file under 200Kb.</p>
									</div>
								</div>
								
							</div>
							<div class="form-group">
								<label for="username">Username</label>
								<input type="text" class="form-control ff-field-dark" id="username" name="username" value="<?= (!empty($profile['user_name'])?$profile['user_name']:"") ?>">						
							</div>
							<div class="form-group">
								<label for="useremail">Email</label>
								<input type="email" class="form-control ff-field-dark" id="useremail" name="useremail" value="<?= (!empty($profile['email_id'])?$profile['email_id']:"") ?>">						
							</div>
							<div class="form-group">
								<label for="userphone">Phone</label>
								<input type="text" class="form-control ff-field-dark" id="userphone" name="userphone" value="<?= (!empty($profile['phone'])?$profile['phone']:"") ?>" >						
							</div>
							<div class="form-group">
								<input type="submit" class="btn-ff btn-primary-dark text-uppercase" value="Update Account">
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Edit Profile Section -->
	<!-- Update Password Section -->
	<section class="page-section pt-50 pb-50 ptmob-25 pbmob-25" id="change-password">
		<div class="container">		
			<div class="row">
				<div class="col-12 offset-sm-2 col-sm-6">
					<div class="edit-profile-wrapper">
						<form class="" id="form-password" method="post" enctype="multipart/form-data">
							<input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden" class="txt_csrfname">
							<input name="user_id" id="id" value="<?= (!empty($profile['user_id'])?$profile['user_id']:"") ?>" type="hidden">

							<div class="form-group">
								<label for="currentpassword">Current Password</label>
								<input type="password" class="form-control ff-field-dark" id="currentpassword" name="currentpassword">						
							</div>
							<div class="form-group">
								<label for="setpassword">Set a Password</label>
								<input type="password" class="form-control ff-field-dark" id="password" name="password">
								<a href="#/" class="showpassword"><img src="images/show.svg" alt=""></a>	
							</div>
							<div class="form-group">
								<label for="confirmpassword">Password Confirmation</label>
								<input type="password" class="form-control ff-field-dark" id="confirmpassword" name="confirmpassword">
								<a href="#/" class="confirmpassword"><img src="images/show.svg" alt=""></a>	
							</div>
							<div class="form-group">
								<input type="submit" class="btn-ff btn-primary-dark text-uppercase" value="Change Password">
							</div>
						</form>
					</div>
				</div>
			</div>			
		</div>
	</section>
	<!-- Update Password Section -->	
</section>

</body>

</html>