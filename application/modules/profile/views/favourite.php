<div class="row">
    <?php if(!empty($fav_data) && isset($fav_data)){
        foreach ($fav_data as $key => $value) {
            if(!empty($value[0]['video_id']) && isset($value[0]['video_id'])){?>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="block-single ">
                        <a href="<?= base_url('reel_detail/'.$value[0]['video_id'])?>">
                            <h3><?= $value[0]['video_title']?></h3>
                            <div class="block-info">
                                <div>
                                    <p class="block-info-title">Reel Number</p>
                                    <p class="block-info-content"><?=$value[0]['reel_number']?></p>
                                </div>
                                <span class="fav-tag fav-reel">Reel</span>
                            </div>
                        </a>						
                        <a href="#/">
                            <div class="hover-options-container">
                                <div class="box-single-checkbox">
                                    <label class="checkbox-container">
                                        <!-- <input type="checkbox"> -->
                                        <input type="checkbox" class="cls_checkbox"  value="<?= $value[0]['video_id']?>" name="reels[]"id="">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="box-single-share">
                                    <!-- <span class="share-single share-favourite addtofavourite-reel" onclick="addToFav('single',<?=$value[0]['video_id']?>)"></span>
                                
                                    <span class="share-msg-text share-favourite-text">
                                        Add to Favourite
                                    </span> -->
                                    <span class="share-single share-social reelshare" data-fancybox data-src="#sharetheme-content" data-url="<?= base_url('reel_detail/'.$value[0]['video_id'])?>" data-options='{"touch" : false}'></span>	
                                    <span class="share-msg-text share-social-text">
                                        Share 
                                    </span>									
                                    <span class="share-single share-project" data-fancybox data-src="#noprojects-content" data-options='{"touch" : false}'></span>
                                    <span class="share-msg-text share-project-text">
                                        Add to Project
                                    </span>										
                                    
                                    <span class="share-single share-cart"  onclick="addToCart('single',<?=$value[0]['video_id']?>)"></span>
                                    <span class="share-msg-text share-cart-text">
                                        Add to Cart
                                    </span>
                                </div>
                                <iframe src="<?= $value[0]['vimeo_url']?>?byline=0&portrait=0" class="listing-video" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                            </div>
                        </a>
                    </div>
                </div>
            <?php }elseif (!empty($value[0]['sub_theme_id']) && isset($value[0]['sub_theme_id'])){?>
                <!-- <div class="col-12 col-md-6 col-lg-4">
                    <div class="block-single ">
                        <a href="<?= base_url('reels/'.$value[0]['sub_theme_id'])?>">
                            <h3><?= $value[0]['sub_theme_name']?></h3>
                            <div class="block-info">
                                <div>
                                    <p class="block-info-title">Total Reels</p>
                                    <p class="block-info-content"><?=$value[0]['total_reel']?></p>
                                </div>
                                <span class="fav-tag fav-subtheme">Sub-Theme</span>
                            </div>
                        </a>						
                        <a href="<?= base_url('reels/'.$value[0]['sub_theme_id'])?>">
                            <div class="hover-options-container">
                                <iframe src="<?= $value[0]['sub_main_video']?>?byline=0&portrait=0" class="listing-video" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                            </div>
                        </a>
                    </div>
                </div> -->
                <div class="col-12 col-md-6 col-lg-4">
	<div class="block-single theme-single themeview" data-themeid="<?= $value['theme_id']?>">
		<a href="<?= base_url('subtheme/'.$value['theme_id'])?>">
			<h3><?= $value['theme_name']?></h3>
			<div class="block-info">
				<div>
					<p class="block-info-title">Sub-themes</p>
					<p class="block-info-content"><?= $value['totol_sub_themes']?></p>
				</div>
				<div>
					<p class="block-info-title">Total Reels</p>
					<p class="block-info-content"><?= $value['total_reals']?></p>
				</div>
			</div>							
		</a>
		<a href="javascript:void(0)">
			<div class="hover-options-container">
				<div class="box-single-checkbox">
					<label class="checkbox-container">
						<!-- <input type="checkbox"> -->
						<input type="checkbox" class="cls_checkbox"  value="<?= $value['theme_id']?>" name="reels[]"id="">
						<span class="checkmark"></span>
					</label>
				</div>
				<div class="box-single-share">
					<!-- <span class="share-single share-favourite addtofavourite-reel"></span>
					<span class="share-msg-text share-favourite-text">
						Add to Favourite
					</span> -->
					<span class="share-single share-favourite addtofavourite-reel" onclick="addToFav('single',<?=$value['theme_id']?>)"></span>			
					<span class="share-msg-text share-favourite-text">
						Add to Favourite
					</span>
					<span class="share-single share-social" data-fancybox data-src="#sharetheme-content" data-options='{"touch" : false}'></span>
					<span class="share-msg-text share-social-text">
						Share
					</span>									
				</div>
				<img src="<?= base_url('images/theme_image/'.$value['thumbnail_image'])?>" alt="<?= $value['theme_name']?>" class="img-fluid">
			</div>
		</a>
	</div>
</div>
            <?php }else{?>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="block-single ">
                        <a href="<?= base_url('subtheme/'.$value[0]['theme_id'])?>">
                            <h3><?= $value[0]['theme_name']?></h3>
                            <div class="block-info">
                                <div>
                                    <p class="block-info-title">Sub-themes</p>
                                    <p class="block-info-content"><?=$value[0]['totol_sub_themes']?></p>
                                </div>
                                <div>
                                    <p class="block-info-title">Total Reels</p>
                                    <p class="block-info-content"><?= $value[0]['total_reals']?></p>
                                </div>
                                <span class="fav-tag fav-theme">Theme</span>
                            </div>
                        </a>						
                        <a href="<?= base_url('subtheme/'.$value[0]['theme_id'])?>">
                            <div class="hover-options-container">	
                                <iframe src="<?= $value[0]['main_video']?>?byline=0&portrait=0" class="listing-video" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                            </div>
                        </a>
                    </div>
                </div>
            <?php }
        
        }
    }
    ?>
</div>