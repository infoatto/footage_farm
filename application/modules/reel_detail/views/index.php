<!-- Enquiry Sent Message -->
<section class="page-section pt-40" id="enquiry-sent-content">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="enquiry-sent-wrapper">
					<img src="<?= base_url()?>images/success.svg" alt="">
					<p class="enquiry-success">Enquiries Successfully Sent!</p>
					<p class="enquiry-success-note">Thank you for sending the request. We will reach you shortly to proceed further</p>
					<a class="btn-ff btn-primary-dark text-uppercase" href="<?= base_url()?>">Go Back to Homepage</a>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Enquiry Sent Message -->
<!-- Innerpage Search -->
<section class="page-container innerpage-top">	
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="searchbar-top">
		<div class="home-search-wrapper">
			<form class="home-search search-form" action="">
				<input type="text" list="browsers" placeholder="Search for footages and themes" name="search" autocomplete="off">
				  <datalist id="browsers">
				    
				  </datalist>
				<button type="submit"><img src="<?= base_url()?>images/search-icon.svg" alt=""></button>
			</form>
		</div>
	</div>				
</section>
<!-- Innerpage Search -->
<section class="page-container">
	<!-- Mobile Search -->
	<div class="multicolored-line multicolored-line-mobile">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="home-search-wrapper search-wrapper-mobile">
		<form class="home-search" action="">
			<input type="text" placeholder="Search for footages and themes" name="home-search">
			<button type="submit"><img src="<?= base_url()?>images/search-icon.svg" alt=""></button>
		</form>
	</div>
	<!-- Mobile Search -->
	<!-- Innerpage Banner -->
	<section class="page-section mb-0">		
		<div class="innerpage-banner innerpage-banner-transparent pt-0 pb-0">
			<div class="innerpage-banner-grey-bg"></div>
			<div class="container">
				<div class="row">
					<div class="col-12 no-padding-mobile">						
						<div class="banner-top-vimeo-wrapper">
							<iframe src="<?= $reel_data['vimeo_url']?>" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="banner-top-vimeo">
							</iframe>
						</div>
						<!-- <div class="clearfix"></div> -->
						<!-- <script src="https://player.vimeo.com/api/player.js"></script>							 -->
					</div>					
				</div>
			</div>			
		</div>				
	</section>
	<!-- Innerpage Banner -->
	<!-- Selected Video Details -->
	<section class="page-section pt-25 pb-50 mb-0 pbmob-25">
		<div class="container">
			<div class="row">				
				<div class="col-12 col-md-9 selected-video-details-wrapper">
					<div class="selected-video-details">
						<h1 class="selected-video-title"><?=  $reel_data['video_title']?></h1>
					</div>
					<div class="selected-video-request">
						<div class="svr-left">

							<?php if($this->session->userdata('footage_farm_user')) { ?>
								<a href="#/" data-fancybox data-src="#selectprojects-content" data-options='{"touch" : false}' class="">
							<?php }else{?>
								<a href="#/" class="" data-fancybox="" data-src="#signin-content" data-options="{&quot;touch&quot; : false}">
							<?php } ?>
							<img src="<?= base_url()?>images/project-blue.svg" alt=""></a>
							

							<?php if(!$this->session->userdata('footage_farm_user')) { ?>
								<a href="#/" class="addtofavourite-reel" data-fancybox="" data-src="#signin-content" data-options="{&quot;touch&quot; : false}">
							<?php }else{?>
								<a href="Javascript:void(0)" class="addtofavourite-reel" onclick="addToFav(<?= $reel_data['video_id']?>)">
							<?php }?>
								<img src="<?= base_url()?>images/favourite-blue.svg" alt="">
							</a>

							<a href="#/" class="reelshare" data-fancybox data-src="#sharetheme-content" data-url="<?= base_url('reel_detail/'.$reel_data['video_id'].'/'.$reel_data['sub_theme_id'])?>" data-options='{"touch" : false}'><img src="<?= base_url()?>images/share-blue.svg" alt=""></a>
						</div>
						<?php if($this->session->userdata('footage_farm_user')){?>
						<div class="svr-right">
							<a href="#/" class="btn-ff btn-ff-icon btn-secondary-dark text-uppercase request-reel-btn" data-fancybox data-src="#enquiry-master-content" data-options='{"touch" : false}'>Request a reel</a>	
							<a href="javascript:void(0)" onclick="addToCart('single',<?= $reel_data['video_id']?>)" class="btn-ff btn-ff-icon btn-primary-dark text-uppercase enquiry-cart-btn">Add to enquiry cart</a>	
						</div>
						<?php }else{?>

							<a href="#/"  style="margin-right: -460px;"class="btn-ff btn-ff-icon btn-secondary-dark text-uppercase request-reel-btn" data-fancybox data-src="#signin-content" data-options='{"touch" : false}' >Request a reel</a>	
							<a href="#/" class="btn-ff btn-ff-icon btn-primary-dark text-uppercase enquiry-cart-btn" data-fancybox data-src="#signin-content" data-options='{"touch" : false}'>Add to enquiry cart</a>	
						<?php } ?>
					</div>
					<div class="selected-video-info">
						<div class="svi-col-1">
								<div class="svi-single">
									<p><strong>Reel Number</strong> : <?=$reel_data['reel_number'];?></p>
								</div>
								<div class="svi-single">
									<p><strong>Color</strong> : <?=$reel_data['colour_id'];?></p>
								</div>
								<div class="svi-single">
									<p><strong>Sound</strong> : <?=$reel_data['sound_id'];?></p>
								</div>
						</div>
						<div class="svi-col-2">
							<div class="svi-single">
								<p><strong>Year / Date</strong> : <?= $reel_data['year']?></p>
							</div>
							<div class="svi-single">
								<p><strong>Country</strong> : <?= $reel_data['country_names'] ?></p>
							</div>
							<div class="svi-single">
								<p><strong>Location</strong> : <?= $reel_data['location_names'] ?></p>
							</div>	
						</div>
						<div class="svi-col-3">
							<div class="svi-single">
								<p><strong>TC Begins</strong> : <?= $reel_data['tc_begins'] ?></p>
							</div>
							<div class="svi-single">
								<p><strong>TC Ends</strong> : <?= $reel_data['tc_ends'] ?></p>
							</div>
							<div class="svi-single">
								<p><strong>Duration</strong> :<?= $reel_data['duration']?></p>
							</div>	
						</div>
					</div>
					<div class="selected-video-commentary">
						<p><?=  $reel_data['long_description']?></p>
					</div>
					<a href="#/" class="show-svc">Show more</a>
					<div class="blog-tags">
					<?php $all_tags = explode(",",$reel_data['tag_names']);
						foreach ($all_tags as $tagkey => $tagvalue) { ?>
							<a href="#/"><?= $tagvalue?></a>
						<?php } ?>
					</div>
				</div>
				<div class="col-12 col-sm-3">
					<div class="next-in-queue">
							<h3>Related Reels</h3>
							<div class="next-in-queue-videos">
								<?php
								if(!empty($related_reels)){
								foreach ($related_reels as $key => $value) {?>
								<div class="niqv-single">
									<div class="niqv-video">
										<div style="padding:75% 0 0 0;position:relative;"><iframe src="<?= $value['vimeo_url']?>?byline=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>
										<!-- <script src="https://player.vimeo.com/api/player.js"></script> -->
										<div class="niqv-overlay">
											<a href="<?= base_url('reel_detail/'.$value['video_id'].'/'.$value['sub_theme_id'])?>">
												<p><?= $value['video_title']?></p>
											</a>
										</div>	
									</div>	
								</div>
								<?php } } ?>

								
							</div>
					</div>		
				</div>				
			</div>
		</div>
	</section>
	<!-- Selected Video Details -->	
	<!-- Similar SubThemes -->
	<section class="page-section pt-75 mb-0 similar-section-wrapper">
		<div class="grey-bg"></div>
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="multicolored-line">
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
					</div>
					<div class="section-title-wrapper">
						<h2 class="section-title">
							<img src="<?= base_url()?>images/section-headline.svg" alt=""> Similar Sub-Themes
						</h2>						
					</div>
				</div>

				<?php
			    if(!empty($similar_subtheme)){
				foreach ($similar_subtheme as $key => $value) { ?>
				<div class="col-12 col-md-6 col-lg-4">
					<div class="block-single theme-single">
						<a href="<?= base_url('reels/'.$value['sub_theme_id'])?>">
							<h3><?= $value['sub_theme_name']?></h3>
							<div class="block-info">
								<div>
									<p class="block-info-title">Total Reels</p>
									<p class="block-info-content"><?= $value['total_reals']?></p>
								</div>
							</div>
						</a>
						<a href="#/">
							<div class="hover-options-container">
								<div class="box-single-checkbox">
									<label class="checkbox-container">
										<input type="checkbox">
										<span class="checkmark"></span>
									</label>
								</div>
								<div class="box-single-share">
									<span class="share-single share-favourite addtofavourite-reel"></span>
									<span class="share-msg-text share-favourite-text">
										Add to Favourite
									</span>
									<span class="share-single share-social" data-fancybox data-src="#sharetheme-content" data-options='{"touch" : false}'></span>
									<span class="share-msg-text share-social-text">
										Share
									</span>
								</div>
								<img src="<?= base_url('images/sub_theme_image/'.$value['sub_thumbnail_image'])?>" alt="<?= $value['sub_thumbnail_image']?>" class="img-fluid">
							</div>
						</a>
					</div>
				</div>
				<?php } } ?>	


			</div>
		</div>
	</section>						
	<!-- Similar SubThemes -->
</section>



<!-- Request Enquiry Master -->
<div style="display: none;max-width:700px;" id="enquiry-master-content" class="ff-modal">
	<div class="modal-header">		
		<h3>Request Enquiry</h3>		
	</div>
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="ff-modal-content">
		<form class="enquiry-master-form" id="form-enquiry-cart-detail" method="post" enctype="multipart/form-data" >
		<div class="modal-section">
			<!-- <form class="enquiry-master-form"> -->
			<!--<form class="enquiry-master-form details-enquiry-form" id="form-enquiry-cart-detail" method="post" enctype="multipart/form-data" >-->			
			

				<input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden" class="txt_csrfname">		
				<input type="hidden" id="video_id" name="video_id" value="<?=$reel_data['video_id']?>">
	
				<div class="form-group">					
					<div class="reel-title">
						<iframe src="<?= $reel_data['vimeo_url']?>" frameborder="0" allow="autoplay; fullscreen" allowfullscreen class="reel_detail_video">
						</iframe>
						<h3><?=$reel_data['video_title']?></h3>
					</div>					
				</div>
				<div class="form-group">
					<div class="request-reel-type">
						<label for="selectreeltype">Please select the reel type</label>
						<label class="radio-container-theme">
							<span class="radio-label">Request Preview</span>
							<input type="radio" name="reeltype" value ="preview" >
							<span class="radio"></span>
						</label>
						<label class="radio-container-theme">
							<span class="radio-label">Request Master</span>
							<input type="radio" name="reeltype" value="master">
							<span class="radio"></span>
						</label>
					</div>
					<label for="reeltype" class="error"></label>	
				</div>
				<div class="form-group">
					<label for="inouttime">Specify the footage's in and out time</label>

					<div class="begin-end-tc">
                    	<div class="tc-time begin-tc">
							<label for="begintc">Begin TC
								<a href="#" class="tooltip-info">
									<img src="<?= base_url()?>images/info.svg" alt="">
									<span class="tooltip-msg">
										Footage start time
									</span>
								</a>
							</label>
							<input type="text" class="form-control ff-field-dark" id="begintc<?=$reel_data['video_id']?>" name="begintc[]" placeholder="00:00:00" >
                   		</div>
						<div class="tc-time end-tc">
							<label for="endtc">End TC
								<a href="#" class="tooltip-info">
									<img src="<?= base_url()?>images/info.svg" alt="">
									<span class="tooltip-msg">
										Footage end time
									</span>
								</a>
							</label>
							<input type="text" class="form-control ff-field-dark" id="endtc<?=$reel_data['video_id']?>" name="endtc[]" placeholder="00:00:00" >
						</div>
                	</div>

					<div id="addmore-begin-end-tc"></div>
					<div class="tc-addmore">
						<a href="#/" class="tc-addmore-link"><img src="<?= base_url()?>images/add-black.svg" alt=""> <span>Add another in and out time codes</span></a>
					</div>					 		
				</div>
				<div class="form-group">	
					<label>Do you have a file with multiple in and out time codes?</label>
					<p>Add jpegs, excel or word files here. Each file size should be under 2MB.</p>
					<div id="upload-reel-time">						
						<input type="file" name="reelfiles[]" multiple  id="reelfile" >
					</div>
				</div>
				<div class="form-group">					
					<div class="format-framerate">
						<div class="format formatframe">
							<label for="begintc">Format</label>
							<?php $format = array('MP4','3GP','WMV','WEBM','WEBM','FLV','AVI','QuickTime'); ?> 
							<select class="niceselect ff-select-dark formatselect" name="format" id="format<?=$reel_data['video_id']?>"  >
								<?php  $sel = '' ;
									foreach ($format as $key => $value) { ?>
										<option value="<?= $value ?>" <?= $sel;?>><?= $value ?></option>
									<?php  } ?>
						
							</select>
							<label id="formatselect-error" class="error" for=""></label>
						</div>
						<div class="framerate formatframe">
							<label for="endtc">Frame Rate</label>
							<?php $frame_rate = array('24FPS','25FPS','30FPS','50FPS','60FPS') ?>
							<select class="niceselect ff-select-dark framerateselect"  name="frame_rate" id="frame_rate<?=$reel_data['video_id']?>"  >

							<?php  $sel = '' ;
									foreach ($frame_rate as $key => $value) { ?>
										<option value="<?= $value ?>" <?= $sel;?>><?= $value ?></option>
									<?php  } ?>
						
							</select>
							<label id="framerateselect-error" class="error" for=""></label>
						</div>						
					</div>											 		
				</div>
				<div class="form-group">
					<label for="codec">Codec 
						<a href="#" class="tooltip-info">
							<img src="<?= base_url()?>images/info.svg" alt="">
							<span class="tooltip-msg">
								Codec type
							</span>
						</a>
					</label>
					<input type="text" class="form-control ff-field-dark" id="codec<?=$reel_data['video_id']?>" value="" name="codec" placeholder="QT ProRes422HQ" >
				</div>
				<div class="form-group">
					<label for="projectdesc">Message (optional)</label>
					<textarea class="form-control ff-field-dark" name="message" id="message<?=$reel_data['video_id']?>" rows="5"></textarea>
				</div>
			</div>
			<div class="modal-content-wrapper">			
				<div class="modal-actions">
					<input type="submit" data-fancybox-close class="btn-ff btn-secondary-dark text-uppercase"  value="Cancel">
					<input type="submit" class="btn-ff btn-primary-dark text-uppercase" value="Send Request">
				</div>	
			</div>
			</form>		
	</div>
</div>
<!-- Request Enquiry Master -->

<script>
	getProject();
	$(document).ready(function(){
		/*Notification messages*/
		$('.addtofavourite-subtheme').click(function() {
			$('.notification-message').fadeIn();
			$('.notification-modal-text').text('You saved this subtheme as your favourites!')
			setTimeout(function() {
				$('.notification-message').fadeOut();
			}, 2000);
		});
		$('.addtofavourite-reel').click(function() {
			$('.notification-message').fadeIn();
			$('.notification-modal-text').text('You saved this theme as your favourites!')
			setTimeout(function() {
				$('.notification-message').fadeOut();
			}, 2000);
		});
		$('.select-all').click(function() {
			$('.notification-message').fadeIn();
			$('.notification-modal-text').text('You selected all themes!')
			setTimeout(function() {
				$('.notification-message').fadeOut();
			}, 2000);
		});
		$('.deselect-all').click(function() {
			$('.notification-message').fadeIn();
			$('.notification-modal-text').text('You deselected all themes!')
			setTimeout(function() {
				$('.notification-message').fadeOut();
			}, 2000);
		});
		$('.addtocart-reel').click(function() {
			$('.notification-message').fadeIn();
			$('.notification-modal-text').text('5 themes have been added to the cart!')
			setTimeout(function() {
				$('.notification-message').fadeOut();
			}, 2000);
		})
		$('.share-cart').click(function() {
			$('.notification-message').fadeIn();
			$('.notification-modal-text').text('Reel added to cart!')
			setTimeout(function() {
				$('.notification-message').fadeOut();
			}, 2000);
			$('.selected-items-wrapper').fadeIn()
		})
		/*Notification messages*/
		/*Addmore Start/ End TC*/
		$('.tc-addmore-link').click(function() {
			$('#addmore-begin-end-tc').append(
				"<div class='begin-end-tc'>" +
				"<div class='tc-time begin-tc'>" +
				"<label for='begintc'>Begin TC" +
				"</label>" +
				"<input type='text' class='form-control ff-field-dark' id='begintc' name='begintc[]' placeholder='00:00:00'>" +
				"</div>" +
				"<div class='tc-time end-tc'>" +
				"<label for='endtc'>End TC" +
				"</label>" +
				"<input type='text' class='form-control ff-field-dark' id='endtc' name='endtc[]' placeholder='00:00:00'>" +
				"<a href='#/' class='remove-tc'><img src='<?= base_url('assets/')?>images/close.svg'/></a>" +
				"</div>	" +
				"</div>"
			)
		});
		$('body').on('click', '.remove-tc', function() {
			$(this).closest('.begin-end-tc').remove();
		});
		/*Addmore Start/ End TC*/
		$('#upload-reel-times').dropzone({
			url: "/file/post"
		});
		$('.file-remove-icon').click(function() {
			$(this).closest('.file-single').remove();
		});
		$('.share-reel-form').validate();
		$('.enquiry-master-form').validate({
			ignore: []
		});
		$('.show-svc').click(function(){
			$('.selected-video-commentary').toggleClass('show')
		});			
		if($(window).width()>1023){
			$('.niqv-video').hover(function(){
			$(this).children('.niqv-overlay').fadeIn();
			},function(){
				$(this).children('.niqv-overlay').fadeOut();
			})
		}

	
		
	});

		// get project listing code by shiv on  21-10-2020
	function getProject() {
		var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
		var csrfHash = $('.txt_csrfname').val(); // CSRF hash
		$.ajax({
			url: "<?= base_url('project/projectdata')?>",
			type: "POST",
			data:{ [csrfName]: csrfHash },
			dataType: "json",
			success: function(response){
				if(response.success){
					$("#projectshow").html(response.data);		
				}else{
					$("#projectshow").html('');
				}
				
				$('.mep-single').hover(function () {
					$(this).children('.hover-options-container').children('.mep-box-single-checkbox').show();
				}, function () {
					$(this).children('.hover-options-container').children('.mep-box-single-checkbox').hide();
					/*Keep checkbox visible if checked*/
					var mepcheckbox = $(this).children('.hover-options-container').children('.mep-box-single-checkbox').children('label.checkbox-container').children('input[type="checkbox"]');
					if (mepcheckbox.is(':checked')) {
						$(this).children('.hover-options-container').children('.mep-box-single-checkbox').addClass('selected');
						//$(this).children('.hover-options-container').siblings('.mep-single-overlay').addClass('selected')
					} else {
						$(this).children('.hover-options-container').children('.mep-box-single-checkbox').removeClass('selected');
						//$(this).children('.hover-options-container').siblings('.mep-single-overlay').removeClass('selected')
					}
					/*Keep checkbox visible if checked*/
				});


			}
		});
	}
	// use for project form submit
	$("#form-project").validate({
		ignore:[],
		rules: {
			// nameofproject:{required:true},
			// projectdesc/:{required:true},
		},
		messages: {
			// nameofproject:{required:"Please Enter Project Name."},
			// projectdesc:{required:"Please Enter Description."},
		},
		submitHandler: function(form) 
		{	
			var myCheckboxes = new Array();
			var reels = []; 
			reels.push(<?= $reel_data['video_id']?>);
			//var pagedata = { "keyword": myCheckboxes};
			
			var act = "<?= base_url('project/submitForm')?>";
			$("#form-project").ajaxSubmit({
				url: act, 
				type: 'post',
				data: { reel:reels, myCheckboxes },
				dataType: 'json',
				cache: false,
				clearForm: false,
				async:false,
				beforeSubmit : function(arr, $form, options){
					$(".btn btn-black").hide();
				},
				success: function(response) 
				{
					if(response.success){
						getProject();
						swal("done", response.msg, {
							icon : "success",
							buttons: {        			
								confirm: {
									className : 'btn btn-success'
								}
							},
						}).then(
						function() {

							// $("#selectprojects-content").modal('show');
						});
					}else{	
						swal("Failed", response.msg, {
							icon : "error",
							buttons: {        			
								confirm: {
									className : 'btn btn-danger'
								}
							},
						});
					}
				}
			});
		}
	});

	// add to cart shiv code on 22-9-2020
	function addToCart(fav_type,video_id){
		var selected_reels = [];
		selected_reels.push(video_id);

		console.log(selected_reels);
		var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
		var csrfHash = $('.txt_csrfname').val(); // CSRF hash
		if(selected_reels){
				$.ajax({
					url: "<?= base_url('reels/addToCart')?>",
					type: "POST",
					data:{ selected_reels , [csrfName]: csrfHash },
					dataType: "json",
					success: function(response){
						if(response.success){
							$('.notification-message').fadeIn();		
							$('.notification-modal-text').text(' Reel have been added to the cart!')
							setTimeout(function(){ $('.notification-message').fadeOut();	 }, 2000);
						}else{
							$('.notification-message').fadeIn();		
							$('.notification-modal-text').text(response.msg)
							setTimeout(function(){
								$('.notification-message').fadeOut();	 
								}, 2000);	
						}
					}
				});
			}
	}

	// add to fav shiv code on 21-10-20202
	function addToFav(video_id){
		var selected_reels = [];
		selected_reels.push(video_id);
		console.log(selected_reels);
		var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
		var csrfHash = $('.txt_csrfname').val(); // CSRF hash
		if(selected_reels){
				$.ajax({
					url: "<?= base_url('reels/addToFav')?>",
					type: "POST",
					data:{ selected_reels , [csrfName]: csrfHash },
					dataType: "json",
					success: function(response){
						if(response.success){
							$('.notification-message').fadeIn();		
							$('.notification-modal-text').text('You saved this reel as your favourites!')
							setTimeout(function(){
								$('.notification-message').fadeOut();	 
								}, 2000);	
						}else{
							$('.notification-message').fadeIn();		
							$('.notification-modal-text').text(response.msg)
							setTimeout(function(){
								$('.notification-message').fadeOut();	 
								}, 2000);	
						}
					}
				});
			}
	}


	// use for each form submisson
$("#form-enquiry-cart-detail").validate({
	ignore:[],
	rules: {
		reeltype:{required:true},
		format:{required:true},
		frame_rate:{required:true},
		codec:{required:true},
		"begintc[]":{required:true},
		"endtc[]":{required:true},
	},
	messages: {
		reeltype:{required:"Please Select Reel type of video."},
		format:{required:"Please select Format of video."},
		frame_rate:{required:"Please select Frame Rate of video."},
		codec:{required:"Please Enter Codec for this Reel."},
		"begintc[]":{required:"Please Enter Begin TC."},
		"endtc[]":{required:"Please Enter End TC."},
	},
	submitHandler: function(form) 
	{	
		var act = "<?= base_url('reel_detail/submitForm')?>";
		$("#form-enquiry-cart-detail").ajaxSubmit({
			url: act, 
			type: 'post',
			dataType: 'json',
			cache: false,
			clearForm: false,
			async:false,
			beforeSubmit : function(arr, $form, options){
				$(".btn btn-black").hide();
			},
			success: function(response) 
			{
				if(response.success){
					$('#enquiry-master-content').modal('hide');
					swal("done", response.msg, {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					}).then(
					function() {
						location.reload();
					});
				}else{	
					swal("Failed", response.msg, {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}
			}
		});
	}
});

</script>
