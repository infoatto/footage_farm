<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Reel_detail extends MX_Controller
{
	
	public function __construct(){
		parent::__construct();
		//checklogin();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('reeldetailmodel','',TRUE);
	}

	function _remap($method_name){
		if(!method_exists($this, $method_name)){
			$this->index();
		}else{
		$this->{$method_name}();
		}
	}

	public function index(){
		$videoid = $this->uri->segment(2);
		$subthemeid = $this->uri->segment(3);

		$result = array();

	    $condition = "sb.video_id = '".$videoid."' AND sb.sub_theme_id = '".$subthemeid."' ";
		$main_table = array("tbl_videos as tv", array("tv.*"));
		$join_tables =  array();
		$join_tables = array(
				  array("", "tbl_video_theme_subtheme_mapping as  sb", "tv.video_id = sb.video_id", array("sb.theme_id, sb.sub_theme_id")),
				  array("", "tbl_video_tag_mapping as  tm", "tm.video_id = sb.video_id", array("sb.theme_id, sb.sub_theme_id")),
				  array("", "tbl_tags as  tn", "tn.tag_id = tm.tag_id", array("GROUP_CONCAT(DISTINCT(tn.tag_name)) as tag_names")),
				  array("", "tbl_video_country_location_mapping as  tvclm", "tvclm.video_id = tv.video_id", array()),
				  array("", "tbl_country_location as  tcl", "tcl.country_location_id = tvclm.country_location_id", array("GROUP_CONCAT(DISTINCT(tcl.location_name)) as location_names")),
				  array("", "countries as  c", "c.country_id = tcl.country_id", array("GROUP_CONCAT(DISTINCT(c.country_name)) as country_names")),
		          );

		$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("tv.video_id" => "DESC"),"",null); 
		$featured_theme_data = $this->common->MySqlFetchRow($rs, "array");
		$result['reel_data']  = $featured_theme_data[0];

		$theme_id = $featured_theme_data[0]['theme_id'];
		$condition = "sub_theme_id != $subthemeid AND theme_id = $theme_id";
		$sub_themes = $this->common->getData("tbl_sub_themes",'*',$condition);
		if (!empty($sub_themes)) {
			foreach ($sub_themes as $key => $value) {
			$condition = "1=1 AND sub_theme_id = ".$value['sub_theme_id'];
			$total_reel = $this->common->getData("tbl_video_theme_subtheme_mapping",'count(video_id) as total_reel',$condition,"","","video_id");
			$sub_themes[$key]['total_reals'] =count($total_reel);
		}
			
		}
		$result['similar_subtheme']  = $sub_themes;

		$condition = "sb.video_id != $videoid AND sb.sub_theme_id = $subthemeid ";
		$main_table = array("tbl_videos as t", array("t.*"));
		$join_tables =  array();
		$join_tables = array(
		          array("", "tbl_video_theme_subtheme_mapping as  sb", "t.video_id = sb.video_id", array("sb.sub_theme_id")),
		          );

		$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("t.video_id" => "DESC"),"",null); 
		$related_reels = $this->common->MySqlFetchRow($rs, "array");
		$result['related_reels']  = $related_reels;				
		// echo "<pre>";print_r($result['reel_data']);exit;

			
		$result['meta_description'] = $result['reel_data']['meta_description'];
		$result['meta_keywords'] = $result['reel_data']['meta_title'];

		$this->load->view('header',$result);
		$this->load->view('index',$result);
		$this->load->view('footer');

	}

	public function addToFav(){
		// print_r($_POST['selected_reels']);
		// exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			if(!empty($_POST['selected_reels']) && isset($_POST['selected_reels'])){
				foreach ($_POST['selected_reels'] as $key => $value) {
					$data = array();
					$data['video_id'] = $value;
					$data['user_id'] = $this->session->userdata('footage_farm_admin')[0]['user_id'];
					$data['created_on'] = date("Y-m-d H:i:s");
					$data['favorite_date'] = date("Y-m-d");
					$data['created_by'] =  $this->session->userdata('footage_farm_admin')[0]['user_id'];
					$condition = "user_id = '".$this->session->userdata('footage_farm_admin')[0]['user_id']."' AND  video_id = '".$value."' ";
					$check_exist_fav = $this->common->getData("tbl_favourites",'*',$condition);
					if($check_exist_fav){
						$data['updated_on'] = date("Y-m-d H:i:s");
						$data['updated_by'] = $this->session->userdata('footage_farm_admin')[0]['user_id'];
						$condition = "favorite_id = '".$check_exist_fav[0]['favorite_id']."' ";
						$result = $this->common->updateData("tbl_favourites",$data,$condition);
					}else{
						$data['created_on'] = date("Y-m-d H:i:s");
						$data['created_by'] =  $this->session->userdata('footage_farm_admin')[0]['user_id'];
						$result = $this->common->insertData('tbl_favourites',$data,'1');
					}
				} //end of foreach 
					if(!empty($result)){
						echo json_encode(array('success'=>true, 'msg'=>'Record Added Successfully.'));
						exit;
					}else{
						echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
						exit;
					}
			}else{
				echo json_encode(array('success'=>false, 'msg'=>'Please select the reels first.'));
				exit;
			}
		

		}
	}

	public function addToCart(){
		// print_r($_POST['selected_reels']);
		// exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			if(!empty($_POST['selected_reels']) && isset($_POST['selected_reels'])){
				foreach ($_POST['selected_reels'] as $key => $value) {
					$data = array();
					$data['video_id'] = $value;
					$data['user_id'] = $this->session->userdata('footage_farm_admin')[0]['user_id'];
					$data['created_on'] = date("Y-m-d H:i:s");
					$data['video_inquiry_date'] = date("Y-m-d");
					$data['created_by'] =  $this->session->userdata('footage_farm_admin')[0]['user_id'];
					$condition = "user_id = '".$this->session->userdata('footage_farm_admin')[0]['user_id']."' AND  video_id = '".$value."' ";
					$check_exist_cart = $this->common->getData("tbl_video_cart",'*',$condition);
					if($check_exist_cart){
						$data['updated_on'] = date("Y-m-d H:i:s");
						$data['updated_by'] = $this->session->userdata('footage_farm_admin')[0]['user_id'];
						$condition = "cart_id = '".$check_exist_cart[0]['cart_id']."' ";
						$result = $this->common->updateData("tbl_video_cart",$data,$condition);
					}else{
						$data['created_on'] = date("Y-m-d H:i:s");
						$data['created_by'] =  $this->session->userdata('footage_farm_admin')[0]['user_id'];
						$result = $this->common->insertData('tbl_video_cart',$data,'1');
					}
				} //end of foreach 
					if(!empty($result)){
						echo json_encode(array('success'=>true, 'msg'=>'Record Added Successfully.'));
						exit;
					}else{
						echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
						exit;
					}
			}else{
				echo json_encode(array('success'=>false, 'msg'=>'Please select the reel first.'));
				exit;
			}
		

		}
	}
	public function submitForm(){
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{	
			$uploaded_files_data = array();
			$uploaded_files_type = array();
			if(isset($_FILES) && isset($_FILES["reelfiles"]["name"])){
				for($i=0;$i<count($_FILES["reelfiles"]["name"]);$i++){
					// print_r($value);		
					$_FILES['reelfile']['name'] = $_FILES['reelfiles']['name'][$i];
					$_FILES['reelfile']['type'] = $_FILES['reelfiles']['type'][$i];
					$_FILES['reelfile']['tmp_name'] = $_FILES['reelfiles']['tmp_name'][$i];
					$_FILES['reelfile']['error'] = $_FILES['reelfiles']['error'][$i];
					$_FILES['reelfile']['size'] = $_FILES['reelfiles']['size'][$i];	

					$config = array();
					$config['upload_path'] = DOC_ROOT_FRONT."/images/cart_upload/";
					$config['max_size']    = '2000';
					$config['allowed_types'] = 'gif|jpg|png|jpeg|pdf|docx|doc|msword|xlsx|xls|csv';
					$this->load->library('upload', $config);
		
					if (!$this->upload->do_upload("reelfile")){
						$image_error = array('error' => $this->upload->display_errors());
						echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
						exit;
					}else{
						$upload_files_data = array('upload_data' => $this->upload->data());
						$uploaded_files_data[] = $upload_files_data['upload_data']['file_name']; 
						$uploaded_files_type[] =  $upload_files_data['upload_data']['file_ext'];
					}	
				}
			}
				$data['user_id'] =$this->session->userdata('footage_farm_user')[0]['user_id'];
				$data['video_id'] = $_POST['video_id'];
				$data['request_type'] = $_POST['reeltype'];
				$data['format'] = $_POST['format'];
				$data['enquiry_date'] = date("Y-m-d");
				$data['frame_rate'] = $_POST['frame_rate'];
				$data['message'] = (!empty($_POST['message'])?$_POST['message']:" ");
				$data['codec'] = $_POST['codec'];
				$data['status']= 'Active';
				$data['created_on'] = date("Y-m-d H:i:s");
				$data['created_by'] =  $this->session->userdata('footage_farm_user')[0]['user_id'];
				$result = $this->common->insertData('tbl_video_enquiry_details',$data,'1');
				
				if($result){
					foreach ($_POST['begintc'] as $keytc => $valuetc) {
						$data_tc = array();
						$data_tc['user_id'] = $this->session->userdata('footage_farm_user')[0]['user_id'];
						$data_tc['video_id'] = $_POST['video_id'];
						$data_tc['begin_tc']= $valuetc;
						$data_tc['end_tc']= $_POST['endtc'][$keytc];
						$data_tc['status']= 'Active';
						$data_tc['created_on'] = date("Y-m-d H:i:s");
						$data_tc['created_by'] =  $this->session->userdata('footage_farm_user')[0]['user_id'];
						$result = $this->common->insertData('tbl_video_enquiry_tc',$data_tc,'1');
					}
				}

				if($result){
					if($uploaded_files_data){
						foreach ($uploaded_files_data as $keyupload => $valueupload) {
							$data_upload  = array();
							$data_upload['user_id'] = $this->session->userdata('footage_farm_user')[0]['user_id'];
							$data_upload['video_id'] = $_POST['video_id'];
							$data_upload['uploaded_document'] = $valueupload;
							$data_upload['document_type'] = $uploaded_files_type[$keyupload];
							$data_upload['created_on'] = date("Y-m-d H:i:s");
							$data_upload['created_by'] =  $this->session->userdata('footage_farm_user')[0]['user_id'];
							$result = $this->common->insertData('tbl_video_enquiry_uploads',$data_upload,'1');
						}
					}
					
				}
				if($result){
					echo json_encode(array('success'=>true, 'msg'=>'Thank you for sending the request. We will reach you shortly to proceed further'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
					exit;
				}

		}
	}

}
?>