<!-- Innerpage Search -->
<style type="text/css">
	.medium-insert-buttons-show{
		display: none;
	}

</style>
<section class="page-container innerpage-top">
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="searchbar-top">
		<div class="home-search-wrapper">
			<form class="home-search search-form" action="">
				<input type="text" list="browsers" placeholder="Search for footages and themes" name="search" autocomplete="off">
				  <datalist id="browsers">
				    
				  </datalist>
				<button type="submit"><img src="<?=base_url();?>images/search-icon.svg" alt=""></button>
			</form>
		</div>
	</div>
</section>
<!-- Innerpage Search -->
<section class="page-container">
	<!-- Mobile Search -->
	<div class="multicolored-line multicolored-line-mobile">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="home-search-wrapper search-wrapper-mobile">
		<form class="home-search" action="">
			<input type="text" placeholder="Search for footages and themes" name="home-search">
			<button type="submit"><img src="<?=base_url();?>images/search-icon.svg" alt=""></button>
		</form>
	</div>
	<!-- Mobile Search -->
	<!-- Innerpage Banner -->
	<section class="page-section mb-0">		
		<div class="innerpage-banner innerpage-banner-transparent pt-25 pb-0">
			<div class="innerpage-banner-grey-bg full-height"></div>
			<div class="container">
				<div class="row">
					<div class="col-12 no-padding-mobile">
						<div class="blog-intro-wrapper">
							<h1 class="page-title-small"><?= $blog_data[0]['blog_title'];?></h1>
							<div class="blog-tagline">
								<p><?= $blog_data[0]['blog_subheading']?></p>
							</div>
							<div class="author-share-wrapper">
								<div class="author-info">
									<img src="<?=($blog_data[0]['profile_photo']!='')?base_url('images/profile_image/'.$blog_data[0]['profile_photo']): base_url('images/blog-avatar.svg') ?>" alt="">
									<div class="author-name">
									<p class="ba-name"><?= $blog_data[0]['author_name']?></p>
									<?php $date_convert = date('Y-M-d',strtotime($blog_data[0]['blog_date']));
									$date_convert_array = explode('-',$date_convert); ?>
									<p class="ba-date"><?= $date_convert_array[2].' '.$date_convert_array[1].' '.$date_convert_array[0]?></p>
									</div>									
								</div>
								<div class="blog-share">
									<a href="#/" class="bs-twitter"><img src="images/twitter-black.svg" alt=""></a>
									<a href="#/" class="bs-facebook"><img src="images/facebook-black.svg" alt=""></a>
									<a href="#/" class="bs-linkedin"><img src="images/linkedin-black.svg" alt=""></a>
								</div>
							</div>	
						</div>	
					</div>					
				</div>
			</div>			
		</div>				
	</section>
	<!-- Innerpage Banner -->
	<!-- Blog Detail Section -->
	<section class="page-section pt-50 pb-0 ptmob-25 pbmob-25">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="blog-content-wrapper">
						<?= $blog_data[0]['blog_post_body'];?>
						
						<div class="blog-detail-tags">
							<h4>Tags</h4>
							<div class="blog-tags">
							<!-- <?php if(!empty($tags_data)){
					            foreach ($tags_data as $key => $value) {?>
								<a href="#/"><?= $value['tag_name']?></a>
							<?php } } ?> -->
							<?php $all_tags = explode(",",$blog_data[0]['tag_name']);
							foreach ($all_tags as $tagkey => $tagvalue) {?>
								<a href="#/"><?= $tagvalue?></a>
							<?php }?>
								<!-- <a href="#/">German Troops</a>
								<a href="#/">Marshal Von Hindenburg</a>
								<a href="#/">Flash Intertile</a>
								<a href="#/">American Aero Cop</a>
								<a href="#/">Military Intervention</a>
								<a href="#/">MS Prisoners</a>
								<a href="#/">Bactrian Camels </a>
								<a href="#/">POWs</a>
								<a href="#/">British Soldiers</a> -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Blog Detail Section -->
	<!-- Similar Themes -->
	<section class="page-section pt-75 mb-0 similar-section-wrapper similar-blogs">
		<div class="grey-bg"></div>
		<div class="container">
			<div class="row">
			<?php if(!empty($similar_blog_data)){?>
				<div class="col-12">
					<div class="multicolored-line">
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
					</div>
					<div class="section-title-wrapper">
						<h2 class="section-title">
							<img src="<?=base_url();?>images/section-headline.svg" alt=""> Similar Blogs
						</h2>						
					</div>
				</div>
				
					<?php foreach ($similar_blog_data as $key => $value) {?>
				<div class="col-12 col-md-6 col-lg-4">
					<div class="blog-single">
						<a href="<?= base_url('blog_detail/'.$value['blog_id'])?>">
							<div class="blog-title-date">
								<h3><?= $value['blog_title']?></h3>
								<div class="date-info">
									<?php $date_convert = date('Y-M-d',strtotime($value['blog_date']));
									$date_convert_array = explode('-',$date_convert); ?>
									<span class="date-number"><?= $date_convert_array[2]?></span>
									<span class="month"><?= $date_convert_array[1]?></span>
									<span class="year"><?= $date_convert_array[0]?></span>
								</div>
							</div>
							<div class="blog-author">
								<div class="arrow-right-dark-small"></div>
								<p><?= $value['author_name']?></p>
							</div>
						</a>
						<div class="blog-tags">
						<?php $all_tags = explode(",",$value['tag_name']);
						foreach ($all_tags as $tagkey => $tagvalue) { ?>
							<a href="#/"><?= $tagvalue?></a>
						<?php } ?>
						</div>
						<a href="<?= base_url('blog_detail/'.$value['blog_id'])?>">
							<div class="hover-options-container">								
								<div class="box-single-share">									
									<span class="share-single share-social" data-fancybox data-src="#sharetheme-content" data-options='{"touch" : false}'></span>
									<span class="share-msg-text share-social-text">
										Share
									</span>									
								</div>
								<img src="<?= base_url('images/blog_main_image/'.$value['thumbnail_image']);?>" alt="" class="img-fluid">
							</div>
						</a>
					</div>
				</div>
				<?php } }  ?>
			</div>
		</div>
	</section>						
	<!-- Similar Themes -->
</section>
<!-- Share Blog Post -->
<div style="display: none;max-width:580px;" id="sharetheme-content" class="ff-modal">
	<div class="modal-header">		
		<h3>Share Blog</h3>		
	</div>
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="ff-modal-content">
		<form class="share-reel-form">
			<div class="modal-section">			
					<div class="form-group">					
						<input type="text" class="form-control ff-field-dark linkfield" id="reelink" name="reelink" placeholder="Blog link will be shown here">
						<a href="#/" class="copylink">Copy Link</a>						
					</div>
					<div class="form-group">
						<label for="social">Social</label>
						<div class="modal-social-share">
							<a href="#/"><img src="<?=base_url();?>images/twitter-black.svg" alt="Share on Twitter"></a>
							<a href="#/"><img src="<?=base_url();?>images/facebook-black.svg" alt="Share on Facebook"></a>
							<a href="#/"><img src="<?=base_url();?>images/linkedin-black.svg" alt="Share on Linkedin"></a>				
						</div>
					</div>
					<div class="form-group">
						<label for="sendtoemail">Send to Email Address</label>
						<input type="text" class="form-control ff-field-dark" id="sendtoemail" name="sendtoemail" required>		
						<span class="emailaddress">niraali@bokapp.design <a href="#/"><img src="<?=base_url();?>images/close-black.svg" alt=""></a></span>
						<span class="emailaddress">gonzalo.c@footagefarm.com <a href="#/"><img src="<?=base_url();?>images/close-black.svg" alt=""></a></span>
						<span class="emailaddress">gintare.k@footagefarm.com <a href="#/"><img src="<?=base_url();?>images/close-black.svg" alt=""></a></span>
						<span class="emailaddress">mat.f@footagefarm.com <a href="#/"><img src="<?=base_url();?>images/close-black.svg" alt=""></a></span>						 		
					</div>	
			</div>
			<div class="modal-content-wrapper">			
				<div class="modal-actions">
					<input type="submit" data-fancybox-close class="btn-ff btn-secondary-dark text-uppercase"  value="Cancel">
					<input type="submit" class="btn-ff btn-primary-dark text-uppercase" value="Send">
				</div>
			</div>
		</form>		
	</div>
</div>
<!-- Share Blog Post -->

<script>
$(document).ready(function(){
	/*Subtheme video select*/
	if($(window).width()>991){			
		$('.blog-single a').hover(function () {

			$(this).children('.hover-options-container').children('.box-single-checkbox').show();
			$(this).children('.hover-options-container').children('.box-single-share').show();

		}, function () {

			$(this).children('.hover-options-container').children('.box-single-checkbox').hide();
			$(this).children('.hover-options-container').children('.box-single-share').hide();

			if ($(this).children('.hover-options-container').children('.box-single-checkbox').children('label.checkbox-container').children('input[type="checkbox"]').is(':checked')) {

				$(this).children('.hover-options-container').children('.box-single-checkbox').addClass('selected');
				$(this).children('.hover-options-container').addClass('selected');

			} else {

				$(this).children('.hover-options-container').children('.box-single-checkbox').removeClass('selected');
				$(this).children('.hover-options-container').removeClass('selected');

			}
		});
	}
	/*Subtheme video select*/
})
</script>
</body>
</html>