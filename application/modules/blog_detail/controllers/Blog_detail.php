<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog_detail  extends MX_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('blogdetailmodel','',TRUE);
	}

	function _remap($method_name){
		if(!method_exists($this, $method_name)){
			$this->index();
		}else{
			$this->{$method_name}();
		}
	}

	public function index()
	{
		$blog_id = $this->uri->segment(2);
		$result = array();

		//get blog data
		$condition = "b.blog_slug = '".$blog_id."' AND b.status= 'Active' ";
		$main_table = array("tbl_blogs as b", array("b.*"));
		$join_tables =  array();
		$join_tables = array(
							array("", "tbl_blog_tag_mapping as  bt", "bt.blog_id = b.blog_id"),
							array("", "tbl_blog_tags as  t", "t.blog_tag_id = bt.tag_id", array("group_concat(t.blog_tag_name) as tag_name,group_concat(t.blog_tag_id) as tag_id")),
							array("", "tbl_users as  u", "u.user_id = b.author_id", array("user_name as author_name, profile_photo")));
	
		$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("b.blog_id" => "DESC"),"b.blog_id",null); 
		 // fetch query
		$result['blog_data']= $this->common->MySqlFetchRow($rs, "array"); // fetch result


		$condition = "b.blog_slug != '".$blog_id."' AND b.status= 'Active' AND b.featured_blog= 'Yes' ";
		$main_table = array("tbl_blogs as b", array("b.*"));
		$join_tables =  array();
		$join_tables = array(
							array("", "tbl_blog_tag_mapping as  bt", "bt.blog_id = b.blog_id"),
							array("", "tbl_blog_tags as  t", "t.blog_tag_id = bt.tag_id", array("group_concat(t.blog_tag_name) as tag_name,group_concat(t.blog_tag_id) as tag_id")),
							array("", "tbl_users as  u", "u.user_id = b.author_id", array("user_name as author_name")));

		$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("b.blog_id" => "DESC"),"b.blog_id",null);
		$similar_blog_data = $this->common->MySqlFetchRow($rs, "array");
		$result['similar_blog_data']= array_slice($similar_blog_data, 0, 3);

		$where = "1=1 AND status= 'Active' ";
		// $result['tags_data'] = $this->common->getData("tbl_tags",'tag_id, tag_name, tag_description',$where);

		// echo "<pre>";print_r($result);exit;
		
		$result['meta_description'] = $result['blog_data'][0]['meta_description'];
		$result['meta_keywords'] = $result['blog_data'][0]['meta_title'];

		$this->load->view('header',$result);
		$this->load->view('index',$result);
		$this->load->view('footer');
	}

	
}?>