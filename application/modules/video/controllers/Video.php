<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Video extends MX_Controller
{
	
	public function __construct(){
		parent::__construct();
		checklogin();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('videomodel','',TRUE);
	}

	public function index(){
		$result = array();
		$condition = "1=1";
		$main_table = array("tbl_videos as tv", array("tv.*"));
		$join_tables =  array();
		$join_tables = array(
							array("", "tbl_video_theme_subtheme_mapping as  ttsm", "ttsm.video_id = tv.video_id", array("ttsm.theme_id")),
							array("", "tbl_themes as  tt", "tt.theme_id = ttsm.theme_id", array("ttsm.theme_id,GROUP_CONCAT(tt.theme_name) as theme_name")),
							);
	
		$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("tv.video_id" => "DESC"),"tv.video_id",null); 
		 // fetch query
		$result['video_data'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result

		// $result['video_data'] = $this->common->getData("tbl_videos",'*',$condition);
		// echo "<pre>";
		// print_r($result);
		// exit;
		$this->load->view('main-header.php');
		$this->load->view('index.php',$result);
		$this->load->view('footer.php');
	}

	public function addEdit(){

		$video_id = "";
		//print_r($_GET);
		$result = array();
		if(!empty($_GET['text']) && isset($_GET['text'])){
			$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
			parse_str($varr,$url_prams);
			$video_id = $url_prams['id'];
			$condition = "video_id ='".$video_id."' ";
			$result['video_data'] = $this->common->getData("tbl_videos",'*',$condition);
			$tags_data= $this->common->getData("tbl_video_tag_mapping",'*',$condition);

			$tags_video_data = array();
			foreach ($tags_data as $key => $value) {
				// print_r($value['tag_id']);
				array_push($tags_video_data,$value['tag_id']);
			}
			$result['tags_data'] = $tags_video_data;
			$result['theme_data'] = $this->common->getData("tbl_video_theme_subtheme_mapping",'*,group_concat(sub_theme_id) as total_sub_themes',$condition,"video_theme_id","ASC","theme_id");
			

			$main_table = array("tbl_video_country_location_mapping as vclm", array("vclm.*"));
			$join_tables =  array();
			$join_tables = array(
								array("", "tbl_country_location as  cl", "vclm.country_location_id = cl.country_location_id", array("group_concat(cl.country_location_id) as country_location_id, cl.country_id,cl.location_name"))
								);
		
			$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("vclm.video_country_location_id" => "ASC"),"cl.country_id",null); 
			 // fetch query
			$result['country_location_data'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result
			// $result['country_location_data'] = $this->common->getData("tbl_video_country_location_mapping",'*',$condition);
			

		}
		$condition = "1=1";
		$result['countries'] = $this->common->getData("countries",'*',$condition);
		$condition = "status = 'Active' ";
		// $result['locations'] = $this->common->getData("tbl_location",'*',$condition);
		$result['colors'] = $this->videomodel->enum_select("tbl_videos","colour_id");
		$result['sounds'] = $this->videomodel->enum_select("tbl_videos","sound_id");
		$result['years'] = $this->videomodel->enum_select("tbl_videos","year");
		$result['tags'] = $this->common->getData("tbl_tags",'*',$condition);
		$result['themes'] = $this->common->getData("tbl_themes",'*',$condition);

		// echo "<pre>";
		// 	print_r($result['country_location_data']);
		// 	exit;
		//echo $user_id;
		$this->load->view('main-header.php');
		$this->load->view('addEdit.php',$result);
		$this->load->view('footer.php');
	}

	function submitformtesting(){
		// codeigniter form validation 
		// $this->form_validation->set_rules('videoname', 'videoname', 'trim|required|alpha');
		// $this->form_validation->set_rules('mainvideo', 'Main video', 'trim|required');
		// $this->form_validation->set_rules('meta_title', 'Meta title', 'trim|required');
		// $this->form_validation->set_rules('meta_description', 'Meta description', 'trim|required');
		// $this->form_validation->set_rules('mainvideo', 'Main video', 'trim|required');

		// if($this->form_validation->run()) {  
		// 	// true  if validation property sectifice properly
		// 	$videoname = $this->input->post('videoname');  
		// 	$mainvideo = $this->input->post('mainvideo'); 
		// 	$videoname = $this->input->post('meta_title');  
		// 	$mainvideo = $this->input->post('meta_description'); 
		// 	$videoname = $this->input->post('mainvideo');  
		// 	$mainvideo = $this->input->post('meta_title'); 
		// }else{  
		// 	// if validation is not proper
		// 	$this->index();  
		// } 
	}

	public function delete_row(){
		// print_r($_POST);
		// exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

			$condition="video_id = '".$_POST['video_id']."' AND ".$_POST['column_name']." IN (".$_POST['id'].") ";
			// echo $condition;
			// exit;
			$result = $this->common->deleteRecord($_POST['tbl_name'],$condition);
			if($result){
				echo json_encode(array('success'=>true, 'msg'=>'Record deleted Successfully.'));
				exit;
			}else{
				echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
				exit;
			}
		}
		// echo "<pre>";
		// print_r($_POST);
		// exit;

	}

	public  function getLocation(){
		// print_r($_POST);
		// exit;
		// if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
		// 	$data = array();
		// 	$condition = " 1=1 AND country_id = '".$this->input->post('country_id')."' AND location_name like '%".$this->input->post('location_name')."%' ";
		// 	$getlocation = $this->common->getData("tbl_country_location_mapping",'*',$condition);
		// 	// echo $this->db->last_query();
		// 	if($getlocation){
		// 		// $list_location = array();
		// 		foreach ($getlocation as $key => $value) {
		// 			$list_location[] = $value['location_name'];
		// 		}
		// 		echo json_encode($list_location);
		// 	}
		// }
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();
			$condition = " 1=1 AND country_id = '".$this->input->post('country_id')."' ";
			$getlocation = $this->common->getData("tbl_country_location",'*',$condition);
			// echo $this->db->last_query();
			if($getlocation){
				$option         = '';
				$country_location_id = '';
				
				if(isset($_REQUEST['country_location_id']) && !empty($_REQUEST['country_location_id'])) {
					$country_location_id = explode(",",$_REQUEST['country_location_id']);
				}
				if(!empty($getlocation)) 
				{
					for ($i = 0; $i < sizeof($getlocation); $i++) 
					{	$sel ='';
						if(isset($_REQUEST['country_location_id']) && !empty($_REQUEST['country_location_id'])) {
							$sel = (in_array($getlocation[$i]['country_location_id'],$country_location_id))? 'selected="selected"' : '';
						}
						$option .= '<option value="' . $getlocation[$i]['country_location_id'] . '" ' . $sel . ' >' . $getlocation[$i]['location_name']. '</option>';
					}
				}
				echo json_encode(array("status" => "success", "option" => $option));
				exit;
			}
		}
	}
 
	public function getSubtheme(){

		// echo "<pre>";
		// print_r($_POST);
			
		$result = $this->common->getData("tbl_sub_themes", "*", "theme_id = '".$_REQUEST['theme_id']."' ");
		// print_r($result);
		// exit;
	   $option         = '';
	   $sub_theme_id = '';
	   
	   if(isset($_REQUEST['sub_theme_id']) && !empty($_REQUEST['sub_theme_id'])) {
		  $sub_theme_id = explode(",",$_REQUEST['sub_theme_id']);
	   }
	   if(!empty($result)) 
	   {
		   for ($i = 0; $i < sizeof($result); $i++) 
		   {	$sel ='';
			if(isset($_REQUEST['sub_theme_id']) && !empty($_REQUEST['sub_theme_id'])) {
				$sel = (in_array($result[$i]['sub_theme_id'],$sub_theme_id))? 'selected="selected"' : '';
			}
			   $option .= '<option value="' . $result[$i]['sub_theme_id'] . '" ' . $sel . ' >' . $result[$i]['sub_theme_name']. '</option>';
		   }
	   }
	   echo json_encode(array("status" => "success", "option" => $option));
	   exit;
	}


 	public function submitForm(){
		// print_r($_POST);
		// print_r($_FILES);
		// exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();
			
			$thumnail_value = "";
			if(isset($_FILES) && isset($_FILES["videofile"]["name"])){
				 $config = array();
				$config['upload_path'] = DOC_ROOT_FRONT."/images/video_image/";
				$config['max_size']    = '0';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$config['min_width']            = 1000;
				$config['min_height']           = 1000;
				$config['file_name']     = $_FILES["videofile"]["name"];
				$this->load->library('upload', $config);
				if (!$this->upload->do_upload("videofile")){
					$image_error = array('error' => $this->upload->display_errors());
					echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
					exit;
				}else{
					$image_data = array('upload_data' => $this->upload->data());
					$thumnail_value = $image_data['upload_data']['file_name']; 
					$this->setCompresseredImage($image_data['upload_data']['file_name']);
				}	
				/* Unlink previous category image */
				if(!empty($this->input->post('video_id'))){	
					$condition_image = " video_id = ".$this->input->post('video_id');
					$image =$this->common->getData("tbl_videos",'*',$condition_image);
					if(is_array($image) && !empty($image[0]->thumbnail_image) && file_exists(DOC_ROOT_FRONT."/images/video_thumbnail_image/".$image[0]->thumbnail_image))
					{
						unlink(DOC_ROOT_FRONT."/images/video_thumbnail_image/".$image[0]->thumbnail_image);
					}
				}
			}else{
				$thumnail_value = $this->input->post('pre_videofile_name');
			}

			$data['thumbnail_image'] = $thumnail_value;
			$data['video_title'] = $this->input->post('videotitle');
			$data['short_description'] = $this->input->post('short_description');
			$data['long_description'] = $this->input->post('long_description');
			$data['status'] = $this->input->post('video-status');
			$data['reel_number'] = $this->input->post('reelno');
			$data['year'] = $this->input->post('year');
			$data['colour_id'] = $this->input->post('color');
			$data['sound_id'] = $this->input->post('sound');
			$data['tc_begins'] = $this->input->post('tc_begin');
			$data['tc_ends'] = $this->input->post('tc_end');
			$data['duration'] = $this->input->post('duration');
			$data['vimeo_url'] = $this->input->post('vimeourl');
			$data['meta_title'] = $this->input->post('meta_title');
			$data['meta_description'] = $this->input->post('meta_description');
			$data['updated_on'] = date("Y-m-d H:i:s");
			$data['updated_by'] = $this->session->userdata('footage_farm_admin')[0]['user_id'];

			if(!empty($this->input->post('video_id'))){
				$video_id = $this->input->post('video_id');
				$condition = "video_id = '".$this->input->post('video_id')."' ";
				$result = $this->common->updateData("tbl_videos",$data,$condition);
				if($result){
					$all_country_id = $all_location_id = '';
					if(!empty($_POST['country'])){
						$all_country_id = implode(",",array_unique($_POST['country']));
					}
					if(!empty($_POST['location'])){
						$all_location_id = implode(",",array_unique($_POST['location']));
					}

					if($all_country_id && $all_location_id){
						$condition = "1=1 && country_id IN (".$all_country_id.") && country_location_id IN (".$all_location_id.")";
						$get_selected_country_location = $this->common->getData("tbl_country_location",'*',$condition);

						if($get_selected_country_location){
							$data_country_location_map = array();
							$condition = "video_id = '".$this->input->post('video_id')."' ";
							$this->common->deleteRecord("tbl_video_country_location_mapping",$condition);
							foreach ($get_selected_country_location as $key => $value) {
								$data_theme_sub_theme = array();
								$data_country_location_map['video_id'] = $video_id;
								 $data_country_location_map['country_location_id'] = $value['country_location_id'];
								 $data_country_location_map['created_on'] = date("Y-m-d H:i:s");
								 $data_country_location_map['created_by'] =  $this->session->userdata('footage_farm_admin')[0]['user_id'];
								 $result_country_location_map = $this->common->insertData('tbl_video_country_location_mapping',$data_country_location_map,'1');
							}
						}
					}
					// foreach ($_POST['country'] as $key => $value) {
						// $condition = "1=1 AND country_id = '".$value."' AND location_name = '".trim(strtolower($_POST['location'][$key]))."' ";
						//  $get_country_location = $this->common->getData("tbl_country_location_mapping",'*',$condition);

						//  if(empty($get_country_location)){
						// 	 $country_location = array();
						// 	 $country_location['country_id'] = $value; ;
						// 	 $country_location['location_name'] = trim(strtolower($_POST['location'][$key]));
						// 	 $get_country_location_id = $this->common->insertData('tbl_country_location_mapping',$country_location,'1');
						//  }else{
						// 	$get_country_location_id = $get_country_location[0]['country_location_id'];
						//  }
						//  $data_country_location_map['video_id'] = $video_id;
						//  $data_country_location_map['country_location_id'] = $get_country_location_id;
						//  $data_country_location_map['location_id'] = trim(strtolower($_POST['location'][$key]));
						//  $data_country_location_map['created_on'] = date("Y-m-d H:i:s");
						//  $data_country_location_map['created_by'] =  $this->session->userdata('footage_farm_admin')[0]['user_id'];
						//  $result_country_location_map = $this->common->insertData('tbl_video_country_location_mapping',$data_country_location_map,'1');
					
					// }
					if(!empty($result_country_location_map)){
						$condition = "video_id = '".$this->input->post('video_id')."' ";
						$this->common->deleteRecord("tbl_video_tag_mapping",$condition);
						$data_tag_map = array();
						foreach ($_POST['tags'] as $key => $value) {
							$data_tag_map['video_id'] = $video_id ;
							$data_tag_map['tag_id'] = $value ;
							$data_tag_map['created_on'] = date("Y-m-d H:i:s");
							$data_tag_map['created_by'] =  $this->session->userdata('footage_farm_admin')[0]['user_id'];
							$result_data_tag_map = $this->common->insertData('tbl_video_tag_mapping',$data_tag_map,'1');
						}
							if(!empty($result_data_tag_map)){

								$all_themes_id = $all_sub_themes_id = '';
								if(!empty($_POST['theme_id'])){
									$all_themes_id = implode(",",array_unique($_POST['theme_id']));
								}
								if(!empty($_POST['sub_theme_id'])){
									$all_sub_themes_id = implode(",",array_unique($_POST['sub_theme_id']));
								}

								if($all_themes_id && $all_sub_themes_id){
									$condition = "1=1 && theme_id IN (".$all_themes_id.") && sub_theme_id IN (".$all_sub_themes_id.")";
									$get_selected_theme_subtheme = $this->common->getData("tbl_sub_themes",'*',$condition);

									if($get_selected_theme_subtheme){
										$condition = "video_id = '".$this->input->post('video_id')."' ";
										$this->common->deleteRecord("tbl_video_theme_subtheme_mapping",$condition);
										foreach ($get_selected_theme_subtheme as $key => $value) {
											$data_theme_sub_theme = array();
											$data_theme_sub_theme['video_id'] = $video_id;
											$data_theme_sub_theme['theme_id'] = $value['theme_id'];
											$data_theme_sub_theme['sub_theme_id'] = $value['sub_theme_id'];
											$data_theme_sub_theme['created_on'] = date("Y-m-d H:i:s");
											$data_theme_sub_theme['created_by'] =  $this->session->userdata('footage_farm_admin')[0]['user_id'];
											$result_theme_sub_theme = $this->common->insertData('tbl_video_theme_subtheme_mapping',$data_theme_sub_theme,'1');
										}
									}
										if($result_theme_sub_theme){
											echo json_encode(array('success'=>true, 'msg'=>'Record updated Successfully.'));
											exit;
										}else{
											echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
											exit;
										}
								}
							}
								
					}else{
						echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
						exit;
					}

					echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while updating data.'));
					exit;
				}
			}else{
				$data['created_on'] = date("Y-m-d H:i:s");
				$data['created_by'] =  $this->session->userdata('footage_farm_admin')[0]['user_id'];
				$result = $this->common->insertData('tbl_videos',$data,'1');
				$video_id = $result;
				if(!empty($result)){
					$data_country_location_map = array();
					$all_country_id = $all_location_id = '';
					if(!empty($_POST['country'])){
						$all_country_id = implode(",",array_unique($_POST['country']));
					}
					if(!empty($_POST['location'])){
						$all_location_id = implode(",",array_unique($_POST['location']));
					}

					if($all_country_id && $all_location_id){
						$condition = "1=1 && country_id IN (".$all_country_id.") && country_location_id IN (".$all_location_id.")";
						$get_selected_country_location = $this->common->getData("tbl_country_location",'*',$condition);

						if($get_selected_country_location){
							$data_country_location_map = array();
						
							foreach ($get_selected_country_location as $key => $value) {
								$data_theme_sub_theme = array();
								$data_country_location_map['video_id'] = $video_id;
								 $data_country_location_map['country_location_id'] = $value['country_location_id'];
								 $data_country_location_map['created_on'] = date("Y-m-d H:i:s");
								 $data_country_location_map['created_by'] =  $this->session->userdata('footage_farm_admin')[0]['user_id'];
								 $result_country_location_map = $this->common->insertData('tbl_video_country_location_mapping',$data_country_location_map,'1');
							}
						}
					}
					// foreach ($_POST['country'] as $key => $value) {
					// 	$condition = "1=1 AND country_id = '".$value."' AND location_name = '".trim(strtolower($_POST['location'][$key]))."' ";
					// 	 $get_country_location = $this->common->getData("tbl_country_location_mapping",'*',$condition);

					// 	 if(empty($get_country_location)){
					// 		 $country_location = array();
					// 		 $country_location['country_id'] = $value; ;
					// 		 $country_location['location_name'] = trim(strtolower($_POST['location'][$key]));
					// 		 $get_country_location_id = $this->common->insertData('tbl_country_location_mapping',$country_location,'1');
					// 	 }else{
					// 		$get_country_location_id = $get_country_location[0]['country_location_id'];
					// 	 }
					// 	 $data_country_location_map['video_id'] = $video_id;
					// 	 $data_country_location_map['country_location_id'] = $get_country_location_id;
					// 	//  $data_country_location_map['location_id'] = trim(strtolower($_POST['location'][$key]));
					// 	 $data_country_location_map['created_on'] = date("Y-m-d H:i:s");
					// 	 $data_country_location_map['created_by'] =  $this->session->userdata('footage_farm_admin')[0]['user_id'];
					// 	 $result_country_location_map = $this->common->insertData('tbl_video_country_location_mapping',$data_country_location_map,'1');
					
					// }
					if(!empty($result_country_location_map)){
						
						$data_tag_map = array();
						foreach ($_POST['tags'] as $key => $value) {
							$data_tag_map['video_id'] = $video_id ;
							$data_tag_map['tag_id'] = $value ;
							$data_tag_map['created_on'] = date("Y-m-d H:i:s");
							$data_tag_map['created_by'] =  $this->session->userdata('footage_farm_admin')[0]['user_id'];
							$result_data_tag_map = $this->common->insertData('tbl_video_tag_mapping',$data_tag_map,'1');
						}
							if(!empty($result_data_tag_map)){


								$all_themes_id = $all_sub_themes_id = '';
								if(!empty($_POST['theme_id'])){
									$all_themes_id =implode(",",$_POST['theme_id']);
								}
								if(!empty($_POST['sub_theme_id'])){
									$all_sub_themes_id =implode(",",$_POST['sub_theme_id']);
								}

								if($all_themes_id && $all_sub_themes_id){
									$condition = "1=1 && theme_id IN (".$all_themes_id.") && sub_theme_id IN (".$all_sub_themes_id.")";
									$get_selected_theme_subtheme = $this->common->getData("tbl_sub_themes",'*',$condition);

									if($get_selected_theme_subtheme){
										foreach ($get_selected_theme_subtheme as $key => $value) {
											$data_theme_sub_theme = array();
											$data_theme_sub_theme['video_id'] = $video_id;
											$data_theme_sub_theme['theme_id'] = $value['theme_id'];
											$data_theme_sub_theme['sub_theme_id'] = $value['sub_theme_id'];
											$data_theme_sub_theme['created_on'] = date("Y-m-d H:i:s");
											$data_theme_sub_theme['created_by'] =  $this->session->userdata('footage_farm_admin')[0]['user_id'];
											$result_theme_sub_theme = $this->common->insertData('tbl_video_theme_subtheme_mapping',$data_theme_sub_theme,'1');
										}
									}
										if($result_theme_sub_theme){
											echo json_encode(array('success'=>true, 'msg'=>'Record Added Successfully.'));
											exit;
										}else{
											echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
											exit;
										}
								}
							}
								
					}else{
						echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
						exit;
					}
							
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
					exit;
				}
			}
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem While Add/Edit Data.'));
			exit;
		}
	}

	public function setCompresseredImage($image_name){
		$config1 = array();
		$this->load->library('image_lib');
		$config1['image_library'] = 'gd2';
		$config1['source_image'] = DOC_ROOT_FRONT."images/video_thumbnail_image/".$image_name;
		$config1['maintain_ratio'] = TRUE;
		$config1['quality'] = '100%';
		$config1['width'] = 1000;
		$config1['height'] = 1000;
		$config1['new_image'] = DOC_ROOT_FRONT."images/video_thumbnail_image/".$image_name;
		$this->image_lib->initialize($config1);
		$this->image_lib->resize();
		$this->image_lib->clear();
	}
}
?>