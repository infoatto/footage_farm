<?php
include_once APPPATH . "libraries/vendor/autoload.php";
	  $google_client = new Google_Client();
	  $google_client->setClientId('93305526721-8s7quffh5q90p4hjaqg7gij6cie56p9n.apps.googleusercontent.com'); //Define your ClientID
	  $google_client->setClientSecret('ghqTxb2VPtQe7GZSEDT-OQEI'); //Define your Client Secret Key
	  $google_client->setRedirectUri('http://footage.webshowcase-india.com/google_login/login'); //Define your Redirect Uri
	  $google_client->addScope('email');
	  $google_client->addScope('profile');
	  if(isset($_GET["code"]))
	  {
	   $token = $google_client->fetchAccessTokenWithAuthCode($_GET["code"]);
	   if(!isset($token["error"]))
	   {
	    $google_client->setAccessToken($token['access_token']);
	    $this->session->set_userdata('access_token', $token['access_token']);
	    $google_service = new Google_Service_Oauth2($google_client);
	   }
	  }
	  $login_button = '';
	  if(!$this->session->userdata('access_token'))
	  {
	   $login_button = $google_client->createAuthUrl();
	   
	  }

?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<meta name="description" content="<?= (!empty($meta_description)?$meta_description:"")  ?>">
	<meta name="keywords" content="<?= (!empty($meta_keywords)?$meta_keywords:"")?>">
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Footage Farm Ltd - Archive Public Domain Footage Library</title>
	<link rel="icon" href="<?= base_url()?>assets/images/favicon-32x32.png" type="<?= base_url()?>images/png" sizes="32x32">
	<link rel="stylesheet" href="<?= base_url()?>assets/css/bootstrap.css" />
	<link rel="stylesheet" href="<?= base_url()?>assets/css/owl.carousel.min.css" />
	<link rel="stylesheet" href="<?= base_url()?>assets/css/jquery.fancybox.min.css" />
	<link rel="stylesheet" href="<?= base_url()?>assets/css/jquery.mCustomScrollbar.min.css" />
	<link rel="stylesheet" href="<?= base_url()?>assets/css/dropzone.min.css" />
	<link rel="stylesheet" href="<?= base_url()?>assets/css/style.css" />
	<link rel="stylesheet" href="<?= base_url()?>assets/css/responsive.css" />
</head>
<script src="<?= base_url()?>assets/js/jquery-3.2.1.min.js"></script>
<script src="<?= base_url()?>assets/js/jquery.validate.min.js"></script>
<script src='<?php echo base_url()?>assets/js/jquery.form.js'></script> 
<script src='<?php echo base_url()?>assets/js/form-validation.js'></script>
<script src="<?php echo base_url("assets/js/plugin/sweetalert/sweetalert.min.js"); ?>"></script>

<body>
<style type="text/css">
	.swal-overlay {
    z-index: 100000;
}


</style>

<nav class="navbar navbar-expand-lg ff-navbar">
		<div class="container">
			<a class="navbar-brand" href="<?= base_url()?>">
				<img src="<?= base_url()?>assets/images/ff-color-logo.svg" alt="Footage Farm" id="top-logo">
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
				<span class="navbar-toggler-icon"></span>
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active">
						<a class="nav-link" href="<?= base_url('theme')?>">Themes</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?= base_url('aboutus')?>">About</a>
					</li>
					<li>
						<a href="<?= base_url('how_we_operate')?>" class="nav-link">How we operate</a>
					</li>
					<li>
						<a href="<?= base_url('faq')?>" class="nav-link">FAQs</a>
					</li>
					<li>
						<a href="<?= base_url('blog')?>" class="nav-link">Blog</a>
					</li>
					<li>
						<a href="<?= base_url('contactus')?>" class="nav-link">Contact</a>
					</li>
				</ul>
				<ul class="navbar-nav ml-auto">
					<?php if($this->session->userdata('footage_farm_user')) {
					
						?>
						<li class="nav-item dropdown signin-link-desktop">
							<!-- User initials if profile picture not set -->
							<!-- <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownLogin" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">  -->
							<!-- <?php //$profile_image = ((!empty($this->session->userdata('footage_farm_user')[0]['profile_photo']))? FRONT_URL.'/images/profile_image/'.$this->session->userdata('footage_farm_user')[0]['profile_photo']:"https://via.placeholder.com/50")?> -->
							<!-- <img src="<?= $profile_image?>" alt="..." class="avatar-img rounded-circle" style="width: 8rem;height: 8rem;"> -->
							<!-- <span class="client-initials-logo"> -->
								<!-- BO -->
							<!-- </span>  -->
							<!-- <img src="<?= base_url()?>images/down-arrow.svg" alt=""></a> -->
							<!-- Profile picture set -->
							
							<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownLogin" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
							<?php 
							if(empty($this->session->userdata('footage_farm_user')[0]['profile_photo'])){?>
								<span class="client-initials-logo">BO</span> 
							<?php }else{?>
								<img src="<?= base_url('images/profile_image/'.$this->session->userdata('footage_farm_user')[0]['profile_photo'])?>" alt=""class="client-avatar">
							<?php }?>
							<img src="<?= base_url()?>images/down-arrow.svg" alt=""></a>



							<div class="dropdown-menu loggedin-flyout" aria-labelledby="navbarDropdown">						
								<a class="dropdown-item" href="<?= base_url('profile/editprofile')?>">My Profile</a>
								<a class="dropdown-item" href="<?= base_url('profile')?>">Account Settings</a>							
								<a class="dropdown-item" href="<?= base_url('register/logout')?>">Logout</a>
							</div>
						</li>
					<?php }else { ?>
					<li class="nav-item dropdown signin-link-desktop">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Sign In <img src="<?= base_url()?>assets/images/down-arrow.svg" alt=""></a>
						<div class="dropdown-menu signin-flyout" aria-labelledby="navbarDropdown">
							<div class="signin-flyout-wrapper">
								<div class="create-account">
									<h3>New Here</h3>
									<p class="signin-text">New to Footage Farm? Create an account to get started today.</p>
									<a href="#/" class="btn-ff btn-secondary-dark ff-register-link" data-fancybox data-src="#signin-content" data-options='{"touch" : false}'>Create an account</a>
								</div>
								<div class="sign-in-user">
									<h3>Registered Users</h3>
									<p class="signin-text">Have an account? Sign in now. </p>
									<a href="#/" class="btn-ff btn-primary-dark" data-fancybox data-src="#signin-content" data-options='{"touch" : false}'>Sign in</a>
								</div>
							</div>
						</div>
					</li>
				<?php } ?>
					
					<li class="nav-item signin-link-mobile">
						<a href="#/" class="nav-link">Sign In</a>
					</li>
					<li class="nav-item">
						<?php if($this->session->userdata('footage_farm_user')) { 
							// $this->load->model('common_model/common_model','common',TRUE);
							$condition = "user_id = '".$this->session->userdata('footage_farm_user')[0]['user_id']."'  ";
							$getCartReel = $this->common->getData("tbl_video_cart",'*',$condition);
								if(!empty($getCartReel) && isset($getCartReel)){?>
									<a class="enquiry btn-ff btn-ff-icon btn-tertiary-dark" href="<?= base_url('cart')?>">Enquiry</a>
								<?php }else{?>
									<a class="enquiry btn-ff btn-ff-icon btn-tertiary-dark" href="<?= base_url('enquiry')?>">Enquiry</a>
								<?php }?>
						<?php }else{?>
							<a class="enquiry btn-ff btn-ff-icon btn-tertiary-dark" href="<?= base_url('enquiry')?>">Enquiry</a>
						<?php }?>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<!-- Signin flyout mobile -->
	<div class="signin-flyout-mobile">
		<a href="#/" class="close-mobile-signin"><img src="<?= base_url()?>assets/images/close.svg" alt=""></a>
		<div class="signin-flyout-wrapper">
			<div class="create-account">
				<h3>New Here</h3>
				<p class="signin-text">New to Footage Farm? Create an account to get started today.</p>
				<a href="#/" class="btn-ff btn-secondary-dark">Create an account</a>
			</div>
			<div class="sign-in-user">
				<h3>Registered Users</h3>
				<p class="signin-text">Have an account? Sign in now. </p>
				<a href="#/" class="btn-ff btn-primary-dark" data-fancybox data-src="#signin-content" data-options='{"touch" : false}' >Sign in</a>
			</div>
		</div>
	</div>
	<!-- Signin flyout mobile -->
	<!-- Login/Signup -->	
	<div style="display: none;max-width:580px;" id="signin-content" class="ff-modal">
		<!-- Login Content -->
		<div class="modal-header">		
			<h3>Sign In</h3>		
		</div>
		<div class="multicolored-line">
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			<div></div>
		</div>
		<div class="ff-modal-content ff-login">
			<div class="modal-content-wrapper">		
				<div class="modal-section">
					<div class="alert alert-success alert-dismissible" style="display: none;">
					    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					    <strong class="showalert"></strong>
					</div>

					<div class="header-login">
						<p class="login-text text-center">Sign in with</p>
						<a href="<?= $login_button; ?>" class="google-login"><img src="<?= base_url()?>assets/images/google.svg" alt=""> Login With Google</a>
						
          				<form class="" id="form-login" method="post" enctype="multipart/form-data">
          					<input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden" class="txt_csrfname">
						<?php //echo '<label class="text-danger">'.$this->session->flashdata("error").'</label>';   ?>
							<label class="text-danger"></label>				
							<div class="form-group">
								<label for="username">Username</label>
								<input type="text" class="form-control ff-field-dark" id="username" name="username" required>
								<span class="text-danger1"><?php echo form_error("username"); ?></span>	
							</div>
							<div class="form-group">
								<label for="password">Password</label>
								<input type="password" class="form-control ff-field-dark password" id="password" name="password" required>
								<a href="#/" class="showpassword"><img src="<?= base_url()?>assets/images/show.svg" alt=""></a>
								<span class="text-danger1"><?php echo form_error("password"); ?></span>	
							</div>
							<div class="form-group keepsignedin-wrapper">
								<div class="keepsignedin">
									<label class="checkbox-container-theme">
										<input type="checkbox"> <span class="checkmark-theme-label">Keep me signed in</span>
										<span class="checkmark-theme"></span>
									</label>
								</div>
								<div class="forgotpassword">
									<a href="#/" class="login-link ff-forgotpass-link">Forgot Password</a>
								</div>
							</div>
							<div class="form-group mb-0">
								<p class="login-text">New to Footage Farm? &nbsp;<br/><a href="#/" class="login-link ff-register-link">Create an Account</a></p>
							</div>	

							<div class="modal-actions">					
								<input type="submit" class="btn-ff btn-primary-dark text-uppercase full-width-btn" value="Sign in">
							</div>
						</form>			
					</div>
				</div>				
							
			</div>			
		</div>
		<!-- Login Content -->
		<!-- Register Content -->		
		<div class="ff-modal-content ff-register">			
			<div class="modal-content-wrapper">
				
				<div class="modal-section">
					<div class="header-login">
						<p class="login-text text-center">Already have an account? &nbsp;<br/><a href="#/" class="login-link ff-login-link" id="gologin">Log in</a></p>						
						<a href="#/" class="google-login mb-5"><img src="<?= base_url()?>assets/images/google.svg" alt=""> Login With Google</a>
						<p class="login-text text-center">or create with</p>	
		
						<form class="" id="form-register" method="post" enctype="multipart/form-data">
						<input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden" class="txt_csrfname">				
							<div class="form-group">
								<label for="first_name">First Name</label>
								<input type="text" class="form-control ff-field-dark" id="first_name" name="first_name">	
							</div>

							<div class="form-group">
								<label for="">Last Name</label>
								<input type="text" class="form-control ff-field-dark" id="last_name" name="last_name">	
							</div>

							<div class="form-group">
								<label for="email">Email</label>
								<input type="email" class="form-control ff-field-dark" id="email" name="email">	
							</div>

							<div class="form-group">
								<label for="phone">Phone</label>
								<input type="text" class="form-control ff-field-dark" id="phone" name="phone">	
							</div>

							<div class="form-group">
								<label for="user_password">Set a Password</label>
								<input type="password" class="form-control ff-field-dark password" id="user_password" name="user_password">
								<a href="#/" class="showpassword"><img src="<?= base_url()?>assets/images/show.svg" alt=""></a>	
							</div>
							<div class="form-group keepsignedin-wrapper mb-5">
								<div class="keepsignedin">
									<label class="checkbox-container-theme">
										<input type="checkbox"> <span class="checkmark-theme-label">I agree with your Terms of Service, Privacy Policy</span>
										<span class="checkmark-theme"></span>
									</label>
								</div>								
							</div>
							<div class="modal-actions">					
								<input type="submit" class="btn-ff btn-primary-dark text-uppercase full-width-btn" value="Create Account">
							</div>												
						</form>			
					</div>
				</div>				
				
			
			</div>			
		</div>
		<!-- Register Content -->
		<!-- Forgot Password Content -->		
		<div class="ff-modal-content ff-forgotpass">
			<div class="modal-content-wrapper">		
				<div class="modal-section">
					<div class="header-login">												
					<form class="" id="form-forgotpassword" method="post" enctype="multipart/form-data">
						<input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden" class="txt_csrfname">
							<div class="form-group">
								<label for="resetpassword">Enter registered email id to reset your password</label>
								<input type="email" class="form-control ff-field-dark" id="resetpassword" name="email" placeholder="Email id">	
							</div>
							<div class="form-group mb-0">
								<a href="#/" class="login-link ff-login-link">Back to Login</a>
							</div>								
									
					</div>
				</div>				
				<div class="modal-actions">					
					<input type="submit" class="btn-ff btn-primary-dark text-uppercase full-width-btn" value="Send Email">
				</div>	
				</form>
			</div>			
		</div>
		<!-- Forgot Password Content -->
	</div>
<!-- Login/Signup -->
<!-- Verify Email -->	
<div style="display: none;max-width:580px;" id="verifyemail-content" class="ff-modal">
		<!-- Login Content -->
		<div class="modal-header">		
			<h3>Please verify your email</h3>		
		</div>
		<div class="multicolored-line">
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			<div></div>
		</div>					
		<div class="ff-modal-content">
			<div class="modal-content-wrapper">		
				<div class="modal-section">
					<div class="header-login">	
						<p class="login-text mb-20">We are glad to have you onboard! But before we start we will need to make sure we’ve got the right email for you.</p>														
						<a href="#/" class="btn-ff btn-primary-dark text-uppercase">Go to Homepage</a>
						<p class="login-text mt-20">Didn’t recieve email? &nbsp;<br><a href="#/" class="login-link">Click here to change/resend email.</a></p>		
					</div>
				</div>
			</div>			
		</div>		
	</div>
<!-- Verify Email -->



<!-- Select Projects -->
<div style="display: none;" id="selectprojects-content" class="ff-modal">
	<div class="modal-header">
		<h3>Add to a Project</h3>
	</div>
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="ff-modal-content">
		<div class="modal-content-wrapper">
			<div class="modal-project-thumbnail">
				<img src="https://via.placeholder.com/360x460.png" alt="" class="modal-project-thumb">
			</div>

			<form class="" id="form-project" method="post" enctype="multipart/form-data">
			<input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden" class="txt_csrfname">
			<div class="modal-project-details">
				<div class="modal-create-new-project modal-section">
					<a href="#/" class="modal-create-btn">
						<img src="<?= base_url()?>images/plus-black.svg" alt=""> Create New Project
					</a>
				</div>
				<div class="modal-existing-projects modal-section">
					<div class="mep-search">
						<img src="<?= base_url()?>images/search-black.svg" alt="Search">
						<input type="text" placeholder="Search" id="mep-search">
					</div>

					<div id="projectshow">

					</div>
				</div>
				<!-- New Project -->
				<div class="modal-addnew-project modal-section">
						<div class="form-group">
							<label for="nameofproject">Name of Project</label>
							<input type="text" class="form-control ff-field-dark" id="nameofproject" name="nameofproject">
						</div>
						<div class="form-group">
							<label for="projectdesc">Description (optional)</label>
							<textarea class="form-control ff-field-dark" name="projectdesc" rows="5"></textarea>
						</div>

				</div>
			</div>
			<div class="modal-actions modal-addnew-project">
				<input type="button" class="btn-ff btn-secondary-dark text-uppercase modal-cancel-newproject" value="Cancel">
				<input type="submit" class="btn-ff btn-primary-dark text-uppercase" name="submit" value="Create">
			</div>
			
			<!-- New Project -->
			<div class="modal-actions modal-existing-actions">
				<input type="submit" data-fancybox-close class="btn-ff btn-secondary-dark text-uppercase modal-cancel-newproject" value="Cancel">
				<input type="submit" class="btn-ff btn-primary-dark text-uppercase" name="submit" value="Save">
			</div>
		</form>
		</div>
	</div>
</div>
<!-- Select Projects -->
<!-- Share Theme/Reel -->
<div style="display: none;max-width:580px;" id="sharetheme-content" class="ff-modal">
	<div class="modal-header">
		<h3>Share a Reel data</h3>
	</div>
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="ff-modal-content">
		<form class="share-reel-form" id="form-share" method="post" enctype="multipart/form-data" >
			<input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden" class="txt_csrfname">
			<div class="modal-section">
				<div class="form-group">
					<input type="text" class="form-control ff-field-dark linkfield" id="reelink" name="reelink" placeholder="Theme/Reel link will be shown here">
					<a href="#/" class="copylink">Copy Link</a>
				</div>
				<div class="form-group">
					<label for="social">Social</label>
					<div class="modal-social-share">
						<a href="#/" target="_blank" class="thref"><img src="<?= base_url()?>images/twitter-black.svg" alt="Share on Twitter"></a>
						<a href="#/" target="_blank" class="fhref"><img src="<?= base_url()?>images/facebook-black.svg" alt="Share on Facebook"></a>
						<a href="#/" target="_blank" class="lhref"><img src="<?= base_url()?>images/linkedin-black.svg" alt="Share on Linkedin"></a>
					</div>
				</div>
				<div class="form-group">
					<label for="sendtoemail">Send to Email Address</label>
					<input type="text" class="form-control ff-field-dark" id="sendtoemail" name="sendtoemail" required>
					<!-- <span class="emailaddress">niraali@bokapp.design <a href="#/"><img src="<?= base_url()?>images/close-black.svg" alt=""></a></span>
					<span class="emailaddress">gonzalo.c@footagefarm.com <a href="#/"><img src="<?= base_url()?>images/close-black.svg" alt=""></a></span>
					<span class="emailaddress">gintare.k@footagefarm.com <a href="#/"><img src="<?= base_url()?>images/close-black.svg" alt=""></a></span>
					<span class="emailaddress">mat.f@footagefarm.com <a href="#/"><img src="<?= base_url()?>images/close-black.svg" alt=""></a></span> -->
					
				</div>
			</div>
			<div class="modal-content-wrapper">
				<div class="modal-actions">
					<input type="button" data-fancybox-close class="btn-ff btn-secondary-dark text-uppercase" value="Cancel">
					<input type="submit" class="btn-ff btn-primary-dark text-uppercase" value="Send">
				</div>
			</div>
		</form>
	</div>
</div>
<!-- Share Theme/Reel -->
<!-- Request Enquiry Master -->
<!-- <div style="display: none;max-width:700px;" id="enquiry-master-content" class="ff-modal">
	<div class="modal-header">
		<h3>Request Enquiry</h3>
	</div>
	<div class="multicolored-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="ff-modal-content">
		<form class="enquiry-master-form">
			<div class="modal-section">

				<div class="form-group">
					<div class="reel-title">
						<img src="https://via.placeholder.com/147x83.png" alt="">
						<h3>1910 - 1918 - WWI Era: Aerials; German Officers & Troops; France Fighting; Trans-Caucasia Winter</h3>
					</div>
				</div>
				<div class="form-group">
					<div class="request-reel-type">
						<label for="selectreeltype">Please select the reel type</label>
						<label class="radio-container-theme">
							<span class="radio-label">Request Preview</span>
							<input type="radio" name="reeltype" required>
							<span class="radio"></span>
						</label>
						<label class="radio-container-theme">
							<span class="radio-label">Request Master</span>
							<input type="radio" name="reeltype">
							<span class="radio"></span>
						</label>
					</div>
					<label for="reeltype" class="error"></label>
				</div>
				<div class="form-group">
					<label for="inouttime">Specify the footage's in and out time</label>
					<div class="begin-end-tc">
						<div class="tc-time begin-tc">
							<label for="begintc">Begin TC
								<a href="#" class="tooltip-info">
									<img src="<?= base_url()?>images/info.svg" alt="">
									<span class="tooltip-msg">
										Footage start time
									</span>
								</a>
							</label>
							<input type="text" class="form-control ff-field-dark" id="begintc" name="begintc" placeholder="00:00:00" required>
						</div>
						<div class="tc-time end-tc">
							<label for="endtc">End TC
								<a href="#" class="tooltip-info">
									<img src="<?= base_url()?>images/info.svg" alt="">
									<span class="tooltip-msg">
										Footage end time
									</span>
								</a>
							</label>
							<input type="text" class="form-control ff-field-dark" id="endtc" name="endtc" placeholder="00:00:00" required>
						</div>
					</div>
					<div id="addmore-begin-end-tc"></div>
					<div class="form-addmore">
						<a href="#/" class="form-addmore-link tc-addmore-link"><img src="<?= base_url()?>images/add-black.svg" alt=""> <span>Add another in and out time codes</span></a>
					</div>
				</div>
				<div class="form-group">
					<label>Do you have a file with multiple in and out time codes?</label>
					<p>Add jpegs, excel or word files here. Each file size should be under 2MB.</p>
					<div class="uploaded-files-list">
						<div class="file-single">
							<img src="<?= base_url()?>images/jpg.svg" alt="" class="file-type-icon">
							<span>Screenshota_qe52gknknf.jpg</span>
							<a href="#/"><img src="<?= base_url()?>images/close.svg" alt="" class="file-remove-icon"></a>
						</div>
						<div class="file-single">
							<img src="<?= base_url()?>images/doc.svg" alt="" class="file-type-icon">
							<span>datasheet-with-timecodes.doc</span>
							<a href="#/"><img src="<?= base_url()?>images/close.svg" alt="" class="file-remove-icon"></a>
						</div>
						<div class="file-single">
							<img src="<?= base_url()?>images/xls.svg" alt="" class="file-type-icon">
							<span>animate-sys.xls</span>
							<a href="#/"><img src="<?= base_url()?>images/close.svg" alt="" class="file-remove-icon"></a>
						</div>
					</div>
					
					<div id="upload-reel-times">
						<img src="<?= base_url()?>images/upload-file.svg" alt="">
						<p>Drag and drop here or browse</p>
						<a href="#/" class="login-link">Browse</a>
					</div>
					
				</div>
				<div class="form-group">
					<div class="format-framerate">
						<div class="format formatframe">
							<label for="begintc">Format</label>
							<select class="niceselect ff-select-dark formatselect" required>
								<option value="">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
							</select>
							<label id="formatselect-error" class="error" for=""></label>
						</div>
						<div class="framerate formatframe">
							<label for="endtc">Frame Rate</label>
							<select class="niceselect ff-select-dark framerateselect" required>
								<option value="">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
								<option value="test">Placeholder text</option>
							</select>
							<label id="framerateselect-error" class="error" for=""></label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="codec">Codec
						<a href="#" class="tooltip-info">
							<img src="<?= base_url()?>images/info.svg" alt="">
							<span class="tooltip-msg">
								Codec type
							</span>
						</a>
					</label>
					<input type="text" class="form-control ff-field-dark" id="codec" name="codec" placeholder="QT ProRes422HQ" required>
				</div>
				<div class="form-group">
					<label for="projectdesc">Message (optional)</label>
					<textarea class="form-control ff-field-dark" rows="5"></textarea>
				</div>
			</div>
			<div class="modal-content-wrapper">
				<div class="modal-actions">
					<input type="submit" data-fancybox-close class="btn-ff btn-secondary-dark text-uppercase" value="Cancel">
					<input type="submit" class="btn-ff btn-primary-dark text-uppercase" value="Send Request">
				</div>
			</div>
		</form>
	</div>
</div> -->
<!-- Request Enquiry Master -->



<script>
	$(document).ready(function() {
		
	});
</script>