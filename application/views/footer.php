<script src="<?= base_url()?>assets/js/popper.min.js"></script>
<script src="<?= base_url()?>assets/js/bootstrap.min.js"></script>
<script src="<?= base_url()?>assets/js/owl.carousel.min.js"></script>
<script src="<?= base_url()?>assets/js/jquery.fancybox.min.js"></script>
<script src="<?= base_url()?>assets/js/jquery.nice-select.min.js"></script>
<script src="<?= base_url()?>assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<?= base_url()?>assets/js/jquery.lazy.min.js"></script>
<script src="<?= base_url()?>assets/js/dropzone.min.js"></script>
<script src="<?= base_url()?>assets/js/script.js"></script>

<!-- Footer -->
<div class="clearfix">&nbsp;</div>
<footer>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="footer-getintouch-wrapper">
					<div class="footer-getintouch">
						<p><span>Can't find what you want? Get in touch and we'll find it for you</span><a href="<?= base_url('contactus')?>" class="btn-ff btn-tertiary-light">Get in Touch</a></p>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<div class="footer-intro-wrapper">
					<img src="<?= base_url()?>assets/images/ff-white-logo.svg" alt="" class="footer-logo">
					<p class="footer-intro">
						Free Range Archive producers to not have to struggle with high licence fees and to enable them to access and afford archival material of high quality.
					</p>
				</div>
			</div>
			<div class="col-sm-8">
				<div class="footer-links-wrapper">
					<div class="footer-links-single">
						<h4>Learn More</h4>
						<ul>
							<li><a href="#/">About Us</a></li>
							<li><a href="#/">Blog</a></li>
							<li><a href="#/">How We Operate</a></li>
						</ul>
					</div>
					<div class="footer-links-single">
						<h4>SUPPORT</h4>
						<ul>
							<li><a href="#/">+44(0)207 631 3773</a></li>
							<li><a href="#/">Contact Us</a></li>
							<li><a href="#/">FAQs</a></li>
						</ul>
					</div>
				</div>
				<div class="social-links-wrapper">
					<a href="https://vimeo.com/footagefarm" target="_blank"><img src="<?= base_url()?>assets/images/vimeo.svg" alt=""></a>
					<a href="https://www.youtube.com/user/footagefarm" target="_blank"><img src="<?= base_url()?>assets/images/youtube.svg" alt=""></a>
					<a href="https://twitter.com/footagefarmltd" target="_blank"><img src="<?= base_url()?>assets/images/twitter.svg" alt=""></a>
					<a href="https://www.facebook.com/FootageFarmLtd/" target="_blank"><img src="<?= base_url()?>assets/images/facebook.svg" alt=""></a>
					<a href="https://www.linkedin.com/company/footage-farm" target="_blank"><img src="<?= base_url()?>assets/images/linkedin.svg" alt=""></a>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="footer-seperator"></div>
				<div class="copyright-wrapper">
					<p class="copyright-info">&copy; 2020 Footage Farm</p>
					<p class="privacy">
						<a href="<?= base_url('terms_condition')?>">Terms and Conditions</a>
						<span>|</span>
						<a href="<?= base_url('terms_condition')?>" class="privacy-link">Privacy Policy</a>
					</p>
					<p class="backtotop-mobile">
						<a href="#top-logo">Back to Top</a>
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="multicolored-line multicolred-footer-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<?php if(!isset($_COOKIE['footage_farm'])) { ?> 
	<div class="cookie-container">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="cookie-content">
						<div class="cookie-text">	
							<p>We use cookies to provide you with better experience. By using our website you agree to the Footage Farm's 
								<a href="<?= base_url('terms_condition')?>">Terms and Conditions</a> and 
								<a href="<?= base_url('terms_condition')?>">Privacy Policy</a> which includes Footage Farm's practices regarding personal data and cookies.
							</p>	
						</div>
						<div class="cookie-link"><a href="#/" class="accept-cookie">I Agree</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php } ?>	
</footer>
<!-- Footer -->
<!-- Notification Modal -->
<div class="notification-message">
	<div class="notification-overlay"></div>
	<div class="notification-modal">
		<div class="modal-message">
			<img src="<?= base_url('images/completed.svg')?>">
			<p class="notification-modal-text"></p>
		</div>
	</div>									
</div>
<!-- Notification Modal -->

<script type="text/javascript">
<?php 
if($this->session->flashdata('success_msg')) {?>
	// alert("<?= $this->session->flashdata('success_msg')?>");
	// swal("Welcome <?= $this->session->userdata('footage_farm_user')[0]['user_name']?>","<?= $this->session->flashdata('success_msg')?>", {
	// 	icon : "success",
	// 	buttons: {        			
	// 		confirm: {
	// 			className : 'btn btn-success'
	// 		}
	// 	},
	// })
<?php }
?>
</script>
	
<script>

$(document).ready(function(){

	$(document).on("click",".reelshare",function () {
        var link = $(this).attr("data-url");
        var linkfield = $('.linkfield').val(link);
        var facebook = "https://www.facebook.com/sharer.php?u="+link;
        var twitter = "https://twitter.com/share?url="+link+"&text=reel&via=reel&hashtags=reel";
        var linkedin = "https://www.linkedin.com/shareArticle?url="+link+"&title=reel";
	    $('.fhref').attr("href", facebook);
	    $('.thref').attr("href", twitter);
	    $('.lhref').attr("href", linkedin);
	});


	$(document).on("click",".accept-cookie",function () {
        var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
        var csrfHash = $('.txt_csrfname').val(); // CSRF hash
		$.ajax({
		url: "<?= base_url('home/cookies')?>",
		type: "POST",
		data:{ [csrfName]: csrfHash },
		dataType: "json",
			success: function(response){
				// location.reload();
			}
		}); 
	});
	

	var duration = $("#durationid").val();
	$("#duration").val(duration);

	var type = $("#typeid").val();
	$("#type").val(type);


	$(document).on("submit",".search-form",function (e) {
		 e.preventDefault();
        var status = $(this).val();
        var search = $("input[name=search]").val();
        if(search != ''){
        	
        	window.location = "<?= base_url('search?key=')?>"+search;

        }
		
	    });

	$(document).on("keyup","input[name='search']",function () {
		var key = $(this).val();
        
        var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
	    var csrfHash = $('.txt_csrfname').val(); // CSRF hash
	    if (key.length > 2) {
	    	
	    	$.ajax({
			url: "<?= base_url('search/listdata')?>",
			type: "POST",
			data:{ key, [csrfName]: csrfHash },
			dataType: "json",
			success: function(response){
				var select = "";     
                $(response).each(function (index, o) {
                  
                    select += '<option value="' + o + '">';
                     
                });
                   
                $("#browsers").html(select);
				
			}
		});

	    }
		
	      
	    });

	$(document).on("change","input[name='search']",function () {
		var key = $(this).val();
        if(key != ''){
        	window.location = "<?= base_url('search?key=')?>"+key;
        }
	});


});

// use for form submit
var vRules = 
{
	first_name:{required:true},
	last_name:{required:true},
	email:{required:true},
	phone:{required:true},
	user_password:{required:true},
};
var vMessages = 
{
	first_name:{required:"Please Enter First Name."},
	last_name:{required:"Please Enter Last Name."},
	email:{required:"Please Enter Email."},
	phone:{required:"Please Enter Phone No."},
	user_password:{required:"Please Enter Password."},

};

$("#form-register").validate({
	ignore:[],
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{	
		var act = "<?= base_url('register/submitForm')?>";
		$("#form-register").ajaxSubmit({
			url: act, 
			type: 'post',
			dataType: 'json',
			cache: false,
			clearForm: false,
			async:false,
			beforeSubmit : function(arr, $form, options){
				$(".btn btn-black").hide();
			},
			success: function(response) 
			{
				if(response.success){
					//alert(response.msg);
					$(".alert-success").show();
					$(".showalert").html(response.msg);
					$("#gologin").trigger("click");
					//window.location = "<?= base_url('register')?>";
				}else{	
					alert(response.msg);
				}
			}
		});

	}
});

$("#form-login").validate({
  ignore:[],
  rules: {
  username:{required:true},
  password:{required:true},
    },
  messages: {
  username:{required:"Please Enter User Name."},
  password:{required:"Please Enter Password."},
    },
  submitHandler: function(form) 
  { 
    var act = "<?= base_url('login/form_validate')?>";
    $("#form-login").ajaxSubmit({
      url: act, 
      type: 'post',
      dataType: 'json',
      cache: false,
      clearForm: false,
      async:false,
      beforeSubmit : function(arr, $form, options){
        $(".btn btn-black").hide();
      },
      success: function(response) 
      {
        if(response.success){
          window.location = "<?= base_url('home')?>";
        }else{

          $(".text-danger").html(response.msg);

        }
      }
    });

  }
});

$("#form-forgotpassword").validate({
	ignore:[],
	rules: { email:{required:true} },	
	messages: { email:{required:"Please Enter Email."} },
	submitHandler: function(form) 
	{	
		var act = "<?= base_url('register/forgotpassword')?>";
		$("#form-forgotpassword").ajaxSubmit({
			url: act, 
			type: 'post',
			dataType: 'json',
			cache: false,
			clearForm: false,
			async:false,
			beforeSubmit : function(arr, $form, options){
				$(".btn btn-black").hide();
			},
			success: function(response) 
			{
				if(response.success){
					swal("done", response.msg, {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					}).then(
					function() {
						window.location = "<?= base_url('home')?>";
						// $this->router->fetch_class().'/'.$this->router->fetch_method() ?>";
					});
				}else{	
					swal("Failed", response.msg, {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}
			}
		});

	}
});

$("#form-profile").validate({
	ignore:[],
	rules: {
	username:{required:true},
	useremail:{required:true},
	userphone:{required:true},
    },
	messages: {
	username:{required:"Please Enter User Name."},
	useremail:{required:"Please Enter Email."},
	userphone:{required:"Please Enter Phone No."},

    },
	submitHandler: function(form) 
	{	
		var act = "<?= base_url('profile/submitForm')?>";
		$("#form-profile").ajaxSubmit({
			url: act, 
			type: 'post',
			dataType: 'json',
			cache: false,
			clearForm: false,
			async:false,
			beforeSubmit : function(arr, $form, options){
				$(".btn btn-black").hide();
			},
			success: function(response) 
			{
				if(response.success){
					swal("done", response.msg, {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					}).then(
					function() {
						window.location = "<?= base_url('profile/editprofile')?>";
					});
				}else{	
					swal("Failed", response.msg, {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}
			}
		});

	}
});

$("#form-password").validate({
	ignore:[],
	rules: {
	currentpassword:{required:true},
	password:{required:true},
	confirmpassword:{required:true},
    },
	messages: {
	currentpassword:{required:"Please Enter Current Password."},
	password:{required:"Please Enter New Password."},
	confirmpassword:{required:"Please Enter Confirm Password."},

    },
	submitHandler: function(form) 
	{	
		var act = "<?= base_url('profile/changepassword')?>";
		$("#form-password").ajaxSubmit({
			url: act, 
			type: 'post',
			dataType: 'json',
			cache: false,
			clearForm: false,
			async:false,
			beforeSubmit : function(arr, $form, options){
				$(".btn btn-black").hide();
			},
			success: function(response) 
			{
				if(response.success){
					swal("done", response.msg, {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					}).then(
					function() {
						window.location = "<?= base_url('profile/editprofile')?>";
					});
				}else{	
					swal("Failed", response.msg, {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}
			}
		});

	}
});

// use for form submit
$("#form-enquiry").validate({
	ignore:[],
	rules: {
		name:{required:true},
		email:{required:true},
		phone:{required:true},
		company:{required:true},
		project:{required:true},
		"reelurl[]":{required:true},
		projectdesc:{required:true},
	},
	messages: {
		name:{required:"Please Enter Name."},
		email:{required:"Please Enter Email."},
		phone:{required:"Please Enter Phone."},
		company:{required:"Please Enter Company ."},
		project:{required:"Please Enter Project."},
		"reelurl[]":{required:"Please Enter Reelurl."},
		projectdesc:{required:"Please Enter Projectdesc."},
	},
	submitHandler: function(form) 
	{	
		var act = "<?= base_url('enquiry/submitForm')?>";
		$("#form-enquiry").ajaxSubmit({
			url: act, 
			type: 'post',
			dataType: 'json',
			cache: false,
			clearForm: false,
			async:false,
			beforeSubmit : function(arr, $form, options){
				$(".btn btn-black").hide();
			},
			success: function(response) 
			{
				if(response.success){
					swal("done", response.msg, {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					}).then(
					function() {
						window.location = "<?= base_url('enquiry')?>";
						// $this->router->fetch_class().'/'.$this->router->fetch_method() ?>";
					});
				}else{	
					swal("Failed", response.msg, {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}
			}
		});
	}
});

$("#form-share").validate({
	ignore:[],
	rules: {
	reelink:{required:true},
	sendtoemail:{required:true},
    },
	messages: {
	reelink:{required:"Please Enter Link."},
	sendtoemail:{required:"Please Enter Email."},

    },
	submitHandler: function(form) 
	{	
		var act = "<?= base_url('register/sharelink')?>";
		$("#form-share").ajaxSubmit({
			url: act, 
			type: 'post',
			dataType: 'json',
			cache: false,
			clearForm: false,
			async:false,
			beforeSubmit : function(arr, $form, options){
				$(".btn btn-black").hide();
			},
			success: function(response) 
			{
				if(response.success){
					swal("done", response.msg, {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					}).then(
					function() {
						location.reload();
						//window.location = "<?= base_url('profile/editprofile')?>";
					});
				}else{	
					swal("Failed", response.msg, {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}
			}
		});

	}
});


</script>

<script>
	$(document).ready(function() {
		$('.profile-purchase-history-link').click(function() {
			$('#profile-favourites,#profile-projects').hide();
			$('#profile-purchase-history').show();
			$('#myprofile-projectfilter, #myprofile-addnewproject,#myprofile-favoritefilter').hide()
		})
		$('.profile-favourite-link').click(function() {
			$('#profile-purchase-history,#profile-projects').hide();
			$('#profile-favourites,#myprofile-favoritefilter').show();
			$('#myprofile-projectfilter, #myprofile-addnewproject').hide()
		})
		$('.profile-projects-link').click(function() {
			$('#profile-favourites,#profile-purchase-history').hide();
			$('#profile-projects').show();
			$('#myprofile-favoritefilter').hide();						$('#myprofile-projectfilter, #myprofile-addnewproject').show()
		})
		$('.tabs__items li a').click(function() {
			$('.tabs__items li a').removeClass('active');
			$(this).addClass('active');
		})
		/*Favourites section scripts*/
		/*Notification messages*/
		$('.addtofavourite-subtheme').click(function() {
			$('.notification-message').fadeIn();
			$('.notification-modal-text').text('You saved this subtheme as your favourites!')
			setTimeout(function() {
				$('.notification-message').fadeOut();
			}, 2000);
		});
		
		$('.select-all').click(function() {
			$('.notification-message').fadeIn();
			$('.notification-modal-text').text('You selected all reels!')
			setTimeout(function() {
				$('.notification-message').fadeOut();
			}, 2000);
		});
		$('.deselect-all').click(function() {
			$('.notification-message').fadeIn();
			$('.notification-modal-text').text('You deselected all reels!')
			setTimeout(function() {
				$('.notification-message').fadeOut();
			}, 2000);
		});
	
		$('.share-cart').click(function(){
			$('.notification-message').fadeIn();		
			$('.notification-modal-text').text('Reel added to cart!')
			setTimeout(function(){ $('.notification-message').fadeOut();	 }, 2000);
			$('.selected-items-wrapper').fadeIn()
		})
		/*Notification messages*/
		// /*Addmore Start/ End TC*/
		// $('.tc-addmore-link').click(function() {
		// 	$('#addmore-begin-end-tc').append(
		// 		"<div class='begin-end-tc'>" +
		// 		"<div class='tc-time begin-tc'>" +
		// 		"<label for='begintc'>Begin TC" +
		// 		"</label>" +
		// 		"<input type='text' class='form-control ff-field-dark' id='begintc' name='begintc' placeholder='00:00:00'>" +
		// 		"</div>" +
		// 		"<div class='tc-time end-tc'>" +
		// 		"<label for='endtc'>End TC" +
		// 		"</label>" +
		// 		"<input type='text' class='form-control ff-field-dark' id='endtc' name='endtc' placeholder='00:00:00'>" +
		// 		"<a href='#/' class='remove-tc'><img src='images/close.svg'/></a>" +
		// 		"</div>	" +
		// 		"</div>"
		// 	)
		// });
		// $('body').on('click', '.remove-tc', function() {
		// 	$(this).closest('.begin-end-tc').remove();
		// });
		/*Addmore Start/ End TC*/
		$('#upload-reel-times').dropzone({
			url: "/file/post"
		});
		$('.file-remove-icon').click(function() {
			$(this).closest('.file-single').remove();
		});
		$('.share-reel-form').validate();
		$('.enquiry-master-form').validate({
			ignore: []
		});
		/*Favourites section scripts*/				/*Cookie Close*/		$('.accept-cookie').click(function(){						$('.cookie-container').slideToggle();		})		/*Cookie Close*/
	});
</script>

<script>
	$(document).ready(function() {
		$('.edit-profile-link').click(function() {
			$('#change-password').hide();
			$('#edit-profile').show();
		})
		$('.change-password-link').click(function() {
			$('#edit-profile').hide();
			$('#change-password').show();
		})
		$('.tabs__items li a').click(function() {
			$('.tabs__items li a').removeClass('active');
			$(this).addClass('active');
		})		
		$('.imgupload-btn').click(function(){
			$('#imgupload').trigger('click'); 
		})
	});
</script>