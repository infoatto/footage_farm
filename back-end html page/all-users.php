
<?php require_once('include/head.php')?>
<body>
	<div class="wrapper">
    <?php require_once('include/main-header.php')?>
		<?php require_once('include/sidebar.php')?>
		<div class="main-panel">
			<div class="container">
				<div class="page-inner">
					<div class="col-12">
						<div class="row ff-title-block">
							<div class="col-12 col-sm-12 col-md-6 col-lg-6">
									<h1 class="ff-page-title">All Users</h1> 
							</div>							
						</div>
					</div>					
					<div class="row">
							<div class="col-sm-12">
									<div class="card">
											<div class="card-body">
												<div class="table-responsive">
													<table id="add-row" class="display table table-striped table-hover" >
														<thead>
															<tr>
																<th>Name</th>
																<th>Email</th>																
																<th>Mobile</th>																
																<th>Status</th>	
																<th>Actions</th>															
															</tr>
														</thead>
														<tfoot>
															<tr>
																<th>Name</th>
																<th>Email</th>																
																<th>Mobile</th>																
																<th>Status</th>	
																<th>Actions</th>															
															</tr>
														</tfoot>
														<tbody>
															<tr>																
																<td>Brett Hall</td>
																<td>bretthall345@gmail.com</td>																
																<td>+91-123-456-7890</td>																
																<td>Active</td>	
																<td>
																	<div class="form-button-action">
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Edit">
																			<i class="fa fa-edit"></i>
																		</a>
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																			<i class="fa fa-times"></i>
																		</a>
																	</div>
																</td>													
															</tr>
															<tr>																
																<td>Jason Stevenson</td>
																<td>jasonstevenson_21@yahoomail.co.</td>																
																<td>+91-123-456-7890</td>																
																<td>Active</td>	
																<td>
																	<div class="form-button-action">
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Edit">
																			<i class="fa fa-edit"></i>
																		</a>
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																			<i class="fa fa-times"></i>
																		</a>
																	</div>
																</td>													
															</tr>
															<tr>																
																<td>Pauline Caldwell</td>
																<td>paulinecaldwell_1921@hotmail.com</td>																
																<td>+91-123-456-7890</td>																
																<td>Active</td>	
																<td>
																	<div class="form-button-action">
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Edit">
																			<i class="fa fa-edit"></i>
																		</a>
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																			<i class="fa fa-times"></i>
																		</a>
																	</div>
																</td>													
															</tr>
															<tr>																
																<td>Timothy Yates</td>
																<td>timyates-35@gmail.com</td>																
																<td>+91-123-456-7890</td>																
																<td>Active</td>	
																<td>
																	<div class="form-button-action">
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Edit">
																			<i class="fa fa-edit"></i>
																		</a>
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																			<i class="fa fa-times"></i>
																		</a>
																	</div>
																</td>													
															</tr>
															<tr>																
																<td>Georgie Miles</td>
																<td>georgie_miles@gmail.com</td>																
																<td>+91-123-456-7890</td>																
																<td>Active</td>	
																<td>
																	<div class="form-button-action">
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Edit">
																			<i class="fa fa-edit"></i>
																		</a>
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																			<i class="fa fa-times"></i>
																		</a>
																	</div>
																</td>													
															</tr>
															
														</tbody>
													</table>
												</div>
											</div>
									</div>
							</div>
					</div>
				</div>
			</div>
			
		</div>						
	</div>
	<?php require_once('include/footer.php')?>
	<script >
		$(document).ready(function() {
			// Add Row
			$('#add-row').DataTable({
				"pageLength": 5,
			});			
		});
	</script>
</body>
</html>
