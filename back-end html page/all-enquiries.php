
<?php require_once('include/head.php')?>
<body>
	<div class="wrapper">
    <?php require_once('include/main-header.php')?>
		<?php require_once('include/sidebar.php')?>
		<div class="main-panel">
			<div class="container">
				<div class="page-inner">
					<div class="col-12">
						<div class="row ff-title-block">
							<div class="col-12 col-sm-12 col-md-6 col-lg-6">
									<h1 class="ff-page-title">All Enquiries</h1> 
							</div>							
						</div>
					</div>					
					<div class="row">
							<div class="col-sm-12">
									<div class="card">
											<div class="card-body">
												<div class="table-responsive">
													<table id="add-row" class="display table table-striped table-hover" >
														<thead>
															<tr>
																<th>Enquiry ID</th>
																<th>Name</th>
																<th>Email</th>																
																<th>No. of Videos</th>																
																<th>Date</th>	
																<th>Actions</th>															
															</tr>
														</thead>
														<tfoot>
															<tr>
																<th>Enquiry ID</th>
																<th>Name</th>
																<th>Email</th>																
																<th>No. of Videos</th>																
																<th>Date</th>	
																<th>Actions</th>															
															</tr>
														</tfoot>
														<tbody>
															<tr>																
																<td>#2121</td>
																<td>Ronnie Schultz</td>																
																<td>ronnie_schultz@hotmail.com</td>			
																<td>12</td>													
																<td>12 Aug, 20</td>	
																<td>
																	<div class="form-button-action">
																		<a href="enquiry-reply.php" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Reply">
																			<i class="fa fa-edit"></i>
																		</a>
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																			<i class="fa fa-times"></i>
																		</a>
																	</div>
																</td>													
															</tr><tr>																
																<td>#1000</td>
																<td>Ronnie Schultz</td>																
																<td>ronnie_schultz@hotmail.com</td>			
																<td>12</td>													
																<td>12 Aug, 20</td>	
																<td>
																	<div class="form-button-action">
																		<a href="enquiry-reply.php" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Reply">
																			<i class="fa fa-edit"></i>
																		</a>
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																			<i class="fa fa-times"></i>
																		</a>
																	</div>
																</td>													
															</tr>
															</tr><tr>																
																<td>#2121</td>
																<td>shiv Schultz</td>																
																<td>ronnie_schultz@hotmail.com</td>			
																<td>12</td>													
																<td>12 Aug, 20</td>	
																<td>
																	<div class="form-button-action">
																		<a href="enquiry-reply.php" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Reply">
																			<i class="fa fa-edit"></i>
																		</a>
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																			<i class="fa fa-times"></i>
																		</a>
																	</div>
																</td>													
															</tr>
															</tr>
															<tr>																
																<td>#2121</td>
																<td>Ronnie Schultz</td>																
																<td>ronnie_schultz@hotmail.com</td>			
																<td>12</td>													
																<td>12 Aug, 20</td>	
																<td>
																	<div class="form-button-action">
																		<a href="enquiry-reply.php" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Reply">
																			<i class="fa fa-edit"></i>
																		</a>
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																			<i class="fa fa-times"></i>
																		</a>
																	</div>
																</td>													
															</tr>
															<tr>																
																<td>#2121</td>
																<td>Ronnie Schultz</td>																
																<td>ronnie_schultz@hotmail.com</td>			
																<td>12</td>													
																<td>12 Aug, 20</td>	
																<td>
																	<div class="form-button-action">
																		<a href="enquiry-reply.php" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Reply">
																			<i class="fa fa-edit"></i>
																		</a>
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																			<i class="fa fa-times"></i>
																		</a>
																	</div>
																</td>													
															</tr>
															<tr>																
																<td>#2121</td>
																<td>Ronnie Schultz</td>																
																<td>ronnie_schultz@hotmail.com</td>			
																<td>12</td>													
																<td>12 Aug, 20</td>	
																<td>
																	<div class="form-button-action">
																		<a href="enquiry-reply.php" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Reply">
																			<i class="fa fa-edit"></i>
																		</a>
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																			<i class="fa fa-times"></i>
																		</a>
																	</div>
																</td>													
															</tr>
															<tr>																
																<td>#2121</td>
																<td>Ronnie Schultz</td>																
																<td>ronnie_schultz@hotmail.com</td>			
																<td>12</td>													
																<td>12 Aug, 20</td>	
																<td>
																	<div class="form-button-action">
																		<a href="enquiry-reply.php" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Reply">
																			<i class="fa fa-edit"></i>
																		</a>
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																			<i class="fa fa-times"></i>
																		</a>
																	</div>
																</td>													
															</tr>

															</tr><tr>																
																<td>#2121</td>
																<td>Ronnie Schultz</td>																
																<td>rahul@hotmail.com</td>			
																<td>12</td>													
																<td>12 Aug, 20</td>	
																<td>
																	<div class="form-button-action">
																		<a href="enquiry-reply.php" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Reply">
																			<i class="fa fa-edit"></i>
																		</a>
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																			<i class="fa fa-times"></i>
																		</a>
																	</div>
																</td>													
															</tr>
															
															
														</tbody>
													</table>
												</div>
											</div>
									</div>
							</div>
					</div>
				</div>
			</div>
			
		</div>						
	</div>
	<?php require_once('include/footer.php')?>
	<script >
		$(document).ready(function() {
			// Add Row
			$('#add-row').DataTable({
				"pageLength": 5,
			});			
		});
	</script>
</body>
</html>
