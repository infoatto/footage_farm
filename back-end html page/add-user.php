
<?php require_once('include/head.php')?>
<body>
	<div class="wrapper">
        <?php require_once('include/main-header.php')?>
		<?php require_once('include/sidebar.php')?>
		<div class="main-panel">
			<div class="container">
				<div class="page-inner">
					<div class="col-12">
						<div class="row ff-title-block">
								<div class="col-12 col-sm-12 col-md-6 col-lg-6">
										<h1 class="ff-page-title">Add/Edit User</h1> 
								</div>
								<div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
										<a href="#" class="btn btn-light btn-border ff-page-title-btn">
												Cancel
										</a>
								</div>
						</div>
					</div>					
					<div class="row">
						<div class="col-sm-12">
							<div class="card">
									<div class="card-body">
											<form action="">                                        
													<div class="form-group">
														<label for="username">Name</label>
														<input type="text" class="form-control" id="username" placeholder="John Doe">	
													</div>
													<div class="form-group">
														<label for="useremail">Email</label>
														<input type="email" class="form-control" id="useremail" placeholder="johndoe@gmail.com">	
													</div>
													<div class="form-group">
														<label for="userphone">Mobile</label>
														<input type="text" class="form-control" id="userphone" placeholder="+91-123-456-7890">	
													</div>																				
													<div class="form-check">
														<label>Status</label><br>
														<div class="switch-field">
															<input type="radio" id="radio-one" name="switch-one" value="yes" checked/>
															<label for="radio-one">Active</label>
															<input type="radio" id="radio-two" name="switch-one" value="no" />
															<label for="radio-two">Inactive</label>
														</div>
													</div>	
													<div class="form-group">
														<button class="btn btn-black">Save</button>
														<button class="btn btn-cancel btn-border">Cancel</button>
													</div>
											</form>
									</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>						
	</div>
	<?php require_once('include/footer.php')?>
</body>
</html>
