
<?php require_once('include/head.php')?>
<body>
	<div class="wrapper">
        <?php require_once('include/main-header.php')?>
		<?php require_once('include/sidebar.php')?>
		<div class="main-panel">
			<div class="container">
				<div class="page-inner">
                    <div class="col-12">
                        <div class="row ff-title-block">
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <h1 class="ff-page-title">Website Pages</h1>                               
                                <ul class="breadcrumbs">   
                                    <li class="nav-item">
                                        <a href="#">Home</a>
                                    </li>
                                    <li class="separator">
                                        <i class="flaticon-right-arrow"></i>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#">Master</a>
                                    </li>
                                </ul>
                            </div>
                            
                        </div>
                    </div>					
					<div class="row ff-master-options">
						<div class="col-md-6">
								<a href="master-new-tag.php">
										<div class="card">								
												<div class="card-body">
												<span class="h3">Blogs</span>
												</div>
										</div>
								</a>							
						</div>
						<div class="col-md-6">
								<a href="#/">
										<div class="card">								
												<div class="card-body">
														<span class="h3">About us</span>
												</div>
										</div>
								</a>							
						</div>
						<div class="col-md-6">
								<a href="#/">
										<div class="card">								
												<div class="card-body">
														<span class="h3">FAQs</span>
												</div>
										</div>
								</a>							
						</div>
						<div class="col-md-6">
								<a href="#/">
										<div class="card">								
												<div class="card-body">
														<span class="h3">Contact Us</span>
												</div>
										</div>
								</a>							
						</div>
						<div class="col-md-6">
								<a href="#/">
										<div class="card">								
												<div class="card-body">
														<span class="h3">Homepage</span>
												</div>
										</div>
								</a>							
						</div>
						
					</div>
				</div>
			</div>
			
		</div>						
	</div>
	<?php require_once('include/footer.php')?>
</body>
</html>