
<?php require_once('include/head.php')?>
<body>
	<div class="wrapper">
    <?php require_once('include/main-header.php')?>
		<?php require_once('include/sidebar.php')?>
		<div class="main-panel">
			<div class="container">
				<div class="page-inner">
					<div class="col-12">
						<div class="row ff-title-block">
							<div class="col-12 col-sm-12 col-md-10 col-lg-10">
									<h1 class="ff-page-title">Welcome Mark!</h1> 
							</div>	
							<div class="col-12 col-sm-12 col-md-2 col-lg-2">  
								<select class="form-control form-control" id="defaultSelect">
									<option>Monthly</option>
									<option>Weekly</option>
									<option>Daily</option>
									<option>Yearly</option>									
								</select>	
							</div>						
						</div>
					</div>					
					<div class="row">
							<div class="col-sm-12">
									<div class="card">
											<div class="card-body">
												<div class="row">
													<div class="col-sm-12">
															<h2><strong>New Enquiries</strong></h2>
														<hr/>
														<div class="table-responsive">
															<table id="add-row" class="display table table-striped table-hover" >
																<thead>
																	<tr>
																		<th>Enquiry ID</th>
																		<th>Name</th>
																		<th>Email</th>																
																		<th>No. of Videos</th>																
																		<th>Date</th>	
																		<th>Actions</th>															
																	</tr>
																</thead>
																<tfoot>
																	<tr>
																		<th>Enquiry ID</th>
																		<th>Name</th>
																		<th>Email</th>																
																		<th>No. of Videos</th>																
																		<th>Date</th>	
																		<th>Actions</th>															
																	</tr>
																</tfoot>
																<tbody>
																	<tr>																
																		<td>#2121</td>
																		<td>Ronnie Schultz</td>																
																		<td>ronnie_schultz@hotmail.com</td>			
																		<td>12</td>													
																		<td>12 Aug, 20</td>	
																		<td>
																			<div class="form-button-action">
																				<a href="enquiry-reply.php" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Reply">
																					<i class="fa fa-edit"></i>
																				</a>
																				<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																					<i class="fa fa-times"></i>
																				</a>
																			</div>
																		</td>													
																	</tr><tr>																
																		<td>#2121</td>
																		<td>Ronnie Schultz</td>																
																		<td>ronnie_schultz@hotmail.com</td>			
																		<td>12</td>													
																		<td>12 Aug, 20</td>	
																		<td>
																			<div class="form-button-action">
																				<a href="enquiry-reply.php" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Reply">
																					<i class="fa fa-edit"></i>
																				</a>
																				<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																					<i class="fa fa-times"></i>
																				</a>
																			</div>
																		</td>													
																	</tr>
																	</tr><tr>																
																		<td>#2121</td>
																		<td>Ronnie Schultz</td>																
																		<td>ronnie_schultz@hotmail.com</td>			
																		<td>12</td>													
																		<td>12 Aug, 20</td>	
																		<td>
																			<div class="form-button-action">
																				<a href="enquiry-reply.php" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Reply">
																					<i class="fa fa-edit"></i>
																				</a>
																				<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																					<i class="fa fa-times"></i>
																				</a>
																			</div>
																		</td>													
																	</tr>
																	</tr><tr>																
																		<td>#2121</td>
																		<td>Ronnie Schultz</td>																
																		<td>ronnie_schultz@hotmail.com</td>			
																		<td>12</td>													
																		<td>12 Aug, 20</td>	
																		<td>
																			<div class="form-button-action">
																				<a href="enquiry-reply.php" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Reply">
																					<i class="fa fa-edit"></i>
																				</a>
																				<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																					<i class="fa fa-times"></i>
																				</a>
																			</div>
																		</td>													
																	</tr>
																	</tr><tr>																
																		<td>#2121</td>
																		<td>Ronnie Schultz</td>																
																		<td>ronnie_schultz@hotmail.com</td>			
																		<td>12</td>													
																		<td>12 Aug, 20</td>	
																		<td>
																			<div class="form-button-action">
																				<a href="enquiry-reply.php" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Reply">
																					<i class="fa fa-edit"></i>
																				</a>
																				<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																					<i class="fa fa-times"></i>
																				</a>
																			</div>
																		</td>													
																	</tr>														
																	
																</tbody>
															</table>
														</div>
													</div>
												</div>
												<div class="row mt-5">
													<div class="col-sm-6">
														<div class="row ff-title-block">
															<div class="col-12 col-sm-12 col-md-7 col-lg-7">
																<h2><strong>Inactive Videos</strong></h2>
															</div>	
															<div class="col-12 col-sm-12 col-md-5 col-lg-5">  
																<select class="form-control form-control" id="defaultSelect">
																	<option>By Theme</option>
																	<option>By User</option>
																	<option>By Time</option>
																	<option>By Sales</option>									
																</select>	
															</div>						
														</div>														
														<hr/>
														<div class="table-responsive">
															<table class="table table-striped mt-3">
																<thead>
																	<tr>
																		<th scope="col">#</th>
																		<th scope="col">Name</th>
																		<th scope="col">Inactive no.</th>																		
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td>1</td>
																		<td>Civil Rights March - 1965, Alabama</td>
																		<td>21</td>																		
																	</tr>
																	<tr>
																		<td>2</td>
																		<td>Educational VideosI</td>
																		<td>19</td>																		
																	</tr>
																	<tr>
																		<td>3</td>
																		<td>1910 South India, British Raj</td>
																		<td>19</td>																		
																	</tr>
																	<tr>
																		<td>4</td>
																		<td>Animals</td>
																		<td>10</td>																		
																	</tr>
																	<tr>
																		<td>5</td>
																		<td>Great Recession - Post WWI</td>
																		<td>12</td>																		
																	</tr>
																</tbody>
															</table>
														</div>
													</div>
													<div class="col-sm-6">
														<div class="row ff-title-block">
															<div class="col-12 col-sm-12 col-md-7 col-lg-7">
																<h2><strong>Inquiry Report</strong></h2>
															</div>	
															<div class="col-12 col-sm-12 col-md-5 col-lg-5">  
																<select class="form-control form-control" id="defaultSelect">
																	<option>By Theme</option>
																	<option>By User</option>
																	<option>By Time</option>
																	<option>By Sales</option>									
																</select>	
															</div>						
														</div>														
														<hr/>
													</div>
												</div>
												
												
											</div>
									</div>
							</div>
					</div>
				</div>
			</div>
			
		</div>						
	</div>
	<?php require_once('include/footer.php')?>
	<script >
		$(document).ready(function() {
			// Add Row
			$('#add-row').DataTable({
				"pageLength": 5,
			});			
		});
	</script>
</body>
</html>
