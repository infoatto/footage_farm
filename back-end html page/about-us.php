
<?php require_once('include/head.php')?>
<body>
	<div class="wrapper">
        <?php require_once('include/main-header.php')?>
		<?php require_once('include/sidebar.php')?>
		<div class="main-panel">
			<div class="container">
				<div class="page-inner">
					<div class="col-12">
						<div class="row ff-title-block">
							<div class="col-12 col-sm-12 col-md-6 col-lg-6">
								<h1 class="ff-page-title">About Us</h1> 
							</div>
							<div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
								<a href="#" class="btn btn-light btn-border ff-page-title-btn">
										Cancel
								</a>
							</div>
						</div>
					</div>					
					<div class="row">
						<div class="col-sm-12">
							<div class="card">
									<div class="card-body">
										<h3>Header</h3>
										<hr/>
										<div class="form-group">
												<label for="exampleFormControlFile1">Banner Image</label>
												<input type="file" class="form-control-file" id="exampleFormControlFile1">
										</div>
										<div class="form-group">
											<label for="themename">Banner Heading</label>
											<input type="text" class="form-control" id="themename" placeholder="Early 19th Century">	
										</div>
										<div class="form-group">
											<label for="themedesc">Banner Subtext</label>
											<textarea class="form-control" id="themedesc" rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."></textarea>	
										</div>
										<div class="clearfix">&nbsp;</div>
										<h3>Section 1</h3>
										<hr/>
										<div class="form-group">
												<label for="exampleFormControlFile1">Image</label>
												<input type="file" class="form-control-file" id="exampleFormControlFile1">
										</div>
										<div class="form-group">
											<label for="themename">Section 1 - Heading</label>
											<input type="text" class="form-control" id="themename" placeholder="Early 19th Century">	
										</div>
										<div class="form-group">
											<label for="themedesc">Section 1 - Paragraph</label>
											<textarea class="form-control" id="themedesc" rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."></textarea>	
										</div>
										<div class="clearfix">&nbsp;</div>
										<h3>Statistic Numbers</h3>
										<hr/>
										<div class="form-group">
												<label for="stat1">Stat one</label>
												<input type="text" class="form-control mb-3" id="themename" placeholder="5000+">	
												<input type="text" class="form-control" id="themename" placeholder="hours of footage">	
										</div>
										<div class="form-group">
												<label for="stat1">Stat two</label>
												<input type="text" class="form-control mb-3" id="themename" placeholder="24 hours">	
												<input type="text" class="form-control" id="themename" placeholder="Turnaround service">	
										</div>
										<div class="form-group">
												<label for="stat1">Stat three</label>
												<input type="text" class="form-control mb-3" id="themename" placeholder="25000">	
												<input type="text" class="form-control" id="themename" placeholder="entries in our masters database">	
										</div>
										<div class="clearfix">&nbsp;</div>
										<h3>Section 2</h3>
										<hr/>
										<div class="form-group">
												<label for="exampleFormControlFile1">Image</label>
												<input type="file" class="form-control-file" id="exampleFormControlFile1">
										</div>
										<div class="form-group">
											<label for="themename">Section 2 - Heading</label>
											<input type="text" class="form-control" id="themename" placeholder="Early 19th Century">	
										</div>
										<div class="form-group">
											<label for="themedesc">Section 2 - Paragraph</label>
											<textarea class="form-control" id="themedesc" rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."></textarea>	
										</div>
										<div class="clearfix">&nbsp;</div>
										<h3>Meta Tags</h3>											
										<div class="form-group">
											<label for="themename">Meta Title</label>
											<input type="text" class="form-control" id="themename" placeholder="Early 19th Century">	
										</div>
										<div class="form-group">
											<label for="themedesc">Meta Description</label>
											<textarea class="form-control" id="themedesc" rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."></textarea>	
										</div>										
										<div class="form-group">
											<button class="btn btn-black">Save</button>
											<button class="btn btn-cancel btn-border">Cancel</button>
										</div>
											
									</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>						
	</div>
	<?php require_once('include/footer.php')?>
</body>
</html>
