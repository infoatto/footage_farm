
<?php require_once('include/head.php')?>
<body>
	<div class="wrapper">
    <?php require_once('include/main-header.php')?>
		<?php require_once('include/sidebar.php')?>
		<div class="main-panel">
			<div class="container">
				<div class="page-inner">
					<div class="col-12">
						<div class="row ff-title-block">
							<div class="col-12 col-sm-12 col-md-6 col-lg-6">
								<h1 class="ff-page-title">New Blog Post</h1> 
							</div>
							<div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
								<a href="#" class="btn btn-light btn-border ff-page-title-btn">
										Cancel
								</a>
							</div>
						</div>
					</div>					
					<div class="row">
						<div class="col-sm-12">
							<div class="card">
								<div class="card-body">
									<form action="">
											<div class="form-group">
												<label for="themename">Title</label>
												<input type="text" class="form-control" id="themename" placeholder="eg 1910-1918- WWI  Era: Aerials, German Officer & Troops.">	
											</div>
											<div class="form-group">
												<label for="themename">Slug</label>
												<input type="text" class="form-control" id="themename" placeholder="eg. 110-quick-tips-about-blogging">	
											</div>
											<div class="form-group">
												<label for="themedesc">Post Body</label>
												<textarea class="form-control" id="themedesc" rows="20" placeholder="eg. Asurprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."></textarea>	
											</div>
											<div class="form-group">
													<label for="exampleFormControlFile1">Main Image</label>
													<input type="file" class="form-control-file" id="exampleFormControlFile1">
											</div>
											<div class="form-group">
													<label for="exampleFormControlFile1">Thumbnail Image</label>
													<input type="file" class="form-control-file" id="exampleFormControlFile1">
											</div>
											<div class="form-group">
												<label for="themename">Tags</label>
												<input type="text" class="form-control" id="themename" placeholder="Early 19th Century">	
											</div>										
											<div class="form-group">
												<label for="themename">Meta Title</label>
												<input type="text" class="form-control" id="themename" placeholder="Early 19th Century">	
											</div>
											<div class="form-group">
												<label for="themedesc">Meta Description</label>
												<textarea class="form-control" id="themedesc" rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."></textarea>	
											</div>
											<div class="form-check">
												<label>Featured </label><br>
												<div class="switch-field">
													<input type="radio" id="radio-one" name="switch-one" value="yes" checked/>
													<label for="radio-one">Yes</label>
													<input type="radio" id="radio-two" name="switch-one" value="no" />
													<label for="radio-two">No</label>
												</div>
											</div>
											<div class="form-check">
												<label>Active</label><br>
												<div class="switch-field">
													<input type="radio" id="radio-three" name="switch-two" value="yes" checked/>
													<label for="radio-three">Yes</label>
													<input type="radio" id="radio-four" name="switch-two" value="no" />
													<label for="radio-four">No</label>
												</div>
											</div>	
											<div class="form-group">
												<button class="btn btn-black">Save</button>
												<button class="btn btn-cancel btn-border">Cancel</button>
											</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>						
	</div>
	<?php require_once('include/footer.php')?>
</body>
</html>
