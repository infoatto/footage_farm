
<?php require_once('include/head.php')?>
<body>
	<div class="wrapper">
        <?php require_once('include/main-header.php')?>
		<?php require_once('include/sidebar.php')?>
		<div class="main-panel">
			<div class="container">
				<div class="page-inner">
					<div class="col-12">
						<div class="row ff-title-block">
							<div class="col-12 col-sm-12 col-md-6 col-lg-6">
								<h1 class="ff-page-title">Contact Us</h1> 
							</div>
							<div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
								<a href="#" class="btn btn-light btn-border ff-page-title-btn">
										Cancel
								</a>
							</div>
						</div>
					</div>					
					<div class="row">
						<div class="col-sm-12">
							<div class="card">
									<div class="card-body">
										<h3>Header</h3>
										<hr/>
										<div class="form-group">
												<label for="contacturl">Header Video URL</label>
												<input type="text" class="form-control" id="contacturl" placeholder="https://vimeo.com/55900216">
										</div>
										<div class="form-group">
											<label for="themename">Heading</label>
											<input type="text" class="form-control" id="themename" placeholder="Contact us">	
										</div>
										<div class="form-group">
											<label for="themedesc">Banner Heading</label>
											<textarea class="form-control" id="themedesc" rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."></textarea>	
										</div>																	
										<div class="form-group">
											<button class="btn btn-black">Save</button>
											<button class="btn btn-cancel btn-border">Cancel</button>
										</div>
											
									</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>						
	</div>
	<?php require_once('include/footer.php')?>
</body>
</html>
