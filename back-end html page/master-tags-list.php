
<?php require_once('include/head.php')?>
<body>
	<div class="wrapper">
        <?php require_once('include/main-header.php')?>
		<?php require_once('include/sidebar.php')?>
		<div class="main-panel">
			<div class="container container-full">
				<div class="page-inner page-inner-fill">
					<div class="mail-wrapper bg-white">
						
						<div class="page-content mail-content">
							<div class="inbox-head d-lg-flex d-block">
								<h1 class="ff-page-title">Tags</h1>
								<form action="#" class="ml-auto">
									<div class="input-group">
										<input type="text" placeholder="Search Tags" class="form-control">
										<div class="input-group-append">
											<span class="input-group-text">
												<i class="fa fa-search search-icon"></i>
											</span>
										</div>
									</div>
								</form>
							</div>
							<div class="inbox-body">
								<div class="mail-option">
									<div class="email-filters-left">
										<div class="chk-all">
											<div class="btn-group">
												<div class="form-check">
													<label class="custom-control custom-checkbox">
														<input type="checkbox" class="custom-control-input"><span class="custom-control-label"></span>
													</label>
												</div>
											</div>
										</div>
										<div class="btn-group">
											<button data-toggle="dropdown" type="button" class="btn btn-secondary btn-border dropdown-toggle"> With selected </button>
											<div role="menu" class="dropdown-menu">
												<a href="#" class="dropdown-item">Mark as active</a>
												<a href="#" class="dropdown-item">Mark as inactive</a>
												<a href="#" class="dropdown-item">Delete</a>																								
											</div>
										</div>										
										<div class="btn-group">
											<button data-toggle="dropdown" type="button" class="btn btn-secondary btn-border dropdown-toggle" aria-expanded="false">Order by </button>
											<div role="menu" class="dropdown-menu dropdown-menu-right">
												<a href="#" class="dropdown-item">Date</a>
												<a href="#" class="dropdown-item">Alphabetically</a>
												<a href="#" class="dropdown-item">Views</a>												
											</div>
										</div>
									</div>

									<div class="email-filters-right ml-auto"><span class="email-pagination-indicator">1-50 of 213</span>
										<div class="btn-group ml-3">
											<button type="button" class="btn btn-secondary btn-border"><i class="fa fa-angle-left"></i></button>
											<button type="button" class="btn btn-secondary btn-border"><i class="fa fa-angle-right"></i></button>
										</div>
									</div>
								</div>

								<div class="email-list">
									
									<div class="email-list-item unread">
										<div class="email-list-actions">
											<div class="d-flex">
												<label class="custom-control custom-checkbox">
													<input type="checkbox" class="custom-control-input"><span class="custom-control-label"></span>
												</label>												
											</div>
										</div>
										<div class="email-list-detail">
											<span class="date float-right">Active</span><span class="from">Tag 1</span>
											<p class="msg">Lorem ipsum dolor sit amet, consectetur adipiscing…</p>
										</div>
									</div>
									<div class="email-list-item unread">
										<div class="email-list-actions">
											<div class="d-flex">
												<label class="custom-control custom-checkbox">
													<input type="checkbox" class="custom-control-input"><span class="custom-control-label"></span>
												</label>												
											</div>
										</div>
										<div class="email-list-detail">
											<span class="date float-right">Active</span><span class="from">Tag 2</span>
											<p class="msg">Lorem ipsum dolor sit amet, consectetur adipiscing…</p>
										</div>
									</div>
									<div class="email-list-item">
										<div class="email-list-actions">
											<div class="d-flex">
												<label class="custom-control custom-checkbox">
													<input type="checkbox" class="custom-control-input"><span class="custom-control-label"></span>
												</label>												
											</div>
										</div>
										<div class="email-list-detail">
											<span class="date float-right">Active</span><span class="from">Tag 3</span>
											<p class="msg">Lorem ipsum dolor sit amet, consectetur adipiscing…</p>
										</div>
									</div>
									<div class="email-list-item">
										<div class="email-list-actions">
											<div class="d-flex">
												<label class="custom-control custom-checkbox">
													<input type="checkbox" class="custom-control-input"><span class="custom-control-label"></span>
												</label>												
											</div>
										</div>
										<div class="email-list-detail">
											<span class="date float-right">Active</span><span class="from">Tag 4</span>
											<p class="msg">Lorem ipsum dolor sit amet, consectetur adipiscing…</p>
										</div>
									</div>
									<div class="email-list-item">
										<div class="email-list-actions">
											<div class="d-flex">
												<label class="custom-control custom-checkbox">
													<input type="checkbox" class="custom-control-input"><span class="custom-control-label"></span>
												</label>												
											</div>
										</div>
										<div class="email-list-detail">
											<span class="date float-right">Active</span><span class="from">Tag 5</span>
											<p class="msg">Lorem ipsum dolor sit amet, consectetur adipiscing…</p>
										</div>
									</div>
									<div class="email-list-item">
										<div class="email-list-actions">
											<div class="d-flex">
												<label class="custom-control custom-checkbox">
													<input type="checkbox" class="custom-control-input"><span class="custom-control-label"></span>
												</label>												
											</div>
										</div>
										<div class="email-list-detail">
											<span class="date float-right">Active</span><span class="from">Tag 6</span>
											<p class="msg">Lorem ipsum dolor sit amet, consectetur adipiscing…</p>
										</div>
									</div>
									<div class="email-list-item">
										<div class="email-list-actions">
											<div class="d-flex">
												<label class="custom-control custom-checkbox">
													<input type="checkbox" class="custom-control-input"><span class="custom-control-label"></span>
												</label>												
											</div>
										</div>
										<div class="email-list-detail">
											<span class="date float-right">Active</span><span class="from">Tag 7</span>
											<p class="msg">Lorem ipsum dolor sit amet, consectetur adipiscing…</p>
										</div>
									</div>
									<div class="email-list-item">
										<div class="email-list-actions">
											<div class="d-flex">
												<label class="custom-control custom-checkbox">
													<input type="checkbox" class="custom-control-input"><span class="custom-control-label"></span>
												</label>												
											</div>
										</div>
										<div class="email-list-detail">
											<span class="date float-right">Active</span><span class="from">Tag 8</span>
											<p class="msg">Lorem ipsum dolor sit amet, consectetur adipiscing…</p>
										</div>
									</div>
									<div class="email-list-item">
										<div class="email-list-actions">
											<div class="d-flex">
												<label class="custom-control custom-checkbox">
													<input type="checkbox" class="custom-control-input"><span class="custom-control-label"></span>
												</label>												
											</div>
										</div>
										<div class="email-list-detail">
											<span class="date float-right">Active</span><span class="from">Tag 9</span>
											<p class="msg">Lorem ipsum dolor sit amet, consectetur adipiscing…</p>
										</div>
									</div>
									<div class="email-list-item">
										<div class="email-list-actions">
											<div class="d-flex">
												<label class="custom-control custom-checkbox">
													<input type="checkbox" class="custom-control-input"><span class="custom-control-label"></span>
												</label>												
											</div>
										</div>
										<div class="email-list-detail">
											<span class="date float-right">Active</span><span class="from">Tag 10</span>
											<p class="msg">Lorem ipsum dolor sit amet, consectetur adipiscing…</p>
										</div>
									</div>
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>			
		</div>						
	</div>
	<?php require_once('include/footer.php')?>
</body>
</html>
