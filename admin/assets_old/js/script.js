$(document).ready(function() {
    /*Slider above footer*/
    $('.owl-carousel').owlCarousel({
            loop: true,
            nav: true,
            navText: ["<img src='assets/images/left-gallery-button.svg'>", "<img src='assets/images/right-gallery-button.svg'>"],
            responsive: {
                0: {
                    items: 1,
                    margin: 10,
                    stagePadding: 40
                },
                768: {
                    items: 1,
                    margin: 30,
                    stagePadding: 200
                }
            }
        })
        /*Slider above footer*/
    if ($(window).width() <= 600) {
        $('.footer-links-section h3').click(function(e) {
            $('div.footer-links-container').find('div.footer-links-section ul').slideUp('fast');
            $('.footer-links-section h3').children('img').removeClass('rotate');
            if ($(this).next('.footer-links-list').is(':visible')) {
                $(this).next('.footer-links-list').fadeOut('fast');
                $(this).children('img').removeClass('rotate');
            } else {
                $(this).next('.footer-links-list').fadeIn('fast');
                $(this).children('img').addClass('rotate');
            }
            //$(this).children('img').toggleClass('rotate');		
        })
    }
    /*Reports section - Governance page*/
    $('.reports-links-section h3').click(function(e) {
            $('div.reports-links-container').find('div.reports-links-section ul').slideUp('fast');
            $('.reports-links-section h3').children('img').removeClass('rotate');
            if ($(this).next('.reports-links-list').is(':visible')) {
                $(this).next('.reports-links-list').fadeOut('fast');
                $(this).children('img').removeClass('rotate');
            } else {
                $(this).next('.reports-links-list').fadeIn('fast');
                $(this).children('img').addClass('rotate');
            }
        })
        /*Reports section - Governance page*/
        /*Openings section - Work with us*/
    $('.openings-links-section h3').click(function(e) {
            $('div.openings-links-container').find('div.openings-links-section div.openings-links-list').slideUp('fast');
            $('.openings-links-section h3').children('img').removeClass('rotate');
            if ($(this).next('a').next('.openings-links-list').is(':visible')) {
                $(this).next('a').next('.openings-links-list').fadeOut('fast');
                $(this).children('img').removeClass('rotate');
            } else {
                $(this).next('a').next('.openings-links-list').fadeIn('fast');
                $(this).children('img').addClass('rotate');
            }
        })
        /*Openings section - Work with us*/
        // function changeMe(sel)
        //{sel.style.color = "#000";}


    /*Add to cart toggle*/
    $('.add-to-cart').click(function() {
        $(this).hide();
        $(this).next('a').css({ "display": "block" });
    });
    $('.add-to-cart-remove').click(function() {
        $(this).hide();
        $(this).prev('a').css({ "display": "block" });
    });
    /*Add to cart toggle*/
    $('.ffl-wrapper').floatingFormLabels();
    /*Custom File Input*/
    /* $('#upload-resume').click(function(){
    	alert('select file');
    }); */
    $('#upload-resume').change(function(e) {
        var fileName = e.target.files[0].name;
        //alert('The file "' + fileName +  '" has been selected.');
        $('.custom-file-input label').text(fileName);
        $('.custom-file-input img').hide();
        $('.custom-file-input label').css({ 'margin': '0' })
    });
    /*Custom File Input*/
    $('.donate-single').click(function() {
        $('.donate-single').removeClass('donate-single-active');
        $(this).addClass('donate-single-active');
    });

    /* if ($(window).width() >= 992) {
        $("body").mCustomScrollbar({
            theme: "3d-thick",
            scrollbarPosition: "inside",
            scrollInertia: 2000
        });
        $(document).on("click", "a.banner-scroll-title[href^='#'],a.backtotop[href^='#']", function(e) {
            var href = $(this).attr("href"),
                target = $(href).parents(".mCustomScrollbar");
            if (target.length) {
                e.preventDefault();
                target.mCustomScrollbar("scrollTo", href);
            }
        });
    } */

});

$(window).on("load", function() {
    $('#loader').fadeOut('1000');
    /*Work section vertical align content*/
    if ($(window).width() >= 768) {
        $('.work-section-row').each(function() {
            var workSectionImage = $(this).find('.work-section-image img').height();
            $(this).find('.work-section-content-container').css({ 'min-height': workSectionImage });
        });
    }
    /*Work section vertical align content*/
    /*Our Journey vertical align content*/
    /* if($(window).width()>=768){
    	$('.tab-container').each(function(){
    		var tabSectionImage = $(this).find('.tab-content-img').height();
    		$(this).find('.journey-tab-content').css({'min-height': tabSectionImage});
    	 });
    } */
    /*Our Journey vertical align content*/
    /*Our Journey tabs*/
    $(document).on('click', '.tab-numbers-container a', function(e) {
        //e.preventDefault();
        //$(this).next('.item-content').toggle();		
        $('.tab-numbers-container a').removeClass('active');
        $(this).addClass('active');
        var tagid = $(this).data('tag');
        $('.tab-container').removeClass('active-tab').addClass('hidden-tab');
        $('#' + tagid).addClass('active-tab').removeClass('hidden-tab');
        //$('.tab-container').hide();
        //$('#'+tagid).fadeIn();
    });
    /* $('.tab-numbers-container a').click(function(){
    	$('.tab-numbers-container a').removeClass('active');
    	$(this).addClass('active');
    	var tagid = $(this).data('tag');
    	//$('.tab-container').removeClass('active-tab').addClass('hidden-tab');
    	//$('#'+tagid).addClass('active-tab').removeClass('hidden-tab');
    	$('.tab-container').hide();
    	$('#'+tagid).fadeIn();
    }); */

    /*Our Journey tabs*/
    /* $(".governance-supporter-content").mCustomScrollbar({ theme: "dark-3" });
    $(".impact-popup-content").mCustomScrollbar({
        theme: "dark-3"
    });
    if ($(window).width() >= 992) {
        $("body").mCustomScrollbar({
            theme: "3d-thick",
            scrollbarPosition: "inside",
            scrollInertia: 2000
        });
        $(document).on("click", "a[href^='#']", function(e) {
            var href = $(this).attr("href"),
                target = $(href).parents(".mCustomScrollbar");
            if (target.length) {
                e.preventDefault();
                target.mCustomScrollbar("scrollTo", href);
            }
        });
    } */

    /*Our Journey Slider*/
  /*  $('span.ui-state-default').first().hide();
    // set up an array to hold the months

    var months = arr;
    // lets be fancy for the demo and select the current month.
    var activeMonth = new Date().getMonth();
    $(".slider")
        // activate the slider with options
        .slider({
            range: 'min',
            min: 0,
            max: months.length - 1,
            value: 0
        })
        // add pips with the labels set to "months"
        .slider("pips", {
            rest: "label",
            labels: months
        })
        // and whenever the slider changes, lets echo out the month
        .on("slidechange", function(e, ui) {
            $('.timeline-container').hide();
            $('#' + months[ui.value]).fadeIn();
            //$("#labels-months-output").text( "You selected " + months[ui.value] + " (" + ui.value + ")");
            $('.timeline-container').each(function() {
                $(this).find('.tab-container').removeClass('active-tab');
                $(this).find('.tab-container').first().addClass('active-tab');
            })
        });*/
    /*Our Journey Slider*/
    

});


/*Add to cart counter*/
function increaseCount(e, el) {
    var input = el.previousElementSibling;
    var value = parseInt(input.value, 10);
    value = isNaN(value) ? 0 : value;
    value++;
    input.value = value;
}

function decreaseCount(e, el) {
    var input = el.nextElementSibling;
    var value = parseInt(input.value, 10);
    if (value > 1) {
        value = isNaN(value) ? 0 : value;
        value--;
        input.value = value;
    }
}
/*Add to cart counter*/



$(window).scroll(function() {
  if ($(window).scrollTop() > 300) { 
    $('#buttontop').addClass('show');
  } else {
    $('#buttontop').removeClass('show');
  }
});
