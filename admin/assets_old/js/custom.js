// JavaScript Document

// Loader

/*  jQuery("body").prepend('<div class="loader">Loading...</div>');
		jQuery(document).ready(function() {
				jQuery(".loader").remove();
		}); */
	
	jQuery('a.close-advert').click(function(){
		jQuery('#advert-content').fadeOut();
	})

// Back to Top
jQuery(window).on('load', function () {	
	/* if(jQuery(window).width()<992){				
		var test_arrow = jQuery('.wpt-container').outerHeight();
		jQuery('#testimonialhome-nav button.owl-prev').css({'top': test_arrow+45})
		jQuery('#testimonialhome-nav button.owl-next').css({'top': test_arrow+45})
	} */

	var maxHeight = 0;
	jQuery('.row-price').find(
		jQuery('.list-pri-dec').each(function(){			
			if (jQuery(this).height() > maxHeight) { maxHeight = jQuery(this).height(); }
		})
	);
	$(".list-pri-dec").height(maxHeight);

	/* if(jQuery(window).width()>991){	 */
		var slideHeight = 0;		
		jQuery('#testimonial-home').find(
			jQuery('.service-single').each(function(){						
				if (jQuery(this).height() > slideHeight) { slideHeight = jQuery(this).height(); }
			})
		);	
		if(jQuery(window).width()>991){	
			jQuery(".service-single").height(slideHeight-120);
		}else{
			jQuery(".service-single").height(slideHeight);
		}
		if(jQuery(window).width()<992){	
			jQuery('#testimonialhome-nav button.owl-prev').css({'top': slideHeight+85})
			jQuery('#testimonialhome-nav button.owl-next').css({'top': slideHeight+85})
		}
	/* } */	
	
});

jQuery(document).ready(function(){ 
		jQuery(window).scroll(function(){ 
				if (jQuery(this).scrollTop() > 100) { 
						jQuery('#scroll').fadeIn(); 
				} else { 
						jQuery('#scroll').fadeOut(); 
				} 
		}); 
		jQuery('#scroll').click(function(){ 
				jQuery("html, body").animate({ scrollTop: 0 }, 600); 
				return false; 
		}); 
		jQuery(window).scroll(function () {
			if (jQuery(this).scrollTop() > 1) { 
				jQuery('#sc-logo').attr('src','assets/images/logo-sirf-coffee.svg');
			}
			else { 
				jQuery('#sc-logo').attr('src','assets/images/sirfcoffee-orange.svg');
			}
		})
		jQuery('#homestory').on('hidden.bs.modal', function () {
			/* $('#homestory').get(0).stopVideo(); */
			//$("#homestory").attr("src", $("#homestory iframe").attr("src"));	
			callPlayer('yt-player', 'stopVideo');
			$("#homestory").attr("src", $("#homestory iframe").attr("src"));		
			//alert('test');
		})
		jQuery('.navbar-toggler').click(function(){
			$(this).toggleClass('open');
		});

		
		jQuery('#testimonial-home').owlCarousel({		 
			loop:true,		
			nav:true,  	
			navContainer:'#testimonialhome-nav',			
			//navText: ["<img src='assets/images/arrow-left.png'>","<img src='assets/images/arrow-right.png'>"],	
			navText: ["",""],			
			animateIn: 'fadeIn',
			animateOut: 'fadeOut',
			responsive:{
				0:{
					items:1,
					margin:5,
					stagePadding: 0						
				},				
				768:{
					items:1,
					margin:5,
					stagePadding: 0						
				}
			}				
		});				
		
		

		
		
		if($(window).width() > 767){
			jQuery('#whowearetimeline').owlCarousel({
				loop:false,		
				nav:true,  						
				navText: ["<img src='assets/images/left-orange-arrow.svg'>","<img src='assets/images/right-orange-arrow.svg'>"],
				animateIn: 'fadeIn',
				animateOut: 'fadeOut',
				responsive:{
					0:{
						items:1,
						margin:0,
						stagePadding: 0						
					},				
					768:{
						items:4,
						margin:0,
						stagePadding: 0						
					}
				}				
			});
		}else{
			jQuery('#whowearetimeline').owlCarousel({
				loop:false,		
				nav:true,  	
				//autoplay:true,
				autoplayTimeout:5000,					
				navText: ["",""],
				animateIn: 'fadeIn',
				animateOut: 'fadeOut',
				responsive:{
					0:{
						items:1,
						margin:0,
						stagePadding: 30						
					},				
					768:{
						items:4,
						margin:0,
						stagePadding: 0						
					}
				}				
			});
		}
		
		/* jQuery('#moveleft').click(function(){			
			jQuery('.timeline-container').find(
				jQuery('.timeline-single').each(function(){
					jQuery(this).css({'transform':'translateX(-25%)'})
				})
			)
		}) */


		jQuery('#member-stories').owlCarousel({		 
			loop:true,		
				/*nav:true,  	
		 navText: ["<img src='assets/images/arrow-left.png'>","<img src='assets/images/arrow-right.png'>"],	 */	
			dots:true,	
			animateIn: 'fadeIn',
			animateOut: 'fadeOut',
			responsive:{
				0:{
					items:1,
					margin:20,
					stagePadding:60						
				},				
				768:{
					items:3,
					margin:20,
					stagePadding:30						
				},
				992:{
					items:3,
					margin:5,
					stagePadding: 0						
				},
				1199:{
					items:4,
					margin:5,
					stagePadding: 0						
				}
			}
		});
		/* if($(window).width()>767){
			jQuery('#ourblog-home').owlCarousel({		 
				loop:true,		
				nav:true,  	
				 navText: ["<img src='assets/images/blognew-left.png'>","<img src='assets/images/blognew-right.png'>"],			
				animateIn: 'fadeIn',
				animateOut: 'fadeOut',
				responsive:{
					0:{
						items:1,
						margin:15,
						stagePadding:30						
					},				
					768:{
						items:3,
						margin:5,
						stagePadding: 0						
					}				
				}
			});	
		} */
		//if(jQuery(window).width()<768){
				//var presence = jQuery('.service-single').outerHeight();
				//alert(presence)
			//#testimonial-home.owl-carousel .owl-nav button.owl-prev
			//jQuery('#testimonial-home.owl-carousel .owl-nav button.owl-prev').css({'top':presence});
		//}


		AOS.init({
			once: true,
			delay: 100,
			duration: 500
		});

	/* 	if(jQuery(window).width()<768){
			//alert('test');
			var test_arrow = jQuery('.wpt-container').outerHeight();
			console.log(test_arrow)	
			jQuery('#testimonialhome-nav button.owl-prev').css({'top': test_arrow+10})
			jQuery('#testimonialhome-nav button.owl-next').css({'top': test_arrow+10})
		} */
});


// Sticky

jQuery(window).scroll(function() {
		if(jQuery(this).scrollTop()>5) {
				jQuery( ".sticky" ).addClass("fixed-me");
		} else {
				jQuery( ".sticky" ).removeClass("fixed-me");
		}
});



