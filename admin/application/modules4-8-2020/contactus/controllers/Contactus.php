<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Contactus extends MX_Controller
{
	
	public function __construct(){
		parent::__construct();
		checklogin();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('contactusmodel','',TRUE);
	}

	public function index(){
		$result = array();
		$condition = "1=1";
		$result['contactus_data'] = $this->common->getData("tbl_contact_us",'*',$condition);

		// echo "<pre>";	
		// print_r($result);
		// exit;

		$this->load->view('main-header.php');
		$this->load->view('addEdit.php',$result);
		$this->load->view('footer.php');
	}

	public function addEdit(){
		$contact_id = "";
		$result = array();
		if(!empty($_GET['text']) && isset($_GET['text'])){
			$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
			parse_str($varr,$url_prams);
			$contact_id = $url_prams['id'];
			$condition = " contact_id ='".$contact_id."' ";
			$result['contactus_data'] = $this->common->getData("tbl_contact_us",'*',$condition);
		}
		
		$this->load->view('main-header.php');
		$this->load->view('addEdit.php',$result);
		$this->load->view('footer.php');
	}

	function submitformtesting(){
		// codeigniter form validation 
		// $this->form_validation->set_rules('themename', 'Themename', 'trim|required|alpha');
		// $this->form_validation->set_rules('mainvideo', 'Main video', 'trim|required');
		// $this->form_validation->set_rules('meta_title', 'Meta title', 'trim|required');
		// $this->form_validation->set_rules('meta_description', 'Meta description', 'trim|required');
		// $this->form_validation->set_rules('mainvideo', 'Main video', 'trim|required');

		// if($this->form_validation->run()) {  
		// 	// true  if validation property sectifice properly
		// 	$themename = $this->input->post('themename');  
		// 	$mainvideo = $this->input->post('mainvideo'); 
		// 	$themename = $this->input->post('meta_title');  
		// 	$mainvideo = $this->input->post('meta_description'); 
		// 	$themename = $this->input->post('mainvideo');  
		// 	$mainvideo = $this->input->post('meta_title'); 
		// }else{  
		// 	// if validation is not proper
		// 	$this->index();  
		// } 
	}


	function submitForm(){
		// echo "<pre>";	
		// print_r($_POST);
		// exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();
			$data['video_url'] = $this->input->post('video_url');
			$data['heading'] = $this->input->post('heading');
			$data['banner_heading'] = $this->input->post('banner_heading');
			$data['updated_on'] = date("Y-m-d H:i:s");
			$data['updated_by'] = $this->session->userdata('footage_farm_admin')[0]['user_id'];
			if(!empty($this->input->post('contact_id'))){
				// print_r($data);
				$condition = "contact_id = '".$this->input->post('contact_id')."' ";
				$result = $this->common->updateData("tbl_contact_us",$data,$condition);
				if($result){
					echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
					exit;
				}
				else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while updating data.'));
					exit;
				}
			}else{
				$data['created_on'] = date("Y-m-d H:i:s");
				$data['created_by'] =  $this->session->userdata('footage_farm_admin')[0]['user_id'];
				$result = $this->common->insertData('tbl_contact_us',$data,'1');
				if(!empty($result)){
					echo json_encode(array('success'=>true, 'msg'=>'Record Added Successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
					exit;
				}
			}
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem While Add/Edit Data.'));
			exit;
		}
	}
}