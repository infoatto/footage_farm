
<div class="wrapper">
	<div class="main-panel">
		<div class="container">
			<div class="page-inner">
				<div class="col-12">
					<div class="row ff-title-block">
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">
							<h1 class="ff-page-title">New FAQ</h1> 
						</div>
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
							<a href="<?= base_url('faq')?>" class="btn btn-light btn-border ff-page-title-btn">
								Cancel
							</a>
						</div>
					</div>
				</div>					
				<div class="row">
					<div class="col-sm-12">
						<div class="card">
							<div class="card-body">
								<form class="" id="form-faq" method="post" enctype="multipart/form-data">
								
									<input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden">
									<input name="faq_id" id="faq_id" value="<?= (!empty($faq_data[0]['faq_id'])?$faq_data[0]['faq_id']:"") ?>" type="hidden">
								
											<div class="form-group">
												<label for="Question">Question</label>
												<input type="text" class="form-control" id="Question" name="Question" value="<?= (!empty($faq_data[0]['question'])?$faq_data[0]['question']:"") ?>" placeholder="eg. Bryan Schreier">	
											</div>
										
											<div class="form-group">
												<label for="Answer">Answer</label>
												<textarea class="form-control" id="Answer" name="Answer" rows="5" placeholder="eg. We offer a 24-48 hour turnaround service for masters we hold in our London office. If we need to duplicate the material from Washington, the delays are similar to those you would incur if you went straight to the source." spellcheck="false"><?= (!empty($faq_data[0]['answer'])?$faq_data[0]['answer']:"") ?></textarea>	
											</div>
										
											<div class="form-check">
												<label>Activate FAQ</label><br>
												<div class="switch-field">
														<?php $active = 'checked';
															 $in_active= '';
														if(!empty($faq_data)){
															if($faq_data[0]['status']=='Active'){
																$active = "checked";
															}else{
																$in_active = "checked";
															} 
														}?>
													<input type="radio" id="activate-tag-three" name="tag-status" value="Active" <?= $active?>/>
													<label for="activate-tag-three">Yes</label>
													<input type="radio" id="activate-tag-four" name="tag-status" value="In-active"  <?= $in_active?>/>
													<label for="activate-tag-four">No</label>
												</div>
											</div>	
											<div class="form-group">
												<button  type="submit" class="btn btn-black">Create</button>
												<a href="<?= base_url('faq')?>" class="btn btn-cancel btn-border">Cancel</a>
											</div>
									</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>						
</div>

<script>
$(document).ready(function(){
});


// use for form submit
var vRules = 
{
	Question:{required:true},
	Answer:{required:true}
};
var vMessages = 
{
	Question:{required:"Please Enter Question."},
	Answer:{required:"Please Enter Answer."},
};

$("#form-faq").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{	
		var act = "<?= base_url('faq/submitform')?>";
		$("#form-faq").ajaxSubmit({
			url: act, 
			type: 'post',
			dataType: 'json',
			cache: false,
			clearForm: false,
			async:false,
			beforeSubmit : function(arr, $form, options){
				// $(".btn-primary").hide();
			},
			success: function(response) 
			{
				if(response.success){
					swal("done", response.msg, {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					}).then(
					function() {
						window.location = "<?= base_url('faq')?>";
						// $this->router->fetch_class().'/'.$this->router->fetch_method() ?>";
					});
				}else{	
					swal("Failed", response.msg, {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}
			}
		});
	}
});


</script>