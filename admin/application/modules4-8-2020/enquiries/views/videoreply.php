
<body>
	<div class="wrapper">
		<div class="main-panel">
			<div class="container">
				<div class="page-inner">
					<div class="col-12">
						<div class="row ff-title-block">
							<div class="col-2">
								<a href="#/"></a><img src="<?= base_url()?>assets/images/footage-thumb-enquiry.jpg" alt="" class="img-fluid"></a>
							</div>
							<div class="col-8 col-sm-8">
									<h1 class="ff-page-title">1910-1918- WWI  Era: Aerials, German Officer & Troops; France Fighting; Trans-Caucasia Winter</h1> 
							</div>
							<div class="col-2 col-sm-2">                                
									<a href="all-enquiries.php" class="btn btn-light btn-border ff-page-title-btn">
											Back
									</a>
							</div>
						</div>
						<div class="row ff-userenquiry-status">
							<div class="col-2">&nbsp;</div>
							<div class="col-sm-2"><strong>Request Type</strong>:<br/> Master</div>
							<div class="col-sm-2"><strong>Order Date</strong>:<br/> 12 Aug 2020</div>
							<div class="col-sm-2"><strong>Status</strong>:<br/> 
								<select name="statusselect" id="statusselect">
									<option value="">Ongoing</option>
									<option value="">New</option>
									<option value="">Purchased</option>
								</select> 
							</div>
							<div class="col-sm-2"><strong>Client</strong>:<br/> Pauline Caldwell</div>
						</div>
					</div>					
					<div class="row">
						<div class="col-sm-12">
							<div class="card">
									<div class="card-body">
											<div class="row">
												<div class="col-12 col-sm-6 user-enquiry-info">
													<p class="text-muted">Reel Number</p>			
													<p>583458854285</p>	
													<p class="text-muted">Title</p>			
													<p>1910-1918- WWI  Era: Aerials, German Officer & Troops; France Fighting; Trans-Caucasia Winter</p>	
													<p class="text-muted">Reel Number</p>			
													<p>583458854285</p>
													<div class="row">
														<div class="col-sm-6">
															<p class="text-muted">Begin TC</p>			
															<p>12:34:54</p>
														</div>	
														<div class="col-sm-6">
															<p class="text-muted">End TC</p>			
															<p>12:54:24</p>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-6">
															<p class="text-muted">Format</p>			
															<p>mov</p>
														</div>	
														<div class="col-sm-6">
															<p class="text-muted">Frame Rate</p>			
															<p>60fps</p>
														</div>
													</div>
													<p class="text-muted">Message</p>			
													<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec diam massa, pellentesque et sapien et, pretium rutrum turpis. Integer mollis mollis lectus, sed accumsan nulla suscipit vitae. In et nunc eu augue lobortis tristique non sed ante.</p>									
												</div>
												<div class="col-12 col-sm-6 user-enquiry-status">													
														<h3>Status Update</h3>
														<table class="table table-striped mt-3">
															<thead>
																<tr>																	
																	<th scope="col">Status</th>
																	<th scope="col">Updated On</th>
																	<th scope="col">Updated By</th>
																</tr>
															</thead>
															<tbody>
																<tr>																
																	<td>New</td>
																	<td>12 Aug, 20</td>
																	<td>Gonzalo</td>
																</tr>
																<tr>																
																	<td>Ongoing</td>
																	<td>12 Aug, 20</td>
																	<td>Gonzalo</td>
																</tr>
																<tr>																
																	<td>Purchased</td>
																	<td>12 Aug, 20</td>
																	<td>Gonzalo</td>
																</tr>
															</tbody>
														</table>
												</div>
											</div>
									</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>						
	</div>
</body>
</html>
