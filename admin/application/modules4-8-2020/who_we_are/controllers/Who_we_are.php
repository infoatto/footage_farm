<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Who_we_are extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('who_we_aremodel','',TRUE);
		$this->load->model('common_model/common_model','common',TRUE);
		
	}
 
	function index(){
		// if(empty($result['page_link'])){
		// 	$condition = " page_link= '".$page_link."'  ";
		// 	// put 404 error page if there is not page found on front ends
		// }else{
		// 	$condition = " page_link= '".$result['page_link']."'  ";
		// // }
		$result['page_link'] = $this->uri->segment(1);
		// echo  $this->uri->segment(1);

		
		$condition = " page_link like '".$result['page_link']."'  ";
			// echo $condition;
		$getPageDetails = $this->common->getData("tbl_page_template","*",$condition); 
		$page_id = $getPageDetails[0]['page_id'];

		$result = array();
		$condition = " tpt.page_id = '".$page_id."' AND tpts.status = 'Active' ";
		$main_table = array("tbl_page_template as tpt", array('tpt.page_id,tpt.page_link'));
		$join_tables = array(			
					array("left", "tbl_page_template_section as tpts", "tpts.page_id = tpt.page_id", array('tpts.*'))
				);
		$total_rs = $this->common-> JoinFetch($main_table, $join_tables, $condition, array("tpts.section_order" => "ASC"));
		$section_details = $this->common->MySqlFetchRow($total_rs, "array");
		$all_gallaries = $this->common->getData("tbl_gallery","*");

		// fetch all datas of banner
		$bannerData = $this->common->getData("tbl_banner_page_html","*","page_id ='".$page_id."' ");
		$contentData = $this->common->getData("tbl_contents_page_html","*","page_id ='".$page_id."' ");
		$gridData = $this->common->getData("tbl_grid_page_html","*","page_id ='".$page_id."' ");
		if(!empty($gridData)){
			$gridData = $this->arrayFormation($gridData,"template_section_id");
		}
		$taglineData = $this->common->getData("tbl_tagline_page_html","*","page_id ='".$page_id."' ");
		$galleryData = $this->common->getData("tbl_gallary_page_section_mapping_html","*","page_id ='".$page_id."' ");
		$twoImageData = $this->common->getData("tbl_two_images_html","*","page_id ='".$page_id."' ");

		// section array forming
		$section = array();
		$bannerCount = 0;
		$gridCount = 0;
		$contentCount = 0;
		$twoImageCount = 0;
		$taglineCount = 0;
		$galleryCount = 0;
		if(!empty($section_details)){
			foreach ($section_details as $key => $value) {
				if($value['section_type']== 'banner'){
					if(!empty($bannerData[$bannerCount])){
						$section[$key] = $bannerData[$bannerCount];
					}
					$bannerCount++;
				}else if($value['section_type']== 'two-imagewithicon'){ 
					if(!empty($twoImageData[$twoImageCount])){
						$section[$key] = $twoImageData[$twoImageCount];
					}
					$twoImageCount++;
				}else if($value['section_type']== 'content'){ 
					if(!empty($contentData[$contentCount])){
						$section[$key] = $contentData[$contentCount];
					}
					$contentCount++;
				}else if($value['section_type'] == 'tagline'){ 
					if(!empty($taglineData[$taglineCount])){
						$section[$key] = $taglineData[$taglineCount];
					}
					$taglineCount++;
				}else if ($value['section_type'] == 'gallary') {
					if(!empty($galleryData[$galleryCount])){
						$galleryCondition = " gi.gallery_id = '".$galleryData[$galleryCount]['gallary_id']."' ";
						$main_table = array("tbl_gallery_images as gi", array('gi.gallery_image_id,gi.gallery_image,gi.image_title'));
						$join_tables = array(			
							array("left", "tbl_gallery as g", "g.gallery_id = gi.gallery_id", array('g.*'))
						);
						$total_rs = $this->common-> JoinFetch($main_table, $join_tables, $galleryCondition, array("gi.image_order" => "ASC"));
						$section[$key] = $this->common->MySqlFetchRow($total_rs, "array");
					}
					$galleryCount++;
				}else if ($value['section_type'] == 'grid') {
					if(!empty($gridData[$value['page_template_saction_id']])){
						$section[$key] = $gridData[$value['page_template_saction_id']];
					}
				} 
			}
		}
		$result['sections'] = $section;
		// echo "<pre>";print_r($result);exit;
		$this->load->view('template/header.php');
		$this->load->view('index',$result);
		$this->load->view('template/footer.php');
	}

	
	function arrayFormation($array,$label){
		$rerurn_array = array();
		$temp_variable = "";
		foreach($array as $key=>$val){
			if($temp_variable == $val[$label]){
				$return_array[$val[$label]][] = $val;
			}else{
				$temp_variable = $val[$label];
				$return_array[$val[$label]][] = $val;
			}
		}
		return $return_array;
	}
}
