<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Dashboard extends MX_Controller
{
	
	public function __construct(){
		// this is helper function  use to check whether the user is logged in or not file is written in helper folder inside application 
		checklogin();
		parent::__construct();
	}

	public function index(){
		$this->load->view('main-header.php');
		$this->load->view('index.php');
		$this->load->view('footer.php');
	}
	
	public function logOut(){
		$this->session->unset_userdata('footage_farm_admin');
		redirect(base_url()); 

	}
}