<?PHP
class Common_model extends CI_Model {
	
	function getData($table,$select = "*", $condition = '', $order_by = "",$order = ""){
		$this->db->select($select);
		$this->db->from($table);
		if(!empty($condition)){ 
			$this->db->where($condition);
		}	
		if(!empty($order_by) && !empty($order)){ 
			$this->db->order_by($order_by, $order);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return false;
		}
	}
	//  limit wise data
	function getDataLimit($table,$select = "*", $condition = '', $order_by = "",$order = "", $limit){
		$this->db->select($select);
		$this->db->from($table);
		if(!empty($condition)){ 
			$this->db->where($condition);
		}	
		if(!empty($order_by) && !empty($order)){ 
			$this->db->order_by($order_by, $order);
		}
		if(!empty($limit) && !empty($limit)){ 
			$this->db->limit($limit);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return false;
		}
	}

	/*
		 * Fetch All records from single table with multiple condition, Sorting, Limit, Group By
	*/
	function Fetch($table, $columns, $condition = null, $sort_by = null, $group_by = null, $limit = null) {
		if (is_array($columns)) {
			$columns = implode(", ", $columns);
		}

		if (is_null($condition) || $condition == "") {
			$condition = "1=1";
		}

		$sort_order = "";
		if (is_array($sort_by) && $sort_by != null) {
			foreach ($sort_by as $key => $val) {
				$sort_order .= ($sort_order == "") ? "order by $key $val" : ", $key $val";
			}
		}

		if ($group_by != null) {
			$group_by = "group by " . $group_by;
		}

		$limit_by = "";
		if(is_array($limit) && $limit["offset"] != null && $limit["rows"] != null) {
			$limit_by = "limit ".$limit["offset"].", ".$limit["rows"];
		}
	
		$sql = trim("select $columns from $table where $condition $group_by $sort_order $limit_by");
		$query = $this->db->query($sql);
		return $query;
	}

	/*Fetch records from multiple tables [Join Queries] with multiple condition, Sorting, Limit, Group By*/
	function JoinFetch($main_table = array(), $join_tables = array(), $condition = null, $sort_by = null, $group_by = null) {
		$columns = isset($main_table[1]) ? $main_table[1] : array();
		$main_table = $main_table[0];
		$join_str = "";
		foreach ($join_tables as $join_table) {
			$join_str .= $join_table[0] . " join " . $join_table[1] . " on (" . $join_table[2] . ") ";
			if (isset($join_table[3])) {
				$columns = array_merge($columns, $join_table[3]);
			}
		}
		$columns = (sizeof($columns) > 0) ? implode(", ", $columns) : "*";
		if (is_null($condition) || $condition == "") {
			$condition = "1=1";
		}
		$sort_order = "";
		if (is_array($sort_by) && $sort_by != null) {
			foreach ($sort_by as $key => $val) {
				$sort_order .= ($sort_order == "") ? "order by $key $val" : ", $key $val";
			}
		}
		if ($group_by != null) {
			$group_by = "group by " . $group_by;
		}
		$sql = trim("select $columns from $main_table $join_str where $condition $group_by $sort_order");
		$query = $this->db->query($sql);
		return $query;
	}

	/*Fetch as per the request like [Array,Object,Asscociative Array]*/
	function MySqlFetchRow($result, $type = 'assoc') {
		$row = false;
		if ($result != false) {
			switch ($type) {
			case 'array':
				$row = @$result->result_array();
				break;
			case 'object':
				$row = @$result->result();
				break;
			default:
			case 'assoc':
				$row = @$result->row_array();
				break;
			}
		}
		return $row;
	}
	
	function insertData($tbl_name, $data_array) {
		$result_id = "";
		$this->db->insert($tbl_name, $data_array);
		$result_id = $this->db->insert_id();
		if($result_id > 0) {
			return $result_id;
		}else{
			return false;
		}
	}

	function updateData($tbl_name, $data_array, $condition) {
		$this->db->where($condition);
		if ($this->db->update($tbl_name, $data_array)) {
			return true;
		} else {
			return false;
		}
	}
	
	function deleteRecord($tbl_name,$condition){
		$this->db->where($condition);
		if($this->db->delete($tbl_name)){
			return true;
		}else{
			return false;
		}
	}
		
	function getEmailContent($mail_key){	
		$this->db->select('*');
		$this->db->from('tbl_emailcontents');
		$this->db->where("mail_Key", $mail_key);
		$query = $this->db->get();
		if($query -> num_rows() > 0){
			$res = $query->result_array();
			return $res[0];
		}else{
			return false;
		}
	}

	

}

?>