<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Blog extends MX_Controller
{
	
	public function __construct(){
		// this is helper function  use to check whether the user is logged in or not file is written in helper folder inside application 
		checklogin();
		
		$this->load->model('common_model/common_model','common',TRUE);
		parent::__construct();
	}

	public function index(){
		$condition = "1=1";
		$result['blog_data'] = $this->common->getData("tbl_blogs",'*',$condition);
		$this->load->view('main-header.php');
		$this->load->view('index.php',$result);
		$this->load->view('footer.php');
	}

	public function addEdit(){
		$blog_id = "";
		$result = array();
		if(!empty($_GET['text']) && isset($_GET['text'])){
			$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
			parse_str($varr,$url_prams);
			$blog_id = $url_prams['id'];
			$condition = " blog_id ='".$blog_id."' ";
			$result['blog_data'] = $this->common->getData("tbl_blogs",'*',$condition);
		}
		
		// echo "<pre>";
		// print_r($result);
		// exit;
		//echo $user_id;
		$this->load->view('main-header.php');
		$this->load->view('addEdit.php',$result);
		$this->load->view('footer.php');
	}

	
	public function logOut(){
		$this->session->unset_userdata('footage_farm_admin');
		redirect(base_url()); 

	}
	public function submitForm(){
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();
			// for main image 
			$main_value = "";
			if(isset($_FILES) && isset($_FILES["main_image"]["name"])){
				$this->load->library('upload');
				$this->upload->initialize($this->set_upload_options());
				if (!$this->upload->do_upload("main_image")){
					$image_error = array('error' => $this->upload->display_errors());
					echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
					exit;
				}else{
					$image_data = array('upload_data' => $this->upload->data());
					$main_value = $image_data['upload_data']['file_name']; 
				}	
				/* Unlink previous category image */
				if(!empty($this->input->post('blog_id'))){	
					$condition_image = " blog_id = ".$this->input->post('blog_id');
					$image =$this->common->getData("tbl_blogs",'*',$condition_image);
					if(is_array($image) && !empty($image[0]->main_image) && file_exists(DOC_ROOT_FRONT."/images/blog_main_image/".$image[0]->main_image))
					{
						unlink(DOC_ROOT_FRONT."/images/blog_main_image/".$image[0]->main_image);
					}
				}
			}else{
				$main_value = $this->input->post('pre_blog_main_image');
			}

			// for thumnail image 
			$thumnail_value = "";
			if(isset($_FILES) && isset($_FILES["thumbnail_image"]["name"])){
				$this->load->library('upload');
				$this->upload->initialize($this->set_upload_options());
				if (!$this->upload->do_upload("thumbnail_image")){
					$image_error = array('error' => $this->upload->display_errors());
					echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
					exit;
				}else{
					$image_data = array('upload_data1' => $this->upload->data());
					$thumnail_value = $image_data['upload_data1']['file_name']; 
				}	
				/* Unlink previous category image */
				if(!empty($this->input->post('blog_id'))){	
					$condition_image = " blog_id = ".$this->input->post('blog_id');
					$image =$this->common->getData("tbl_blogs",'*',$condition_image);
					if(is_array($image) && !empty($image[0]->thumbnail_image) && file_exists(DOC_ROOT_FRONT."/images/blog_main_image/".$image[0]->thumbnail_image))
					{
						unlink(DOC_ROOT_FRONT."/images/blog_main_image/".$image[0]->thumbnail_image);
					}
				}
			}else{
				$thumnail_value = $this->input->post('pre_blog_main_image');
			}
			

			$data['main_image'] = $main_value;
			$data['thumbnail_image'] = $thumnail_value;
			$data['blog_title'] = $this->input->post('blog_title');
			$data['blog_slug'] = $this->input->post('blog_slug');
			$data['blog_post_body'] = $this->input->post('blog_post_body');
			$data['tagname'] = $this->input->post('tagname');
			$data['featured_theme'] = $this->input->post('featured-theme');
			$data['status'] = $this->input->post('theme-status');
			$data['meta_title'] = $this->input->post('meta_title');
			$data['meta_description'] = $this->input->post('meta_description');
			$data['updated_on'] = date("Y-m-d H:i:s");
			$data['updated_by'] = $this->session->userdata('footage_farm_admin')[0]['user_id'];
			if(!empty($this->input->post('blog_id'))){
				$condition = "blog_id = '".$this->input->post('blog_id')."' ";
				$result = $this->common->updateData("tbl_blogs",$data,$condition);
				if($result){
					echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
					exit;
				}
				else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while updating data.'));
					exit;
				}
			}else{
				$data['created_on'] = date("Y-m-d H:i:s");
				$data['created_by'] =  $this->session->userdata('footage_farm_admin')[0]['user_id'];
				$result = $this->common->insertData('tbl_blogs',$data,'1');
				if(!empty($result)){
					echo json_encode(array('success'=>true, 'msg'=>'Record Added Successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
					exit;
				}
			}
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem While Add/Edit Data.'));
			exit;
		}
	}

	
	function set_upload_options($file_name = ""){   
		$config = array();
		if(!empty($file_name) &&  $file_name != ""){
			$config['file_name']   = $file_name.time();
		}
		$config['upload_path'] = DOC_ROOT_FRONT."/images/blog_main_image/";
		if(!is_dir($config['upload_path'])){
			mkdir($config['upload_path'],0777, true);		
		}
		$config['allowed_types'] = 'gif|jpg|png|jpeg|svg';
		$config['max_size']      = '0';
		$config['overwrite']     = FALSE;
		return $config;
	} 

}