
<div class="wrapper">
	<div class="main-panel">
		<div class="container">
			<div class="page-inner">
				<div class="col-12">
					<div class="row ff-title-block">
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">
							<h1 class="ff-page-title">New Blog Post</h1> 
						</div>
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
							<a href="<?= base_url('blog')?>" class="btn btn-light btn-border ff-page-title-btn">
								Cancel
							</a>
						</div>
					</div>
				</div>					
				<div class="row">
					<div class="col-sm-12">
						<div class="card">
							<div class="card-body">
								<form class="" id="form-blog" method="post" enctype="multipart/form-data">
									<!-- <?php  print_r($blog_data) ;?> -->
									<input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden">
									<input name="blog_id" id="blog_id" value="<?= (!empty($blog_data[0]['blog_id'])?$blog_data[0]['blog_id']:"") ?>" type="hidden">
                                            
                                            <div class="form-group">
												<label for="blog_title">Title</label>
												<input type="text" class="form-control" name="blog_title" id="blog_title" value="<?= (!empty($blog_data[0]['blog_title'])?$blog_data[0]['blog_title']:"") ?>" placeholder="eg 1910-1918- WWI  Era: Aerials, German Officer &amp; Troops.">	
											</div>

                                            <div class="form-group">
												<label for="blog_slug">Slug</label>
												<input type="text" class="form-control" name="blog_slug"  id="blog_slug" value="<?= (!empty($blog_data[0]['blog_slug'])?$blog_data[0]['blog_slug']:"") ?>" id="blog_slug" name="blog_slug" placeholder="eg. 110-quick-tips-about-blogging">	
											</div>

                                            <div class="form-group">
												<label for="blog_post_body">Post Body</label>
												<textarea class="form-control" id="blog_post_body" rows="20" name="blog_post_body" placeholder="eg. Asurprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor." spellcheck="false"><?= (!empty($blog_data[0]['blog_post_body'])?$blog_data[0]['blog_post_body']:"") ?></textarea>	
											</div>

                                            <div class="form-group">
                                                <label for="main_image">Main Image</label>
                                                <?php $req= '';
                                                if(empty($blog_data[0]['main_image'])){
                                                    $req ='required';
                                                }else{?>
                                                <br>
                                                    <img src="<?= FRONT_URL.'/images/blog_main_image/'.$blog_data[0]['main_image']?>"class="img-thumbnail" alt="Cinque Terre" width="250" height="300">
                                                    <input name="pre_blog_main_image" id="pre_blog_main_image"  value="<?= (!empty($blog_data[0]['main_image'])?$blog_data[0]['main_image']:"") ?>" type="hidden">
                                                <?php }?>
                                                
                                                <input type="file" class="form-control-file" id="main_image" name="main_image" <?= $req;?> >
											</div>

                                            <div class="form-group">
                                                <label for="thumbnail_image">Thumbnail Image</label>
                                                <?php $req= '';
                                                if(empty($blog_data[0]['thumbnail_image'])){
                                                    $req ='required';
                                                }else{?>
                                                <br>
                                                    <img src="<?= FRONT_URL.'/images/blog_main_image/'.$blog_data[0]['thumbnail_image']?>"class="img-thumbnail" alt="Cinque Terre" width="250" height="300">
                                                    <input name="pre_blog_main_image" id="pre_blog_main_image" value="<?= (!empty($blog_data[0]['thumbnail_image'])?$blog_data[0]['thumbnail_image']:"") ?>" type="hidden">
                                                <?php }?>
                                                
                                                <input type="file" class="form-control-file" id="thumbnail_image" name="thumbnail_image" <?= $req;?> >
											</div>

											<div class="form-group">
												<label for="Tags">Tags</label>
												<input type="text" class="form-control" value="<?= (!empty($blog_data[0]['tagname'])?$blog_data[0]['tagname']:"") ?>" id="tagname" name="tagname" placeholder="Early 19th Century">	
											</div>


											<div class="form-group">
												<label for="meta_title">Meta Title</label>
												<input type="text" class="form-control" id="meta_title" value="<?= (!empty($blog_data[0]['meta_title'])?$blog_data[0]['meta_title']:"") ?>"  name="meta_title" placeholder="Early 19th Century">	
											</div>

											<div class="form-group">
												<label for="meta_description">Meta Description</label>
												<textarea class="form-control" id="meta_description" name="meta_description" rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."><?= (!empty($blog_data[0]['meta_description'])?$blog_data[0]['meta_description']:"") ?></textarea>	
											</div>

											<div class="form-check">
												<label>Featured</label><br>
												<div class="switch-field">
														<?php $feature_yes =  'checked';
															  $feature_no='';
														if(!empty($blog_data)){
															if($blog_data[0]['featured_theme']=='Yes'){
																$feature_yes = "checked";
															}else{
																$feature_no = "checked";
															} 
														}?>
														
													<input type="radio" id="featured-one" name="featured-theme" value="Yes" <?= $feature_yes?>/>
													<label for="featured-one">Yes</label>
													<input type="radio" id="featured-two" name="featured-theme" value="No"  <?= $feature_no?>/>
													<label for="featured-two">No</label>
												</div>
											</div>

											<div class="form-check">
												<label>Active</label><br>
												<div class="switch-field">
														<?php $active = 'checked';
															 $in_active= '';
														if(!empty($blog_data)){
															if($blog_data[0]['status']=='Active'){
																$active = "checked";
															}else{
																$in_active = "checked";
															} 
														}?>
													<input type="radio" id="activate-theme-three" name="theme-status" value="Active" <?= $active?>/>
													<label for="activate-theme-three">Yes</label>
													<input type="radio" id="activate-theme-four" name="theme-status" value="In-active"  <?= $in_active?>/>
													<label for="activate-theme-four">No</label>
												</div>
											</div>	

											<div class="form-group">
												<button  type="submit" class="btn btn-black">Create</button>
												<a href="<?= base_url('theme')?>" class="btn btn-cancel btn-border">Cancel</a>
											</div>
									</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>						
</div>

<script>
$(document).ready(function(){
});


// use for form submit
var vRules = 
{
	blog_title:{required:true},
	blog_slug:{required:true},
	blog_post_body:{required:true},
	tagname:{required:true},
	meta_title:{required:true},
	meta_description:{required:true},
};
var vMessages = 
{
	blog_title:{required:"Please Enter blog Title."},
	blog_slug:{required:"Please Enter blog slug."},
	blog_post_body:{required:"Please Enter blog post ."},
    tagname:{required:"Please Enter tagname."},
	meta_title:{required:"Please Enter Meta Title."},
	meta_description:{required:"Please Enter Meta Description."},

};

$("#form-blog").validate({
	// rules: vRules,
	// messages: vMessages,
	submitHandler: function(form) 
	{	
		var act = "<?= base_url('blog/submitform')?>";
		$("#form-blog").ajaxSubmit({
			url: act, 
			type: 'post',
			dataType: 'json',
			cache: false,
			clearForm: false,
			async:false,
			beforeSubmit : function(arr, $form, options){
				// $(".btn-primary").hide();
			},
			success: function(response) 
			{
				if(response.success){
					swal("done", response.msg, {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					}).then(
					function() {
						window.location = "<?= base_url('blog')?>";
						// $this->router->fetch_class().'/'.$this->router->fetch_method() ?>";
					});
				}else{	
					swal("Failed", response.msg, {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}
			}
		});
	}
});


</script>