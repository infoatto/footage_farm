
<body>
	<div class="wrapper">
		<div class="main-panel">
			<div class="container">
				<div class="page-inner">
					<div class="col-12">
						<div class="row ff-title-block">
							<div class="col-12 col-sm-12 col-md-6 col-lg-6">
									<h1 class="ff-page-title">Blogs</h1> 
							</div>
							<div class="col-12 col-sm-12 col-md-6 col-lg-6">  
									<a href="<?= base_url('blog/addEdit')?>" class="btn btn-primary ml-2 ff-page-title-btn">
											<i class="fas fa-plus"></i> 
											New Blog					
									</a>	
							</div>
						</div>
					</div>					
					<div class="row">
							<div class="col-sm-12">
									<div class="card">
											<div class="card-body">
												<div class="table-responsive">
													<table id="add-row" class="display table table-striped table-hover" >
														<thead>
															<tr>																
																<th style="width:60px"></th>																
																<th>Title</th>																
																<th>Created at</th>	
																<th>Published on</th>	
																<th>Status</th>		
																<th>Actions</th>												
															</tr>
														</thead>
														<tfoot>
															<tr>
																<th></th>																
																<th>Title</th>																
																<th>Created at</th>	
																<th>Published on</th>	
																<th>Status</th>		
																<th>Actions</th>																	
															</tr>
														</tfoot>
														<tbody>
														<?php 
															if(!empty($blog_data)){
																foreach ($blog_data as $key => $value) {?>
																	<tr>																
																		<td><img src="<?= FRONT_URL."/images/blog_main_image/".$value['thumbnail_image']?>" alt="" class="blog-list-thumb"></td>
																		<td><?= $value['blog_title']?></td>
																		<td><?= date('d M, Y', strtotime($value['created_on']))?></td>
																		<td><?= date('d M, Y', strtotime($value['updated_on']))?></td>
																		<td><?= $value['status']?></td>	
																		<td>
																			<div class="form-button-action">
																				<!-- <a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Edit">
																					<i class="fa fa-edit"></i>
																				</a> -->
																				<a href="<?= base_url('blog/addEdit?text='.rtrim(strtr(base64_encode("id=".$value['blog_id']), '+/', '-_'), '=').'')?>" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Edit">
																					<i class="fa fa-edit"></i>
																				</a>
																				<!-- <a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																					<i class="fa fa-times"></i>
																				</a> -->
																			</div>
																		</td>																											
																	</tr>
																<?php }
															}?>
																												
														</tbody>
													</table>
												</div>
											</div>
									</div>
							</div>
					</div>
				</div>
			</div>
			
		</div>						
	</div>
	<script >
		$(document).ready(function() {
			// Add Row
			$('#add-row').DataTable({
				"pageLength": 5,
			});			
		});
	</script>
</body>
</html>
