
<div class="main-panel">
	<div class="container">
		<div class="page-inner">
			<div class="col-12">
				<div class="row ff-title-block">
					<div class="col-12 col-sm-12 col-md-6 col-lg-6">
						<h1 class="ff-page-title">Master</h1>                               
						<ul class="breadcrumbs">   
							<li class="nav-item">
								<a href="#">Home</a>
							</li>
							<li class="separator">
								<i class="flaticon-right-arrow"></i>
							</li>
							<li class="nav-item">
								<a href="#">Master</a>
							</li>
						</ul>
					</div>
					<div class="col-12 col-sm-12 col-md-6 col-lg-6">
						<a href="#/" class="btn btn-primary ml-2 ff-page-title-btn">
							<i class="fas fa-plus"></i> 
							New Testimonial							
						</a>
						<a href="#" class="btn btn-light btn-border ff-page-title-btn">
							Cancel
						</a>
					</div>
				</div>
			</div>					
			<div class="row ff-master-options">
				<div class="col-md-6">
						<a href="<?= base_url('tag');?>">
								<div class="card">								
										<div class="card-body">
										<span class="h3">Tags</span>
										</div>
								</div>
						</a>							
				</div>
				<div class="col-md-6">
						<a href="<?= base_url('theme')?>">
								<div class="card">								
										<div class="card-body">
												<span class="h3">Theme Master</span>
										</div>
								</div>
						</a>							
				</div>
				<div class="col-md-6">
							<a href="<?= base_url('subtheme')?>">
								<div class="card">								
										<div class="card-body">
												<span class="h3">Sub Theme Master</span>
										</div>
								</div>
						</a>							
				</div>
				<div class="col-md-6">
						<a href="<?= base_url('video')?>">
								<div class="card">								
										<div class="card-body">
												<span class="h3">Video Master</span>
										</div>
								</div>
						</a>							
				</div>
				
			</div>
		</div>
	</div>
	
</div>						
