<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Video extends MX_Controller
{
	
	public function __construct(){
		parent::__construct();
		checklogin();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('videomodel','',TRUE);
	}

	public function index(){
		$result = array();
		$condition = "1=1";
		$result['video_data'] = $this->common->getData("tbl_videos",'*',$condition);
		// echo "<pre>";
		// print_r($result);
		// exit;
		$this->load->view('main-header.php');
		$this->load->view('index.php',$result);
		$this->load->view('footer.php');
	}

	public function addEdit(){

		$video_id = "";
		//print_r($_GET);
		$result = array();
		if(!empty($_GET['text']) && isset($_GET['text'])){
			$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
			parse_str($varr,$url_prams);
			$video_id = $url_prams['id'];
			$condition = "video_id ='".$video_id."' ";
			$result['video_data'] = $this->common->getData("tbl_videos",'*',$condition);
		}
		$condition = "1=1";
		$result['countries'] = $this->common->getData("tbl_country",'*',$condition);
		$condition = "status = 'Active' ";
		// $result['locations'] = $this->common->getData("tbl_location",'*',$condition);
		$result['colors'] = $this->videomodel->enum_select("tbl_videos","colour_id");
		$result['sounds'] = $this->videomodel->enum_select("tbl_videos","sound_id");
		$result['tags'] = $this->common->getData("tbl_tags",'*',$condition);
		$result['themes'] = $this->common->getData("tbl_themes",'*',$condition);

		// echo "<pre>";
		// print_r($result);
		// exit;
		//echo $user_id;
		$this->load->view('main-header.php');
		$this->load->view('addEdit_bk.php',$result);
		$this->load->view('footer.php');
	}

	function submitformtesting(){
		// codeigniter form validation 
		// $this->form_validation->set_rules('videoname', 'videoname', 'trim|required|alpha');
		// $this->form_validation->set_rules('mainvideo', 'Main video', 'trim|required');
		// $this->form_validation->set_rules('meta_title', 'Meta title', 'trim|required');
		// $this->form_validation->set_rules('meta_description', 'Meta description', 'trim|required');
		// $this->form_validation->set_rules('mainvideo', 'Main video', 'trim|required');

		// if($this->form_validation->run()) {  
		// 	// true  if validation property sectifice properly
		// 	$videoname = $this->input->post('videoname');  
		// 	$mainvideo = $this->input->post('mainvideo'); 
		// 	$videoname = $this->input->post('meta_title');  
		// 	$mainvideo = $this->input->post('meta_description'); 
		// 	$videoname = $this->input->post('mainvideo');  
		// 	$mainvideo = $this->input->post('meta_title'); 
		// }else{  
		// 	// if validation is not proper
		// 	$this->index();  
		// } 
	}

	public  function getLocation(){
		// print_r($_POST);
		// exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();
			$condition = " 1=1 AND country_id = '".$this->input->post('country_id')."' AND location_name like '%".$this->input->post('location_name')."%' ";
			$getlocation = $this->common->getData("tbl_country_location_mapping",'*',$condition);
			// echo $this->db->last_query();
			if($getlocation){
				// $list_location = array();
				foreach ($getlocation as $key => $value) {
					$list_location[] = $value['location_name'];
				}
				echo json_encode($list_location);
			}
		}
	}
 
	public function getSubtheme(){
		$result = $this->common->getData("tbl_sub_themes", "*", "theme_id = '".$_REQUEST['theme_id']."' ");
		// print_r($result);
		// exit;
	   $option         = '';
	   $sub_theme_id = '';
	   
	   if(isset($_REQUEST['sub_theme_id']) && !empty($_REQUEST['sub_theme_id'])) {
		  $sub_theme_id = $_REQUEST['sub_theme_id'];
	   }
	   if(!empty($result)) 
	   {
		   for ($i = 0; $i < sizeof($result); $i++) 
		   {
			   $sel = ($result[$i]['sub_theme_id'] == $sub_theme_id) ? 'selected="selected"' : '';
			   $option .= '<option value="' . $result[$i]['sub_theme_id'] . '" ' . $sel . ' >' . $result[$i]['sub_theme_name']. '</option>';
		   }
	   }
	   echo json_encode(array("status" => "success", "option" => $option));
	   exit;
	}


 	public function submitForm(){
		// print_r($_POST);
		// print_r($_FILES);
		// exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();
			
			$thumnail_value = "";
			if(isset($_FILES) && isset($_FILES["videofile"]["name"])){
				 $config = array();
				$config['upload_path'] = DOC_ROOT_FRONT."/images/video_image/";
				$config['max_size']    = '0';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				// $config['allowed_types'] = '*';
				// $config['file_name']     = md5(uniqid("100_ID", true));
				$config['file_name']     = $_FILES["videofile"]["name"];
				$this->load->library('upload', $config);
				if (!$this->upload->do_upload("videofile")){
					$image_error = array('error' => $this->upload->display_errors());
					echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
					exit;
				}else{
					$image_data = array('upload_data' => $this->upload->data());
					$thumnail_value = $image_data['upload_data']['file_name']; 
				}	
				/* Unlink previous category image */
				if(!empty($this->input->post('video_id'))){	
					$condition_image = " video_id = ".$this->input->post('video_id');
					$image =$this->common->getData("tbl_videos",'*',$condition_image);
					if(is_array($image) && !empty($image[0]->thumbnail_image) && file_exists(DOC_ROOT_FRONT."/images/video_thumbnail_image/".$image[0]->thumbnail_image))
					{
						unlink(DOC_ROOT_FRONT."/images/video_thumbnail_image/".$image[0]->thumbnail_image);
					}
				}
			}else{
				$thumnail_value = $this->input->post('pre_videofile_name');
			}

			$data['thumbnail_image'] = $thumnail_value;
			$data['short_description'] = $this->input->post('videoname');
			$data['long_description'] = $this->input->post('featured-video');
			$data['status'] = $this->input->post('video-status');
			$data['reel_number'] = $this->input->post('mainvideo');
			$data['year'] = $this->input->post('video_description');
			$data['colour_id'] = $this->input->post('video_description');
			$data['sound_id'] = $this->input->post('video_description');
			$data['tc_begins'] = $this->input->post('tc_begins');
			$data['tc_ends'] = $this->input->post('tc_begins');
			$data['duration'] = $this->input->post('tc_begins');
			$data['vimeo_url'] = $this->input->post('tc_begins');
			$data['meta_title'] = $this->input->post('meta_title');
			$data['meta_description'] = $this->input->post('meta_description');
			$data['updated_on'] = date("Y-m-d H:i:s");
			$data['updated_by'] = $this->session->userdata('footage_farm_admin')[0]['user_id'];
			if(!empty($this->input->post('video_id'))){
				$condition = "video_id = '".$this->input->post('video_id')."' ";
				$result = $this->common->updateData("tbl_videos",$data,$condition);
				if($result){
					echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
					exit;
				}
				else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while updating data.'));
					exit;
				}
			}else{
				$data['created_on'] = date("Y-m-d H:i:s");
				$data['created_by'] =  $this->session->userdata('footage_farm_admin')[0]['user_id'];
				$result = $this->common->insertData('tbl_videos',$data,'1');
				$video_id = $result;
				if(!empty($result)){
					$data_country_location_map = array();
					foreach ($country as $key => $value) {
						 $data_country_location_map['video_id'] = $video_id;
						 $data_country_location_map['country_id'] = $value;
						 $data_country_location_map['location_id'] = $location[$key];
						 $data_country_location_map['created_on'] = date("Y-m-d H:i:s");
						 $data_country_location_map['created_by'] =  $this->session->userdata('footage_farm_admin')[0]['user_id'];
						 $result_country_location_map = $this->common->insertData('tbl_video_country_location_mapping',$data_country_location_map,'1');
					}
					if(!empty($result_country_location_map)){
						
						$data_tag_map = array();
						foreach ($tags as $key => $value) {
							$data_tag_map['video_id'] = $video_id ;
							$data_tag_map['tag_id'] = $value ;
							$data_tag_map['created_on'] = date("Y-m-d H:i:s");
							$data_tag_map['created_by'] =  $this->session->userdata('footage_farm_admin')[0]['user_id'];
							$result_data_tag_map = $this->common->insertData('tbl_video_tag_mapping',$data_tag_map,'1');
						}
							if(!empty($result_data_tag_maps)){

								$all_themes_id = $all_sub_themes_id = '';
								if(!empty($_POST['theme_id'])){
									$all_themes_id =implode(",",$_POST['theme_id']);
								}
								if(!empty($_POST['sub_theme_id'])){
									$all_sub_themes_id =implode(",",$_POST['sub_theme_id']);
								}

								if($all_themes_id && $all_sub_themes_id){
									$condition = "1=1 && theme_id IN (".$all_themes_id.") && sub_theme_id IN (".$all_sub_themes_id.")";
									$get_selected_theme_subtheme = $this->common->getData("tbl_sub_themes",'*',$condition);

									if($get_selected_theme_subtheme){
										foreach ($get_selected_theme_subtheme as $key => $value) {
											$data_theme_sub_theme = array();
											$data_theme_sub_theme['video_id'] = $video_id;
											$data_theme_sub_theme['theme_id'] = $value['theme_id'];
											$data_theme_sub_theme['sub_theme_id'] = $value['sub_theme_id'];;
											$data_theme_sub_theme['video_id'] = $video_id;
											$data_theme_sub_theme['created_on'] = date("Y-m-d H:i:s");
											$data_theme_sub_theme['created_by'] =  $this->session->userdata('footage_farm_admin')[0]['user_id'];
											$result_theme_sub_theme = $this->common->insertData('tbl_video_theme_subtheme_mapping',$data_theme_sub_theme,'1');
										}
									}
										if($result_theme_sub_theme){
											echo json_encode(array('success'=>true, 'msg'=>'Record Added Successfully.'));
											exit;
										}else{
											echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
											exit;
										}
								}
							}
								
					}else{
						echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
						exit;
					}
							
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
					exit;
				}
			}
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem While Add/Edit Data.'));
			exit;
		}
	}
}