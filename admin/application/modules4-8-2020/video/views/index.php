<body>
	<div class="wrapper">
		<div class="main-panel">
			<div class="container">
				<div class="page-inner">
					<div class="col-12">
						<div class="row ff-title-block">
							<div class="col-12 col-sm-12 col-md-6 col-lg-6">
									<h1 class="ff-page-title">All Videos</h1> 
							</div>
							<div class="col-12 col-sm-12 col-md-6 col-lg-6">  
									<a href="<?= base_url('video/addEdit')?>" class="btn btn-primary ml-2 ff-page-title-btn">
											<i class="fas fa-plus"></i> 
											Add New Video					
									</a>	
							</div>
						</div>
					</div>					
					<div class="row">
							<div class="col-sm-12">
									<div class="card">
											<div class="card-body">
												<div class="table-responsive">
													<table id="add-row" class="display table table-striped table-hover" >
														<thead>
															<tr>
																<th>Footage</th>
																<th>Reel No.</th>
																<th>Video Title</th>
																<th>Themes</th>
																<th style="width: 5%">Actions</th>
															</tr>
														</thead>
														<tfoot>
															<tr>
															<th>Footage</th>
																<th>Reel No.</th>
																<th>Video Title</th>
																<th>Themes</th>
																<th>Actions</th>
															</tr>
														</tfoot>
														<tbody>
															<tr>
																<td><a href="#/"><img src="assets/images/footage-thumb.jpg" alt=""></a></td>
																<td>#583458854285</td>
																<td>1910-1918- WWI  Era: Aerials, German Officer & Tr…</td>
																<td>Revisiting 1896 paris, Apollo 11 Moon Walk,</td>
																<td>
																	<div class="form-button-action">
																		<a href="edit-video.php" data-toggle="tooltip" title="" class="btn btn-link btn-primary btn-lg btn-edit" data-original-title="Edit">
																			<i class="fa fa-edit"></i>
																		</a>
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																			<i class="fa fa-times"></i>
																		</a>
																	</div>
																</td>
															</tr>
															<tr>
																<td><a href="#/"><img src="assets/images/footage-thumb.jpg" alt=""></a></td>
																<td>#124158253195</td>
																<td>Why We Fight: The Battle of Britain, 1940</td>
																<td>Revisiting 1896 paris, Apollo 11 Moon Walk,</td>
																<td>
																	<div class="form-button-action">
																		<a href="edit-video.php" data-toggle="tooltip" title="" class="btn btn-link btn-primary btn-lg btn-edit" data-original-title="Edit">
																			<i class="fa fa-edit"></i>
																		</a>
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																			<i class="fa fa-times"></i>
																		</a>
																	</div>
																</td>
															</tr>
															<tr>
																<td><a href="#/"><img src="assets/images/footage-thumb.jpg" alt=""></a></td>
																<td>#624215653199</td>
																<td>USSR Destruction, WWII Aftermath</td>
																<td>World War II,Civil Rights March - 1965,</td>
																<td>
																	<div class="form-button-action">
																		<a href="edit-video.php" data-toggle="tooltip" title="" class="btn btn-link btn-primary btn-lg btn-edit" data-original-title="Edit">
																			<i class="fa fa-edit"></i>
																		</a>
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																			<i class="fa fa-times"></i>
																		</a>
																	</div>
																</td>
															</tr>
															<tr>
																<td><a href="#/"><img src="assets/images/footage-thumb.jpg" alt=""></a></td>
																<td>#683458854145</td>
																<td>German Troops March Into Austria (1938)</td>
																<td>Revisiting 1896 paris, Civil Rights March - 1965,</td>
																<td>
																	<div class="form-button-action">
																		<a href="edit-video.php" data-toggle="tooltip" title="" class="btn btn-link btn-primary btn-lg btn-edit" data-original-title="Edit">
																			<i class="fa fa-edit"></i>
																		</a>
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																			<i class="fa fa-times"></i>
																		</a>
																	</div>
																</td>
															</tr>
															<tr>
																<td><a href="#/"><img src="assets/images/footage-thumb.jpg" alt=""></a></td>
																<td>#683458854146</td>
																<td>1910-1918- WWI  Era: Aerials, German Officer & Tr…</td>
																<td>Revisiting 1896 paris, World War II,</td>
																<td>
																	<div class="form-button-action">
																		<a href="edit-video.php" data-toggle="tooltip" title="" class="btn btn-link btn-primary btn-lg btn-edit" data-original-title="Edit">
																			<i class="fa fa-edit"></i>
																		</a>
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																			<i class="fa fa-times"></i>
																		</a>
																	</div>
																</td>
															</tr>
															<tr>
																<td><a href="#/"><img src="assets/images/footage-thumb.jpg" alt=""></a></td>
																<td>#583458854285</td>
																<td>1910-1918- WWI  Era: Aerials, German Officer & Tr…</td>
																<td>Revisiting 1896 paris, Apollo 11 Moon Walk,</td>
																<td>
																	<div class="form-button-action">
																		<a href="edit-video.php" data-toggle="tooltip" title="" class="btn btn-link btn-primary btn-lg btn-edit" data-original-title="Edit">
																			<i class="fa fa-edit"></i>
																		</a>
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																			<i class="fa fa-times"></i>
																		</a>
																	</div>
																</td>
															</tr>
															<tr>
																<td><a href="#/"><img src="assets/images/footage-thumb.jpg" alt=""></a></td>
																<td>#124158253195</td>
																<td>Why We Fight: The Battle of Britain, 1940</td>
																<td>Revisiting 1896 paris, Apollo 11 Moon Walk,</td>
																<td>
																	<div class="form-button-action">
																		<a href="edit-video.php" data-toggle="tooltip" title="" class="btn btn-link btn-primary btn-lg btn-edit" data-original-title="Edit">
																			<i class="fa fa-edit"></i>
																		</a>
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																			<i class="fa fa-times"></i>
																		</a>
																	</div>
																</td>
															</tr>
															<tr>
																<td><a href="#/"><img src="assets/images/footage-thumb.jpg" alt=""></a></td>
																<td>#624215653199</td>
																<td>USSR Destruction, WWII Aftermath</td>
																<td>World War II,Civil Rights March - 1965,</td>
																<td>
																	<div class="form-button-action">
																		<a href="edit-video.php" data-toggle="tooltip" title="" class="btn btn-link btn-primary btn-lg btn-edit" data-original-title="Edit">
																			<i class="fa fa-edit"></i>
																		</a>
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																			<i class="fa fa-times"></i>
																		</a>
																	</div>
																</td>
															</tr>
															<tr>
																<td><a href="#/"><img src="assets/images/footage-thumb.jpg" alt=""></a></td>
																<td>#683458854145</td>
																<td>German Troops March Into Austria (1938)</td>
																<td>Revisiting 1896 paris, Civil Rights March - 1965,</td>
																<td>
																	<div class="form-button-action">
																		<a href="edit-video.php" data-toggle="tooltip" title="" class="btn btn-link btn-primary btn-lg btn-edit" data-original-title="Edit">
																			<i class="fa fa-edit"></i>
																		</a>
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																			<i class="fa fa-times"></i>
																		</a>
																	</div>
																</td>
															</tr>
															<tr>
																<td><a href="#/"><img src="assets/images/footage-thumb.jpg" alt=""></a></td>
																<td>#683458854146</td>
																<td>1910-1918- WWI  Era: Aerials, German Officer & Tr…</td>
																<td>Revisiting 1896 paris, World War II,</td>
																<td>
																	<div class="form-button-action">
																		<a href="edit-video.php" data-toggle="tooltip" title="" class="btn btn-link btn-primary btn-lg btn-edit" data-original-title="Edit">
																			<i class="fa fa-edit"></i>
																		</a>
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																			<i class="fa fa-times"></i>
																		</a>
																	</div>
																</td>
															</tr>
														</tbody>
													</table>
												</div>
											</div>
									</div>
							</div>
					</div>
				</div>
			</div>
			
		</div>						
	</div>
	<script >
		$(document).ready(function() {
			// Add Row
			$('#add-row').DataTable({
				"pageLength": 5,
			});			
		});
	</script>
</body>
</html>
