<?php
foreach ($roles_users as $key => $value) {?>
  <div class="col-sm-6 col-md-6">
    <div class="card card-round">
        <div class="card-body ">
            <div class="row">
                <div class="col-12 col-sm-7">
                    <h4><?= $value['user_name']?></h4>
                    <p><?= $value['email_id']?></p>
                </div>
                <div class="col-12 col-sm-3">
                        <div class="form-group" style="padding:0;">
                            <label for="role">Role</label><br/>																						
                            <select name="statusselect" id="statusselect<?=$key?>" onchange="changeUserStatus('<?= $value['user_id']?>',this.value)">
                                <?php 
                                    // $sel = '';
                                foreach ($roles as $rolekey => $rolevalue) {
                                    // print_r($rolevalue);
                                    // exit;
                                    $sel = ($rolevalue['role_name'] == $value['user_type'] ? "selected=selected":" ");?>
                                    <option value="<?= $rolevalue['role_id'] ?>"<?=$sel?> ><?= $rolevalue['role_name']?></option>
                                
                                <?php }?>
                               
                            </select> 
                        </div>
                </div>
                <div class="col-12 col-sm-2">
                    <a href="#/" onclick="deleteUser('<?= $value['user_id']?>')" class="delete-team-member">
                        <i class="fas fa-trash-alt fa-lg"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php }?>
