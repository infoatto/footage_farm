<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Profile extends MX_Controller
{
	
	public function __construct(){
		parent::__construct();
		checklogin();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('profilemodel','',TRUE);
		$this->load->model('video/videomodel','',TRUE);

	}

	public function index(){
		$result = array();
		$condition = "1=1 AND user_id = ".$this->session->userdata('footage_farm_admin')[0]['user_id']." ";
		$result['profile_data'] = $this->common->getData("tbl_users",'*',$condition);
		//get  enum value
		$result['roles'] = $this->common->getData("tbl_roles","*");
		// echo "<pre>";
		// print_r($result);
		// exit;
		$this->load->view('main-header.php');
		$this->load->view('index',$result);
		$this->load->view('footer.php');
	}

	public function addEdit(){

		$user_id = "";
		//print_r($_GET);
		$result = array();
		if(!empty($_GET['text']) && isset($_GET['text'])){
			$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
			parse_str($varr,$url_prams);
			$user_id = $url_prams['id'];
			$condition = " user_id ='".$user_id."' ";
			$result['theme_data'] = $this->common->getData("tbl_users",'*',$condition);
		}
		
		// echo "<pre>";
		// print_r($result);
		// exit;
		//echo $user_id;
		$this->load->view('main-header.php');
		$this->load->view('addEdit.php',$result);
		$this->load->view('footer.php');
	}

	function submitformtesting(){
		// codeigniter form validation 
		// $this->form_validation->set_rules('themename', 'Themename', 'trim|required|alpha');
		// $this->form_validation->set_rules('mainvideo', 'Main video', 'trim|required');
		// $this->form_validation->set_rules('meta_title', 'Meta title', 'trim|required');
		// $this->form_validation->set_rules('meta_description', 'Meta description', 'trim|required');
		// $this->form_validation->set_rules('mainvideo', 'Main video', 'trim|required');

		// if($this->form_validation->run()) {  
		// 	// true  if validation property sectifice properly
		// 	$themename = $this->input->post('themename');  
		// 	$mainvideo = $this->input->post('mainvideo'); 
		// 	$themename = $this->input->post('meta_title');  
		// 	$mainvideo = $this->input->post('meta_description'); 
		// 	$themename = $this->input->post('mainvideo');  
		// 	$mainvideo = $this->input->post('meta_title'); 
		// }else{  
		// 	// if validation is not proper
		// 	$this->index();  
		// } 
	}
		
	public function submitFormMember(){

		// print_r($_POST);
		// exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();
			$condition =" role_id = ".$_POST['role_id']." ";
			$get_usertype = $this->common->getData("tbl_roles","*",$condition);
			$data['user_type'] = $get_usertype[0]['role_name'];
			$data['role_id'] = $this->input->post('role_id');
			$data['user_name'] = $this->input->post('member_username');
			$data['email_id'] = $this->input->post('member_email');
			$data['password'] = md5('123456');
			
			$data['created_on'] = date("Y-m-d H:i:s");
			$data['created_by'] = $this->session->userdata('footage_farm_admin')[0]['user_id'];

			$result = $this->common->insertData('tbl_users',$data,'1');
			if($result){

				$this->send_mail( $get_usertype[0]['role_name'],$this->input->post('member_username'),$this->input->post('member_email'));
				echo json_encode(array('success'=>true, 'msg'=>'Record inserted Successfully.'));
				exit;
			}else{
				echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
				exit;
			}
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem While Add/Edit Data.'));
			exit;
		}
	}

	public function send_mail($role_type,$username,$email_id){
		$result = array();
		$result['user_type']= $role_type;
		$result['username']=  $username;
		$result['email_id']= $email_id ;
		$result['password']= '123456' ;
	
		$member_mail_hmtl = $this->load->view('member-mail',$result,true);
		$this->email->clear();
		$this->email->from(FROM_EMAIL); // change it to yours
		$this->email->to($email_id); // change it to yours
		$this->email->subject("Enrollement in Footage farm");
		$this->email->message($member_mail_hmtl);
		$this->email->send();

	}


	public function submitForm(){
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();
			$condition = "email_id = '".$this->input->post('email')."' ";
			if($this->input->post('user_id') && $this->input->post('user_id') > 0){
				$condition .= " AND  user_id != ".$this->input->post('user_id')." ";
			}			
			$check_name = $this->common->getData("tbl_users",'*',$condition);

			if(!empty($check_name[0]->user_id)){
				echo json_encode(array("success"=>false, 'msg'=>'Email Already Allocated to Another user!'));
				exit;
			}
			
			$thumnail_value = "";
			if(isset($_FILES) && isset($_FILES["profile"]["name"])){
				 $config = array();
				$config['upload_path'] = DOC_ROOT_FRONT."/images/profile_image/";
				$config['max_size']    = '0';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				// $config['allowed_types'] = '*';
				// $config['file_name']     = md5(uniqid("100_ID", true));
				$config['file_name']     = $_FILES["profile"]["name"];
				$this->load->library('upload', $config);
				if (!$this->upload->do_upload("profile")){
					$image_error = array('error' => $this->upload->display_errors());
					echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
					exit;
				}else{
					$image_data = array('upload_data' => $this->upload->data());
					$thumnail_value = $image_data['upload_data']['file_name']; 
				}	
				/* Unlink previous category image */
				if(!empty($this->input->post('user_id'))){	
					$condition_image = " user_id = ".$this->input->post('user_id');
					$image =$this->common->getData("tbl_users",'*',$condition_image);
					if(is_array($image) && !empty($image[0]->profile_photo) && file_exists(DOC_ROOT_FRONT."/images/profile_image/".$image[0]->profile_photo))
					{
						unlink(DOC_ROOT_FRONT."/images/profile_image/".$image[0]->profile_photo);
					}
				}
			}else{
				$thumnail_value = $this->input->post('pre_profile_name');
			}

			$data['profile_photo'] = $thumnail_value;
			$data['user_name'] = $this->input->post('username');
			$data['email_id'] = $this->input->post('email');
			$data['phone'] = $this->input->post('phone');
			$data['dob'] = (!empty($this->input->post('dob'))) ? date("Y-m-d", strtotime($this->input->post('dob'))) : '';
			$data['gender'] = $this->input->post('gender');
			$data['updated_on'] = date("Y-m-d H:i:s");

			if(!empty($this->input->post('currentpass'))){
				if($this->input->post('currentpass') == $this->input->post('newpass')){
					$data['password'] = md5($this->input->post('newpass'));
				}else{
					echo json_encode(array("success"=>false, 'msg'=>'Please make sure both password is same !'));
					exit;
				}
			}

			$data['updated_by'] = $this->session->userdata('footage_farm_admin')[0]['user_id'];
			if(!empty($this->input->post('user_id'))){
				// print_r($data);
				$condition = "user_id = '".$this->input->post('user_id')."' ";
				$result = $this->common->updateData("tbl_users",$data,$condition);
				$result = $this->common->getData('tbl_users','*',$condition);
				if($result){
					$session_data = array('footage_farm_admin'=>$result);  
					$this->session->set_userdata($session_data);  
					echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
					exit;
				}
				else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while updating data.'));
					exit;
				}
			}
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem While Add/Edit Data.'));
			exit;
		}
	}

	function submitform_security(){
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();
			if(!empty($this->input->post('currentpass'))){
				if($this->input->post('currentpass') == $this->input->post('newpass')){
					$data['password'] = md5($this->input->post('newpass'));
				}else{
					echo json_encode(array("success"=>false, 'msg'=>'Please make sure both password is same !'));
					exit;
				}
			}

			$data['updated_by'] = $this->session->userdata('footage_farm_admin')[0]['user_id'];
			if(!empty($this->input->post('user_id'))){
				// print_r($data);
				$condition = "user_id = '".$this->input->post('user_id')."' ";
				$result = $this->common->updateData("tbl_users",$data,$condition);
				if($result){
					echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while updating data.'));
					exit;
				}
			}
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem While Add/Edit Data.'));
			exit;
		}
	}

	function getMemberslisting(){
		$result['roles'] = $this->common->getData("tbl_roles","*");
		//get total number of roles user available
		$condition = " user_type != ('customer')";
		$result['roles_users'] = $this->common->getData("tbl_users","*",$condition);
		$getMemberhtml=$this->load->view('members_view',$result,true);
		if($getMemberhtml){
			echo json_encode(array('success'=>true, 'html'=>$getMemberhtml));
			exit;
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'No Member found'));
			exit;
		}
	}

	function changeUserStatus(){

		$data = array();
		$condition =" role_id = ".$_POST['role_id']." ";
		$get_usertype = $this->common->getData("tbl_roles","*",$condition);
		$data['user_type'] = $get_usertype[0]['role_name'];
		$data['role_id'] = $this->input->post('role_id');
	
		$data['updated_on'] = date("Y-m-d H:i:s");
		$data['updated_by'] = $this->session->userdata('footage_farm_admin')[0]['user_id'];
		$condition = "user_id = '".$this->input->post('user_id')."' ";
		$result = $this->common->updateData("tbl_users",$data,$condition);
		if($result){
			echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
			exit;
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem while updating data.'));
			exit;
		}
	}

	function deleteUser(){
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

			$condition="user_id = '".$_POST['user_id']."' ";
			$result = $this->common->deleteRecord('tbl_users',$condition);
			if($result){
				echo json_encode(array('success'=>true, 'msg'=>'User deleted Successfully.'));
				exit;
			}else{
				echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
				exit;
			}
		}
	}
}