
<body>
	<div class="wrapper">
    
		<div class="main-panel">
			<div class="container">
				<div class="page-inner">
					<div class="col-12">
						
						<div class="row ff-title-block">
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">
							<h1 class="ff-page-title">All work</h1> 
						</div>
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
							<a href="<?= base_url('how_we_operate/addEdit')?>" class="btn btn-primary ff-page-title-btn ff-page-title-btn">
								Add work
							</a>
						</div>
					</div>
					</div>					
					<div class="row">
							<div class="col-sm-12">
									<div class="card">
											<div class="card-body">
												<div class="table-responsive">
													<table id="add-row" class="display table table-striped table-hover" >
														<thead>
															<tr>
																<th>Work ID</th>
																<th>Title</th>
																<th>Theme Image</th>	
																<th>Description</th>													
																<th>Actions</th>															
															</tr>
														</thead>
														<tfoot>
															<tr>
																<th>Work ID</th>
																<th>Title</th>
																<th>Theme Image</th>	
																<th>Description</th>													
																<th>Actions</th>														
															</tr>
														</tfoot>
														<tbody>
														<?php
														// echo "<pre>";
														// print_r($operate_data);
														if(!empty($operate_data) && isset($operate_data))	{
															$cnt = 0;
															foreach ($operate_data as $key => $value) {
																	$cnt= ++$cnt?>
																	<tr>														
																		<td>#<?=$cnt?></td>
																		<td><img src="<?= FRONT_URL.'/images/how_we_work/'.$value['thumbnail_image']?>"class="img-thumbnail" alt="Cinque Terre" width="100" height="100"></td>																
																		<td><?= $value['title']?></td>																
																		<td><?= $value['description']?></td>														
																		<td>
																			<div class="form-button-action">
																				<a href="<?= base_url('how_we_operate/AddEdit?text='.rtrim(strtr(base64_encode("id=".$value['how_we_operate_id']), '+/', '-_'), '=').'')?>" data-toggle="tooltip" title="" class="btn btn-primary ff-page-title-btn  btn-edit" data-original-title="Edit">
																					<i class="fa fa-edit"></i>
																				</a>
																				<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" onclick="delete_row('tbl_how_we_operate','how_we_operate_id',<?=$value['how_we_operate_id']?>)" data-original-title="Remove">
																					<i class="fa fa-times"></i>
																				</a>
																			</div>
																		</td>													
																	</tr>
															<?php }
														}?>
														</tbody>
													</table>
												</div>
											</div>
									</div>
							</div>
					</div>
				</div>
			</div>
			
		</div>						
	</div>
	<script >
		$(document).ready(function() {
			$('#add-row').DataTable({
				"pageLength": 10,
			});			
		});


		function delete_row(tbl_name,column_name,id){
		var csrfName = "<?= $this->security->get_csrf_token_name()?>"; // Value specified in $config['csrf_token_name']
		var csrfHash = "<?= $this->security->get_csrf_hash()?>"; // CSRF hash
		if(tbl_name && id && column_name){
			var r = confirm("Are you want to sure delete this Work");
			if (r == true) {
				$.ajax({
					url: "<?= base_url('how_we_operate/delete_row')?>",
					data:{tbl_name,column_name,id,[csrfName]: csrfHash },
					dataType: "json",
					type: "POST",
					success: function(response){
					
						if(response.success){
							swal("done", response.msg, {
								icon : "success",
								buttons: {        			
									confirm: {
										className : 'btn btn-success'
									}
								},
							}).then(
							function() {
								location.reload();
							});
						}else{	
							swal("Failed", response.msg, {
								icon : "error",
								buttons: {        			
									confirm: {
										className : 'btn btn-danger'
									}
								},
							});
						}
					}
				});
			}
		}


	}
	</script>
</body>
</html>
