
<div class="wrapper">
	<div class="main-panel">
		<div class="container">
			<div class="page-inner">
				<div class="col-12">
					<div class="row ff-title-block">
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">
							<h1 class="ff-page-title">How We Operate</h1> 
						</div>
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
							<a href="<?= base_url('how_we_operate')?>" class="btn btn-primary ff-page-title-btn">
								Cancel
							</a>
						</div>
					</div>
				</div>					
				<div class="row">
					<div class="col-sm-12">
						<div class="card">
							<div class="card-body">
								<form class="" id="form-work" method="post" enctype="multipart/form-data">
									<!-- <?php  print_r($operate_data) ;?> -->
									<input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden">
									<input name="work_id" id="work_id" value="<?= (!empty($operate_data[0]['how_we_operate_id'])?$operate_data[0]['how_we_operate_id']:"") ?>" type="hidden">
											
											<div class="form-group">
												<label for="short_description"> Main Page Short Decription</label>
												<textarea class="form-control   my-text-editor" id="short_description"  name="short_description"  rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."><?= (!empty($operate_data[0]['short_description'])?$operate_data[0]['short_description']:"") ?></textarea>	
											</div>


											<h3>Section 1</h3>
											<hr/>

											<div class="form-group">
												<label for="title">Work Title 1</label>
												<input type="text" class="form-control" id="title_1" name="title_1" value="<?= (!empty($operate_data[0]['title_1'])?$operate_data[0]['title_1']:"") ?>" placeholder="Early 19th Century">	
											</div>

											<div class="form-group">
												<label for="work">Work Thumbnail Image 1</label>
												<?php $req= '';
												if(empty($operate_data[0]['thumbnail_image_1'])){
													$req ='required';
												}else{?>
												<br>
													<img src="<?= FRONT_URL.'/images/how_we_work/'.$operate_data[0]['thumbnail_image_1']?>"class="img-thumbnail" alt="Cinque Terre" width="250" height="300">
													<input name="pre_work_name_1" id="pre_work_name_1" value="<?= (!empty($operate_data[0]['thumbnail_image_1'])?$operate_data[0]['thumbnail_image_1']:"") ?>" type="hidden">
												<?php }?>
												
												<input type="file" class="form-control-file" id="work_1" name="work_1" <?= $req;?> >
											</div>

											<div class="form-group">
												<label for="main_description_1">Main Page Decription 1</label>
												<textarea class="form-control my-text-editor1" id="main_description_1"  name="main_description_1"  rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."><?= (!empty($operate_data[0]['main_description_1'])?$operate_data[0]['main_description_1']:"") ?></textarea>	
											</div>
													
											
											<div class="form-group">
												<label for="home_description_1">Home Page Decription 1</label>
												<textarea class="form-control my-text-editor2" id="home_description_1"  name="home_description_1"  rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."><?= (!empty($operate_data[0]['home_description_1'])?$operate_data[0]['home_description_1']:"") ?></textarea>	
											</div>

										




											<h3>Section 2</h3>
											<hr/>

											<div class="form-group">
												<label for="title">Work Title 2</label>
												<input type="text" class="form-control" id="title_2" name="title_2" value="<?= (!empty($operate_data[0]['title_2'])?$operate_data[0]['title_2']:"") ?>" placeholder="Early 29th Century">	
											</div>

											<div class="form-group">
												<label for="work">Work Thumbnail Image 2</label>
												<?php $req= '';
												if(empty($operate_data[0]['thumbnail_image_2'])){
													$req ='required';
												}else{?>
												<br>
													<img src="<?= FRONT_URL.'/images/how_we_work/'.$operate_data[0]['thumbnail_image_2']?>"class="img-thumbnail" alt="Cinque Terre" width="250" height="300">
													<input name="pre_work_name_2" id="pre_work_name_2" value="<?= (!empty($operate_data[0]['thumbnail_image_2'])?$operate_data[0]['thumbnail_image_2']:"") ?>" type="hidden">
												<?php }?>
												
												<input type="file" class="form-control-file" id="work_2" name="work_2" <?= $req;?> >
											</div>

											
											<div class="form-group">
												<label for="main_description_2">Main Page Decription 2</label>
												<textarea class="form-control my-text-editor3" id="main_description_2"  name="main_description_2"  rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."><?= (!empty($operate_data[0]['main_description_2'])?$operate_data[0]['main_description_2']:"") ?></textarea>	
											</div>


											<div class="form-group">
												<label for="home_description_2">Home Page Decription 2</label>
												<textarea class="form-control my-text-editor4" id="home_description_2"  name="home_description_2"  rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."><?= (!empty($operate_data[0]['home_description_2'])?$operate_data[0]['home_description_2']:"") ?></textarea>	
											</div>

										




											<h3>Section 3</h3>
											<hr/>

											<div class="form-group">
												<label for="title">Work Title 3</label>
												<input type="text" class="form-control" id="title_3" name="title_3" value="<?= (!empty($operate_data[0]['title_3'])?$operate_data[0]['title_3']:"") ?>" placeholder="Early 39th Century">	
											</div>

											<div class="form-group">
												<label for="work">Work Thumbnail Image 3</label>
												<?php $req= '';
												if(empty($operate_data[0]['thumbnail_image_3'])){
													$req ='required';
												}else{?>
												<br>
													<img src="<?= FRONT_URL.'/images/how_we_work/'.$operate_data[0]['thumbnail_image_3']?>"class="img-thumbnail" alt="Cinque Terre" width="250" height="300">
													<input name="pre_work_name_3" id="pre_work_name_3" value="<?= (!empty($operate_data[0]['thumbnail_image_3'])?$operate_data[0]['thumbnail_image_3']:"") ?>" type="hidden">
												<?php }?>
												
												<input type="file" class="form-control-file" id="work_3" name="work_3" <?= $req;?> >
											</div>

											<div class="form-group">
												<label for="main_description_3">Main Page Decription 3</label>
												<textarea class="form-control my-text-editor5" id="main_description_3"  name="main_description_3"  rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."><?= (!empty($operate_data[0]['main_description_3'])?$operate_data[0]['main_description_3']:"") ?></textarea>	
											</div>

											<div class="form-group">
												<label for="home_description_3">Home Page Decription 3</label>
												<textarea class="form-control my-text-editor6" id="home_description_3"  name="home_description_3"  rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."><?= (!empty($operate_data[0]['home_description_3'])?$operate_data[0]['home_description_3']:"") ?></textarea>	
											</div>		


											
											<h3>Ordering /Declaring /Research </h3>
											<div class="form-group">
												<label for="description">Main Content</label>
												<textarea class="form-control my-text-editor7" id="description"  name="description"  rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."><?= (!empty($operate_data[0]['description'])?$operate_data[0]['description']:"") ?></textarea>	
											</div>


											<hr/>					


										
											<div class="form-group">
												<label for="meta_title">Meta Title</label>
												<input type="text" class="form-control" id="meta_title" value="<?= (!empty($operate_data[0]['meta_title'])?$operate_data[0]['meta_title']:"") ?>"  name="meta_title" placeholder="Early 19th Century">	
											</div>
											<div class="form-group">
												<label for="meta_description">Meta Description</label>
												<textarea class="form-control" id="meta_description" name="meta_description" rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."><?= (!empty($operate_data[0]['meta_description'])?$operate_data[0]['meta_description']:"") ?></textarea>	
											</div>
									
											<div class="form-check">
												<label>Status (show this work on website)</label><br>
												<div class="switch-field">
														<?php $active = 'checked';
															 $in_active= '';
														if(!empty($operate_data)){
															if($operate_data[0]['status']=='Active'){
																$active = "checked";
															}else{
																$in_active = "checked";
															} 
														}?>
													<input type="radio" id="activate-work-three" name="work_status" value="Active" <?= $active?>/>
													<label for="activate-work-three">Yes</label>
													<input type="radio" id="activate-work-four" name="work_status" value="In-active"  <?= $in_active?>/>
													<label for="activate-work-four">No</label>
												</div>
											</div>	
											<div class="form-group">
												<button  type="submit" class="btn btn-black"><?=(!empty($operate_data)?"Update":"Create")?></button>
												<a href="<?= base_url('how_we_operate')?>" class="btn btn-cancel btn-border">Cancel</a>
											</div>
									</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>						
</div>

<script>

$(document).ready(function(){
});


// use for form submit
var vRules = 
{
	title:{required:true},
	description:{required:true}

};
var vMessages = 
{
	title:{required:"Please Enter Title Name."},
	description:{required:"Please Enter work Description Name."}
};

$("#form-work").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{	
		var act = "<?= base_url('how_we_operate/submitform')?>";
		$("#form-work").ajaxSubmit({
			url: act, 
			type: 'post',
			dataType: 'json',
			cache: false,
			clearForm: false,
			async:false,
			beforeSubmit : function(arr, $form, options){
				// $(".btn-primary").hide();
			},
			success: function(response) 
			{
				if(response.success){
					swal("done", response.msg, {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					}).then(
					function() {
						window.location = "<?= base_url('how_we_operate')?>";
						// $this->router->fetch_class().'/'.$this->router->fetch_method() ?>";
					});
				}else{	
					swal("Failed", response.msg, {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}
			}
		});
	}
});

document.title="Add/Edit how we operate"
</script>