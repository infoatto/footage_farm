<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class How_we_operate extends MX_Controller
{
	
	public function __construct(){
		parent::__construct();
		checklogin();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('how_we_operatemodel','',TRUE);
	}

	public function index(){
		$result = array();
		$condition = "1=1";
		$result['operate_data'] = $this->common->getData("tbl_how_we_operate",'*',$condition);
		// echo "<pre>";
		// print_r($result);
		// exit;
		$this->load->view('main-header.php');
		$this->load->view('index.php',$result);
		$this->load->view('footer.php');
	}

	public function addEdit(){

		$work_id = "1";
		$condition = " how_we_operate_id ='".$work_id."' ";
		$result['operate_data'] = $this->common->getData("tbl_how_we_operate",'*',$condition);
		// echo "<pre>";
		// print_r($result);
		// exit;
		//echo $user_id;
		$this->load->view('main-header.php');
		$this->load->view('addEdit.php',$result);
		$this->load->view('footer.php');
	}


	public function delete_row(){
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

			$condition= $_POST['column_name']."= '".$_POST['id']."' ";
			$result = $this->common->deleteRecord($_POST['tbl_name'],$condition);
			if($result){
				echo json_encode(array('success'=>true, 'msg'=>'Record deleted Successfully.'));
				exit;
			}else{
				echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
				exit;
			}
		}
		// echo "<pre>";
		// print_r($_POST);
		// exit;

	}
	public function submitForm(){
		// echo "<pre>";
		// print_r($_POST);
		// print_r($_FILES);
		// exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();

			$thumnail_value_1 = "";
			if(isset($_FILES) && isset($_FILES["work_1"]["name"])){
				 $config = array();
				$config['upload_path'] = DOC_ROOT_FRONT."/images/how_we_work/";
				$config['max_size']    = '0';
				$config['allowed_types'] = 'gif|jpg|png|jpeg|svg';
				$config['file_name']     = $_FILES["work_1"]["name"];
				$this->load->library('upload', $config);
				if (!$this->upload->do_upload("work_1")){
					$image_error = array('error' => $this->upload->display_errors());
					echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
					exit;
				}else{
					$image_data = array('upload_data' => $this->upload->data());
					$thumnail_value_1 = $image_data['upload_data']['file_name']; 
					$this->setCompresseredImage($image_data['upload_data']['file_name']);

				}	
				/* Unlink previous category image */
				if(!empty($this->input->post('work_id'))){	
					$condition_image = " how_we_operate_id = ".$this->input->post('work_id');
					$image =$this->common->getData("tbl_how_we_operate",'*',$condition_image);
					if(is_array($image) && !empty($image[0]['thumbnail_image_1']) && file_exists(DOC_ROOT_FRONT."/images/how_we_work/".$image[0]['thumbnail_image_1']))
					{
						unlink(DOC_ROOT_FRONT."/images/how_we_work/".$image[0]['thumbnail_image_1']);
					}
				}
			}else{
				$thumnail_value_1 = $this->input->post('pre_work_name_1');
			}


			$thumnail_value_2 = "";
			if(isset($_FILES) && isset($_FILES["work_2"]["name"])){
				 $config = array();
				$config['upload_path'] = DOC_ROOT_FRONT."/images/how_we_work/";
				$config['max_size']    = '0';
				$config['allowed_types'] = 'gif|jpg|png|jpeg|svg';
				$config['file_name']     = $_FILES["work_2"]["name"];
				$this->load->library('upload', $config);
				if (!$this->upload->do_upload("work_2")){
					$image_error = array('error' => $this->upload->display_errors());
					echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
					exit;
				}else{
					$image_data = array('upload_data' => $this->upload->data());
					$thumnail_value_2 = $image_data['upload_data']['file_name']; 
					$this->setCompresseredImage($image_data['upload_data']['file_name']);

				}	
				/* Unlink previous category image */
				if(!empty($this->input->post('work_id'))){	
					$condition_image = " how_we_operate_id = ".$this->input->post('work_id');
					$image =$this->common->getData("tbl_how_we_operate",'*',$condition_image);
					if(is_array($image) && !empty($image[0]['thumbnail_image_2']) && file_exists(DOC_ROOT_FRONT."/images/how_we_work/".$image[0]['thumbnail_image_2']))
					{
						unlink(DOC_ROOT_FRONT."/images/how_we_work/".$image[0]['thumbnail_image_2']);
					}
				}
			}else{
				$thumnail_value_2 = $this->input->post('pre_work_name_2');
			}

			$thumnail_value_3 = "";
			if(isset($_FILES) && isset($_FILES["work_3"]["name"])){
				 $config = array();
				$config['upload_path'] = DOC_ROOT_FRONT."/images/how_we_work/";
				$config['max_size']    = '0';
				$config['allowed_types'] = 'gif|jpg|png|jpeg|svg';
				$config['file_name']     = $_FILES["work_3"]["name"];
				$this->load->library('upload', $config);
				if (!$this->upload->do_upload("work_3")){
					$image_error = array('error' => $this->upload->display_errors());
					echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
					exit;
				}else{
					$image_data = array('upload_data' => $this->upload->data());
					$thumnail_value_3 = $image_data['upload_data']['file_name']; 
					$this->setCompresseredImage($image_data['upload_data']['file_name']);

				}	
				/* Unlink previous category image */
				if(!empty($this->input->post('work_id'))){	
					$condition_image = " how_we_operate_id = ".$this->input->post('work_id');
					$image =$this->common->getData("tbl_how_we_operate",'*',$condition_image);
					if(is_array($image) && !empty($image[0]['thumbnail_image_3']) && file_exists(DOC_ROOT_FRONT."/images/how_we_work/".$image[0]['thumbnail_image_3']))
					{
						unlink(DOC_ROOT_FRONT."/images/how_we_work/".$image[0]['thumbnail_image_3']);
					}
				}
			}else{
				$thumnail_value_3 = $this->input->post('pre_work_name_3');
			}





			$data['short_description'] = $this->input->post('short_description');

			$data['thumbnail_image_1'] = $thumnail_value_1;
			$data['title_1'] = $this->input->post('title_1');
			$data['main_description_1'] = $this->input->post('main_description_1');
			$data['home_description_1'] = $this->input->post('home_description_1');

			$data['thumbnail_image_2'] = $thumnail_value_2;
			$data['title_2'] = $this->input->post('title_2');
			$data['main_description_2'] = $this->input->post('main_description_2');
			$data['home_description_2'] = $this->input->post('home_description_2');
			
			$data['thumbnail_image_3'] = $thumnail_value_3;
			$data['title_3'] = $this->input->post('title_3');
			$data['main_description_3'] = $this->input->post('main_description_3');
			$data['home_description_3'] = $this->input->post('home_description_3');


			$data['status'] = $this->input->post('work_status');
			$data['description'] = $this->input->post('description');
			$data['meta_title'] = $this->input->post('meta_title');
			$data['meta_description'] = $this->input->post('meta_description');
			$data['updated_on'] = date("Y-m-d H:i:s");
			$data['updated_by'] = $this->session->userdata('footage_farm_admin')[0]['user_id'];
			if(!empty($this->input->post('work_id'))){
				$condition = "how_we_operate_id = '".$this->input->post('work_id')."' ";
				$result = $this->common->updateData("tbl_how_we_operate",$data,$condition);
				if($result){
					echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
					exit;
				}
				else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while updating data.'));
					exit;
				}
			}else{
				$data['created_on'] = date("Y-m-d H:i:s");
				$data['created_by'] =  $this->session->userdata('footage_farm_admin')[0]['user_id'];
				$result = $this->common->insertData('tbl_how_we_operate',$data,'1');
				if(!empty($result)){
					echo json_encode(array('success'=>true, 'msg'=>'Record Added Successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
					exit;
				}
			}
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem While Add/Edit Data.'));
			exit;
		}
	}
	public function setCompresseredImage($image_name){
		$config1 = array();
		$this->load->library('image_lib');
		$config1['image_library'] = 'gd2';
		$config1['source_image'] = DOC_ROOT_FRONT."/images/how_we_work/".$image_name;
		$config1['maintain_ratio'] = TRUE;
		$config1['quality'] = '100%';
		// $config1['width'] = 1000;
		// $config1['height'] = 1000;
		$config1['new_image'] = DOC_ROOT_FRONT."/images/how_we_work/".$image_name;
		$this->image_lib->initialize($config1);
		$this->image_lib->resize();
		$this->image_lib->clear();
		}
}