<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class privacy_policy extends MX_Controller
{
	
	public function __construct(){
		checklogin();
		parent::__construct();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('privacy_policymodel','',TRUE);
	}

	public function index(){
		$result = array();
		$condition = "1=1";
		$result['privacy_policy_data'] = $this->common->getData("tbl_privacy_policy",'*',$condition);

		$this->load->view('main-header.php');
		$this->load->view('addEdit',$result);
		$this->load->view('footer.php');
	}

	function submitForm(){
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();
			$data['privacy_policy'] = $this->input->post('privacy_policy');
			$data['updated_on'] = date("Y-m-d H:i:s");
			$data['updated_by'] = $this->session->userdata('footage_farm_admin')[0]['user_id'];
			if(!empty($this->input->post('privacy_policy_id'))){
				// print_r($data);
				$condition = "privacy_policy_id = '".$this->input->post('privacy_policy_id')."' ";
				$result = $this->common->updateData("tbl_privacy_policy",$data,$condition);
				if($result){
					echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
					exit;
				}
				else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while updating data.'));
					exit;
				}
			}else{
				$data['created_on'] = date("Y-m-d H:i:s");
				$data['created_by'] =  $this->session->userdata('footage_farm_admin')[0]['user_id'];
				$result = $this->common->insertData('tbl_privacy_policy',$data,'1');
				if(!empty($result)){
					echo json_encode(array('success'=>true, 'msg'=>'Record Added Successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
					exit;
				}
			}
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem While Add/Edit Data.'));
			exit;
		}
	}
}