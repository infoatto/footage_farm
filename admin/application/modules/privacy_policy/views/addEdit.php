
<div class="wrapper">
	<div class="main-panel">
		<div class="container">
			<div class="page-inner">
				<div class="col-12">
					<div class="row ff-title-block">
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">
							<h1 class="ff-page-title">Privacy Policy</h1> 
						</div>
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
							<a href="<?= base_url('privacy_policy')?>" class="btn btn-light btn-border ff-page-title-btn">
								Cancel
							</a>
						</div>
					</div>
				</div>					
				<div class="row">
					<div class="col-sm-12">
						<div class="card">
							<div class="card-body">
								<form class="" id="form-faq" method="post" enctype="multipart/form-data">
								
									<input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden">
									<input name="privacy_policy_id" id="privacy_policy_id" value="<?= (!empty($privacy_policy_data[0]['privacy_policy_id'])?$privacy_policy_data[0]['privacy_policy_id']:"") ?>" type="hidden">
							
										<div class="form-group">
											<label for="privacy_policy">Privacy & Policy</label>
											<textarea class="form-control my-text-editor" id="privacy_policy" name="privacy_policy" rows="5" placeholder="Add an privacy_policy" spellcheck="false"><?= (!empty($privacy_policy_data[0]['privacy_policy'])?$privacy_policy_data[0]['privacy_policy']:"") ?></textarea>	
										</div> 
										
										<div class="form-group">
											<button  type="submit" class="btn btn-black"><?=(!empty($privacy_policy_data)?"Update":"Create")?></button>
											<a href="<?= base_url('privacy_policy')?>" class="btn btn-cancel btn-border">Cancel</a>
										</div>
									</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>						
</div>

<script>

$(document).ready(function(){
});


// use for form submit
var vRules = 
{
	Question:{required:true},
	privacy_policy:{required:true}
};
var vMessages = 
{
	Question:{required:"Please Enter Question."},
	privacy_policy:{required:"Please Enter privacy_policy."},
};

$("#form-faq").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{	
		var act = "<?= base_url('privacy_policy/submitform')?>";
		$("#form-faq").ajaxSubmit({
			url: act, 
			type: 'post',
			dataType: 'json',
			cache: false,
			clearForm: false,
			async:false,
			beforeSubmit : function(arr, $form, options){
				// $(".btn-primary").hide();
			},
			success: function(response) 
			{
				if(response.success){
					swal("done", response.msg, {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					}).then(
					function() {
						window.location = "<?= base_url('privacy_policy')?>";
						// $this->router->fetch_class().'/'.$this->router->fetch_method() ?>";
					});
				}else{	
					swal("Failed", response.msg, {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}
			}
		});
	}
});

document.title="Add/Edit Privacy Policy"
</script>