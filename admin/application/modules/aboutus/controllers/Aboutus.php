<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class aboutus extends MX_Controller
{
	
	public function __construct(){
		parent::__construct();
		checklogin();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('aboutusmodel','',TRUE);
	}

	public function index(){
		$result = array();
		$condition = "1=1";
		$result['about_us'] = $this->common->getData("tbl_about_us",'*',$condition);
		// echo "<pre>";
		// print_r($result);
		// exit;
		$this->load->view('main-header.php');
		$this->load->view('addEdit.php',$result);
		$this->load->view('footer.php');
	}

	public function addEdit(){

		$tag_id = "";
		//print_r($_GET);
		$result = array();
		if(!empty($_GET['text']) && isset($_GET['text'])){
			$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
			parse_str($varr,$url_prams);
			$tag_id = $url_prams['id'];
			$condition = " tag_id ='".$tag_id."' ";
			$result['tag_data'] = $this->common->getData("tbl_tags",'*',$condition);
		}
		
		// echo "<pre>";
		// print_r($result);
		// exit;
		//echo $user_id;
		$this->load->view('main-header.php');
		$this->load->view('addEdit.php',$result);
		$this->load->view('footer.php');
	}

	function submitformtesting(){
		// codeigniter form validation 
		// $this->form_validation->set_rules('themename', 'Themename', 'trim|required|alpha');
		// $this->form_validation->set_rules('mainvideo', 'Main video', 'trim|required');
		// $this->form_validation->set_rules('meta_title', 'Meta title', 'trim|required');
		// $this->form_validation->set_rules('meta_description', 'Meta description', 'trim|required');
		// $this->form_validation->set_rules('mainvideo', 'Main video', 'trim|required');

		// if($this->form_validation->run()) {  
		// 	// true  if validation property sectifice properly
		// 	$themename = $this->input->post('themename');  
		// 	$mainvideo = $this->input->post('mainvideo'); 
		// 	$themename = $this->input->post('meta_title');  
		// 	$mainvideo = $this->input->post('meta_description'); 
		// 	$themename = $this->input->post('mainvideo');  
		// 	$mainvideo = $this->input->post('meta_title'); 
		// }else{  
		// 	// if validation is not proper
		// 	$this->index();  
		// } 
	}


	function submitForm(){
		// echo "<pre>";	
		// print_r($_POST);
		// print_r($_FILES);
		// exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();
			$data['vimeo_link'] = $this->input->post('vimeo_link');
			$data['banner_heading'] = $this->input->post('banner_heading');
			$data['home_description'] = $this->input->post('home_description');
			$data['banner_subtext'] = $this->input->post('banner_subtext');
			$data['section1_heading'] = $this->input->post('section1_heading');
			$data['section1_paragraph'] = $this->input->post('section1_paragraph');
			$data['stat_one_no'] = $this->input->post('stat_one_no');
			$data['stat_one_desc'] = $this->input->post('stat_one_desc');
			$data['stat_two_no'] = $this->input->post('stat_two_no');
			$data['stat_two_desc'] = $this->input->post('stat_two_desc');
			$data['stat_three_no'] = $this->input->post('stat_three_no');
			$data['stat_three_desc'] = $this->input->post('stat_three_desc');
			$data['section2_heading'] = $this->input->post('section2_heading');
			$data['section2_paragraph'] = $this->input->post('section2_paragraph');
			$data['meta_title'] = $this->input->post('meta_title');
			$data['meta_description'] = $this->input->post('meta_description');


			$home_banner_image = "";
			if(isset($_FILES) && isset($_FILES["home_banner_image"]["name"])){
				$this->load->library('upload');
				$this->upload->initialize($this->set_upload_options());
				if (!$this->upload->do_upload("home_banner_image")){
					$image_error = array('error' => $this->upload->display_errors());
					echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
					exit;
				}else{
					$image_data = array('upload_data' => $this->upload->data());
					$home_banner_image = $image_data['upload_data']['file_name']; 
					$this->setCompresseredImage($image_data['upload_data']['file_name']);
				}	
				/* Unlink previous category image */
				if(!empty($this->input->post('about_us_id'))){	
					$condition_image = " about_us_id = ".$this->input->post('about_us_id');
					$image =$this->common->getData("tbl_about_us",'*',$condition_image);
					if(is_array($image) && !empty($image[0]->home_banner_image) && file_exists(DOC_ROOT_FRONT."/images/aboutus/".$image[0]->home_banner_image))
					{
						unlink(DOC_ROOT_FRONT."/images/aboutus/".$image[0]->home_banner_image);
					}
				}
			}else{
				$home_banner_image = $this->input->post('pre_home_banner_image');
			}

			$banner_image = "";
			if(isset($_FILES) && isset($_FILES["banner_image"]["name"])){
				$this->load->library('upload');
				$this->upload->initialize($this->set_upload_options());
				if (!$this->upload->do_upload("banner_image")){
					$image_error = array('error' => $this->upload->display_errors());
					echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
					exit;
				}else{
					$image_data = array('upload_data' => $this->upload->data());
					$banner_image = $image_data['upload_data']['file_name']; 
					$this->setCompresseredImage($image_data['upload_data']['file_name']);
				}	
				/* Unlink previous category image */
				if(!empty($this->input->post('about_us_id'))){	
					$condition_image = " about_us_id = ".$this->input->post('about_us_id');
					$image =$this->common->getData("tbl_about_us",'*',$condition_image);
					if(is_array($image) && !empty($image[0]->banner_image) && file_exists(DOC_ROOT_FRONT."/images/aboutus/".$image[0]->banner_image))
					{
						unlink(DOC_ROOT_FRONT."/images/aboutus/".$image[0]->banner_image);
					}
				}
			}else{
				$banner_image = $this->input->post('pre_banner_image');
			}



			$section1_image = "";
			if(isset($_FILES) && isset($_FILES["section1_image"]["name"])){
				$this->load->library('upload');
				$this->upload->initialize($this->set_upload_options());
				if (!$this->upload->do_upload("section1_image")){
					$image_error = array('error' => $this->upload->display_errors());
					echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
					exit;
				}else{
					$image_data = array('upload_data' => $this->upload->data());
					$section1_image = $image_data['upload_data']['file_name']; 
					$this->setCompresseredImage($image_data['upload_data']['file_name']);
				}	
				/* Unlink previous category image */
				if(!empty($this->input->post('about_us_id'))){	
					$condition_image = " about_us_id = ".$this->input->post('about_us_id');
					$image =$this->common->getData("tbl_about_us",'*',$condition_image);
					if(is_array($image) && !empty($image[0]->section1_image) && file_exists(DOC_ROOT_FRONT."/images/aboutus/".$image[0]->section1_image))
					{
						unlink(DOC_ROOT_FRONT."/images/aboutus/".$image[0]->section1_image);
					}
				}
			}else{
				$section1_image = $this->input->post('pre_section1_image');
			}


			$section2_image = "";
			if(isset($_FILES) && isset($_FILES["section2_image"]["name"])){
				$this->load->library('upload');
				$this->upload->initialize($this->set_upload_options());
				if (!$this->upload->do_upload("section2_image")){
					$image_error = array('error' => $this->upload->display_errors());
					echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
					exit;
				}else{
					$image_data = array('upload_data' => $this->upload->data());
					$section2_image = $image_data['upload_data']['file_name']; 
					$this->setCompresseredImage($image_data['upload_data']['file_name']);
				}	
				/* Unlink previous category image */
				if(!empty($this->input->post('about_us_id'))){	
					$condition_image = " about_us_id = ".$this->input->post('about_us_id');
					$image =$this->common->getData("tbl_about_us",'*',$condition_image);
					if(is_array($image) && !empty($image[0]->section2_image) && file_exists(DOC_ROOT_FRONT."/images/aboutus/".$image[0]->section2_image))
					{
						unlink(DOC_ROOT_FRONT."/images/aboutus/".$image[0]->main_image);
					}
				}
			}else{
				$section2_image = $this->input->post('pre_section2_image');
			}

			$data['updated_on'] = date("Y-m-d H:i:s");
			$data['updated_by'] = $this->session->userdata('footage_farm_admin')[0]['user_id'];
			$data['banner_image'] = $banner_image;
			$data['home_banner_image'] = $home_banner_image;
			$data['section1_image'] = $section1_image ;
			$data['section2_image'] = $section2_image;

			if(!empty($this->input->post('about_us_id'))){
				// print_r($data);
				$condition = "about_us_id = '".$this->input->post('about_us_id')."' ";
				$result = $this->common->updateData("tbl_about_us",$data,$condition);
				if($result){
					echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while updating data.'));
					exit;
				}
			}else{
				$data['created_on'] = date("Y-m-d H:i:s");
				$data['created_by'] =  $this->session->userdata('footage_farm_admin')[0]['user_id'];
				$result = $this->common->insertData('tbl_about_us',$data,'1');
				if(!empty($result)){
					echo json_encode(array('success'=>true, 'msg'=>'Record Added Successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
					exit;
				}
			}
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem While Add/Edit Data.'));
			exit;
		}
	}


	function set_upload_options($file_name = ""){   
		$config = array();
		if(!empty($file_name) &&  $file_name != ""){
			$config['file_name']   = $file_name.time();
		}
		$config['upload_path'] = DOC_ROOT_FRONT."/images/aboutus/";
		if(!is_dir($config['upload_path'])){
			mkdir($config['upload_path'],0777, true);		
		}
		$config['allowed_types'] = 'gif|jpg|png|jpeg|svg|gif|mp4';
		$config['max_size']      = '0';
		$config['overwrite']     = FALSE;
		// $config['min_width']            = 1000;
		// $config['min_height']           = 1000;
		return $config;
	}
	public function setCompresseredImage($image_name){
		$config1 = array();
		$this->load->library('image_lib');
		$config1['image_library'] = 'gd2';
		$config1['source_image'] = DOC_ROOT_FRONT."/images/aboutus/".$image_name;
		$config1['maintain_ratio'] = TRUE;
		$config1['quality'] = '100%';
		// $config1['width'] = 1000;
		// $config1['height'] = 1000;
		$config1['new_image'] = DOC_ROOT_FRONT."/images/aboutus/".$image_name;
		$this->image_lib->initialize($config1);
		$this->image_lib->resize();
		$this->image_lib->clear();
	} 
}