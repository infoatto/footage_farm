<body>
	<div class="wrapper">
		<div class="main-panel">
			<div class="container">
				<div class="page-inner">
					<div class="col-12">
						<div class="row ff-title-block">
							<div class="col-12 col-sm-12 col-md-6 col-lg-6">
									<h1 class="ff-page-title">All Videos</h1> 
							</div>
							<div class="col-12 col-sm-12 col-md-6 col-lg-6">  
									<a href="<?= base_url('video/addEdit')?>" class="btn btn-primary ml-2 ff-page-title-btn">
										<i class="fas fa-plus"></i> 
										Add New Video					
									</a>	
									<a href="<?= base_url('video/import')?>" class="btn btn-primary ml-2 ff-page-title-btn">
										<i class="fas fa-file-import"></i> 
										Import Videos
									</a>	
							</div>
						</div>
					</div>					
					<div class="row">
							<div class="col-sm-12">
									<div class="card">
											<div class="card-body">
												<div class="table-responsive">
													<table id="add-row" class="display table table-striped table-hover" >
														<thead>
															<tr>
																<th>Footage</th>
																<th>Reel No.</th>
																<th>Video Title</th>
																<th>Themes</th>
																<th style="width: 5%">Actions</th>
															</tr>
														</thead>
														<tfoot>
															<tr>
															<th>Footage</th>
																<th>Reel No.</th>
																<th>Video Title</th>
																<th>Themes</th>
																<th>Actions</th>
															</tr>
														</tfoot>
														<tbody>
															<?php foreach ($video_data as $key => $value) {?>
																<tr>
																	
																	<!-- <td><a href="#/"><img src="assets/images/footage-thumb.jpg" alt=""></a></td> -->
																	<td><iframe src="<?= $value['vimeo_url']?>" height="60" width="80" title="Iframe Example"></iframe></td>
																	<td>#<?= $value['reel_number']?></td>
																	<td><?= $value['video_title']?></td>
																	<td><?= $value['theme_name']?></td>
																	<td>
																		<div class="form-button-action">
																		<a href="<?= base_url('video/addEdit?text='.rtrim(strtr(base64_encode("id=".$value['video_id']), '+/', '-_'), '=').'')?>" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Edit">
																					<i class="fa fa-edit"></i>
																				</a>
																			<!-- <a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																				<i class="fa fa-times"></i>
																			</a> -->
																		</div>
																	</td>
																</tr>
															<?php } ?>
														</tbody>
													</table>
												</div>
											</div>
									</div>
							</div>
					</div>
				</div>
			</div>
			
		</div>						
	</div>
	<script >
		$(document).ready(function() {
			// Add Row
			$('#add-row').DataTable({
				"pageLength": 5,
			});			
		});
	</script>
</body>
</html>
