<div class="wrapper">
	<div class="main-panel">
		<div class="container">
			<div class="page-inner">
				<div class="col-12">
					<div class="row ff-title-block">
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">
							<h1 class="ff-page-title">Import Master Video</h1> 
						</div>
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">  
                            <a href="<?= base_url('video')?>" class="btn btn-primary ml-2 ff-page-title-btn">
								Cancel
                            </a>
                            <a href="<?= base_url('assets/samplefiles/videos.xls') ?>" class="btn btn-primary ml-2 ff-page-title-btn">
                                Download Sample XLS
                            </a>
                        </div>
					</div>
				</div>					
				<div class="row">
					<div class="col-sm-12">
						<div class="card">
							<div class="card-body">
                                <form class="" id="form-importvideo" method="post" enctype="multipart/form-data">	
                                <input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>"  class="txt_csrfname" type="hidden">
                                    <div class="input-group mb-6">
                                        <div class="form-group col-sm-10">
                                            <input type="file" class="form-control " name="videofile" id="videofile" >	
                                        </div>	
                                        <div class="form-group col-sm-2">
                                            <button type="submit" class="btn btn-primary">Upload</button>
                                        </div>	
                                    </div>
                                    <div class="col-sm-12" id="importResult" style="max-height:400px;overflow-y:auto;display:none;"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    // use for form submit
    var vRules = {
        videofile:{required:true},
    };
    var vMessages = {
        videofile:{required:"Please Select File for Import."},
    };
    $("#form-importvideo").validate({
        ignore:[],
        rules: vRules,
        messages: vMessages,
        submitHandler: function(form){	
            $("#importResult").html("");
            $("#importResult").hide();
            // swal("please wait..!", {button: false,closeOnClickOutside: false});
            var act = "<?= base_url('video/importFiles')?>";
            $("#form-importvideo").ajaxSubmit({
                url: act, 
                type: 'post',
                dataType: 'json',
                cache: false,
                clearForm: false,
                async:false,
                beforeSubmit : function(arr, $form, options){
                    $(".btn-primary").hide();
                },
                success: function(response){
                    // swal.close();
                    $("#importResult").html(response.msg);
                    $("#importResult").show();
                    $(".btn-primary").show();
                }
            });
        }
    });
</script>