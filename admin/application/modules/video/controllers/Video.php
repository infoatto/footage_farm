<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Video extends MX_Controller
{
	
	public function __construct(){
		parent::__construct();
		checklogin();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('videomodel','',TRUE);
	}

	public function index(){
		$result = array();
		$condition = "1=1";
		$main_table = array("tbl_videos as tv", array("tv.*"));
		$join_tables =  array();
		$join_tables = array(
							array("", "tbl_video_theme_subtheme_mapping as  ttsm", "ttsm.video_id = tv.video_id", array("ttsm.theme_id")),
							array("", "tbl_themes as  tt", "tt.theme_id = ttsm.theme_id", array("ttsm.theme_id,GROUP_CONCAT(tt.theme_name) as theme_name")),
							);
	
		$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("tv.video_id" => "DESC"),"tv.video_id",null); 
		 // fetch query
		$result['video_data'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result

		// $result['video_data'] = $this->common->getData("tbl_videos",'*',$condition);
		// echo "<pre>";
		// print_r($result);
		// exit;
		$this->load->view('main-header.php');
		$this->load->view('index.php',$result);
		$this->load->view('footer.php');
	}

	public function addEdit(){

		$video_id = "";
		//print_r($_GET);
		$result = array();
		if(!empty($_GET['text']) && isset($_GET['text'])){
			$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
			parse_str($varr,$url_prams);
			$video_id = $url_prams['id'];
			$condition = "video_id ='".$video_id."' ";
			$result['video_data'] = $this->common->getData("tbl_videos",'*',$condition);
			$tags_data= $this->common->getData("tbl_video_tag_mapping",'*',$condition);

			$tags_video_data = array();
			if(!empty($tags_data)){
				foreach ($tags_data as $key => $value) {
					// print_r($value['tag_id']);
					array_push($tags_video_data,$value['tag_id']);
				}
			}
			$result['tags_data'] = $tags_video_data;
			$result['theme_data'] = $this->common->getData("tbl_video_theme_subtheme_mapping",'*,group_concat(sub_theme_id) as total_sub_themes',$condition,"video_theme_id","ASC","theme_id");
			// echo $this->db->last_query();

			$main_table = array("tbl_video_country_location_mapping as vclm", array("vclm.*"));
			$join_tables =  array();
			$join_tables = array(
								array("", "tbl_country_location as  cl", "vclm.country_location_id = cl.country_location_id", array("group_concat(cl.country_location_id) as country_location_id, cl.country_id,cl.location_name"))
								);
		
			$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("vclm.video_country_location_id" => "ASC"),"cl.country_id",null); 
			 // fetch query
			$result['country_location_data'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result
			// $result['country_location_data'] = $this->common->getData("tbl_video_country_location_mapping",'*',$condition);
			

		}
		$condition = "1=1";
		$result['countries'] = $this->common->getData("countries",'*',$condition);
		$condition = "status = 'Active' ";
		// $result['locations'] = $this->common->getData("tbl_location",'*',$condition);
		$result['colors'] = $this->videomodel->enum_select("tbl_videos","colour_id");
		$result['sounds'] = $this->videomodel->enum_select("tbl_videos","sound_id");
		$result['years'] = $this->videomodel->enum_select("tbl_videos","year");
		$result['tags'] = $this->common->getData("tbl_tags",'*',$condition);
		$result['themes'] = $this->common->getData("tbl_themes",'*',$condition);

		// echo "<pre>";
		// 	print_r($result['country_location_data']);
		// 	exit;
		//echo $user_id;
		$this->load->view('main-header.php');
		$this->load->view('addEdit.php',$result);
		$this->load->view('footer.php');
	}

	public function import(){
		$this->load->view('main-header.php');
		$this->load->view('import.php');
		$this->load->view('footer.php');
	}

	function submitformtesting(){
		// codeigniter form validation 
		// $this->form_validation->set_rules('videoname', 'videoname', 'trim|required|alpha');
		// $this->form_validation->set_rules('mainvideo', 'Main video', 'trim|required');
		// $this->form_validation->set_rules('meta_title', 'Meta title', 'trim|required');
		// $this->form_validation->set_rules('meta_description', 'Meta description', 'trim|required');
		// $this->form_validation->set_rules('mainvideo', 'Main video', 'trim|required');

		// if($this->form_validation->run()) {  
		// 	// true  if validation property sectifice properly
		// 	$videoname = $this->input->post('videoname');  
		// 	$mainvideo = $this->input->post('mainvideo'); 
		// 	$videoname = $this->input->post('meta_title');  
		// 	$mainvideo = $this->input->post('meta_description'); 
		// 	$videoname = $this->input->post('mainvideo');  
		// 	$mainvideo = $this->input->post('meta_title'); 
		// }else{  
		// 	// if validation is not proper
		// 	$this->index();  
		// } 
	}

	public function delete_row(){
		// print_r($_POST);
		// exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

			$condition="video_id = '".$_POST['video_id']."' AND ".$_POST['column_name']." IN (".$_POST['id'].") ";
			// echo $condition;
			// exit;
			$result = $this->common->deleteRecord($_POST['tbl_name'],$condition);
			if($result){
				echo json_encode(array('success'=>true, 'msg'=>'Record deleted Successfully.'));
				exit;
			}else{
				echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
				exit;
			}
		}
		// echo "<pre>";
		// print_r($_POST);
		// exit;

	}

	public  function getLocation(){
		// print_r($_POST);
		// exit;
		// if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
		// 	$data = array();
		// 	$condition = " 1=1 AND country_id = '".$this->input->post('country_id')."' AND location_name like '%".$this->input->post('location_name')."%' ";
		// 	$getlocation = $this->common->getData("tbl_country_location_mapping",'*',$condition);
		// 	// echo $this->db->last_query();
		// 	if($getlocation){
		// 		// $list_location = array();
		// 		foreach ($getlocation as $key => $value) {
		// 			$list_location[] = $value['location_name'];
		// 		}
		// 		echo json_encode($list_location);
		// 	}
		// }
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();
			$condition = " 1=1 AND country_id = '".$this->input->post('country_id')."' ";
			$getlocation = $this->common->getData("tbl_country_location",'*',$condition);
			// echo $this->db->last_query();
			if($getlocation){
				$option         = '';
				$country_location_id = '';
				
				if(isset($_REQUEST['country_location_id']) && !empty($_REQUEST['country_location_id'])) {
					$country_location_id = explode(",",$_REQUEST['country_location_id']);
				}
				if(!empty($getlocation)) 
				{
					for ($i = 0; $i < sizeof($getlocation); $i++) 
					{	$sel ='';
						if(isset($_REQUEST['country_location_id']) && !empty($_REQUEST['country_location_id'])) {
							$sel = (in_array($getlocation[$i]['country_location_id'],$country_location_id))? 'selected="selected"' : '';
						}
						$option .= '<option value="' . $getlocation[$i]['country_location_id'] . '" ' . $sel . ' >' . $getlocation[$i]['location_name']. '</option>';
					}
				}
				echo json_encode(array("status" => "success", "option" => $option));
				exit;
			}
		}
	}
 
	public function getSubtheme(){

		// echo "<pre>";
		// print_r($_POST);
			
		$result = $this->common->getData("tbl_sub_themes", "*", "theme_id = '".$_REQUEST['theme_id']."' ");
		// print_r($result);
		// exit;
	   $option         = '';
	   $sub_theme_id = '';
	   
	   if(isset($_REQUEST['sub_theme_id']) && !empty($_REQUEST['sub_theme_id'])) {
		  $sub_theme_id = explode(",",$_REQUEST['sub_theme_id']);
	   }
	   if(!empty($result)) 
	   {
		   for ($i = 0; $i < sizeof($result); $i++) 
		   {	$sel ='';
			if(isset($_REQUEST['sub_theme_id']) && !empty($_REQUEST['sub_theme_id'])) {
				$sel = (in_array($result[$i]['sub_theme_id'],$sub_theme_id))? 'selected="selected"' : '';
			}
			   $option .= '<option value="' . $result[$i]['sub_theme_id'] . '" ' . $sel . ' >' . $result[$i]['sub_theme_name']. '</option>';
		   }
	   }
	   echo json_encode(array("status" => "success", "option" => $option));
	   exit;
	}

 	public function submitForm(){
		// print_r($_POST);
		// print_r($_FILES);
		// exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();
			
			$thumnail_value = "";
			if(isset($_FILES) && isset($_FILES["videofile"]["name"])){
				 $config = array();
				$config['upload_path'] = DOC_ROOT_FRONT."/images/video_image/";
				$config['max_size']    = '0';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$config['min_width']            = 1000;
				$config['min_height']           = 1000;
				$config['file_name']     = $_FILES["videofile"]["name"];
				$this->load->library('upload', $config);
				if (!$this->upload->do_upload("videofile")){
					$image_error = array('error' => $this->upload->display_errors());
					echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
					exit;
				}else{
					$image_data = array('upload_data' => $this->upload->data());
					$thumnail_value = $image_data['upload_data']['file_name']; 
					$this->setCompresseredImage($image_data['upload_data']['file_name']);
				}	
				/* Unlink previous category image */
				if(!empty($this->input->post('video_id'))){	
					$condition_image = " video_id = ".$this->input->post('video_id');
					$image =$this->common->getData("tbl_videos",'*',$condition_image);
					if(is_array($image) && !empty($image[0]->thumbnail_image) && file_exists(DOC_ROOT_FRONT."/images/video_thumbnail_image/".$image[0]->thumbnail_image))
					{
						unlink(DOC_ROOT_FRONT."/images/video_thumbnail_image/".$image[0]->thumbnail_image);
					}
				}
			}else{
				$thumnail_value = $this->input->post('pre_videofile_name');
			}

			$data['thumbnail_image'] = $thumnail_value;
			$data['video_title'] = $this->input->post('videotitle');
			$data['short_description'] = $this->input->post('short_description');
			$data['long_description'] = $this->input->post('long_description');
			$data['status'] = $this->input->post('video-status');
			$data['reel_number'] = $this->input->post('reelno');
			$data['year'] = $this->input->post('year');
			//year foramtion
			$years = $this->input->post('year');
			$extractedYears = array();
			if(!empty($years)){
				$yearsArray = explode(",",$years);
				if(!empty($yearsArray)){
					foreach($yearsArray as $key=>$val){
						if(strpos($val, "-")){
							$year_range_exploded = explode("-",$val); 
							$start_year = $year_range_exploded[0];
							$end_year = $year_range_exploded[1];
							$yearRangeArray = range($start_year,$end_year,1);
							if(!empty($yearRangeArray)){
								$extractedYears = array_merge($extractedYears,$yearRangeArray);
							}
						}else if(strpos($val, "s") || strpos($val, "S")){
							$yearString = preg_replace("/[^0-9]/", "",$val);
							$start_year = substr_replace($yearString,"0",3);
							$end_year = $start_year+9; 
							$yearRangeArray = range($start_year,$end_year,1);
							if(!empty($yearRangeArray)){
								$extractedYears = array_merge($extractedYears,$yearRangeArray);
							}
						}else{
							array_push($extractedYears,$val);
						}
					}
				}
			}
			$data['year_filter'] = implode(",",array_unique($extractedYears));
			//year filter data end
			$data['colour_id'] = $this->input->post('color');
			$data['sound_id'] = $this->input->post('sound');
			$data['tc_begins'] = $this->input->post('tc_begin');
			$data['tc_ends'] = $this->input->post('tc_end');
			$data['duration'] = $this->input->post('duration');
			$data['vimeo_url'] = $this->input->post('vimeourl');
			$data['meta_title'] = $this->input->post('meta_title');
			$data['meta_description'] = $this->input->post('meta_description');
			$data['updated_on'] = date("Y-m-d H:i:s");
			$data['updated_by'] = $this->session->userdata('footage_farm_admin')[0]['user_id'];

			if(!empty($this->input->post('video_id'))){
				$video_id = $this->input->post('video_id');
				$condition = "video_id = '".$this->input->post('video_id')."' ";
				$result = $this->common->updateData("tbl_videos",$data,$condition);
				if($result){
					$all_country_id = $all_location_id = '';
					if(!empty($_POST['country'])){
						$all_country_id = implode(",",array_unique($_POST['country']));
					}
					if(!empty($_POST['location'])){
						$all_location_id = implode(",",array_unique($_POST['location']));
					}

					if($all_country_id && $all_location_id){
						$condition = "1=1 && country_id IN (".$all_country_id.") && country_location_id IN (".$all_location_id.")";
						$get_selected_country_location = $this->common->getData("tbl_country_location",'*',$condition);

						if($get_selected_country_location){
							$data_country_location_map = array();
							$condition = "video_id = '".$this->input->post('video_id')."' ";
							$this->common->deleteRecord("tbl_video_country_location_mapping",$condition);
							foreach ($get_selected_country_location as $key => $value) {
								$data_theme_sub_theme = array();
								$data_country_location_map['video_id'] = $video_id;
								 $data_country_location_map['country_location_id'] = $value['country_location_id'];
								 $data_country_location_map['created_on'] = date("Y-m-d H:i:s");
								 $data_country_location_map['created_by'] =  $this->session->userdata('footage_farm_admin')[0]['user_id'];
								 $result_country_location_map = $this->common->insertData('tbl_video_country_location_mapping',$data_country_location_map,'1');
							}
						}
					}
					// foreach ($_POST['country'] as $key => $value) {
						// $condition = "1=1 AND country_id = '".$value."' AND location_name = '".trim(strtolower($_POST['location'][$key]))."' ";
						//  $get_country_location = $this->common->getData("tbl_country_location_mapping",'*',$condition);

						//  if(empty($get_country_location)){
						// 	 $country_location = array();
						// 	 $country_location['country_id'] = $value; ;
						// 	 $country_location['location_name'] = trim(strtolower($_POST['location'][$key]));
						// 	 $get_country_location_id = $this->common->insertData('tbl_country_location_mapping',$country_location,'1');
						//  }else{
						// 	$get_country_location_id = $get_country_location[0]['country_location_id'];
						//  }
						//  $data_country_location_map['video_id'] = $video_id;
						//  $data_country_location_map['country_location_id'] = $get_country_location_id;
						//  $data_country_location_map['location_id'] = trim(strtolower($_POST['location'][$key]));
						//  $data_country_location_map['created_on'] = date("Y-m-d H:i:s");
						//  $data_country_location_map['created_by'] =  $this->session->userdata('footage_farm_admin')[0]['user_id'];
						//  $result_country_location_map = $this->common->insertData('tbl_video_country_location_mapping',$data_country_location_map,'1');
					
					// }
					if(!empty($result_country_location_map)){
						$condition = "video_id = '".$this->input->post('video_id')."' ";
						$this->common->deleteRecord("tbl_video_tag_mapping",$condition);
						$data_tag_map = array();
						foreach ($_POST['tags'] as $key => $value) {
							$data_tag_map['video_id'] = $video_id ;
							$data_tag_map['tag_id'] = $value ;
							$data_tag_map['created_on'] = date("Y-m-d H:i:s");
							$data_tag_map['created_by'] =  $this->session->userdata('footage_farm_admin')[0]['user_id'];
							$result_data_tag_map = $this->common->insertData('tbl_video_tag_mapping',$data_tag_map,'1');
						}
							if(!empty($result_data_tag_map)){

								$all_themes_id = $all_sub_themes_id = '';
								if(!empty($_POST['theme_id'])){
									$all_themes_id = implode(",",array_unique($_POST['theme_id']));
								}
								if(!empty($_POST['sub_theme_id'])){
									$all_sub_themes_id = implode(",",array_unique($_POST['sub_theme_id']));
								}

								if($all_themes_id && $all_sub_themes_id){
									$condition = "1=1 && t.theme_id IN (".$all_themes_id.") OR ts.sub_theme_id IN (".$all_sub_themes_id.")";
									$main_table = array("tbl_themes as t", array("t.theme_id"));
									$join_tables =  array();
									$join_tables = array(
														array("left", "tbl_sub_themes as  ts", "t.theme_id = ts.theme_id", array("ts.sub_theme_id"))
														);
								
									$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "","",""); 
									 // fetch query
									$get_selected_theme_subtheme = $this->common->MySqlFetchRow($rs, "array"); // fetch result
							
									if($get_selected_theme_subtheme){
										$condition = "video_id = '".$this->input->post('video_id')."' ";
										$this->common->deleteRecord("tbl_video_theme_subtheme_mapping",$condition);
										foreach ($get_selected_theme_subtheme as $key => $value) {
											$data_theme_sub_theme = array();
											$data_theme_sub_theme['video_id'] = $video_id;
											$data_theme_sub_theme['theme_id'] = $value['theme_id'];
											$data_theme_sub_theme['sub_theme_id'] = $value['sub_theme_id'];
											$data_theme_sub_theme['created_on'] = date("Y-m-d H:i:s");
											$data_theme_sub_theme['created_by'] =  $this->session->userdata('footage_farm_admin')[0]['user_id'];
											$result_theme_sub_theme = $this->common->insertData('tbl_video_theme_subtheme_mapping',$data_theme_sub_theme,'1');
										}
									}
										if($result_theme_sub_theme){
											echo json_encode(array('success'=>true, 'msg'=>'Record updated Successfully.'));
											exit;
										}else{
											echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
											exit;
										}
								}
							}
								
					}else{
						echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
						exit;
					}

					echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while updating data.'));
					exit;
				}
			}else{
				$data['created_on'] = date("Y-m-d H:i:s");
				$data['created_by'] =  $this->session->userdata('footage_farm_admin')[0]['user_id'];
				$result = $this->common->insertData('tbl_videos',$data,'1');
				$video_id = $result;
				if(!empty($result)){
					$data_country_location_map = array();
					$all_country_id = $all_location_id = '';
					if(!empty($_POST['country'])){
						$all_country_id = implode(",",array_unique($_POST['country']));
					}
					if(!empty($_POST['location'])){
						$all_location_id = implode(",",array_unique($_POST['location']));
					}

					if($all_country_id && $all_location_id){
						$condition = "1=1 && country_id IN (".$all_country_id.") && country_location_id IN (".$all_location_id.")";
						$get_selected_country_location = $this->common->getData("tbl_country_location",'*',$condition);

						if($get_selected_country_location){
							$data_country_location_map = array();
						
							foreach ($get_selected_country_location as $key => $value) {
								$data_theme_sub_theme = array();
								$data_country_location_map['video_id'] = $video_id;
								 $data_country_location_map['country_location_id'] = $value['country_location_id'];
								 $data_country_location_map['created_on'] = date("Y-m-d H:i:s");
								 $data_country_location_map['created_by'] =  $this->session->userdata('footage_farm_admin')[0]['user_id'];
								 $result_country_location_map = $this->common->insertData('tbl_video_country_location_mapping',$data_country_location_map,'1');
							}
						}
					}
					// foreach ($_POST['country'] as $key => $value) {
					// 	$condition = "1=1 AND country_id = '".$value."' AND location_name = '".trim(strtolower($_POST['location'][$key]))."' ";
					// 	 $get_country_location = $this->common->getData("tbl_country_location_mapping",'*',$condition);

					// 	 if(empty($get_country_location)){
					// 		 $country_location = array();
					// 		 $country_location['country_id'] = $value; ;
					// 		 $country_location['location_name'] = trim(strtolower($_POST['location'][$key]));
					// 		 $get_country_location_id = $this->common->insertData('tbl_country_location_mapping',$country_location,'1');
					// 	 }else{
					// 		$get_country_location_id = $get_country_location[0]['country_location_id'];
					// 	 }
					// 	 $data_country_location_map['video_id'] = $video_id;
					// 	 $data_country_location_map['country_location_id'] = $get_country_location_id;
					// 	//  $data_country_location_map['location_id'] = trim(strtolower($_POST['location'][$key]));
					// 	 $data_country_location_map['created_on'] = date("Y-m-d H:i:s");
					// 	 $data_country_location_map['created_by'] =  $this->session->userdata('footage_farm_admin')[0]['user_id'];
					// 	 $result_country_location_map = $this->common->insertData('tbl_video_country_location_mapping',$data_country_location_map,'1');
					
					// }
					if(!empty($result_country_location_map)){
						
						$data_tag_map = array();
						foreach ($_POST['tags'] as $key => $value) {
							$data_tag_map['video_id'] = $video_id ;
							$data_tag_map['tag_id'] = $value ;
							$data_tag_map['created_on'] = date("Y-m-d H:i:s");
							$data_tag_map['created_by'] =  $this->session->userdata('footage_farm_admin')[0]['user_id'];
							$result_data_tag_map = $this->common->insertData('tbl_video_tag_mapping',$data_tag_map,'1');
						}
							if(!empty($result_data_tag_map)){


								$all_themes_id = $all_sub_themes_id = '';
								if(!empty($_POST['theme_id'])){
									$all_themes_id =implode(",",$_POST['theme_id']);
								}
								if(!empty($_POST['sub_theme_id'])){
									$all_sub_themes_id =implode(",",$_POST['sub_theme_id']);
								}

								if($all_themes_id && $all_sub_themes_id){
									// $condition = "1=1 && theme_id IN (".$all_themes_id.") && sub_theme_id IN (".$all_sub_themes_id.")";
									// $get_selected_theme_subtheme = $this->common->getData("tbl_sub_themes",'*',$condition);
									$condition = "1=1 && t.theme_id IN (".$all_themes_id.") OR ts.sub_theme_id IN (".$all_sub_themes_id.")";
									$main_table = array("tbl_themes as t", array("t.theme_id"));
									$join_tables =  array();
									$join_tables = array(
														array("left", "tbl_sub_themes as  ts", "t.theme_id = ts.theme_id", array("ts.sub_theme_id"))
														);
								
									$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "","",""); 
									 // fetch query
									$get_selected_theme_subtheme = $this->common->MySqlFetchRow($rs, "array"); // fetch result

									if($get_selected_theme_subtheme){
										foreach ($get_selected_theme_subtheme as $key => $value) {
											$data_theme_sub_theme = array();
											$data_theme_sub_theme['video_id'] = $video_id;
											$data_theme_sub_theme['theme_id'] = $value['theme_id'];
											$data_theme_sub_theme['sub_theme_id'] = $value['sub_theme_id'];
											$data_theme_sub_theme['created_on'] = date("Y-m-d H:i:s");
											$data_theme_sub_theme['created_by'] =  $this->session->userdata('footage_farm_admin')[0]['user_id'];
											$result_theme_sub_theme = $this->common->insertData('tbl_video_theme_subtheme_mapping',$data_theme_sub_theme,'1');
										}
									}
										if($result_theme_sub_theme){
											echo json_encode(array('success'=>true, 'msg'=>'Record Added Successfully.'));
											exit;
										}else{
											echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
											exit;
										}
								}
							}
								
					}else{
						echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
						exit;
					}
							
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
					exit;
				}
			}
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem While Add/Edit Data.'));
			exit;
		}
	}

	public function importFiles(){
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$header = "" ;
			$result_array = array();
			$size = $_FILES['videofile']['size'];
			$name = $_FILES['videofile']['name'];
			$names = explode(".", $name);
			//check the valid file format or not
			if ((end($names) != "xls" && end($names)!="xlsx" && end($names)!="XLS" && end($names)!="XLSX")){
				$result_array[] = "<span class='text-danger'>Inavlid File ..!</span>";
				echo json_encode(array("msg"=>implode("<br>",$result_array)));
				exit;
			}
			// //Load the excel library
			$this->load->library('excel');
			// //Read file from path
			$objPHPExcel = PHPExcel_IOFactory::load($_FILES['videofile']['tmp_name']);
			$video_Data = $objPHPExcel->setActiveSheetIndex(0)->toArray(null,false,false,true);
			$theme_Data = $objPHPExcel->setActiveSheetIndex(1)->toArray(null,false,false,true);
			$location_Data = $objPHPExcel->setActiveSheetIndex(2)->toArray(null,false,false,true);
			if(!empty($video_Data) && !empty($theme_Data) && !empty($location_Data)){
				$video_Data = array_filter($video_Data);
				unset($video_Data[1]);
				$video_Data = array_values($video_Data);
				$theme_Data = array_filter($theme_Data);
				unset($theme_Data[1]);
				$theme_reel_numbers = array_unique(array_column($theme_Data,"A"));
				$theme_Data = array_values($theme_Data);
				$location_Data = array_filter($location_Data);
				unset($location_Data[1]);
				$location_reel_numbers = array_unique(array_column($location_Data,"A"));
				$location_Data = array_values($location_Data);
				foreach($video_Data as $key=>$val){
					//------import process start--------//
					if($val['A'] != "" && $val['B'] != "" && $val['C'] != ""){
						//get themes by sheet
						$theme_array = array();
						$theme_flag = "true";
						$count = 0;
						while($theme_flag == "true"){
							$indexKey = array_search($val['A'], array_column($theme_Data, 'A'));
							if($indexKey > -1){
								$theme_array[] = $theme_Data[$indexKey];
								unset($theme_Data[$indexKey]);
								$theme_Data = array_values($theme_Data);
							}else{
								$theme_flag = "false";
							}
						}

						//creating theme and subtheme array
						$theme_error_flag = "false";
						$subtheme_error_flag = "false";
						if(!empty($theme_array)){
							foreach($theme_array as $tkey=>$tval){
								$existing_theme_data = $this->common->getData("tbl_themes",'*',array("theme_name"=>trim($tval['B'])));
								if(empty($existing_theme_data)){
									$result_array[] = "<span class='text-danger'>Reel Number ".$tval['A']." theme '".$tval['B']."' not exist [ row ".($key+1)." ]</span>";
									$theme_error_flag = "true";
								}else{
									$theme_array[$tkey]['D'] = $existing_theme_data[0]['theme_id'];
									//check sub theme exist or not
									if(!empty($tval['C'])){
										$subtheme_array = explode("||",$tval['C']);
										if(!empty($subtheme_array)){
											foreach($subtheme_array as $stkey=>$stval){
												$existing_subtheme_data = $this->common->getData("tbl_sub_themes",'*',array("sub_theme_name"=>trim($stval),"theme_id"=>$existing_theme_data[0]['theme_id']));
												if(empty($existing_subtheme_data)){
													$result_array[] = "<span class='text-danger'>Reel Number ".$tval['A']." subtheme '".$stval."' not exist for theme '".$tval['B']."' [ row ".($key+1)." ]</span>";
													$subtheme_error_flag = "true";
												}else{
													$subtheme_array[$stkey] = $existing_subtheme_data[0]['sub_theme_id'];
												}
											}
											if($subtheme_error_flag == "false"){
												$theme_array[$tkey]['C'] = $subtheme_array;
											}
										}
									}
								}
								
							}
						}

						//get location by sheet
						$location_array =array();
						$location_flag = "true";
						while($location_flag == "true"){
							$indexKey = array_search($val['A'], array_column($location_Data, 'A'));
							if($indexKey > -1){
								$location_array[] = $location_Data[$indexKey];
								unset($location_Data[$indexKey]);
								$location_Data = array_values($location_Data);
							}else{
								$location_flag = "false";
							}
						}

						//creating country and location array
						$country_error_flag = "false";
						$location_error_flag = "false";
						if(!empty($location_array)){
							foreach($location_array as $lkey=>$lval){
								//check the country name is valid or not
								$existing_country_data = $this->common->getData("countries",'*',array("country_name"=>trim($lval['B'])));
								if(empty($existing_country_data)){
									$result_array[] = "<span class='text-danger'>Reel Number ".$lval['A']." Country '".$lval['B']."' name incorrect [ row ".($key+1)." ]</span>";
									$theme_error_flag = "true";
								}else{
									$location_array[$lkey]['D'] = $existing_country_data[0]['country_id'];
									//create location names array
									if(!empty($lval['C'])){
										$temp_location_array = explode("||",$lval['C']);
										if(!empty($temp_location_array)){
											foreach($temp_location_array as $clkey=>$clval){
												//check the location name is valid or not
												$existing_county_location_data = $this->common->getData("tbl_country_location",'*',array("location_name"=>trim($clval),"country_id"=>$existing_country_data[0]['country_id']));
												if(empty($existing_county_location_data)){
													$result_array[] = "<span class='text-danger'>Reel Number ".$lval['A']." Location '".$clval."' not exist for country '".$lval['B']."' [ row ".($key+1)." ]</span>";
													$location_error_flag = "true";
												}else{
													$temp_location_array[$clkey] = $existing_county_location_data[0]['country_location_id'];
												}
											}
											if($location_error_flag == "false"){
												$location_array[$lkey]['C'] = $temp_location_array;
											}
										}
									}
								}

							}
						}

						$tags_flag = "false";
						if(!empty($val['M'])){
							$tags_array = explode("||",$val['M']);
							foreach($tags_array as $tagkey=>$tagval){
								//check the tags are exist
								$existing_tags = $this->common->getData("tbl_tags",'*',array("tag_name"=>trim($tagval)));
								if(empty($existing_tags)){
									$result_array[] = "<span class='text-danger'>Reel Number ".$val['A']." '".$tagval."' Tag not exist [ row ".($key+1)." ]</span>";
									$tags_flag = "true";		
								}else{
									$tags_array[$tagkey]  = $existing_tags[0]['tag_id'];
								}
							}
							if($tags_flag == "false"){
								$val['M'] = $tags_array;
							}
						}else{
							$result_array[] = "<span class='text-danger'>Reel Number ".$val['A']." Tags are empty [ row ".($key+1)." ]</span>";
							$tags_flag = "true";
						}
						//tags checking

						if(!empty($val['A'])){
							if($theme_error_flag == "false" && $subtheme_error_flag == "false" && $country_error_flag == "false" && $location_error_flag == "false" && $tags_flag == "false"){
								if(in_array($val['A'],$theme_reel_numbers)){
									if(in_array($val['A'],$location_reel_numbers)){
										$existing_video_details = $this->common->getData("tbl_videos",'*',array("reel_number"=>$val['A']));
										if(empty($existing_video_details)){
											$data = array();
											$data['reel_number'] = $val['A'];
											$data['video_title'] = $val['B'];
											$data['year'] = $val['C'];
											$data['colour_id'] = $val['D'];
											$data['sound_id'] = $val['E'];
											$data['tc_begins'] = $val['F'];
											$data['tc_ends'] = $val['G'];
											$data['duration'] = $val['H'];
											$data['short_description'] ="<p>".$val['I']."</p>";
											$data['long_description'] = "<p>".$val['J']."</p>";
											$data['thumbnail_image'] = $val['K'];
											$data['vimeo_url'] = $val['L'];
											$data['meta_title'] =$val['N'];
											$data['meta_description'] = $val['O'];
											// $data['status'] = $val['P'];
											$data['updated_on'] = date("Y-m-d H:i:s");
											$data['updated_by'] = $this->session->userdata('footage_farm_admin')[0]['user_id'];
											$data['created_on'] = date("Y-m-d H:i:s");
											$data['created_by'] = $this->session->userdata('footage_farm_admin')[0]['user_id'];
											$video_id = $this->common->insertData('tbl_videos',$data,'1');
											if($video_id){
												//import tags 
												if(!empty($val['M'])){
													foreach($val['M'] as $tagkey=>$tagval){
														//check existing tags mapping
														$existing_mapped_data = $this->common->getData("tbl_video_tag_mapping",'*',array("video_id"=>$video_id,"tag_id"=>$tagval));
														if(empty($existing_mapped_data)){
															$tag_mapping_data['video_id'] = $video_id; 
															$tag_mapping_data['tag_id'] = $tagval; 
															$tag_mapping_data['updated_on'] = date("Y-m-d H:i:s");
															$tag_mapping_data['updated_by'] = $this->session->userdata('footage_farm_admin')[0]['user_id'];
															$tag_mapping_data['created_on'] = date("Y-m-d H:i:s");
															$tag_mapping_data['created_by'] = $this->session->userdata('footage_farm_admin')[0]['user_id'];
															$this->common->insertData('tbl_video_tag_mapping',$tag_mapping_data,'1');
														}
													}
												}

												//import theme and sub theme
												if(!empty($theme_array)){
													foreach($theme_array as $tkey=>$tval){
														$theme_mapping_data = array();
														if(!empty($tval['C'])){
															foreach($tval['C'] as $stkey=>$stval){
																$existing_mapped_data = $this->common->getData("tbl_video_theme_subtheme_mapping",'*',array("video_id"=>$video_id,"theme_id"=>$tval['D'],"sub_theme_id"=>$stval));
																if(empty($existing_mapped_data)){
																	$theme_mapping_data['video_id'] = $video_id; 
																	$theme_mapping_data['theme_id'] = $tval['D']; 
																	$theme_mapping_data['sub_theme_id'] = $stval; 
																	$theme_mapping_data['updated_on'] = date("Y-m-d H:i:s");
																	$theme_mapping_data['updated_by'] = $this->session->userdata('footage_farm_admin')[0]['user_id'];
																	$theme_mapping_data['created_on'] = date("Y-m-d H:i:s");
																	$theme_mapping_data['created_by'] = $this->session->userdata('footage_farm_admin')[0]['user_id'];
																	$this->common->insertData('tbl_video_theme_subtheme_mapping',$theme_mapping_data,'1');
																}
															}
														}else{
															//check existing mapping
															$existing_mapped_data = $this->common->getData("tbl_video_theme_subtheme_mapping",'*',array("video_id"=>$video_id,"theme_id"=>$tval['D']));
															if(empty($existing_mapped_data)){
																$theme_mapping_data['video_id'] = $video_id; 
																$theme_mapping_data['theme_id'] = $tval['D']; 
																$theme_mapping_data['updated_on'] = date("Y-m-d H:i:s");
																$theme_mapping_data['updated_by'] = $this->session->userdata('footage_farm_admin')[0]['user_id'];
																$theme_mapping_data['created_on'] = date("Y-m-d H:i:s");
																$theme_mapping_data['created_by'] = $this->session->userdata('footage_farm_admin')[0]['user_id'];
																$this->common->insertData('tbl_video_theme_subtheme_mapping',$theme_mapping_data,'1');
															}
														}
													}
												}

												//import country and location
												if(!empty($location_array)){
													foreach($location_array as $lkey=>$lval){
														$location_mapping_data = array();
														if(!empty($lval['C'])){
															foreach($lval['C'] as $clkey=>$clval){
																$existing_mapped_data = $this->common->getData("tbl_video_country_location_mapping",'*',array("video_id"=>$video_id,"country_location_id"=>$clval));
																if(empty($existing_mapped_data)){
																	$location_mapping_data['video_id'] = $video_id; 
																	$location_mapping_data['country_location_id'] = $clval; 
																	$location_mapping_data['updated_on'] = date("Y-m-d H:i:s");
																	$location_mapping_data['updated_by'] = $this->session->userdata('footage_farm_admin')[0]['user_id'];
																	$location_mapping_data['created_on'] = date("Y-m-d H:i:s");
																	$location_mapping_data['created_by'] = $this->session->userdata('footage_farm_admin')[0]['user_id'];
																	$this->common->insertData('tbl_video_country_location_mapping',$location_mapping_data,'1');
																}
															}
														}
													}
												}
											}
											$result_array[] = "<span class='text-success'>Reel Number ".$val['A']." inserted [ row ".($key+1)." ] </span>";
										}else{
											$result_array[] = "<span class='text-danger'>Reel Number is already exist [ row ".($key+1)." ]</span>";
										}
									}else{
										$result_array[] = "<span class='text-danger'>Reel Number ".$val['A']."missing in location sheet [ row ".($key+1)." ]</span>";
									}
								}else{
									$result_array[] = "<span class='text-danger'>Reel Number ".$val['A']."missing in theme sheet [ row ".($key+1)." ]</span>";
								}
							}
						}else{
							$result_array[] = "<span class='text-danger'>Reel Number empty [ row ".$key."]</span>";
						}
					}
					//------import process end--------//
				}
			}else{
				$result_array[] = "<span class='text-danger'>The uploaded file is empty ..!</span>";
			}
			echo json_encode(array("msg"=>implode("<br>",$result_array)));
			exit;
		}
	}

	public function setCompresseredImage($image_name){
		$config1 = array();
		$this->load->library('image_lib');
		$config1['image_library'] = 'gd2';
		$config1['source_image'] = DOC_ROOT_FRONT."images/video_thumbnail_image/".$image_name;
		$config1['maintain_ratio'] = TRUE;
		$config1['quality'] = '100%';
		$config1['width'] = 1000;
		$config1['height'] = 1000;
		$config1['new_image'] = DOC_ROOT_FRONT."images/video_thumbnail_image/".$image_name;
		$this->image_lib->initialize($config1);
		$this->image_lib->resize();
		$this->image_lib->clear();
	}
}