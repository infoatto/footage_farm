<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Subtheme extends MX_Controller
{
	
	public function __construct(){
		parent::__construct();
		checklogin();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('subthememodel','',TRUE);
	}

	public function index(){
		$result = array();
		$condition = "1=1";
		
		// fetch notifications 
		$condition = "1=1";
		$main_table = array("tbl_sub_themes as tst", array("tst.*"));
		$join_tables =  array();
		$join_tables = array(
							array("left", "tbl_themes as  tt", "tt.theme_id = tst.theme_id", array("tt.theme_name"))
							);
	
		$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("tst.sub_theme_id" => "DESC"),null,null); 
		 // fetch query
		$result['sub_theme_data'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result

		// $result['sub_theme_data'] = $this->common->getData("tbl_sub_themes",'*',$condition);
		// echo "<pre>";
		// print_r($result);
		// exit;
		$this->load->view('main-header.php');
		$this->load->view('index.php',$result);
		$this->load->view('footer.php');
	}

	public function addEdit(){

		$subb_theme_id = "";
		//print_r($_GET);
		$result = array();
		$condition = " 1=1 ";
		if(!empty($_GET['text']) && isset($_GET['text'])){
			$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
			parse_str($varr,$url_prams);
			$subb_theme_id = $url_prams['id'];
			$condition .= "  AND sub_theme_id ='".$subb_theme_id."'  ";
			$result['sub_theme_data'] = $this->common->getData("tbl_sub_themes",'*',$condition);
		}
		
		$result['theme'] = $this->common->getData("tbl_themes",'theme_id,theme_name');
		// echo "<pre>";
		// print_r($result);
		// exit;
		//echo $user_id;
		$this->load->view('main-header.php');
		$this->load->view('addEdit.php',$result);
		$this->load->view('footer.php');
	}

	function submitformtesting(){
		// codeigniter form validation 
		// $this->form_validation->set_rules('themename', 'Themename', 'trim|required|alpha');
		// $this->form_validation->set_rules('mainvideo', 'Main video', 'trim|required');
		// $this->form_validation->set_rules('meta_title', 'Meta title', 'trim|required');
		// $this->form_validation->set_rules('meta_description', 'Meta description', 'trim|required');
		// $this->form_validation->set_rules('mainvideo', 'Main video', 'trim|required');

		// if($this->form_validation->run()) {  
		// 	// true  if validation property sectifice properly
		// 	$themename = $this->input->post('themename');  
		// 	$mainvideo = $this->input->post('mainvideo'); 
		// 	$themename = $this->input->post('meta_title');  
		// 	$mainvideo = $this->input->post('meta_description'); 
		// 	$themename = $this->input->post('mainvideo');  
		// 	$mainvideo = $this->input->post('meta_title'); 
		// }else{  
		// 	// if validation is not proper
		// 	$this->index();  
		// } 
	}


	function submitForm(){
		// print_r($_POST);
		// exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();
			$condition = "sub_theme_name = '".$this->input->post('sub_themename')."' ";
			if($this->input->post('sub_theme_id') && $this->input->post('sub_theme_id') > 0){
				$condition .= " AND  sub_theme_id != ".$this->input->post('sub_theme_id')." ";
			}			
			$check_name = $this->common->getData("tbl_sub_themes",'*',$condition);

			if(!empty($check_name[0]->sub_theme_id)){
				echo json_encode(array("success"=>false, 'msg'=>' Sub Theme Name Already Present!'));
				exit;
			}
			
			$thumnail_value = "";
			if(isset($_FILES) && isset($_FILES["subthemefile"]["name"])){
				 $config = array();
				$config['upload_path'] = DOC_ROOT_FRONT."/images/sub_theme_image/";
				$config['max_size']    = '0';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$config['min_width']            = 1000;
				$config['min_height']           = 1000;
				$config['file_name']     = $_FILES["subthemefile"]["name"];
				$this->load->library('upload', $config);
				if (!$this->upload->do_upload("subthemefile")){
					$image_error = array('error' => $this->upload->display_errors());
					echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
					exit;
				}else{
					$image_data = array('upload_data' => $this->upload->data());
					$thumnail_value = $image_data['upload_data']['file_name']; 
					$this->setCompresseredImage($image_data['upload_data']['file_name']);
				}	
				/* Unlink previous category image */
				if(!empty($this->input->post('sub_theme_id'))){	
					$condition_image = " sub_theme_id = ".$this->input->post('sub_theme_id');
					$image =$this->common->getData("tbl_sub_themes",'*',$condition_image);
					if(is_array($image) && !empty($image[0]->sub_thumbnail_image) && file_exists(DOC_ROOT_FRONT."/images/sub_theme_image/".$image[0]->sub_thumbnail_image))
					{
						unlink(DOC_ROOT_FRONT."/images/sub_theme_image/".$image[0]->sub_thumbnail_image);
					}
				}
			}else{
				$thumnail_value = $this->input->post('pre_subthemefile_name');
			}

			$data['sub_thumbnail_image'] = $thumnail_value;
			$data['theme_id'] = $this->input->post('theme_id');
			$data['sub_theme_name'] = $this->input->post('sub_themename');
			$data['status'] = $this->input->post('theme-status');
			$data['sub_main_video'] = $this->input->post('sub_mainvideo');
			$data['sub_theme_description'] = $this->input->post('sub_theme_description');
			$data['sub_meta_title'] = $this->input->post('meta_title');
			$data['sub_meta_description'] = $this->input->post('meta_description');
			$data['updated_on'] = date("Y-m-d H:i:s");
			$data['updated_by'] = $this->session->userdata('footage_farm_admin')[0]['user_id'];
			if(!empty($this->input->post('sub_theme_id'))){
				// print_r($data);
				$condition = "sub_theme_id = '".$this->input->post('sub_theme_id')."' ";
				$result = $this->common->updateData("tbl_sub_themes",$data,$condition);
				if($result){
					echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
					exit;
				}
				else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while updating data.'));
					exit;
				}
			}else{
				$data['created_on'] = date("Y-m-d H:i:s");
				$data['created_by'] =  $this->session->userdata('footage_farm_admin')[0]['user_id'];
				$result = $this->common->insertData('tbl_sub_themes',$data,'1');
				if(!empty($result)){
					echo json_encode(array('success'=>true, 'msg'=>'Record Added Successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
					exit;
				}
			}
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem While Add/Edit Data.'));
			exit;
		}
	}

	public function setCompresseredImage($image_name){
		$config1 = array();
		$this->load->library('image_lib');
		$config1['image_library'] = 'gd2';
		$config1['source_image'] = DOC_ROOT_FRONT."/images/sub_theme_image/".$image_name;
		$config1['maintain_ratio'] = TRUE;
		$config1['quality'] = '100%';
		$config1['width'] = 1000;
		$config1['height'] = 1000;
		$config1['new_image'] = DOC_ROOT_FRONT."/images/sub_theme_image/".$image_name;
		$this->image_lib->initialize($config1);
		$this->image_lib->resize();
		$this->image_lib->clear();
	}
}