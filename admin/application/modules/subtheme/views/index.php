
<body>
	<div class="wrapper">
    
		<div class="main-panel">
			<div class="container">
				<div class="page-inner">
					<div class="col-12">
						
						<div class="row ff-title-block">
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">
							<h1 class="ff-page-title">Sub-Theme Master</h1> 
						</div>
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
							<a href="<?= base_url('subtheme/addEdit')?>" class="btn btn-primary ff-page-title-btn">
								Add sub	 theme
							</a>
						</div>
					</div>
					</div>					
					<div class="row">
							<div class="col-sm-12">
									<div class="card">
											<div class="card-body">
												<div class="table-responsive">
													<table id="add-row" class="display table table-striped table-hover" >
														<thead>
															<tr>
																<th>Sub Theme ID</th>
																<th>Theme Name</th>	
																<th>Sub Theme Name</th>
																<th>Image</th>
																<th>Description</th>																
																<th>Main Video</th>																
																<th>Actions</th>															
															</tr>
														</thead>
														<tfoot>
															<tr>
																<th>Sub Theme ID</th>
																<th>Theme Name</th>	
																<th>Sub Theme Name</th>
																<th>Image</th>
																<th>Description</th>																
																<th>Main Video</th>																
																<th>Actions</th>												
															</tr>
														</tfoot>
														<tbody>
														<?php
														// echo "<pre>";
														// print_r($sub_theme_data);
														if(!empty($sub_theme_data) && isset($sub_theme_data))	{
															$cnt = 0;
															foreach ($sub_theme_data as $key => $value) {
																	$cnt= ++$cnt?>
																	<tr>														
																		<td>#<?=$cnt?></td>
																		<td><?= $value['theme_name']?></td>	
																		<td><?= $value['sub_theme_name']?></td>	
																		<td><img src="<?= FRONT_URL.'/images/sub_theme_image/'.$value['sub_thumbnail_image']?>"class="img-thumbnail" alt="Cinque Terre" width="70" height="70"></td>																
																		<td><?= $value['sub_theme_description']?></td>			
																		<td><iframe src="<?= $value['sub_main_video']?>" height="60" width="80" title="Iframe Example"></iframe></td>													
																		<td>
																			<div class="form-button-action">
																				<a href="<?= base_url('subtheme/AddEdit?text='.rtrim(strtr(base64_encode("id=".$value['sub_theme_id']), '+/', '-_'), '=').'')?>" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Edit">
																					<i class="fa fa-edit"></i>
																				</a>
																				<!-- <a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																					<i class="fa fa-times"></i>
																				</a> -->
																			</div>
																		</td>													
																	</tr>
															<?php }
														}else{?>
																	NO themes Added so far 
														<?php }?>
														</tbody>
													</table>
												</div>
											</div>
									</div>
							</div>
					</div>
				</div>
			</div>
			
		</div>						
	</div>
	<script >
		$(document).ready(function() {
			// Add Row
			$('#add-row').DataTable({
				"pageLength": 5,
			});			
		});
	</script>
</body>
</html>
