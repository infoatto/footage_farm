
<div class="wrapper">
	<div class="main-panel">
		<div class="container">
			<div class="page-inner">
				<div class="col-12">
					<div class="row ff-title-block">
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">
							<h1 class="ff-page-title">New Sub Master Theme</h1> 
						</div>
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
							<a href="<?= base_url('subtheme')?>" class="btn btn-primary ff-page-title-btn">
								Cancel
							</a>
						</div>
					</div>
				</div>					
				<div class="row">
					<div class="col-sm-12">
						<div class="card">
							<div class="card-body">
								<form class="" id="form-sub-theme" method="post" enctype="multipart/form-data">
									<!-- <?php  print_r($sub_theme_data) ;?> -->
									<input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden">
									<input name="sub_theme_id" id="sub_theme_id" value="<?= (!empty($sub_theme_data[0]['sub_theme_id'])?$sub_theme_data[0]['sub_theme_id']:"") ?>" type="hidden">
											<div class="form-group">
												<label for="theme_id">Select theme</label>
												<select class="form-control form-control" id="theme_id" name="theme_id"> 
													<option value="">Select Theme</option>
													<?php $sel=""; foreach ($theme as $key => $value) {
														$sel = ($value['theme_id'] == $sub_theme_data[0]['theme_id']?"selected":"");?>
														<option value="<?= $value['theme_id']?>" <?= $sel?> ><?=$value['theme_name']?></option>
													<?php }	?>
												</select>
											</div>
											<div class="form-group">
												<label for="subthemefile">Sub-Theme Thumbnail Image  (Min. size 1000px X 1000px)</label>
												<?php $req= '';
												if(empty($sub_theme_data[0]['sub_thumbnail_image'])){
													$req ='required';
												}else{?>
												<br>
													<img src="<?= FRONT_URL.'/images/sub_theme_image/'.$sub_theme_data[0]['sub_thumbnail_image']?>"class="img-thumbnail" alt="Cinque Terre" width="250" height="300">
													<input name="pre_subthemefile_name" id="pre_subthemefile_name" value="<?= (!empty($sub_theme_data[0]['sub_thumbnail_image'])?$sub_theme_data[0]['sub_thumbnail_image']:"") ?>" type="hidden">
												<?php }?>
												
												<input type="file" class="form-control-file" id="subthemefile" name="subthemefile" <?= $req;?> >
											</div>
											<div class="form-group">
												<label for="sub_themename">Sub-Theme Name</label>
												<input type="text" class="form-control" id="sub_themename" name="sub_themename" value="<?= (!empty($sub_theme_data[0]['sub_theme_name'])?$sub_theme_data[0]['sub_theme_name']:"") ?>" placeholder="Early 19th Century">	
											</div>
											<div class="form-group">
												<label for="sub_theme_description">Sub-Theme Decription (Optional)</label>
												<textarea class="form-control my-text-editor" id="sub_theme_description"  name="sub_theme_description"  rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."><?= (!empty($sub_theme_data[0]['sub_theme_description'])?$sub_theme_data[0]['sub_theme_description']:"") ?></textarea>	
											</div>
											<div class="form-group">
												<label for="sub_mainvideo">Sub Theme Main video (on all the banners)</label>
												<input type="text" class="form-control" id="sub_mainvideo" value="<?= (!empty($sub_theme_data[0]['sub_main_video'])?$sub_theme_data[0]['sub_main_video']:"") ?>"  name="sub_mainvideo" placeholder="Enter url (eg. https://player.vimeo.com/video/237546641)">	
											</div>
											<div class="form-group">
												<label for="meta_title">Meta Title</label>
												<input type="text" class="form-control" id="meta_title" value="<?= (!empty($sub_theme_data[0]['sub_meta_title'])?$sub_theme_data[0]['sub_meta_title']:"") ?>"  name="meta_title" placeholder="Early 19th Century">	
											</div>
											<div class="form-group">
												<label for="meta_description">Meta Description</label>
												<textarea class="form-control" id="meta_description" name="meta_description" rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."><?= (!empty($sub_theme_data[0]['sub_meta_description'])?$sub_theme_data[0]['sub_meta_description']:"") ?></textarea>	
											</div>
											
											<div class="form-check">
												<label>Activate Sub-theme (show this theme on website)</label><br>
												<div class="switch-field">
														<?php $active = 'checked';
															 $in_active= '';
														if(!empty($sub_theme_data)){
															if($sub_theme_data[0]['status']=='Active'){
																$active = "checked";
															}else{
																$in_active = "checked";
															} 
														}?>
													<input type="radio" id="activate-theme-three" name="theme-status" value="Active" <?= $active?>/>
													<label for="activate-theme-three">Yes</label>
													<input type="radio" id="activate-theme-four" name="theme-status" value="In-active"  <?= $in_active?>/>
													<label for="activate-theme-four">No</label>
												</div>
											</div>	
											<div class="form-group">
												<button  type="submit" class="btn btn-black"><?=(!empty($sub_theme_data)?"Update":"Create")?></button>
												<a href="<?= base_url('subtheme')?>" class="btn btn-cancel btn-border">Cancel</a>
											</div>
									</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>						
</div>

<script>
$(document).ready(function(){
});


// use for form submit
var vRules = 
{
	sub_themename:{required:true},
	theme_id:{required:true},
	sub_mainvideo:{required:true},
	meta_title:{required:true},
	meta_description:{required:true},

};
var vMessages = 
{
	sub_themename:{required:"Please Enter theme Name."},
	theme_id:{required:"Please Select theme."},
	sub_mainvideo:{required:"Please Enter Main video."},
	meta_title:{required:"Please Enter Meta Title."},
	meta_description:{required:"Please Enter Meta Description."},

};

$("#form-sub-theme").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{	
		var act = "<?= base_url('subtheme/submitform')?>";
		$("#form-sub-theme").ajaxSubmit({
			url: act, 
			type: 'post',
			dataType: 'json',
			cache: false,
			clearForm: false,
			async:false,
			beforeSubmit : function(arr, $form, options){
				// $(".btn-primary").hide();
			},
			success: function(response) 
			{
				if(response.success){
					swal("done", response.msg, {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					}).then(
					function() {
						window.location = "<?= base_url('subtheme')?>";
						// $this->router->fetch_class().'/'.$this->router->fetch_method() ?>";
					});
				}else{	
					swal("Failed", response.msg, {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}
			}
		});
	}
});

document.title="Add/Edit Sub-theme"
</script>