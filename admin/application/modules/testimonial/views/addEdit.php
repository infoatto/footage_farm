
<div class="wrapper">
	<div class="main-panel">
		<div class="container">
			<div class="page-inner">
				<div class="col-12">
					<div class="row ff-title-block">
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">
							<h1 class="ff-page-title">New Testimonial</h1> 
						</div>
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
							<a href="#" class="btn btn-light btn-border ff-page-title-btn">
								Cancel
							</a>
						</div>
					</div>
				</div>					
				<div class="row">
					<div class="col-sm-12">
						<div class="card">
							<div class="card-body">
								<form class="" id="form-testimonial" method="post" enctype="multipart/form-data">
								
									<input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden">
									<input name="testimonial_id" id="testimonial_id" value="<?= (!empty($testimonial_data[0]['testimonial_id'])?$testimonial_data[0]['testimonial_id']:"") ?>" type="hidden">
								
											<div class="form-group">
												<label for="testimonial_name">Name</label>
												<input type="text" class="form-control" id="testimonial_name" name="testimonial_name" value="<?= (!empty($testimonial_data[0]['testimonial_name'])?$testimonial_data[0]['testimonial_name']:"") ?>" placeholder="eg. Bryan Schreier">	
											</div>
											
											<div class="form-group">
												<label for="designation_company">Designation</label>
												<input type="text" class="form-control" id="designation_company" value="<?= (!empty($testimonial_data[0]['designation_company'])?$testimonial_data[0]['designation_company']:"") ?>"  name="designation_company" placeholder="eg. Partner at Sequoia Capital">	
											</div>
										
											<div class="form-group">
												<label for="testimonial_text">Testimonial</label>
												<textarea class="form-control" id="testimonial_text" name="testimonial_text" rows="5" placeholder="eg. We offer a 24-48 hour turnaround service for masters we hold in our London office. If we need to duplicate the material from Washington, the delays are similar to those you would incur if you went straight to the source." spellcheck="false"><?= (!empty($testimonial_data[0]['testimonial_text'])?$testimonial_data[0]['testimonial_text']:"") ?></textarea>	
											</div>
										
											<div class="form-check">
												<label>Activate testimonial</label><br>
												<div class="switch-field">
														<?php $active = 'checked';
															 $in_active= '';
														if(!empty($testimonial_data)){
															if($testimonial_data[0]['status']=='Active'){
																$active = "checked";
															}else{
																$in_active = "checked";
															} 
														}?>
													<input type="radio" id="activate-testimonial-three" name="testimonial-status" value="Active" <?= $active?>/>
													<label for="activate-testimonial-three">Yes</label>
													<input type="radio" id="activate-testimonial-four" name="testimonial-status" value="In-active"  <?= $in_active?>/>
													<label for="activate-testimonial-four">No</label>
												</div>
											</div>	
											<div class="form-group">
												<button  type="submit" class="btn btn-black"><?=(!empty($testimonial_data)?"Update":"Create")?></button>
												<a href="<?= base_url('testimonial')?>" class="btn btn-cancel btn-border">Cancel</a>
											</div>
									</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>						
</div>

<script>
document.title="Add/Edit Testimonial"
$(document).ready(function(){
});


// use for form submit
var vRules = 
{
	testimonial_name:{required:true},
	designation_company:{required:true},
	testimonial_text:{required:true}
};
var vMessages = 
{
	testimonial_name:{required:"Please Enter Thestimonial Name."},
	designation_company:{required:"Please designation company Name."},
	testimonial_text:{required:"Please Enter Testimonial text."},
};

$("#form-testimonial").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{	
		var act = "<?= base_url('testimonial/submitform')?>";
		$("#form-testimonial").ajaxSubmit({
			url: act, 
			type: 'post',
			dataType: 'json',
			cache: false,
			clearForm: false,
			async:false,
			beforeSubmit : function(arr, $form, options){
				// $(".btn-primary").hide();
			},
			success: function(response) 
			{
				if(response.success){
					swal("done", response.msg, {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					}).then(
					function() {
						window.location = "<?= base_url('testimonial')?>";
						// $this->router->fetch_class().'/'.$this->router->fetch_method() ?>";
					});
				}else{	
					swal("Failed", response.msg, {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}
			}
		});
	}
});


</script>