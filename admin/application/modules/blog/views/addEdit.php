
    <style>
    .btn-primary, .btn-primary:hover {
        background-color: #000000;
        border-color: #000000;
        color: #fff;
    }
    </style>
    <style>
    .medium-insert-images figure figcaption,
    .mediumInsert figure figcaption,
    .medium-insert-embeds figure figcaption,
    .mediumInsert-embeds figure figcaption {
        font-size: 12px;
        line-height: 1.2em;
    }
    .medium-insert-images-slideshow figure {
        width: 100%;
    }
    .medium-insert-images-slideshow figure img {
        margin: 0;
    }
    .medium-insert-images.medium-insert-images-grid.small-grid figure {
        width: 12.5%;
    }
    @media (max-width: 750px) {
        .medium-insert-images.medium-insert-images-grid.small-grid figure {
            width: 25%;
        }
    }
    @media (max-width: 450px) {
        .medium-insert-images.medium-insert-images-grid.small-grid figure {
            width: 50%;
        }
    }
    </style>
<div class="wrapper">
	<div class="main-panel">
		<div class="container">
			<div class="page-inner">
				<ul class="breadcrumb">
					<li><a href="<?= base_url('blog')?>">Home</a></li>
					<li><a href="#">#</a></li>
				</ul>
				<div class="col-12">
					<div class="row ff-title-block">
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">
							<h1 class="ff-page-title">New Blog Post</h1> 
						</div>
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
							<a href="<?= base_url('blog')?>" class="btn btn-light btn-border ff-page-title-btn">
								Cancel
							</a>
						</div>
					</div>
				</div>					
				<div class="row">
					<div class="col-sm-12">
						<div class="card">
							<div class="card-body">
								<form class="" id="form-blog" method="post" enctype="multipart/form-data">
								
									<input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden">
									<input name="blog_id" id="blog_id" value="<?= (!empty($blog_data[0]['blog_id'])?$blog_data[0]['blog_id']:"") ?>" type="hidden">
                                            
                          	<div class="form-group">
								<label for="blog_title">Title</label>
								<input type="text" class="form-control" name="blog_title" id="blog_title" value="<?= (!empty($blog_data[0]['blog_title'])?$blog_data[0]['blog_title']:"") ?>" placeholder="eg 1910-1918- WWI  Era: Aerials, German Officer &amp; Troops.">	
							</div>
                          	<div class="form-group">
								<label for="blog_title">Blog sub-heading</label>
									<textarea class="form-control" id="blog_subheading" rows="5" name="blog_subheading" placeholder="eg. Asurprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor." spellcheck="false"><?= (!empty($blog_data[0]['blog_subheading'])?$blog_data[0]['blog_subheading']:"") ?></textarea>	
							</div>

							<div class="form-group">
								<label for="blog_slug">Slug</label>
								<input type="text" class="form-control" name="blog_slug"  id="blog_slug" value="<?= (!empty($blog_data[0]['blog_slug'])?str_replace('-', ' ',$blog_data[0]['blog_slug']):" ") ?>" id="blog_slug" name="blog_slug" placeholder="eg. 110-quick-tips-about-blogging">	
								<label for="blog_slug"><input type="checkbox" name="slug_check" id="slug_check" value="1"> Same as title</label>
							</div>
							<div class="form-group">
								<label for="author_name">Author Name</label>
								<!-- <input type="text" class="form-control" id="author_name"  name="author_name" value="<?= (!empty($blog_data[0]['author_name'])?$blog_data[0]['author_name']:"") ?>" placeholder="Author Name">	 -->
								<select class="form-control form-control" id="author_name" name="author_name"> 
									<option value="">Select Author Name</option>
									<?php $sel=""; foreach ($authors as $key => $value) {
										$sel = ($value['user_id'] == $blog_data[0]['author_id']?"selected":"");?>
										<option value="<?= $value['user_id']?>" <?= $sel?> ><?=$value['user_name']?></option>
									<?php }	?>
								</select>
							</div>
							<div class="form-group">
								<label for="blog_date">Blog Date</label>
								<input type="text" class="form-control" id="blog_date"  name="blog_date" value="<?= (!empty($blog_data[0]['blog_date'])) ? date("d-m-Y", strtotime($blog_data[0]['blog_date'])) : '';?>" placeholder="blog date ">	
							</div>

                          <div class="form-group">
							<label for="blog_post_body">Post Body</label>
							<textarea class="form-control my-text-editor" id="blog_post_body" rows="20" name="blog_post_body" placeholder="eg. Asurprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor." spellcheck="false"><?= (!empty($blog_data[0]['blog_post_body'])?$blog_data[0]['blog_post_body']:"") ?></textarea>	
						
					

                      <div class="form-group">
                          <label for="main_image">Main Image (Min. size 1000px X 1000px)</label>
                          <?php $req= '';
                          if(empty($blog_data[0]['main_image'])){
                              $req ='required';
                          }else{?>
                          <br>
                              <img src="<?= FRONT_URL.'/images/blog_main_image/'.$blog_data[0]['main_image']?>"class="img-thumbnail" alt="Cinque Terre" width="250" height="300">
                              <input name="pre_blog_main_image" id="pre_blog_main_image"  value="<?= (!empty($blog_data[0]['main_image'])?$blog_data[0]['main_image']:"") ?>" type="hidden">
                          <?php }?>
                          
                          <input type="file" class="form-control-file" id="main_image" name="main_image" <?= $req;?> >
											</div>

                          <div class="form-group">
                              <label for="thumbnail_image">Thumbnail Image (Min. size 1000px X 1000px)</label>
                              <?php $req= '';
                              if(empty($blog_data[0]['thumbnail_image'])){
                                  $req ='required';
                              }else{?>
                              <br>
                                  <img src="<?= FRONT_URL.'/images/blog_main_image/'.$blog_data[0]['thumbnail_image']?>"class="img-thumbnail" alt="Cinque Terre" width="250" height="300">
                                  <input name="pre_blog_main_image" id="pre_blog_main_image" value="<?= (!empty($blog_data[0]['thumbnail_image'])?$blog_data[0]['thumbnail_image']:"") ?>" type="hidden">
                              <?php }?>
                              
                              <input type="file" class="form-control-file" id="thumbnail_image" name="thumbnail_image" <?= $req;?> >
											</div>

											<div class="form-group">												
												<label for="tags">Blog Tags</label>												
												<select class="newvideotags" name="tags[]" id="tags" multiple="multiple" required>
													<?php $sel="";														
														foreach ($tags as $key => $value) {														
															if(!empty($tags_data)){																
																$sel = (in_array($value['blog_tag_id'],$tags_data)?"selected":" ");					}?>														
														<option value="<?= $value['blog_tag_id']?>" <?= $sel?> ><?=$value['blog_tag_name']?></option>
														<?php } ?>												
												</select>											
											</div>


											<div class="form-group">
												<label for="meta_title">Meta Title</label>
												<input type="text" class="form-control" id="meta_title" value="<?= (!empty($blog_data[0]['meta_title'])?$blog_data[0]['meta_title']:"") ?>"  name="meta_title" placeholder="Early 19th Century">	
											</div>

											<div class="form-group">
												<label for="meta_description">Meta Description</label>
												<textarea class="form-control" id="meta_description" name="meta_description" rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."><?= (!empty($blog_data[0]['meta_description'])?$blog_data[0]['meta_description']:"") ?></textarea>	
											</div>

											<div class="form-check">
												<label>Similar blog</label><br>
												<div class="switch-field">
														<?php $feature_yes =  'checked';
															  $feature_no='';
														if(!empty($blog_data)){
															if($blog_data[0]['featured_blog']=='Yes'){
																$feature_yes = "checked";
															}else{
																$feature_no = "checked";
															} 
														}?>
														
													<input type="radio" id="featured-one" name="featured-theme" value="Yes" <?= $feature_yes?>/>
													<label for="featured-one">Yes</label>
													<input type="radio" id="featured-two" name="featured-theme" value="No"  <?= $feature_no?>/>
													<label for="featured-two">No</label>
												</div>
											</div>

											<div class="form-check">
												<label>Blog Status</label><br>
												<div class="switch-field">
														<?php $active = '';
															 $in_active= '';
														if(!empty($blog_data)){
															if($blog_data[0]['status']=='Active'){
																$active = "checked";
															}else{
																$in_active = "checked";
															} 
														}else{
															$in_active = "checked";

														}?>
													<input type="radio" id="activate-theme-three" name="theme-status" value="Active" <?= $active?>/>
													<label for="activate-theme-three">Publish</label>
													<input type="radio" id="activate-theme-four" name="theme-status" value="In-active"  <?= $in_active?>/>
													<label for="activate-theme-four">Draft</label>
												</div>
											</div>	

											<div class="form-group">
												<button  type="submit" class="btn btn-black"><?=(!empty($blog_data)?"Update":"Create")?></button>
												<a href="<?= base_url('blog')?>" class="btn btn-cancel btn-border">Cancel</a>
											</div>
									</form>
									<div id="output"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>						
</div>



<!-- <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/medium-editor/5.23.0/css/medium-editor.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/medium-editor-insert-plugin/2.4.1/css/medium-editor-insert-plugin.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/medium-editor-insert-plugin/2.4.1/css/medium-editor-insert-plugin-frontend.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/medium-editor/5.23.0/css/themes/default.css">

<script src="https://cdnjs.cloudflare.com/ajax/libs/medium-editor/5.23.0/js/medium-editor.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.8/handlebars.runtime.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-sortable/0.9.13/jquery-sortable.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/blueimp-file-upload/9.18.0/js/vendor/jquery.ui.widget.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/blueimp-file-upload/9.18.0/js/jquery.iframe-transport.js"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/blueimp-file-upload/9.18.0/js/jquery.fileupload.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/medium-editor-insert-plugin/2.4.1/js/medium-editor-insert-plugin.js"></script> -->

<script>

$(document).on('change','#slug_check',function(){
		if ($('#slug_check').is(":checked")) {
			$('#blog_slug').val($('#blog_title').val());
		}else{
			$('#blog_slug').val("");
		}
	});

$(document).ready(function(){ 
	// $('#slug_check').on('change', 'input[type=checkbox]', function() {
		// var id = $(this).val(); // this gives me null
		// alert(id);
		// if (id != null) {
		// //do other things
		// }
		// alert("sds");
	// });

	$("#blog_date").datepicker({
		format: "dd-mm-yyyy",
		autoclose: true,
	});

        
	// var editor = new MediumEditor('.editable', {
	//   paste: {
	//     forcePlainText: false, // otherwise pasting doesn't work
	//   },
	// });
	
	// $(function () {
	//     $('.editable').mediumInsert({
	//         editor: editor,
	//         addons: {
	//             embeds: {
	//                 oembedProxy: null,
	//                 parseOnPaste: true
	//             }
	//         }
	//     });
	// });


});





// use for form submit
var vRules = 
{
	blog_title:{required:true},
	blog_slug:{required:true},
	blog_post_body:{required:true},
	tagname:{required:true},
	meta_title:{required:true},
	meta_description:{required:true},
};
var vMessages = 
{
	blog_title:{required:"Please Enter blog Title."},
	blog_slug:{required:"Please Enter blog slug."},
	blog_post_body:{required:"Please Enter blog post ."},
    tagname:{required:"Please Enter tagname."},
	meta_title:{required:"Please Enter Meta Title."},
	meta_description:{required:"Please Enter Meta Description."},

};

$("#form-blog").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{	
		var act = "<?= base_url('blog/submitform')?>";
		$("#form-blog").ajaxSubmit({
			url: act, 
			type: 'post',
			dataType: 'json',
			cache: false,
			clearForm: false,
			async:false,
			beforeSubmit : function(arr, $form, options){
				// $(".btn-primary").hide();
			},
			success: function(response) 
			{
				if(response.success){
					swal("done", response.msg, {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					}).then(
					function() {
						window.location = "<?= base_url('blog')?>";
						// $this->router->fetch_class().'/'.$this->router->fetch_method() ?>";
					});
				}else{	
					swal("Failed", response.msg, {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}
			}
		});
	}
});
document.title="Add/Edit Blog"
</script>