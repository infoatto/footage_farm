
<div class="wrapper">
	<div class="main-panel">
		<div class="container">
			<div class="page-inner">
				<div class="col-12">
					<div class="row ff-title-block">
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">
							<h1 class="ff-page-title">New Team Members</h1> 
						</div>
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
							<a href="#" class="btn btn-light btn-border ff-page-title-btn">
								Cancel
							</a>
						</div>
					</div>
				</div>					
				<div class="row">
					<div class="col-sm-12">
						<div class="card">
							<div class="card-body">
								<form class="" id="form-member" method="post" enctype="multipart/form-data">
									
									<input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden">
									<input name="meetteam_id" id="meetteam_id" value="<?= (!empty($member_data[0]['meetteam_id'])?$member_data[0]['meetteam_id']:"") ?>" type="hidden">
									<div class="form-group">
										<label for="memberfile">Member Thumbnail Image </label>
										<?php $req= '';
										 if(empty($member_data[0]['thumbnail_image'])){
											 $req ='required';
										}else{?>
										<br>
											<img src="<?= FRONT_URL.'/images/team_member/'.$member_data[0]['thumbnail_image']?>"class="img-thumbnail" alt="Cinque Terre" width="250" height="300">
											<input name="pre_memberfile_name" id="pre_memberfile_name" value="<?= (!empty($member_data[0]['thumbnail_image'])?$member_data[0]['thumbnail_image']:"") ?>" type="hidden">
										<?php }?>
										
										<input type="file" class="form-control-file" id="memberfile" name="memberfile" <?= $req;?> >
											</div>
											<div class="form-group">
												<label for="member_heading">Member heading</label>
												<input type="text" class="form-control" id="member_heading" name="member_heading" value="<?= (!empty($member_data[0]['member_heading'])?$member_data[0]['member_heading']:"") ?>" placeholder="john ">	
											</div>
											<div class="form-group">
												<label for="member_heading">Member Email</label>
												<input type="email" class="form-control" id="member_email" name="member_email" value="<?= (!empty($member_data[0]['member_email'])?$member_data[0]['member_email']:"") ?>" placeholder="abc@gmil.com">	
											</div>
											<div class="form-group">
												<label for="member_description">Member Decription</label>
												<textarea class="form-control my-text-editor" id="member_description"  name="member_description"  rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."><?= (!empty($member_data[0]['member_description'])?$member_data[0]['member_description']:"") ?></textarea>	
											</div>
										
											<div class="form-group">
												<label for="meta_title">Meta Title</label>
												<input type="text" class="form-control" id="meta_title" value="<?= (!empty($member_data[0]['meta_title'])?$member_data[0]['meta_title']:"") ?>"  name="meta_title" placeholder="Early 19th Century">	
											</div>
											<div class="form-group">
												<label for="meta_description">Meta Description</label>
												<textarea class="form-control" id="meta_description" name="meta_description" rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."><?= (!empty($member_data[0]['meta_description'])?$member_data[0]['meta_description']:"") ?></textarea>	
											</div>
										
											<div class="form-check">
												<label>Activate member (show this member on website)</label><br>
												<div class="switch-field">
														<?php $active = 'checked';
															 $in_active= '';
														if(!empty($member_data)){
															if($member_data[0]['status']=='Active'){
																$active = "checked";
															}else{
																$in_active = "checked";
															} 
														}?>
													<input type="radio" id="activate-member-three" name="member-status" value="Active" <?= $active?>/>
													<label for="activate-member-three">Yes</label>
													<input type="radio" id="activate-member-four" name="member-status" value="In-active"  <?= $in_active?>/>
													<label for="activate-member-four">No</label>
												</div>
											</div>	
											<div class="form-group">
												<button  type="submit" class="btn btn-black"><?=(!empty($member_data)?"Update":"Create")?></button>
												<a href="<?= base_url('meetteam')?>" class="btn btn-cancel btn-border">Cancel</a>
											</div>
									</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>						
</div>

<script>
    
$(document).ready(function(){
});


// use for form submit
var vRules = 
{
	member_heading:{required:true},
	member_email:{required:true},

	member_description:{required:true}

};
var vMessages = 
{
	member_heading:{required:"Please Enter Member heading ."},
	member_email:{required:"Please Enter Member Email id ."},
	member_description:{required:"Please Enter Member description."}
};

$("#form-member").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{	
		var act = "<?= base_url('meetteam/submitform')?>";
		$("#form-member").ajaxSubmit({
			url: act, 
			type: 'post',
			dataType: 'json',
			cache: false,
			clearForm: false,
			async:false,
			beforeSubmit : function(arr, $form, options){
				// $(".btn-primary").hide();
			},
			success: function(response) 
			{
				if(response.success){
					swal("done", response.msg, {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					}).then(
					function() {
						window.location = "<?= base_url('meetteam')?>";
						// $this->router->fetch_class().'/'.$this->router->fetch_method() ?>";
					});
				}else{	
					swal("Failed", response.msg, {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}
			}
		});
	}
});

document.title="Add/Edit Team"
</script>