<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Meetteam extends MX_Controller
{
	
	public function __construct(){
		parent::__construct();
		checklogin();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('meetteammodel','',TRUE);
	}

	public function index(){
		$result = array();
		$condition = "1=1";
		$result['theme_data'] = $this->common->getData("tbl_meet_team",'*',$condition);
		// echo "<pre>";
		// print_r($result);
		// exit;
		$this->load->view('main-header.php');
		$this->load->view('index.php',$result);
		$this->load->view('footer.php');
	}

	public function addEdit(){

		$meetteam_id = "";
		//print_r($_GET);
		$result = array();
		if(!empty($_GET['text']) && isset($_GET['text'])){
			$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
			parse_str($varr,$url_prams);
			$meetteam_id = $url_prams['id'];
			$condition = " meetteam_id ='".$meetteam_id."' ";
			$result['member_data'] = $this->common->getData("tbl_meet_team",'*',$condition,"meetteam_id","desc");
		}
		$this->load->view('main-header.php');
		$this->load->view('addEdit.php',$result);
		$this->load->view('footer.php');
	}
	public function submitForm(){
		// print_r($_POST);
		// print_r($_FILES);
		// exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();
			
			$thumnail_value = "";
			if(isset($_FILES) && isset($_FILES["memberfile"]["name"])){
				 $config = array();
				$config['upload_path'] = DOC_ROOT_FRONT."/images/team_member/";
				$config['max_size']    = '0';
				$config['allowed_types'] = 'gif|jpg|png|jpeg|gif';
				// $config['min_width']            = 1000;
    			// $config['min_height']           = 1000;
				$config['file_name']     = $_FILES["memberfile"]["name"];
				$this->load->library('upload', $config);
				if (!$this->upload->do_upload("memberfile")){
					$image_error = array('error' => $this->upload->display_errors());
					echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
					exit;
				}else{
					$image_data = array('upload_data' => $this->upload->data());
					$thumnail_value = $image_data['upload_data']['file_name'];
					$this->setCompresseredImage($image_data['upload_data']['file_name']); 

				}	
				/* Unlink previous category image */
				if(!empty($this->input->post('meetteam_id'))){	
					$condition_image = " meetteam_id = ".$this->input->post('meetteam_id');
					$image =$this->common->getData("tbl_meet_team",'*',$condition_image);
					if(is_array($image) && !empty($image[0]->thumbnail_image) && file_exists(DOC_ROOT_FRONT."/images/team_member/".$image[0]->thumbnail_image))
					{
						unlink(DOC_ROOT_FRONT."/images/theme_image/".$image[0]->thumbnail_image);
					}
				}
			}else{
				$thumnail_value = $this->input->post('pre_memberfile_name');
			}

			$data['thumbnail_image'] = $thumnail_value;
			$data['member_heading'] = $this->input->post('member_heading');
			$data['member_email'] = $this->input->post('member_email');
			$data['member_description'] = $this->input->post('member_description');
			$data['status'] = $this->input->post('member-status');
			$data['meta_title'] = $this->input->post('meta_title');
			$data['meta_description'] = $this->input->post('meta_description');
			$data['updated_on'] = date("Y-m-d H:i:s");
			$data['updated_by'] = $this->session->userdata('footage_farm_admin')[0]['user_id'];
			if(!empty($this->input->post('meetteam_id'))){
				// print_r($data);
				$condition = "meetteam_id = '".$this->input->post('meetteam_id')."' ";
				$result = $this->common->updateData("tbl_meet_team",$data,$condition);
				if($result){
					echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
					exit;
				}
				else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while updating data.'));
					exit;
				}
			}else{
				$data['created_on'] = date("Y-m-d H:i:s");
				$data['created_by'] =  $this->session->userdata('footage_farm_admin')[0]['user_id'];
				$result = $this->common->insertData('tbl_meet_team',$data,'1');
				if(!empty($result)){
					echo json_encode(array('success'=>true, 'msg'=>'Record Added Successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
					exit;
				}
			}
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem While Add/Edit Data.'));
			exit;
		}
	}

	
	public function delete_row(){
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

			$condition= $_POST['column_name']."= '".$_POST['id']."' ";
			$result = $this->common->deleteRecord($_POST['tbl_name'],$condition);
			if($result){
				echo json_encode(array('success'=>true, 'msg'=>'Record deleted Successfully.'));
				exit;
			}else{
				echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
				exit;
			}
		}
		// echo "<pre>";
		// print_r($_POST);
		// exit;

	}

	public function setCompresseredImage($image_name){
		$config1 = array();
		$this->load->library('image_lib');
		$config1['image_library'] = 'gd2';
		$config1['source_image'] = DOC_ROOT_FRONT."/images/team_member/".$image_name;
		$config1['maintain_ratio'] = TRUE;
		$config1['quality'] = '100%';
		// $config1['width'] = 1000;
		// $config1['height'] = 1000;
		$config1['new_image'] = DOC_ROOT_FRONT."/images/team_member/".$image_name;
		$this->image_lib->initialize($config1);
		$this->image_lib->resize();
		$this->image_lib->clear();
	}
}