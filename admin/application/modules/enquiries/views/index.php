<body>
   <div class="wrapper">
      <div class="main-panel">
         <div class="container">
            <div class="page-inner">
               <div class="col-12">
                  <div class="row ff-title-block">
                     <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <h1 class="ff-page-title">All Enquiries</h1>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-12">
                     <div class="card">
                        <div class="card-body">
                           <ul class="nav nav-pills nav-secondary" id="pills-tab" role="tablist">
                              <li class="nav-item">														
                                 <a class="nav-link active" id="pills-general-tab" data-toggle="pill" href="#pills-general" role="tab" aria-controls="pills-general" aria-selected="true">General</a>													
                              </li>
                              <li class="nav-item">														
                                 <a class="nav-link" id="pills-master-tab" data-toggle="pill" href="#pills-master" role="tab" aria-controls="pills-master" aria-selected="false">Preview/ Master</a>													
                              </li>
                           </ul>
                           <div class="tab-content mt-2 mb-3" id="pills-tabContent">
                              <div class="tab-pane fade show active" id="pills-general" role="tabpanel" aria-labelledby="pills-general-tab">
                                 <div class="table-responsive">
                                    <table   id="general-table" class="display table table-striped table-hover" >
                                       <thead>
                                          <tr>
                                             <th>Enquiry ID</th>
                                             <th>Name</th>
                                             <th>Email</th>
                                             <th>Date</th>
                                             <th>Actions</th>
                                          </tr>
                                       </thead>
                                       <tfoot>
                                          <tr>
                                             <th>Enquiry ID</th>
                                             <th>Name</th>
                                             <th>Email</th>
                                             <th>Date</th>
                                             <th>Actions</th>
                                          </tr>
                                       </tfoot>
                                       <tbody>
                                           <?php 
                                          if(!empty($general_data)){
                                             $i = 1;
                                             foreach ($general_data as $key => $value) {
                                                $date_convert = date('y-M-d',strtotime($value['created_on']));
                                                $date_convert_array = explode('-',$date_convert);                                                ?>
                                          <tr>
                                             <td>#<?= $value['enquiry_videos_id']?></td>
                                             <td><?= $value['name']?></td>
                                             <td><?= $value['email_id']?></td>
                                             <td><?= $date_convert_array[2].' '.$date_convert_array[1].', '.$date_convert_array[0] ?></td>
                                             <td>
                                                <div class="form-button-action">
                                                   <a href="<?=base_url('enquiries/general_detail?text='.rtrim(strtr(base64_encode("id=".$value['enquiry_videos_id']), '+/', '-_'), '=').'')?>" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="View"><i class="fa fa-eye"></i>
                                                   </a>
                                                   <a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" onclick="delete_rows(<?=$value['enquiry_videos_id']?>)" data-original-title="Remove">
                                                      <i class="fa fa-times"></i>
                                                   </a>
                                                </div>
                                             </td>
                                          </tr>
                                       <?php $i++;  } } ?>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                              <div class="tab-pane fade" id="pills-master" role="tabpanel" aria-labelledby="pills-master-tab">
                                 <div class="table-responsive">
                                    <table id="preview-master-table" class="display table table-striped table-hover" >
                                       <thead>
                                          <tr>
                                             <th>Enquiry ID</th>
                                             <th>Name</th>
                                             <th>Email</th>
                                             <th>No. of Videos</th>
                                             <th>Date</th>
                                             <th>Actions</th>
                                          </tr>
                                       </thead>
                                       <tfoot>
                                          <tr>
                                             <th>Enquiry ID</th>
                                             <th>Name</th>
                                             <th>Email</th>
                                             <th>No. of Videos</th>
                                             <th>Date</th>
                                             <th>Actions</th>
                                          </tr>
                                       </tfoot>
                                       <tbody>
                                          <?php 
                                          if(!empty($enquiry_data)){
                                             $i = 1;
                                             foreach ($enquiry_data as $key => $value) {
                                                $date_convert = date('y-M-d',strtotime($value['enquiry_date']));
                                                $date_convert_array = explode('-',$date_convert);																 ?>																	
                                          <tr>
                                             <td>#<?= $i; ?></td>
                                             <td><?= $value['user_name']?></td>
                                             <td><?= $value['email_id']?></td>
                                             <td><?= $value['totol_videos']?></td>
                                             <td><?= $date_convert_array[2].' '.$date_convert_array[1].', '.$date_convert_array[0] ?> </td>
                                             <td>
                                                <div class="form-button-action">
                                                   <a href="<?=base_url('enquiries/detail?text='.rtrim(strtr(base64_encode("id=".$value['user_id']), '+/', '-_'), '=').'')?>" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="View">																					<i class="fa fa-eye"></i>
                                                   </a>
                                                   <a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" onclick="delete_row('user_id',<?=$value['user_id']?>)" data-original-title="Remove">							<i class="fa fa-times"></i>																				</a>
                                                </div>
                                             </td>
                                          </tr>
                                          <?php $i++; } } ?>																
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <script >
      $(document).ready(function() {
      	// Add Row
      	$('#preview-master-table').DataTable({
      		"pageLength": 10,
      	});			
      	$('#general-table').DataTable({
      		"pageLength": 10,
      	});			
      });
      
      function delete_row(column_name,id){
      	var csrfName = "<?= $this->security->get_csrf_token_name()?>"; // Value specified in $config['csrf_token_name']
      	var csrfHash = "<?= $this->security->get_csrf_hash()?>"; // CSRF hash
      	if(id && column_name){
      		var r = confirm("Are you want to sure delete this Row");
      		if (r == true) {
      			$.ajax({
      				url: "<?= base_url('enquiries/delete_row')?>",
      				data:{column_name,id,[csrfName]: csrfHash },
      				dataType: "json",
      				type: "POST",
      				success: function(response){
      				
      					if(response.success){
      						swal("done", response.msg, {
      							icon : "success",
      							buttons: {        			
      								confirm: {
      									className : 'btn btn-success'
      								}
      							},
      						}).then(
      						function() {
      							location.reload();
      						});
      					}else{	
      						swal("Failed", response.msg, {
      							icon : "error",
      							buttons: {        			
      								confirm: {
      									className : 'btn btn-danger'
      								}
      							},
      						});
      					}
      				}
      			});
      		}
      	}
      }

      function delete_rows(id){
         var csrfName = "<?= $this->security->get_csrf_token_name()?>"; // Value specified in $config['csrf_token_name']
         var csrfHash = "<?= $this->security->get_csrf_hash()?>"; // CSRF hash
         if(id ){
            var r = confirm("Are you sure want to delete this Row");
            if (r == true) {
               $.ajax({
                  url: "<?= base_url('enquiries/delete_rows')?>",
                  data:{id,[csrfName]: csrfHash },
                  dataType: "json",
                  type: "POST",
                  success: function(response){
                  
                     if(response.success){
                        swal("done", response.msg, {
                           icon : "success",
                           buttons: {                 
                              confirm: {
                                 className : 'btn btn-success'
                              }
                           },
                        }).then(
                        function() {
                           location.reload();
                        });
                     }else{   
                        swal("Failed", response.msg, {
                           icon : "error",
                           buttons: {                 
                              confirm: {
                                 className : 'btn btn-danger'
                              }
                           },
                        });
                     }
                  }
               });
            }
         }
      }
      document.title=" Enquiry Listing"
   </script>
</body>
</html>