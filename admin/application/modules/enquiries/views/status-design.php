<thead>
	<tr>																	
		<th scope="col">Status</th>
		<th scope="col">Updated On</th>
		<th scope="col">Updated By</th>
	</tr>
</thead>
<tbody>
	<?php
if(!empty($status_data)){
foreach ($status_data as $key => $value) {
	$date_convert = date('y-M-d',strtotime($value['updated_on']));
	$date_convert_array = explode('-',$date_convert);
	?>
	<tr>																
		<td><?=$value['enquiry_status'] ?></td>
		<td><?= $date_convert_array[2].' '.$date_convert_array[1].', '.$date_convert_array[0] ?></td>
		<td><?=$value['user_name'] ?></td>
	</tr>

	<?php }
	} ?>

	<!-- <tr>																
		<td>Ongoing</td>
		<td>12 Aug, 20</td>
		<td>Gonzalo</td>
	</tr>
	<tr>																
		<td>Purchased</td>
		<td>12 Aug, 20</td>
		<td>Gonzalo</td>
	</tr> -->
</tbody>