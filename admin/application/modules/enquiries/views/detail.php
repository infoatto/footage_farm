
<body>
	<div class="wrapper">
		<div class="main-panel">
			<div class="container">
				<div class="page-inner">
					<div class="col-12">
						<div class="row ff-title-block">
							<div class="col-12 col-sm-12 col-md-6 col-lg-6">
									<h1 class="ff-page-title">Enquiries Details</h1> 
							</div>							
						</div>
					</div>					
					<div class="row">
							<div class="col-sm-12">
									<div class="card">
											<div class="card-body">
												<div class="table-responsive">
													<table id="add-row" class="display table table-striped table-hover" >
														<thead>
															<tr>
																<th>SNo.</th>
																<th>Request Type</th>
																<th>Format</th>																
																<th>Frame Rate</th>																
																<th>Enquiry Status</th>	
																<th>codec</th>	
																<th>Actions</th>															
															</tr>
														</thead>
														<tfoot>
															<tr>
																<th>SNo.</th>
																<th>Request Type</th>
																<th>Format</th>																
																<th>Frame Rate</th>																
																<th>Enquiry Status</th>
																<th>codec</th>	
																<th>Actions</th>															
															</tr>
														</tfoot>
														<tbody>
														<?php
														if(!empty($enquiry_data)){
														foreach ($enquiry_data as $key => $value) {
														 ?>
															<tr>																
																<td>#<?= $key+1; ?></td>
																<td><?= $value['request_type']?></td>			
																<td><?= $value['format']?></td>
																<td><?= $value['frame_rate']?>fps</td>			
																<td><?= $value['enquiry_status']?></td>	
																<td><?= $value['codec']?></td>	
																<td>
																	<div class="form-button-action">
																		<a href="<?=base_url('enquiries/addEdit?text='.rtrim(strtr(base64_encode("id=".$value['video_enquiry_detail_id']), '+/', '-_'), '=').'&uid='.rtrim(strtr(base64_encode("userid=".$value['user_id']), '+/', '-_'), '='))?>" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="View">
																			<i class="fa fa-eye"></i>
																		</a>
																	
																	</div>
																</td>													
															</tr>
															<?php } } ?>

															
															
														</tbody>
													</table>
												</div>
											</div>
									</div>
							</div>
					</div>
				</div>
			</div>
			
		</div>						
	</div>
	<script >

		document.title="preview/master Enquiry Listing"
		$(document).ready(function() {
			// Add Row
			$('#add-row').DataTable({
				"pageLength": 10,
			});			
		});
	</script>
</body>
</html>
