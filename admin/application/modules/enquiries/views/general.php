<?php $value = $enquiry_data[0]; ?>
<body>
	<div class="wrapper">
		<div class="main-panel">
			<div class="container">
				<div class="page-inner">

						<ul class="breadcrumb">
							<li><a href="<?= base_url('enquiries')?>">Home</a></li>
							<li><a href="#">#<?= $value['enquiry_videos_id']?></a></li>
						</ul>
					<div class="col-12">
						<div class="row ff-title-block">
							<div class="col-2">
								<a href="#/"></a>
								<span>#<?= @$value['enquiry_videos_id']?></span>
								
							</div>
							<div class="col-8 col-sm-8">
									<h1 class="ff-page-title"></h1> 
							</div>
							<div class="col-2 col-sm-2">                                
									<a href="<?=base_url('enquiries')?>" class="btn btn-light btn-border ff-page-title-btn">
											Back
									</a>
							</div>
						</div>
						<div class="row ff-userenquiry-status">
							<div class="col-2">&nbsp;</div>
							<div class="col-sm-2"><strong>Request Type</strong>:<br/> General</div>
							<?php
							$date_convert = date('Y-M-j',strtotime($value['updated_on']));
				            $date_convert_array = explode('-',$date_convert);
				            ?>
							<div class="col-sm-2"><strong>Enquiry Date</strong>:<br/><?= $date_convert_array[2].' '.$date_convert_array[1].' '.$date_convert_array[0] ?></div>
							<div class="col-sm-2"><strong>Status</strong>:<br/> 
								<input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden" class="txt_csrfname">
								<select name="statusselect" id="statusselect1" data-video="<?= $value['enquiry_videos_id']?>">
									<option value="New" <?=($value['status']=='New')?"selected":""; ?>>New</option>
									<option value="Ongoing" <?=($value['status']=='Ongoing')?"selected":""; ?>>Ongoing</option>
									<option value="Purchased" <?=($value['status']=='Purchased')?"selected":""; ?>>Purchased</option>
									<option value="Closed" <?=($value['status']=='Closed')?"selected":""; ?>>Closed</option>
								</select> 
							</div>
							<div class="col-sm-2"><strong>Client</strong>:<br/> <?= $value['name']?></div>
						</div>
					</div>	
					<hr>				
					<div class="row">
						<div class="col-sm-12">
							<div class="card">
									<div class="card-body">
										<div class="row">
												<div class="col-12 col-sm-6 user-enquiry-info">
													<p class="text-muted">Attached URLs</p>
											<?php 
											$all_tags = explode(",",$value['urls']);
												foreach ($all_tags as $tagkey => $tagvalue) {?>
													<p><?= $tagvalue?></p>
												<?php }?>
												
													<p class="text-muted">Message</p>			
													<p><?= $value['message']?>.</p>

													<form method="post" id="admin_note">
													<input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden">
						                            <input name="id" id="id" value="<?= (!empty($value['enquiry_videos_id'])?$value['enquiry_videos_id']:"") ?>" type="hidden">	
													<div class="form-group">														
														<label>Note</label>														
														<textarea class="form-control" name="admin_note" id="admin_note" rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."> <?= (!empty($value['admin_note'])?$value['admin_note']:"") ?> </textarea>													
													</div>															
													<div class="form-group">														
														<button type="submit" class="btn btn-black">Save</button>		
														<button type="button" onclick="goBack()" class="btn btn-cancel btn-border">Cancel</button>
													</div>
													</form>


												</div>
												<div class="col-12 col-sm-6 user-enquiry-status" style="height:500px;">													
														<h3>Status Update</h3>
														<table class="table table-striped mt-3 statuslog">
															
															
														</table>
												</div>
											</div>
									</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>						
	</div>

<script type="text/javascript">
	function goBack() {
		window.history.back()
	}

	$(document).ready(function() {
		getStatus();

		$(document).on("change","#statusselect1",function () {
        var status = $(this).val();
        var videoid = $('#statusselect1').attr('data-video');
        
        var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
	    var csrfHash = $('.txt_csrfname').val(); // CSRF hash
		$.ajax({
			url: "<?= base_url('enquiries/status_general')?>",
			type: "POST",
			data:{ status, videoid, [csrfName]: csrfHash },
			dataType: "json",
			success: function(response){
				if(response.success){
					getStatus();
					swal("done", response.msg, {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					}).then(
					function() {
						//window.location = "<?= base_url('blog')?>";
						
					});
				}else{	
					swal("Failed", response.msg, {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}
			}
		});
	      
	    });

						
	});

	function getStatus() {
		var user_id  = "<?= $value['enquiry_videos_id']?>";
		var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
		var csrfHash = $('.txt_csrfname').val(); // CSRF hash
		$.ajax({
			url: "<?= base_url('enquiries/generalstatus')?>",
			type: "POST",
			data:{ user_id, [csrfName]: csrfHash },
			dataType: "json",
			success: function(response){
				if(response.success){
					$(".statuslog").html(response.data);		
				}else{
					$(".statuslog").html('');
				}

			}
		});
	}


$("#admin_note").validate({
	ignore:[],
	rules: {
	admin_note:{required:true},
    },
	messages: {
	admin_note:{required:"Please Enter Note."},

    },
	submitHandler: function(form) 
	{	
		var act = "<?= base_url('enquiries/submitForm')?>";
		$("#admin_note").ajaxSubmit({
			url: act, 
			type: 'post',
			dataType: 'json',
			cache: false,
			clearForm: false,
			async:false,
			beforeSubmit : function(arr, $form, options){
				$(".btn btn-black").hide();
			},
			success: function(response) 
			{
				if(response.success){
					swal("done", response.msg, {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					}).then(
					function() {
						location.reload();
						//window.location = "<?= base_url('profile/editprofile')?>";
					});
				}else{	
					swal("Failed", response.msg, {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}
			}
		});

	}
});

document.title="General Enquiry"
</script>
