<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Enquiries extends MX_Controller
{
	
	public function __construct(){
		// this is helper function  use to check whether the user is logged in or not file is written in helper folder inside application 
		checklogin();
		parent::__construct();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('enquiriesmodel','',TRUE);
	}

	public function index(){
		$result = array();

		$condition = "1=1 AND c.status= 'Active' ";
		$main_table = array("tbl_video_enquiry_details as c", array("c.user_id,c.enquiry_date,count(c.video_enquiry_detail_id) as totol_videos"));
		$join_tables =  array();
		$join_tables = array(
							array("", "tbl_users as  u", "u.user_id = c.user_id", array("user_name, email_id")),
							
						);
	
		$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("c.video_enquiry_detail_id" => "DESC"),"c.user_id",null); 
		 // fetch query
		$result['enquiry_data']= $this->common->MySqlFetchRow($rs, "array"); // fetch result

		$condition = "1=1 ";
		$result['general_data'] = $this->common->getData("tbl_enquiry_videos",'*',$condition);
		
		//echo "<pre>";print_r($result['general_data']);exit;

		$this->load->view('main-header.php');
		$this->load->view('index.php',$result);
		$this->load->view('footer.php');
	}

	public function detail(){
		$result = array();
		if(!empty($_GET['text']) && isset($_GET['text'])){
			$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
			parse_str($varr,$url_prams);
			$id = $url_prams['id'];
			
		$condition = "1=1 AND cd.status= 'Active' AND cd.user_id='".$id."'  ";
		$main_table = array("tbl_video_enquiry_details as cd", array("cd.*"));
		$join_tables =  array();
		$join_tables = array(
							array("", "tbl_video_enquiry_tc as  ct", "ct.user_id = cd.user_id", array("begin_tc,end_tc")),
							array("left", "tbl_video_enquiry_uploads as  cu", "cu.user_id = cd.user_id", array("uploaded_document")));
	
		$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("cd.video_enquiry_detail_id" => "DESC"),"cd.video_enquiry_detail_id",null); 
		 // fetch query
		$result['enquiry_data']= $this->common->MySqlFetchRow($rs, "array"); // fetch result
		//echo "<pre>";print_r($result['enquiry_data']);exit;
		}
		$this->load->view('main-header.php');
		$this->load->view('detail.php',$result);
		$this->load->view('footer.php');
	}

	public function addEdit(){
		$result = array();
		if(!empty($_GET['text']) && isset($_GET['text'])){
			$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
			parse_str($varr,$url_prams);
			$id = $url_prams['id'];

			$var=base64_decode(strtr($_GET['uid'], '-_', '+/'));	
			parse_str($var,$url);
			$userid = $url['userid'];
			$condition = "1=1 AND cd.status= 'Active' AND cd.user_id='".$userid."' AND cd.video_enquiry_detail_id='".$id."'  ";
			$main_table = array("tbl_video_enquiry_details as cd", array("cd.*"));
			$join_tables =  array();
			$join_tables = array(
								array("", "tbl_video_enquiry_tc as  ct", "ct.video_id = cd.video_id", array("begin_tc,end_tc")),
								array("", "tbl_videos as v", "v.video_id = cd.video_id", array("v.*")),
								array("", "tbl_users as u", "u.user_id = cd.created_by", array("user_name, email_id,u.user_id")),
							);
		
			$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("cd.video_enquiry_detail_id" => "DESC"),"cd.video_enquiry_detail_id",null); 
			// fetch query
			$result['enquiry_data']= $this->common->MySqlFetchRow($rs, "array"); // fetch result
			// echo "<pre>";print_r($result['enquiry_data']);exit;
		}
		$this->load->view('main-header.php');
		$this->load->view('videoreply.php',$result);
		$this->load->view('footer.php');
	}

	public function general_detail(){
		$result = array();
		if(!empty($_GET['text']) && isset($_GET['text'])){
			$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
			parse_str($varr,$url_prams);
			$id = $url_prams['id'];
			
		$condition = "1=1 AND v.enquiry_videos_id='".$id."'  ";
		$main_table = array("tbl_enquiry_videos as v", array("v.*"));
		$join_tables =  array();
		$join_tables = array(
							array("", "tbl_enquiry_video_urls as  vu", "vu.enquiry_video_id = v.enquiry_videos_id", array("group_concat(vu.url) as urls, group_concat(vu.enquiry_videos_url_id) as url_id"))
						);
	
		$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("v.enquiry_videos_id" => "DESC"),"v.enquiry_videos_id",null); 
		 // fetch query
		$result['enquiry_data']= $this->common->MySqlFetchRow($rs, "array"); // fetch result
		//echo "<pre>";print_r($result['enquiry_data']);exit;
		}
		$this->load->view('main-header.php');
		$this->load->view('general.php',$result);
		$this->load->view('footer.php');
	}

	public function delete_row(){
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

			$condition= $_POST['column_name']."= '".$_POST['id']."' ";
			$result = $this->common->deleteRecord(' tbl_video_enquiry_details',$condition);
					  $this->common->deleteRecord(' tbl_video_enquiry_tc',$condition);
					  $this->common->deleteRecord(' tbl_video_enquiry_uploads',$condition);
			if($result){
				echo json_encode(array('success'=>true, 'msg'=>'Record deleted Successfully.'));
				exit;
			}else{
				echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
				exit;
			}
		}
		// echo "<pre>";
		// print_r($_POST);
		// exit;

	}

	public function delete_rows(){
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

			$condition= "enquiry_videos_id = '".$_POST['id']."' ";
			$result = $this->common->deleteRecord('tbl_enquiry_videos',$condition);
			$condition1= "enquiry_video_id = '".$_POST['id']."' ";
			$result = $this->common->deleteRecord(' tbl_enquiry_video_urls',$condition1);
			if($result){
				echo json_encode(array('success'=>true, 'msg'=>'Record deleted Successfully.'));
				exit;
			}else{
				echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
				exit;
			}
		}

	}

	public function status_change(){
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

			// print_r($_POST);
			// exit;
			$status= $_POST['status'];
			$videoid= $_POST['videoid'];
			$data = array(
				'enquiry_status'=> $status,
			);
			$condition = "video_id = '".$videoid."' ";
			$result = $this->common->updateData("tbl_video_enquiry_details",$data,$condition);

			$condition1 = "1=1 AND video_id = ".$videoid." AND enquiry_status ='$status'  ";
		    $log = $this->common->getData("tbl_enquiry_status_log","*",$condition1);
		    if (!empty($log)) {

		    	$update = array(
						'updated_on' => date("Y-m-d H:i:s"),
					);
				$result1 = $this->common->updateData("tbl_enquiry_status_log",$update,$condition1);
		    	
		    }else{
		    	$array = array(
						// 'user_id'  => $this->session->userdata('footage_farm_admin')[0]['user_id'],
						'video_id' => $videoid,
						'enquiry_status' => $status,
						'created_by' => $this->session->userdata('footage_farm_admin')[0]['user_id'],
						'created_on' => date("Y-m-d H:i:s"),
						'updated_by' => $this->session->userdata('footage_farm_admin')[0]['user_id'],
						'updated_on' => date("Y-m-d H:i:s"),
					);
		    	
				$result1 = $this->common->insertData('tbl_enquiry_status_log',$array,'1');
		    }
			
					  
			if($result){
				echo json_encode(array('success'=>true, 'msg'=>'Record update Successfully.'));
				exit;
			}else{
				echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
				exit;
			}
		}
		// echo "<pre>";
		// print_r($_POST);
		// exit;

	}

	public function status_general(){
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

			$status= $_POST['status'];
			$userid= $_POST['videoid'];
			$data = array(
				'status'=> $status,
			);
			$condition = "enquiry_videos_id = '".$userid."' ";
			$result = $this->common->updateData("tbl_enquiry_videos",$data,$condition);

			$condition1 = "1=1 AND user_id = ".$userid." AND enquiry_status ='$status'  ";
		    $log = $this->common->getData("tbl_general_enquiry_status_log","*",$condition1);
		    if (!empty($log)) {

		    	$update = array(
						'updated_on' => date("Y-m-d H:i:s"),
					);
				$result1 = $this->common->updateData("tbl_general_enquiry_status_log",$update,$condition1);
		    	
		    }else{
		    	$array = array(
						'user_id' => $userid,
						'enquiry_status' => $status,
						'created_by' => $this->session->userdata('footage_farm_admin')[0]['user_id'],
						'created_on' => date("Y-m-d H:i:s"),
						'updated_by' => $this->session->userdata('footage_farm_admin')[0]['user_id'],
						'updated_on' => date("Y-m-d H:i:s"),
					);
		    	
				$result1 = $this->common->insertData('tbl_general_enquiry_status_log',$array,'1');
		    }
			
					  
			if($result){
				echo json_encode(array('success'=>true, 'msg'=>'Record update Successfully.'));
				exit;
			}else{
				echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
				exit;
			}
		}
		// echo "<pre>";
		// print_r($_POST);
		// exit;

	}

	public function statusdata(){

		$result = array();
		$user_id = $_POST['user_id'];
		$video_id = $_POST['video_id'];
		$condition = "1=1 AND p.user_id = '$user_id' AND p.video_id = '$video_id' ";
		$main_table = array("tbl_enquiry_status_log as p", array("p.*"));
		$join_tables =  array();
		$join_tables = array(
		          array("", "tbl_users as  u", "u.user_id = p.user_id", array("user_name, email_id"))
		          );

		$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("p.status_id" => "ASC"),"p.status_id",null); 
		 // fetch query
		$status_data = $this->common->MySqlFetchRow($rs, "array"); // fetch result
		//echo '<pre>';print_r($status_data);die;
		$result['status_data'] = $status_data;		
		$statusresponse = $this->load->view('status-design',$result,true);	
		
		if (!empty($statusresponse)) {
			print_r(json_encode(array('success'=>true, 'data'=> $statusresponse)));
			exit;
		}else{
			print_r(json_encode(array('success'=>false, 'data'=> $statusresponse)));
			exit;
			}

	}

	public function generalstatus(){
		$result = array();
		$user_id = $_POST['user_id'];
		$condition = "1=1 AND p.user_id = '$user_id' ";
		$main_table = array("tbl_general_enquiry_status_log as p", array("p.*"));
		$join_tables =  array();
		$join_tables = array(
		          array("", "tbl_users as u", "u.user_id = p.updated_by", array("user_name, email_id"))
		          );

		$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("p.status_id" => "ASC"),"p.status_id",null); 
		 // fetch query
		$status_data = $this->common->MySqlFetchRow($rs, "array"); // fetch result
		//echo '<pre>';print_r($status_data);die;
		$result['status_data'] = $status_data;		
		$statusresponse = $this->load->view('status-design',$result,true);	
		
		if (!empty($statusresponse)) {
			print_r(json_encode(array('success'=>true, 'data'=> $statusresponse)));
			exit;
		}else{
			print_r(json_encode(array('success'=>false, 'data'=> $statusresponse)));
			exit;
			}

	}


	public function submitForm(){
		// print_r($_POST);exit;

		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();
			$data['admin_note'] = $this->input->post('admin_note');

			if(!empty($this->input->post('id'))){
				// print_r($data);
				$condition = "enquiry_videos_id = '".$this->input->post('id')."' ";
				$result = $this->common->updateData("tbl_enquiry_videos",$data,$condition);
				if($result){
					echo json_encode(array('success'=>true, 'msg'=>'Note Create Successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while updating data.'));
					exit;
				}
			}else{

				$result = $this->common->insertData('tbl_enquiry_videos',$data,'1');
				if(!empty($result)){
					echo json_encode(array('success'=>true, 'msg'=>'Record Added Successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
					exit;
				}
			}
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem While Add/Edit Data.'));
			exit;
		}
	}

	public function submitNote(){
		// print_r($_POST);exit;

		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();
			$data['admin_note'] = $this->input->post('admin_note');

			if(!empty($this->input->post('id'))){
				// print_r($data);
				$condition = "video_enquiry_detail_id = '".$this->input->post('id')."' ";
				$result = $this->common->updateData("tbl_video_enquiry_details",$data,$condition);
				if($result){
					echo json_encode(array('success'=>true, 'msg'=>'Note Create Successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while updating data.'));
					exit;
				}
			}else{

				$result = $this->common->insertData('tbl_video_enquiry_details',$data,'1');
				if(!empty($result)){
					echo json_encode(array('success'=>true, 'msg'=>'Record Added Successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
					exit;
				}
			}
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem While Add/Edit Data.'));
			exit;
		}
	}

	
	public function logOut(){
		$this->session->unset_userdata('footage_farm_admin');
		redirect(base_url()); 

	}
}

?>