
<div class="wrapper">
	<div class="main-panel">
		<div class="container">
			<div class="page-inner">
				<div class="col-12">
					<div class="row ff-title-block">
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">
							<h1 class="ff-page-title">New Master Theme</h1> 
						</div>
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
							<a href="#" class="btn btn-light btn-border ff-page-title-btn">
								Cancel
							</a>
						</div>
					</div>
				</div>					
				<div class="row">
					<div class="col-sm-12">
						<div class="card">
							<div class="card-body">
								<!-- <form class="" id="form-theme" method="post" enctype="multipart/form-data"> -->
								<?php $attributes = array('class' => '', 'id' => 'form-theme', "method"=>"post", "enctype"=>"multipart/form-data");
            						echo form_open(base_url('theme/submitform')	,$attributes);?>
									<div class="form-group">
										<label for="themefile">Theme Thumbnail Image</label>
											<input type="file" class="form-control-file" id="themefile" name="themefile" >
											</div>
											<span class="text-danger"><?php echo form_error("themefile"); ?></span>
											<div class="form-group">
												<label for="themename">Theme Name</label>
												<input type="text" class="form-control" id="themename" name="themename" placeholder="Early 19th Century" >	
											<span class="text-danger"><?php echo form_error("themename"); ?></span>
											</div>
											<div class="form-group">
												<label for="theme_description">Theme Decription (Optional)</label>
												<textarea class="form-control" id="theme_description"  name="theme_description"  rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."></textarea>	
											</div>
										
											<div class="form-group">
												<label for="mainvideo">Main video (on all the banners)</label>
												<input type="text" class="form-control" id="mainvideo"  name="mainvideo" placeholder="Enter url (eg. https://vimeo.com/55900216)" >	
											<span class="text-danger"><?php echo form_error("mainvideo"); ?></span>
											</div>
											<div class="form-group">
												<label for="meta_title">Meta Title</label>
												<input type="text" class="form-control" id="meta_title"  name="meta_title" placeholder="Early 19th Century" >	
											<span class="text-danger"><?php echo form_error("meta_title"); ?></span>
											</div>

											<div class="form-group">
												<label for="meta_description">Meta Description</label>
												<textarea class="form-control" id="meta_description" name="meta_description" rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor." ></textarea>	
											<span class="text-danger"><?php echo form_error("meta_description"); ?></span>
											</div>
											<div class="form-check">
												<label>Featured theme (this will help to show theme on home page)</label><br>
												<div class="switch-field">
													<input type="radio" id="featured-one" name="switch-one" value="yes" checked/>
													<label for="featured-one">Yes</label>
													<input type="radio" id="featured-two" name="switch-one" value="no" />
													<label for="featured-two">No</label>
												</div>
											</div>
											<div class="form-check">
												<label>Activate theme (show this theme on website)</label><br>
												<div class="switch-field">
													<input type="radio" id="activate-theme-three" name="switch-two" value="yes" checked/>
													<label for="activate-theme-three">Yes</label>
													<input type="radio" id="activate-theme-four" name="switch-two" value="no" />
													<label for="activate-theme-four">No</label>
												</div>
											</div>	
											<div class="form-group">
												<button  type="submit" class="btn btn-black">Create</button>
												<button class="btn btn-cancel btn-border">Cancel</button>
											</div>
									<!-- </form> -->
									<?php echo form_close();?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>						
</div>

<script>
$(document).ready(function(){
});


// use for form submit
// var vRules = 
// {
// 	themename:{:true},
// 	// theme_description:{required:true},
// 	mainvideo:{required:true},
// 	meta_title:{required:true},
// 	meta_description:{required:true},

// };
// var vMessages = 
// {
// 	themename:{required:"Please Enter theme Name."},
// 	// theme_description:{required:"Please Enter theme Description Name."},
// 	mainvideo:{required:"Please Enter Main video."},
// 	meta_title:{required:"Please Enter Meta Title."},
// 	meta_description:{required:"Please Enter Meta Description."},

// };

// $("#form-theme1").validate({
// 	rules: vRules,
// 	messages: vMessages,
// 	submitHandler: function(form) 
// 	{	
// 		var act = "<?= base_url('theme/submitform')?>";
// 		$("#form-theme1").ajaxSubmit({
// 			url: act, 
// 			type: 'post',
// 			dataType: 'json',
// 			cache: false,
// 			clearForm: false,
// 			async:false,
// 			beforeSubmit : function(arr, $form, options){
// 				// $(".btn-primary").hide();
// 			},
// 			success: function(response) 
// 			{
// 				if(response.success)
// 				{
// 					displayMsg("success",response.msg);
// 					// setTimeout(function(){
// 					// 	window.location = "<?= base_url();$this->router->fetch_class()?>";
// 					// },2000);
// 				}
// 				else
// 				{	
// 					//$("#error_msg").show();
// 					$(".btn-primary").show();
// 					displayMsg("error",response.msg);
// 					return false;
// 				}
// 			}
// 		});
// 	}
// });


</script>