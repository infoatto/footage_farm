
<div class="wrapper">
	<div class="main-panel">
		<div class="container">
			<div class="page-inner">
				<div class="col-12">
					<div class="row ff-title-block">
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">
							<h1 class="ff-page-title">New Master Theme</h1> 
						</div>
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
							<a href="#" class="btn btn-primary ff-page-title-btn">
								Cancel
							</a>
						</div>
					</div>
				</div>					
				<div class="row">
					<div class="col-sm-12">
						<div class="card">
							<div class="card-body">
								<form class="" id="form-theme" method="post" enctype="multipart/form-data">
								
									<input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden">
									<input name="theme_id" id="theme_id" value="<?= (!empty($theme_data[0]['theme_id'])?$theme_data[0]['theme_id']:"") ?>" type="hidden">
									<div class="form-group">
										<label for="themefile">Theme Thumbnail Image (Min. size 1000px X 1000px) </label>
										<?php $req= '';
										 if(empty($theme_data[0]['thumbnail_image'])){
											 $req ='required';
										}else{?>
										<br>
											<img src="<?= FRONT_URL.'/images/theme_image/'.$theme_data[0]['thumbnail_image']?>"class="img-thumbnail" alt="Cinque Terre" width="250" height="300">
											<input name="pre_themefile_name" id="pre_themefile_name" value="<?= (!empty($theme_data[0]['thumbnail_image'])?$theme_data[0]['thumbnail_image']:"") ?>" type="hidden">
										<?php }?>
										
										<input type="file" class="form-control-file" id="themefile" name="themefile" <?= $req;?> >
											</div>
											<div class="form-group">
												<label for="themename">Theme Name</label>
												<input type="text" class="form-control" id="themename" name="themename" value="<?= (!empty($theme_data[0]['theme_name'])?$theme_data[0]['theme_name']:"") ?>" placeholder="Early 19th Century">	
											</div>
											<div class="form-group">
												<label for="theme_description">Theme Decription (Optional)</label>
												<textarea class="form-control my-text-editor" id="theme_description"  name="theme_description"  rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."><?= (!empty($theme_data[0]['theme_description'])?$theme_data[0]['theme_description']:"") ?></textarea>	
											</div>
											<div class="form-group">
												<label for="mainvideo">Main video (on all the banners)</label>
												<input type="text" class="form-control" id="mainvideo" value="<?= (!empty($theme_data[0]['main_video'])?$theme_data[0]['main_video']:"") ?>"  name="mainvideo" placeholder="Enter url (eg. https://player.vimeo.com/video/237546641)">	
											</div>

											<div class="form-group">												
												<label for="themes">Select 3 Similar Themes</label>												
												<select class="form-control newvideotags" name="themes[]" id="themes" multiple="multiple" required>
												<option value="" disabled>Select 3 Similar Themes</option>
												<?php $sel="";														
												foreach ($themes as $key => $value) {															
													if(!empty($similar_theme_data)){																
														$sel = (in_array($value['theme_id'],$similar_theme_data)?"selected":" ");															
														}	?>														
													<option value="<?= $value['theme_id']?>" <?= $sel?> >
													<?=$value['theme_name']?></option>
													<?php } ?>
												</select>											
											</div>	

											<div class="form-group">
												<label for="meta_title">Meta Title</label>
												<input type="text" class="form-control" id="meta_title" value="<?= (!empty($theme_data[0]['meta_title'])?$theme_data[0]['meta_title']:"") ?>"  name="meta_title" placeholder="Early 19th Century">	
											</div>
											<div class="form-group">
												<label for="meta_description">Meta Description</label>
												<textarea class="form-control" id="meta_description" name="meta_description" rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."><?= (!empty($theme_data[0]['meta_description'])?$theme_data[0]['meta_description']:"") ?></textarea>	
											</div>
											<div class="form-check">
												<label>Featured theme (this will help to show theme on home page)</label><br>
												<div class="switch-field">
														<?php $feature_yes =  'checked';
															  $feature_no='';
														if(!empty($theme_data)){
															if($theme_data[0]['featured_theme']=='Yes'){
																$feature_yes = "checked";
															}else{
																$feature_no = "checked";
															} 
														}?>
														
													<input type="radio" id="featured-one" name="featured-theme" value="yes" <?= $feature_yes?>/>
													<label for="featured-one">Yes</label>
													<input type="radio" id="featured-two" name="featured-theme" value="no"  <?= $feature_no?>/>
													<label for="featured-two">No</label>
												</div>
											</div>
											<div class="form-check">
												<label>Activate theme (show this theme on website)</label><br>
												<div class="switch-field">
														<?php $active = 'checked';
															 $in_active= '';
														if(!empty($theme_data)){
															if($theme_data[0]['status']=='Active'){
																$active = "checked";
															}else{
																$in_active = "checked";
															} 
														}?>
													<input type="radio" id="activate-theme-three" name="theme-status" value="Active" <?= $active?>/>
													<label for="activate-theme-three">Yes</label>
													<input type="radio" id="activate-theme-four" name="theme-status" value="In-active"  <?= $in_active?>/>
													<label for="activate-theme-four">No</label>
												</div>
											</div>	
											<div class="form-group">
												<button  type="submit" class="btn btn-black"><?=(!empty($theme_data)?"Update":"Create")?></button>
												<a href="<?= base_url('theme')?>" class="btn btn-cancel btn-border">Cancel</a>
											</div>
									</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>						
</div>

<script>
$(document).ready(function(){
});


// use for form submit
var vRules = 
{
	themename:{required:true},
	// theme_description:{required:true},
	mainvideo:{required:true},
	themes:{required:true},
	meta_title:{required:true},
	meta_description:{required:true},

};
var vMessages = 
{
	themename:{required:"Please Enter theme Name."},
	// theme_description:{required:"Please Enter theme Description Name."},
	mainvideo:{required:"Please Enter Main video."},
	themes:{required:"Please Select Themes."},
	meta_title:{required:"Please Enter Meta Title."},
	meta_description:{required:"Please Enter Meta Description."},

};

$("#form-theme").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{	
		var act = "<?= base_url('theme/submitform')?>";
		$("#form-theme").ajaxSubmit({
			url: act, 
			type: 'post',
			dataType: 'json',
			cache: false,
			clearForm: false,
			async:false,
			beforeSubmit : function(arr, $form, options){
				// $(".btn-primary").hide();
			},
			success: function(response) 
			{
				if(response.success){
					swal("done", response.msg, {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					}).then(
					function() {
						window.location = "<?= base_url('theme')?>";
						// $this->router->fetch_class().'/'.$this->router->fetch_method() ?>";
					});
				}else{	
					swal("Failed", response.msg, {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}
			}
		});
	}
});

document.title="Add/Edit theme"
</script>