
<body>
	<div class="wrapper">
		<div class="main-panel">
			<div class="container">
				<div class="page-inner">
					<div class="col-12">
						<div class="row ff-title-block">
							<div class="col-12 col-sm-12 col-md-10 col-lg-10">
									<h1 class="ff-page-title">Welcome <?= $this->session->userdata('footage_farm_admin')[0]['user_name']?>!</h1> 
							</div>	
							<div class="col-12 col-sm-12 col-md-2 col-lg-2">  
								<select class="form-control form-control" id="defaultSelect">
									<option>Monthly</option>
									<option>Weekly</option>
									<option>Daily</option>
									<option>Yearly</option>									
								</select>	
							</div>						
						</div>
					</div>					
					<div class="row">
							<div class="col-sm-12">
									<div class="card">
											<div class="card-body">
												<div class="row">
													<div class="col-sm-12">
															<h2><strong>New Enquiries</strong></h2>
														<hr/>
														<div class="table-responsive">
															<table id="add-row" class="display table table-striped table-hover" >
																<thead>
																	<tr>
																		<th>Enquiry ID</th>
																		<th>Name</th>
																		<th>Email</th>																
																		<th>No. of Videos</th>																
																		<th>Date</th>	
																		<th>Actions</th>															
																	</tr>
																</thead>
																<tfoot>
																	<tr>
																		<th>Enquiry ID</th>
																		<th>Name</th>
																		<th>Email</th>																
																		<th>No. of Videos</th>																
																		<th>Date</th>	
																		<th>Actions</th>															
																	</tr>
																</tfoot>
																<tbody>
																	<tr>
																	<?php
																		if(!empty($enquiry_data)){
																			$i = 100;
																		foreach ($enquiry_data as $key => $value) {
																			$date_convert = date('y-M-d',strtotime($value['enquiry_date']));
																			$date_convert_array = explode('-',$date_convert);
																		?>
																			<tr>																
																				<td>#<?= $i; ?></td>
																				<td><?= $value['user_name']?></td>																
																				<td><?= $value['email_id']?></td>			
																				<td><?= $value['totol_videos']?></td>													
																				<td><?= $date_convert_array[2].' '.$date_convert_array[1].', '.$date_convert_array[0] ?> </td>	
																				<td>
																					<div class="form-button-action">
																						<a href="<?=base_url('enquiries/detail?text='.rtrim(strtr(base64_encode("id=".$value['user_id']), '+/', '-_'), '=').'')?>" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="View">
																							<i class="fa fa-eye"></i>
																						</a>
																						<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" onclick="delete_row('user_id',<?=$value['user_id']?>)" data-original-title="Remove">
																							<i class="fa fa-times"></i>
																						</a>
																					
																					</div>
																				</td>													
																			</tr>
																			<?php $i++; } } ?>												
																	</tr>														
																</tbody>
															</table>
														</div>
													</div>
												</div>
												<div class="row mt-5">
													<div class="col-sm-6">
														<div class="row ff-title-block">
															<div class="col-12 col-sm-12 col-md-7 col-lg-7">
																<h2><strong>Inactive Videos</strong></h2>
															</div>	
															<div class="col-12 col-sm-12 col-md-5 col-lg-5">  
																<select class="form-control form-control" id="defaultSelect">
																	<option>By Theme</option>
																	<option>By User</option>
																	<option>By Time</option>
																	<option>By Sales</option>									
																</select>	
															</div>						
														</div>														
														<hr/>
														<div class="table-responsive">
															<table class="table table-striped mt-3">
																<thead>
																	<tr>
																		<th scope="col">#</th>
																		<th scope="col">Name</th>
																		<th scope="col">Inactive no.</th>																		
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td>1</td>
																		<td>Civil Rights March - 1965, Alabama</td>
																		<td>21</td>																		
																	</tr>
																	<tr>
																		<td>2</td>
																		<td>Educational VideosI</td>
																		<td>19</td>																		
																	</tr>
																	<tr>
																		<td>3</td>
																		<td>1910 South India, British Raj</td>
																		<td>19</td>																		
																	</tr>
																	<tr>
																		<td>4</td>
																		<td>Animals</td>
																		<td>10</td>																		
																	</tr>
																	<tr>
																		<td>5</td>
																		<td>Great Recession - Post WWI</td>
																		<td>12</td>																		
																	</tr>
																</tbody>
															</table>
														</div>
													</div>
													<div class="col-sm-6">
														<div class="row ff-title-block">
															<div class="col-12 col-sm-12 col-md-7 col-lg-7">
																<h2><strong>Inquiry Report</strong></h2>
															</div>	
															<div class="col-12 col-sm-12 col-md-5 col-lg-5">  
																<select class="form-control form-control" id="defaultSelect">
																	<option>By Theme</option>
																	<option>By User</option>
																	<option>By Time</option>
																	<option>By Sales</option>									
																</select>	
															</div>						
														</div>														
														<hr/>
													</div>
												</div>
												
												
											</div>
									</div>
							</div>
					</div>
				</div>
			</div>
			
		</div>						
	</div>
	<script >
		$(document).ready(function() {
			// Add Row
			$('#add-row').DataTable({
				"pageLength": 5,
			});			
		});

	<?php 
	if($this->session->flashdata('success_msg')) {?>
		// alert("<?= $this->session->flashdata('success_msg')?>");
		swal("Welcome <?= $this->session->userdata('footage_farm_admin')[0]['user_name']?>","<?= $this->session->flashdata('success_msg')?>", {
			icon : "success",
			buttons: {        			
				confirm: {
					className : 'btn btn-success'
				}
			},
		})
	<?php }
	?>

function delete_row(column_name,id){
			var csrfName = "<?= $this->security->get_csrf_token_name()?>"; // Value specified in $config['csrf_token_name']
			var csrfHash = "<?= $this->security->get_csrf_hash()?>"; // CSRF hash
			if(id && column_name){
				var r = confirm("Are you want to sure delete this Enquiry");
				if (r == true) {
					$.ajax({
						url: "<?= base_url('enquiries/delete_row')?>",
						data:{column_name,id,[csrfName]: csrfHash },
						dataType: "json",
						type: "POST",
						success: function(response){
						
							if(response.success){
								swal("done", response.msg, {
									icon : "success",
									buttons: {        			
										confirm: {
											className : 'btn btn-success'
										}
									},
								}).then(
								function() {
									location.reload();
								});
							}else{	
								swal("Failed", response.msg, {
									icon : "error",
									buttons: {        			
										confirm: {
											className : 'btn btn-danger'
										}
									},
								});
							}
						}
					});
				}
			}
		}

		document.title="Footage farm dashboard"
</script>
</body>
</html>
