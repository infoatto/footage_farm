<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Dashboard extends MX_Controller
{
	
	public function __construct(){
		// this is helper function  use to check whether the user is logged in or not file is written in helper folder inside application 
		checklogin();
		parent::__construct();
		$this->load->model('common_model/common_model','common',TRUE);
	}

	public function index(){

		$condition = "1=1 AND c.status= 'Active' ";
		$main_table = array("tbl_video_enquiry_details as c", array("c.user_id,c.enquiry_date,count(c.video_enquiry_detail_id) as totol_videos"));
		$join_tables =  array();
		$join_tables = array(
							array("", "tbl_users as  u", "u.user_id = c.user_id", array("user_name, email_id")),
							
						);
	
		$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("c.video_enquiry_detail_id" => "DESC"),"c.user_id",null); 
		 // fetch query
		$result['enquiry_data']= $this->common->MySqlFetchRow($rs, "array"); // fetch result
		//echo "<pre>";print_r($result['enquiry_data']);exit;

		$this->load->view('main-header');
		$this->load->view('index',$result);
		$this->load->view('footer');
	}
	
	public function logOut(){
		$this->session->unset_userdata('footage_farm_admin');
		redirect(base_url()); 

	}
}