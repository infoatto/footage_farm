
<body>
	<div class="wrapper">
		<div class="main-panel">
			<div class="container">
				<div class="page-inner">
					<div class="col-12">
						<div class="row ff-title-block">
							<div class="col-12 col-sm-12 col-md-6 col-lg-6">
								<h1 class="ff-page-title">Contact Us</h1> 
							</div>
							<div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
								<a href="#" class="btn btn-light btn-border ff-page-title-btn">
										Cancel
								</a>
							</div>
						</div>
					</div>					
					<div class="row">
						<div class="col-sm-12">
							<div class="card">
									<div class="card-body">
										<h3>Header</h3>
										<hr/>
										<form class="" id="form-contact-us" method="post" enctype="multipart/form-data">
										
										<input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden">
										<input name="contact_id" id="contact_id" value="<?= (!empty($contactus_data[0]['contact_id'])?$contactus_data[0]['contact_id']:"") ?>" type="hidden">

										<!-- <div class="form-group">
											<label for="meta_title">Type</label>
											<br>
											<label class="radio-inline">
												<input type="radio" name="optradio" value="video" checked> vimeo 
											</label>
											<label class="radio-inline">
												<input type="radio" name="optradio" value="image" <?=(!empty($contactus_data[0]['banner_image']))?"checked":""?> > Image
											</label>	
										</div> -->
										<!-- <div class="form-group" id="banner_image" style="display:none">
												<label for="banner_image">Image (Min. size 1000px X 1000px) </label>
												<?php $req= '';
													if(empty($contactus_data[0]['banner_image'])){
														$req ='required';
													}else{?>
													<div>
														<img src="<?= FRONT_URL.'/images/contactus/'.$contactus_data[0]['banner_image']?>" alt="..." class="rounded" height="150px" weight="150px">
													</div>
													<br>
														<input name="pre_banner_image" id="pre_banner_image" value="<?= (!empty($contactus_data[0]['banner_image'])?$contactus_data[0]['banner_image']:"") ?>" type="hidden">
													<?php }?>
													
													<input type="file" class="form-control-file" id="banner_image" name="banner_image" <?= $req;?> >
											</div> -->
											<div class="form-group" id="vimeo_link_div">
												<label for="video_url">Google Map embebed Link</label>
												<input type="text" class="form-control" id="video_url" name="video_url" value="<?= (!empty($contactus_data[0]['video_url'])?$contactus_data[0]['video_url']:"") ?>" placeholder="https://vimeo.com/55900216">	
											</div>
										<!-- <div class="form-group">
												<label for="video_url">Header Video URL</label>
												<input type="text" class="form-control" id="video_url"  value="<?= (!empty($contactus_data[0]['video_url'])?$contactus_data[0]['video_url']:"") ?>" name="video_url" placeholder="https://vimeo.com/55900216">
										</div> -->
										<div class="form-group">
											<label for="heading">Heading</label>
											<input type="text" class="form-control" id="heading"  value="<?= (!empty($contactus_data[0]['heading'])?$contactus_data[0]['heading']:"") ?>"  name="heading" placeholder="Contact us">	
										</div>
										<div class="form-group">
											<label for="short_description"> Short Description</label>
											<textarea class="form-control my-text-editor" id="short_description" name="short_description" rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."> <?= (!empty($contactus_data[0]['banner_heading'])?$contactus_data[0]['banner_heading']:"") ?></textarea>	
										</div>
										<div class="form-group">
											<label for="general_enquiries">For general enquiries</label>
											<input type="text" class="form-control" id="general_enquiries"  value="<?= (!empty($contactus_data[0]['general_enquiries'])?$contactus_data[0]['general_enquiries']:"") ?>"  name="general_enquiries" placeholder="Contact us">	
										</div>
										<div class="form-group">
											<label for="phone">Contact Us Phone</label>
											<input type="text" class="form-control" id="phone"  value="<?= (!empty($contactus_data[0]['phone'])?$contactus_data[0]['phone']:"") ?>"  name="phone" placeholder="+44(0)2076313773">	
										</div>
										<div class="form-group">
											<label for="email">Contact Us Email</label>
											<input type="email" class="form-control" id="email"  value="<?= (!empty($contactus_data[0]['email'])?$contactus_data[0]['email']:"") ?>"  name="email" placeholder="info@Footage farm.co.uk">	
										</div>	
										<div class="form-group">
												<label for="meta_title">Meta Title</label>
												<input type="text" class="form-control" id="meta_title" value="<?= (!empty($contactus_data[0]['meta_title'])?$contactus_data[0]['meta_title']:"") ?>"  name="meta_title" placeholder="Early 19th Century">	
											</div>

											<div class="form-group">
												<label for="meta_description">Meta Description</label>
												<textarea class="form-control" id="meta_description" name="meta_description" rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."><?= (!empty($contactus_data[0]['meta_description'])?$contactus_data[0]['meta_description']:"") ?></textarea>	
											</div>
																
										<div class="form-group">
											<button class="btn btn-black">Save</button>
											<a href="<?= base_url('contactus')?>" class="btn btn-cancel btn-border">Cancel</a>
										</div>
										</form>
											
									</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>						
	</div>
</body>
</html>

<script>

$(document).ready(function(){
	// $("input[type='radio']").on("change",function(){
    //         var radioValue = $("input[name='optradio']:checked").val();
    //         if(radioValue){
	// 			if(radioValue == "video"){
	// 				$('#banner_image').hide();
	// 				$('#vimeo_link_div').show();
	// 				$('#pre_banner_image').val("");
	// 			}else{
	// 				$('#banner_image').show();
	// 				$('#vimeo_link_div').hide();
	// 				$('#video_url').val("");

	// 			}
    //             // alert("Your are a - " + radioValue);
    //         }
    //     });
	
});


// use for form submit
var vRules = 
{
	video_url:{required:true},
	banner_heading:{required:true},
	heading:{required:true},
	general_enquiries:{required:true},
	phone:{required:true},
	email:{required:true}
};
var vMessages = 
{
	video_url:{required:"Please Enter Video Url."},
	banner_heading:{required:"Please Enter Banner Heading"},
	heading:{required:"Please Enter Heading."},
	general_enquiries:{required:"This field required"},
	phone:{required:"This field required"},
	email:{required:"This field required"}
};

$("#form-contact-us").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{	
		var act = "<?= base_url('contactus/submitform')?>";
		$("#form-contact-us").ajaxSubmit({
			url: act, 
			type: 'post',
			dataType: 'json',
			cache: false,
			clearForm: false,
			async:false,
			beforeSubmit : function(arr, $form, options){
				// $(".btn-primary").hide();
			},
			success: function(response) 
			{
				if(response.success){
					swal("done", response.msg, {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					}).then(
					function() {
						window.location = "<?= base_url('contactus')?>";
						// $this->router->fetch_class().'/'.$this->router->fetch_method() ?>";
					});
				}else{	
					swal("Failed", response.msg, {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}
			}
		});
	}
});

document.title="Add/Edit Contact us "
</script>