<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Contactus extends MX_Controller
{
	
	public function __construct(){
		parent::__construct();
		checklogin();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('contactusmodel','',TRUE);
	}

	public function index(){
		$result = array();
		$condition = "1=1";
		$result['contactus_data'] = $this->common->getData("tbl_contact_us",'*',$condition);

		// echo "<pre>";	
		// print_r($result);
		// exit;

		$this->load->view('main-header.php');
		$this->load->view('addEdit.php',$result);
		$this->load->view('footer.php');
	}

	public function addEdit(){
		$contact_id = "";
		$result = array();
		if(!empty($_GET['text']) && isset($_GET['text'])){
			$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
			parse_str($varr,$url_prams);
			$contact_id = $url_prams['id'];
			$condition = " contact_id ='".$contact_id."' ";
			$result['contactus_data'] = $this->common->getData("tbl_contact_us",'*',$condition);
		}
		
		$this->load->view('main-header.php');
		$this->load->view('addEdit.php',$result);
		$this->load->view('footer.php');
	}

	

	function submitForm(){
		// echo "<pre>";	
		// print_r($_POST);
		// print_r($_FILES);
		// exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();
			$data['video_url'] = $this->input->post('video_url');
			$data['heading'] = $this->input->post('heading');
			$data['general_enquiries'] = $this->input->post('general_enquiries');
			$data['phone'] = $this->input->post('phone');
			$data['email'] = $this->input->post('email');
			$data['banner_heading'] = $this->input->post('short_description');
			$data['updated_on'] = date("Y-m-d H:i:s");
			$data['updated_by'] = $this->session->userdata('footage_farm_admin')[0]['user_id'];
			$data['meta_title'] = $this->input->post('meta_title');
			$data['meta_description'] = $this->input->post('meta_description');

			$banner_image = "";
			// if(isset($_FILES) && isset($_FILES["banner_image"]["name"])){
			// 	$this->load->library('upload');
			// 	$this->upload->initialize($this->set_upload_options());
			// 	if (!$this->upload->do_upload("banner_image")){
			// 		$image_error = array('error' => $this->upload->display_errors());
			// 		echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
			// 		exit;
			// 	}else{
			// 		$image_data = array('upload_data' => $this->upload->data());
			// 		$banner_image = $image_data['upload_data']['file_name']; 
			// 		$this->setCompresseredImage($image_data['upload_data']['file_name']);
			// 	}	
			// 	/* Unlink previous category image */
			// 	if(!empty($this->input->post('about_us_id'))){	
			// 		$condition_image = " about_us_id = ".$this->input->post('about_us_id');
			// 		$image =$this->common->getData("tbl_about_us",'*',$condition_image);
			// 		if(is_array($image) && !empty($image[0]->banner_image) && file_exists(DOC_ROOT_FRONT."/images/contactus/".$image[0]->banner_image))
			// 		{
			// 			unlink(DOC_ROOT_FRONT."/images/contactus/".$image[0]->banner_image);
			// 		}
			// 	}
			// }else{
			// 	$banner_image = $this->input->post('pre_banner_image');
			// }

			// $data['banner_image'] = $banner_image;

			if(!empty($this->input->post('contact_id'))){
				// print_r($data);
				$condition = "contact_id = '".$this->input->post('contact_id')."' ";
				$result = $this->common->updateData("tbl_contact_us",$data,$condition);
				if($result){
					echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
					exit;
				}
				else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while updating data.'));
					exit;
				}
			}else{
				$data['created_on'] = date("Y-m-d H:i:s");
				$data['created_by'] =  $this->session->userdata('footage_farm_admin')[0]['user_id'];
				$result = $this->common->insertData('tbl_contact_us',$data,'1');
				if(!empty($result)){
					echo json_encode(array('success'=>true, 'msg'=>'Record Added Successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
					exit;
				}
			}
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem While Add/Edit Data.'));
			exit;
		}
	}

	function set_upload_options($file_name = ""){   
		$config = array();
		if(!empty($file_name) &&  $file_name != ""){
			$config['file_name']   = $file_name.time();
		}
		$config['upload_path'] = DOC_ROOT_FRONT."/images/contactus/";
		if(!is_dir($config['upload_path'])){
			mkdir($config['upload_path'],0777, true);		
		}
		$config['allowed_types'] = 'gif|jpg|png|jpeg|svg|gif|mp4';
		$config['max_size']      = '0';
		$config['overwrite']     = FALSE;
		// $config['min_width']            = 1000;
		// $config['min_height']           = 1000;
		return $config;
	}

	public function setCompresseredImage($image_name){
		$config1 = array();
		$this->load->library('image_lib');
		$config1['image_library'] = 'gd2';
		$config1['source_image'] = DOC_ROOT_FRONT."/images/contactus/".$image_name;
		$config1['maintain_ratio'] = TRUE;
		$config1['quality'] = '100%';
		$config1['width'] = 1000;
		$config1['height'] = 1000;
		$config1['new_image'] = DOC_ROOT_FRONT."/images/contactus/".$image_name;
		$this->image_lib->initialize($config1);
		$this->image_lib->resize();
		$this->image_lib->clear();
	} 
}