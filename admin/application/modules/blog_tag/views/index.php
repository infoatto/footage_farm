
<body>
	<div class="wrapper">
    
		<div class="main-panel">
			<div class="container">
				<div class="page-inner">
					<div class="col-12">
						
						<div class="row ff-title-block">
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">
							<h1 class="ff-page-title">Blog Tags </h1> 
						</div>
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
							<a href="<?= base_url('blog_tag/addEdit')?>" class="btn btn-primary ml-2 ff-page-title-btn">
							<i class="fas fa-plus"></i>
								Add Blog Tag
							</a>
						</div>
					</div>
					</div>					
					<div class="row">
							<div class="col-sm-12">
									<div class="card">
											<div class="card-body">
												<div class="table-responsive">
													<table id="add-row" class="display table table-striped table-hover" >
														<thead>
															<tr>
																<th>Blog Tag Name</th>
																<th>Blog Tag  Description</th>																	
																<th>status</th>	
																<th>Action</th>	
																														
															</tr>
														</thead>
														<tfoot>
															<tr>
																<th>Blog Tag Name</th>
																<th>Blog Tag  Description</th>																	
																<th>status</th>	
																<th>Action</th>												
															</tr>
														</tfoot>
														<tbody>
														<?php
														// echo "<pre>";
														// print_r($blog_tag_data);
														if(!empty($blog_tag_data) && isset($blog_tag_data))	{
															$cnt = 0;
															foreach ($blog_tag_data as $key => $value) {
																	$cnt= ++$cnt?>
																	<tr>														
																		<td><?=$value['blog_tag_name']?></td>
																		<td><?=$value['blog_tag_description']?></td>		
																		<td><?= $value['status']?></td>											
																		<td>
																			<div class="form-button-action">
																				<a href="<?= base_url('blog_tag/AddEdit?text='.rtrim(strtr(base64_encode("id=".$value['blog_tag_id']), '+/', '-_'), '=').'')?>" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Edit">
																					<i class="fa fa-edit"></i>
																				</a>
																				<!-- <a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																					<i class="fa fa-times"></i>
																				</a> -->
																			</div>
																		</td>													
																	</tr>
															<?php }
														}?>
														</tbody>
													</table>
												</div>
											</div>
									</div>
							</div>
					</div>
				</div>
			</div>
			
		</div>						
	</div>
	<script >
		$(document).ready(function() {
			// Add Row
			$('#add-row').DataTable({
				"pageLength": 5,
			});			
		});
	</script>
</body>
</html>
