<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Blog_tag extends MX_Controller
{
	
	public function __construct(){
		parent::__construct();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('blog_tagmodel','',TRUE);
	}

	public function index(){
		$result = array();
		$condition = "1=1";
		$result['blog_tag_data'] = $this->common->getData("tbl_blog_tags",'*',$condition);

		$this->load->view('main-header.php');
		$this->load->view('index.php',$result);
		$this->load->view('footer.php');
	}

	public function addEdit(){

		$blog_tag_id = "";
		//print_r($_GET);
		$result = array();
		if(!empty($_GET['text']) && isset($_GET['text'])){
			$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
			parse_str($varr,$url_prams);
			$blog_tag_id = $url_prams['id'];
			$condition = " blog_tag_id ='".$blog_tag_id."' ";
			$result['blog_tag_data'] = $this->common->getData("tbl_blog_tags",'*',$condition);
		}
		
		// echo "<pre>";
		// print_r($result);
		// exit;
		// echo $user_id;
		$this->load->view('main-header.php');
		$this->load->view('addEdit.php',$result);
		$this->load->view('footer.php');
	}

	function submitForm(){
		// echo "<pre>";	
		// print_r($_POST);
		// exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();
			$data['blog_tag_name'] = $this->input->post('blog_tag_name');
			$data['blog_tag_description'] = $this->input->post('blog_tag_description');
			$data['status'] = $this->input->post('tag-status');
			$data['updated_on'] = date("Y-m-d H:i:s");
			$data['updated_by'] = $this->session->userdata('footage_farm_admin')[0]['user_id'];
			if(!empty($this->input->post('blog_tag_id'))){
				// print_r($data);
				$condition = "blog_tag_id = '".$this->input->post('blog_tag_id')."' ";
				$result = $this->common->updateData("tbl_blog_tags",$data,$condition);
				if($result){
					echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
					exit;
				}
				else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while updating data.'));
					exit;
				}
			}else{
				$data['created_on'] = date("Y-m-d H:i:s");
				$data['created_by'] =  $this->session->userdata('footage_farm_admin')[0]['user_id'];
				$result = $this->common->insertData('tbl_blog_tags',$data,'1');
				if(!empty($result)){
					echo json_encode(array('success'=>true, 'msg'=>'Record Added Successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
					exit;
				}
			}
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem While Add/Edit Data.'));
			exit;
		}
	}
}