<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Users extends MX_Controller
{
	
	public function __construct(){
		parent::__construct();
		checklogin();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('usersmodel','',TRUE);

	}

	public function index(){
		$result = array();
		$condition = "1=1 AND role_id = '5' AND  user_type ='customer' ";
		$result['user_data'] = $this->common->getData("tbl_users",'*',$condition);

		// echo "<pre>";
		// print_r($result);
		// exit;
		$this->load->view('main-header.php');
		$this->load->view('index.php',$result);
		$this->load->view('footer.php');
	}

	public function addEdit(){

		$tag_id = "";
		//print_r($_GET);
		$result = array();
		if(!empty($_GET['text']) && isset($_GET['text'])){
			$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
			parse_str($varr,$url_prams);
			$tag_id = $url_prams['id'];
			$condition = " user_id ='".$tag_id."' ";
			$result['user_data'] = $this->common->getData("tbl_users",'*',$condition);
		}
		
		// echo "<pre>";
		// print_r($result);
		// exit;
		//echo $user_id;
		$this->load->view('main-header.php');
		$this->load->view('addEdit.php',$result);
		$this->load->view('footer.php');
	}

	function submitformtesting(){
		// codeigniter form validation 
		// $this->form_validation->set_rules('themename', 'Themename', 'trim|required|alpha');
		// $this->form_validation->set_rules('mainvideo', 'Main video', 'trim|required');
		// $this->form_validation->set_rules('meta_title', 'Meta title', 'trim|required');
		// $this->form_validation->set_rules('meta_description', 'Meta description', 'trim|required');
		// $this->form_validation->set_rules('mainvideo', 'Main video', 'trim|required');

		// if($this->form_validation->run()) {  
		// 	// true  if validation property sectifice properly
		// 	$themename = $this->input->post('themename');  
		// 	$mainvideo = $this->input->post('mainvideo'); 
		// 	$themename = $this->input->post('meta_title');  
		// 	$mainvideo = $this->input->post('meta_description'); 
		// 	$themename = $this->input->post('mainvideo');  
		// 	$mainvideo = $this->input->post('meta_title'); 
		// }else{  
		// 	// if validation is not proper
		// 	$this->index();  
		// } 
	}


	function submitForm(){
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

				$condition = "email_id = '".trim($this->input->post('useremail'))."' ";
				if($this->input->post('user_id') && $this->input->post('user_id') > 0){
					$condition .= " AND  user_id != ".$this->input->post('user_id')." ";
				}			
				$check_name = $this->common->getData("tbl_users",'*',$condition);
		
				if(!empty($check_name[0]['user_id'])){
					echo json_encode(array("success"=>false, 'msg'=>'Email of this user is Already Present!'));
					exit;
				}
			
				$data = array();
				$condition =" role_id = ".$this->input->post('role_id')." ";
				$get_usertype = $this->common->getData("tbl_roles","*",$condition);
				$data['user_type'] = $get_usertype[0]['role_name'];
				$data['role_id'] = $this->input->post('role_id');
				$data['user_name'] = $this->input->post('username');
				$data['email_id'] = $this->input->post('useremail');
				$data['phone'] = $this->input->post('phone');
				$data['status'] = $this->input->post('user-status');
				$data['password'] = md5('123456');
				$data['created_on'] = date("Y-m-d H:i:s");
				$data['created_by'] = $this->session->userdata('footage_farm_admin')[0]['user_id'];


				if(!empty($this->input->post('user_id'))){
					// print_r($data);
					$condition = "user_id = '".$this->input->post('user_id')."' ";
					$result = $this->common->updateData("tbl_users",$data,$condition);
					if($result){
						echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
						exit;
					}
					else{
						echo json_encode(array('success'=>false, 'msg'=>'Problem while updating data.'));
						exit;
					}
				}else{
					$data['created_on'] = date("Y-m-d H:i:s");
					$data['created_by'] =  $this->session->userdata('footage_farm_admin')[0]['user_id'];
					$result = $this->common->insertData('tbl_users',$data,'1');
					if(!empty($result)){
						$this->send_mail("User",$this->input->post('username'),$this->input->post('useremail'));
						echo json_encode(array('success'=>true, 'msg'=>'Record Added Successfully.'));
						exit;
					}else{
						echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
						exit;
					}
				}
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem While Add/Edit Data.'));
			exit;
		}
	}




	
						
	

	public function send_mail($role_type,$username,$email_id){
		
		$data12['notification_text'] = "Hi,<br> shiv Asked the Question in Forum about 'testing' Course <a href='forum/addEdit?text=".rtrim(strtr(base64_encode("id=".$forum_id), '+/', '-_'), '=')." title='Reply'><i class='fa fa-reply'></i>Reply</a>";


		$result = array();
		$result['user_type']= $role_type;
		$result['username']=  $username;
		$result['email_id']= $email_id ;
		$result['password']= '123456' ;
	
		$member_mail_hmtl = $this->load->view('member-mail',$result,true);
		$this->email->clear();
		$this->email->from(FROM_EMAIL); // change it to yours
		$this->email->to($email_id); // change it to yours
		$this->email->subject("Enrollement in Footage farm");
		$this->email->message($member_mail_hmtl);
		$this->email->send();

	}
}