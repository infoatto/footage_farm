
<div class="wrapper">
	<div class="main-panel">
		<div class="container">
			<div class="page-inner">
				<div class="col-12">
					<div class="row ff-title-block">
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">
							<h1 class="ff-page-title">Add/Edit User</h1> 
						</div>
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
							<a href="<?= base_url('users')?>" class="btn btn-light btn-border ff-page-title-btn">
								Cancel
							</a>
						</div>
					</div>
				</div>					
				<div class="row">
					<div class="col-sm-12">
						<div class="card">
							<div class="card-body">
								<form class="" id="form-user" method="post" enctype="multipart/form-data">
								
									<input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden">
									<input name="user_id" id="user_id" value="<?= (!empty($user_data[0]['user_id'])?$user_data[0]['user_id']:"") ?>" type="hidden">
									<input name="role_id" id="role_id" value="5" type="hidden">
								
											<div class="form-group">
												<label for="username">Name</label>
												<input type="text" class="form-control" id="username" name="username"  value="<?= (!empty($user_data[0]['user_name'])?$user_data[0]['user_name']:"") ?>" placeholder="John Doe">	
											</div>
										
											<div class="form-group">
												<label for="useremail">Email</label>
												<input type="email" class="form-control" id="useremail" value="<?= (!empty($user_data[0]['email_id'])?$user_data[0]['email_id']:"") ?>" name="useremail" placeholder="johndoe@gmail.com">	
											</div>

											<div class="form-group">
												<label for="phone">Phone</label>
												<input type="text" class="form-control" id="phone" name="phone" value="<?= (!empty($user_data[0]['phone'])?$user_data[0]['phone']:"") ?>"   minlength="10" maxlength="10" placeholder="123-456-7890">	
											</div>
										
											<div class="form-check">
												<label>User Status</label><br>
												<div class="switch-field">
														<?php $active = 'checked';
															 $in_active= '';
														if(!empty($user_data)){
															if($user_data[0]['status']=='Active'){
																$active = "checked";
															}else{
																$in_active = "checked";
															} 
														}?>
													<input type="radio" id="activate-tag-three" name="user-status" value="Active" <?= $active?>/>
													<label for="activate-tag-three">Active</label>
													<input type="radio" id="activate-tag-four" name="user-status" value="In-active"  <?= $in_active?>/>
													<label for="activate-tag-four">In-active</label>
												</div>
											</div>	
											<div class="form-group">
												<button  type="submit" class="btn btn-black"><?=(!empty($user_data)?"Update":"Create")?></button>
												<a href="<?= base_url('users')?>" class="btn btn-cancel btn-border">Cancel</a>
											</div>
									</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>						
</div>

<script>
document.title="Add/Edit Users"
$(document).ready(function(){
});


// use for form submit
var vRules = 
{
	username:{required:true},
	useremail:{required:true},
	phone:{required:true},

};
var vMessages = 
{
	Queusernamestion:{required:"Please Enter Username."},
	Anuseremailswer:{required:"Please Enter Email id."},
	phone:{required:"Please Enter the Phone Number "},
};

$("#form-user").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{	
		var act = "<?= base_url('users/submitform')?>";
		$("#form-user").ajaxSubmit({
			url: act, 
			type: 'post',
			dataType: 'json',
			cache: false,
			clearForm: false,
			async:false,
			beforeSubmit : function(arr, $form, options){
				// $(".btn-primary").hide();
			},
			success: function(response) 
			{
				if(response.success){
					swal("done", response.msg, {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					}).then(
					function() {
						window.location = "<?= base_url('users')?>";
						// $this->router->fetch_class().'/'.$this->router->fetch_method() ?>";
					});
				}else{	
					swal("Failed", response.msg, {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}
			}
		});
	}
});


</script>