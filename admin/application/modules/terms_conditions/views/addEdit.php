
<div class="wrapper">
	<div class="main-panel">
		<div class="container">
			<div class="page-inner">
				<div class="col-12">
					<div class="row ff-title-block">
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">
							<h1 class="ff-page-title">Terms Conditions</h1> 
						</div>
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
							<a href="<?= base_url('terms_conditions')?>" class="btn btn-light btn-border ff-page-title-btn">
								Cancel
							</a>
						</div>
					</div>
				</div>					
				<div class="row">
					<div class="col-sm-12">
						<div class="card">
							<div class="card-body">
								<form class="" id="form-faq" method="post" enctype="multipart/form-data">
								
									<input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden">
									<input name="terms_conditions_id" id="terms_conditions_id" value="<?= (!empty($terms_conditions_data[0]['terms_conditions_id'])?$terms_conditions_data[0]['terms_conditions_id']:"") ?>" type="hidden">
							
										<div class="form-group">
											<label for="terms_conditions">Terms & Conditions</label>
											<textarea class="form-control my-text-editor" id="terms_conditions" name="terms_conditions" rows="5" placeholder="Add an terms_conditions" spellcheck="false"><?= (!empty($terms_conditions_data[0]['terms_conditions'])?$terms_conditions_data[0]['terms_conditions']:"") ?></textarea>	
										</div> 
										
										<div class="form-group">
											<button  type="submit" class="btn btn-black"><?=(!empty($terms_conditions_data)?"Update":"Create")?></button>
											<a href="<?= base_url('terms_conditions')?>" class="btn btn-cancel btn-border">Cancel</a>
										</div>
									</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>						
</div>

<script>
document.title="Add/Edit terms Conditions"
$(document).ready(function(){
});


// use for form submit
var vRules = 
{
	Question:{required:true},
	terms_conditions:{required:true}
};
var vMessages = 
{
	Question:{required:"Please Enter Question."},
	terms_conditions:{required:"Please Enter terms_conditions."},
};

$("#form-faq").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{	
		var act = "<?= base_url('terms_conditions/submitform')?>";
		$("#form-faq").ajaxSubmit({
			url: act, 
			type: 'post',
			dataType: 'json',
			cache: false,
			clearForm: false,
			async:false,
			beforeSubmit : function(arr, $form, options){
				// $(".btn-primary").hide();
			},
			success: function(response) 
			{
				if(response.success){
					swal("done", response.msg, {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					}).then(
					function() {
						window.location = "<?= base_url('terms_conditions')?>";
						// $this->router->fetch_class().'/'.$this->router->fetch_method() ?>";
					});
				}else{	
					swal("Failed", response.msg, {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}
			}
		});
	}
});


</script>