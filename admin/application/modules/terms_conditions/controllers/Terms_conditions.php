<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Terms_conditions extends MX_Controller
{
	
	public function __construct(){
		checklogin();
		parent::__construct();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('terms_conditionsmodel','',TRUE);
	}

	public function index(){
		$result = array();
		$condition = "1=1";
		$result['terms_conditions_data'] = $this->common->getData("tbl_terms_conditions",'*',$condition);

		$this->load->view('main-header.php');
		$this->load->view('addEdit',$result);
		$this->load->view('footer.php');
	}


	function submitForm(){
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();
			$data['terms_conditions'] = $this->input->post('terms_conditions');
			$data['updated_on'] = date("Y-m-d H:i:s");
			$data['updated_by'] = $this->session->userdata('footage_farm_admin')[0]['user_id'];
			if(!empty($this->input->post('terms_conditions_id'))){
				// print_r($data);
				$condition = "terms_conditions_id = '".$this->input->post('terms_conditions_id')."' ";
				$result = $this->common->updateData("tbl_terms_conditions",$data,$condition);
				if($result){
					echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
					exit;
				}
				else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while updating data.'));
					exit;
				}
			}else{
				$data['created_on'] = date("Y-m-d H:i:s");
				$data['created_by'] =  $this->session->userdata('footage_farm_admin')[0]['user_id'];
				$result = $this->common->insertData('tbl_terms_conditions',$data,'1');
				if(!empty($result)){
					echo json_encode(array('success'=>true, 'msg'=>'Record Added Successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
					exit;
				}
			}
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem While Add/Edit Data.'));
			exit;
		}
	}
}