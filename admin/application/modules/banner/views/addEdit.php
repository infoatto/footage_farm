<div class="wrapper">
	<div class="main-panel">
		<div class="container">
			<div class="page-inner">
				<div class="col-12">
					<div class="row ff-title-block">
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">
							<h1 class="ff-page-title">New Banner</h1> 
						</div>
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
							<a href="<?= base_url('banner')?>" class="btn btn-primary ff-page-title-btn">
								Cancel
							</a>
						</div>
					</div>
				</div>					
				<div class="row">
					<div class="col-sm-12">
						<div class="card">
							<div class="card-body">
								<form class="" id="form-theme" method="post" enctype="multipart/form-data">
									
									<input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden">
									<input name="banner_id" id="banner_id" value="<?= (!empty($banner_data[0]['banner_id'])?$banner_data[0]['banner_id']:"") ?>" type="hidden">
									<input name="withoutall" id="withoutall" value="<?= (!empty($banner_data[0]['withoutall'])?$banner_data[0]['withoutall']:"") ?>" type="hidden">
										

										<div class="form-group" >
												<label for="vimeo_link">Home Heading</label>
												<textarea class="form-control my-text-editor" id="banner_post_body" rows="20" name="banner_post_body" placeholder="eg. Asurprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor." spellcheck="false"><?= (!empty($banner_data[0]['home_heading'])?$banner_data[0]['home_heading']:"") ?></textarea>	
										</div>

											
										<div class="form-group">
											<label for="meta_title">Banner type </label>
											<br>
											<label class="radio-inline">
												<input type="radio" name="optradio" value="video" checked> vimeo 
											</label>
											<label class="radio-inline">
												<input type="radio" name="optradio" value="image" <?=(!empty($banner_data[0]['thumbnail_image']))?"checked":""?> > Image
											</label>	
											<label class="radio-inline">
												<input type="radio" name="optradio" value="withoutall_check" <?=(!empty($banner_data[0]['withoutall']))?"checked":""?> > Without Vimeo/Image
											</label>	
										</div>

											<div class="form-group" id="banner_image" style="display:none">
												<label for="bannerfile">Banner Thumbnail Image (Min. size 1000px X 1000px) </label>
												<?php $req= '';
												if(empty($banner_data[0]['thumbnail_image']) && isset($banner_data[0]['thumbnail_image'])){
													$req ='required';
												}else{?>
												<br>
													<img src="<?= FRONT_URL.'/images/banner_image/'.$banner_data[0]['thumbnail_image']?>"class="img-thumbnail" alt="Cinque Terre" width="250" height="300">
													<input name="pre_bannerfile_name" id="pre_bannerfile_name" value="<?= (!empty($banner_data[0]['thumbnail_image'])?$banner_data[0]['thumbnail_image']:"") ?>" type="hidden">
												<?php }?>
												
												<input type="file" class="form-control-file" id="bannerfile" name="bannerfile" <?= $req;?> >
											</div>

										

											<div class="form-group" id="vimeo_link_div">
												<label for="vimeo_link">Vimeo Link</label>
												<input type="text" class="form-control" id="vimeo_link" name="vimeo_link" value="<?= (!empty($banner_data[0]['vimeo_link'])?$banner_data[0]['vimeo_link']:"") ?>" placeholder="Early 19th Century">	
											</div>

											<div class="form-group" id="banner_title_div" >
												<label for="banner_title">Banner Title</label>
												<input type="text" class="form-control" id="banner_title" name="banner_title" value="<?= (!empty($banner_data[0]['banner_title'])?$banner_data[0]['banner_title']:"") ?>" placeholder="Enter Banner Title here ">	
											</div>
											
										
											<div class="form-group">
												<label for="meta_title">Meta Title</label>
												<input type="text" class="form-control" id="meta_title" value="<?= (!empty($banner_data[0]['meta_title'])?$banner_data[0]['meta_title']:"") ?>"  name="meta_title" placeholder="Early 19th Century">	
											</div>
											<div class="form-group">
												<label for="meta_description">Meta Description</label>
												<textarea class="form-control" id="meta_description" name="meta_description" rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."><?= (!empty($banner_data[0]['meta_description'])?$banner_data[0]['meta_description']:"") ?></textarea>	
											</div>
										
											<div class="form-check">
												<label>Banner Status</label><br>
												<div class="switch-field">
														<?php $active = 'checked';
															 $in_active= '';
														if(!empty($banner_data)){
															if($banner_data[0]['status']=='Active'){
																$active = "checked";
															}else{
																$in_active = "checked";
															} 
														}?>
													<input type="radio" id="activate-theme-three" name="banner_status" value="Active" <?= $active?>/>
													<label for="activate-theme-three">Yes</label>
													<input type="radio" id="activate-theme-four" name="banner_status" value="In-active"  <?= $in_active?>/>
													<label for="activate-theme-four">No</label>
												</div>
											</div>	
											<div class="form-group">
												<button  type="submit" class="btn btn-black"><?=(!empty($banner_data)?"Update":"Create")?></button>
												<a href="<?= base_url('theme')?>" class="btn btn-cancel btn-border">Cancel</a>
											</div>
									</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>						
</div>

<script>
$(document).ready(function(){
	$("input[type='radio']").on("change",function(){
            var radioValue = $("input[name='optradio']:checked").val();
            if(radioValue){
				if(radioValue == "video"){
					$('#banner_image').hide();
					$('#vimeo_link_div').show();
					$('#banner_title_div').show();
					$('#pre_bannerfile_name').val("");
					$('#withoutall').val("0");
				}else if(radioValue == "withoutall_check"){
					$('#banner_image').hide();
					$('#vimeo_link_div').hide();
					$('#banner_title_div').hide();
					$('#pre_bannerfile_name').val("");
					$('#vimeo_link').val("");
					$('#withoutall').val("1");

				}else{
					$('#banner_image').show();
					$('#vimeo_link_div').hide();
					$('#banner_title_div').hide();
					$('#pre_bannerfile_name').val("");
					$('#vimeo_link').val("");
					$('#withoutall').val("0");



				}
                // alert("Your are a - " + radioValue);
            }
        });
});

<?php 
if(!empty($banner_data[0]['thumbnail_image']) && isset($banner_data[0]['thumbnail_image'])){?>
			$('#banner_image').show();
			$('#vimeo_link').val("");
			$('#vimeo_link_div').hide();
	<?php }
?>

// use for form submit
// var vRules = 
// {
// 	baner:{required:true},
// 	// theme_description:{required:true},
// 	mainvideo:{required:true},
// 	meta_title:{required:true},
// 	meta_description:{required:true},

// };
// var vMessages = 
// {
// 	vimeo_link:{required:"Please Enter theme Name."},
// 	// theme_description:{required:"Please Enter theme Description Name."},
// 	mainvideo:{required:"Please Enter Main video."},
// 	meta_title:{required:"Please Enter Meta Title."},
// 	meta_description:{required:"Please Enter Meta Description."},

// };

$("#form-theme").validate({
	// rules: vRules,
	// messages: vMessages,
	submitHandler: function(form) 
	{	
		var act = "<?= base_url('banner/submitform')?>";
		$("#form-theme").ajaxSubmit({
			url: act, 
			type: 'post',
			dataType: 'json',
			cache: false,
			clearForm: false,
			async:false,
			beforeSubmit : function(arr, $form, options){
				$(".btn-primary").hide();
			},
			success: function(response) 
			{
				if(response.success){
					swal("done", response.msg, {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					}).then(
					function() {
						location.reload();
					});
				}else{	
					swal("Failed", response.msg, {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}
			}
		});
	}
});
document.title="Add/Edit Banner"

</script>