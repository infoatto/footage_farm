<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Banner extends MX_Controller
{
	public function __construct(){
		parent::__construct();
		checklogin();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('bannermodel','',TRUE);
	}
	public function index(){
	}

	public function addEdit(){

		$banner_id = "";
		//print_r($_GET);
		$result = array();
		// if(!empty($_GET['text']) && isset($_GET['text'])){
		// 	$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
		// 	parse_str($varr,$url_prams);
		// 	$banner_id = $url_prams['id'];
		// }
		
		$condition = "banner_id ='1' ";
		$result['banner_data'] = $this->common->getData("tbl_banner",'*',$condition);
		// echo "<pre>";
		// print_r($result);
		// exit;
		//echo $user_id;
		$this->load->view('main-header.php');
		$this->load->view('addEdit.php',$result);
		$this->load->view('footer.php');
	}


	public function submitForm(){
		// print_r($_POST);
		// print_r($_FILES);
		// exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();
			
			$thumnail_value = "";
			if(isset($_FILES) && isset($_FILES["bannerfile"]["name"])){
				 $config = array();
				$config['upload_path'] = DOC_ROOT_FRONT."/images/banner_image/";
				$config['max_size']    = '0';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['min_width']            = 1000;
                $config['min_height']           = 1000;
				// $config['allowed_types'] = '*';
				// $config['file_name']     = md5(uniqid("100_ID", true));
				$config['file_name']     = $_FILES["bannerfile"]["name"];
				$this->load->library('upload', $config);
				if (!$this->upload->do_upload("bannerfile")){
					$image_error = array('error' => $this->upload->display_errors());
					echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
					exit;
				}else{
					$image_data = array('upload_data' => $this->upload->data());
					$thumnail_value = $image_data['upload_data']['file_name']; 
					$this->setCompresseredImage($image_data['upload_data']['file_name']);

				}	
				/* Unlink previous category image */
				if(!empty($this->input->post('banner_id'))){	
					$condition_image = " banner_id = ".$this->input->post('banner_id');
					$image =$this->common->getData("tbl_banner",'*',$condition_image);
					if(is_array($image) && !empty($image[0]->thumbnail_image) && file_exists(DOC_ROOT_FRONT."/images/theme_image/".$image[0]->thumbnail_image))
					{
						unlink(DOC_ROOT_FRONT."/images/theme_image/".$image[0]->thumbnail_image);
					}
				}
			}else{
				$thumnail_value = $this->input->post('pre_bannerfile_name');
			}

			$data['thumbnail_image'] = $thumnail_value;
			$data['home_heading'] = $this->input->post('banner_post_body');
			$data['vimeo_link'] = $this->input->post('vimeo_link');
			$data['withoutall'] = $this->input->post('withoutall');
			$data['banner_title'] = $this->input->post('banner_title');
			$data['status'] = $this->input->post('banner_status');
			$data['meta_title'] = $this->input->post('meta_title');
			$data['meta_description'] = $this->input->post('meta_description');
			$data['updated_on'] = date("Y-m-d H:i:s");
			$data['updated_by'] = $this->session->userdata('footage_farm_admin')[0]['user_id'];
			if(!empty($this->input->post('banner_id'))){
				// print_r($data);
				$condition = "banner_id = '".$this->input->post('banner_id')."' ";
				$result = $this->common->updateData("tbl_banner",$data,$condition);
				if($result){
					echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
					exit;
				}
				else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while updating data.'));
					exit;
				}
			}else{
				$data['created_on'] = date("Y-m-d H:i:s");
				$data['created_by'] =  $this->session->userdata('footage_farm_admin')[0]['user_id'];
				$result = $this->common->insertData('tbl_banner',$data,'1');
				if(!empty($result)){
					echo json_encode(array('success'=>true, 'msg'=>'Record Added Successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
					exit;
				}
			}
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem While Add/Edit Data.'));
			exit;
		}
	}
	public function setCompresseredImage($image_name){
		$config1 = array();
		$this->load->library('image_lib');
		$config1['image_library'] = 'gd2';
		$config1['source_image'] = DOC_ROOT_FRONT."/images/theme_image/".$image_name;
		$config1['maintain_ratio'] = TRUE;
		$config1['quality'] = '100%';
		$config1['width'] = 1000;
		$config1['height'] = 1000;
		$config1['new_image'] = DOC_ROOT_FRONT."/images/theme_image/".$image_name;
		$this->image_lib->initialize($config1);
		$this->image_lib->resize();
		$this->image_lib->clear();
		}
}