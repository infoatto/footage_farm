<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title>Footage Farm - Admin Panel</title>
	<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<link rel="icon" href="https://via.placeholder.com/50" type="image/x-icon"/>

	<!-- Fonts and icons -->
	<script src="<?php echo base_url("assets/js/plugin/webfont/webfont.min.js")?>"></script>
	<script>
		WebFont.load({
			google: {"families":["Lato:300,400,700,900"]},
			custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"],
			 urls: ['<?=base_url('assets/css/fonts.min.css') ?>']},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>

	<!-- CSS Files -->
	<link rel="stylesheet" href="<?php echo base_url("assets/css/bootstrap.min.css"); ?>">
	<link rel="stylesheet" href="<?php echo base_url("assets/css/atlantis.css"); ?>">
	<link rel="stylesheet" href="<?php echo base_url("assets/css/jquery.fancybox.min.css"); ?>">
	<link rel="stylesheet" href="<?php echo base_url("assets/css/select2.css"); ?>">
	<link rel="stylesheet" href="<?php echo base_url("assets/css/custom.css"); ?>">

	<link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/medium-editor/latest/css/medium-editor.min.css" type="text/css" charset="utf-8">
    <link rel="stylesheet" href="<?php echo base_url("assets/helpers/medium-demo.css");?>">
    <link rel="stylesheet" href="<?php echo base_url("assets/helpers/medium-insert.css"); ?>">

			<script src="<?php echo base_url("assets/js/jquery.3.2.1.min.js"); ?>"></script>
		<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
			<script src='<?php echo base_url()?>assets/js/jquery.form.js'></script>
			<script src='<?php echo base_url()?>assets/js/jquery.validate.js'></script> 
			<script src='<?php echo base_url()?>assets/js/form-validation.js'></script>	
			<script src="<?php echo base_url(); ?>assets/js/additional-methods.js"></script>
			<!-- Local jQuery -->
			<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script> 
			<script src="<?php echo base_url(); ?>assets/js/typeahead.js"></script>

	<!-- Sweet Alert -->
	<script src="<?php echo base_url("assets/js/plugin/sweetalert/sweetalert.min.js"); ?>"></script>
	<script src="<?PHP echo base_url("assets/js/plugin/bootstrap-datepicker.min.js");?>"></script>
	</head>