<!-- Sidebar -->
<div class="sidebar sidebar-style-2">
	<div class="sidebar-wrapper scrollbar scrollbar-inner">
		<div class="sidebar-content">
			<ul class="nav nav-primary">
				<li class="nav-item  <?php echo($this->uri->segment(1) == "video/addEdit" || $this->uri->segment(1) =="")?"active":"";?>">
					<a href="<?= base_url('video/addEdit')?>" aria-expanded="false">
						<i class="fas fa-plus"></i>
						<p>Add Videos</p>
					</a>
				</li>
				<li class="nav-item <?php echo($this->uri->segment(1) == "dashboard" || $this->uri->segment(1) =="")?"active":"";?>">
					<a href="<?= base_url('dashboard')?>">
						<i class="fas fa-chart-line"></i>
						<p>Dashboard</p>
					</a>
				</li>
				<li class="nav-item <?php echo($this->uri->segment(1) == "video" || $this->uri->segment(1) =="")?"active":"";?> ">
					<a href="<?= base_url('video')?>">
						<i class="fas fa-video"></i>
						<p>All Videos</p>
					</a>
				</li>
				<?php  ?>
				<li class="nav-item <?php echo($this->uri->segment(1) == "enquiries" || $this->uri->segment(1) =="")?"active":"";?>">
					<a href="<?= base_url('enquiries')?>">
						<i class="fas fa-comment-alt"></i>
						<p>All Enquiries</p>
					</a>
				</li>
				<li class="nav-item  <?php echo(in_array($this->uri->segment(1),array("blog","aboutus","faq","contactus","meetteam","banner","how_we_operate","terms_conditions","privacy_policy")))?"active":"" ?>">
					<a data-toggle="collapse" href="#website">
						<i class="fas fa-window-maximize"></i>
						<p>Website Pages</p>
						<span class="caret"></span>
					</a>
					<div class="collapse" id="website">
						<ul class="nav nav-collapse">
							<li class="<?php echo(in_array($this->uri->segment(1),array("blog")))?"active":"" ?>">
								<a href="<?= base_url('blog')?>">
									<span class="sub-item">Blog</span>
								</a>
							</li>
							<li class="<?php echo(in_array($this->uri->segment(1),array("aboutus")))?"active":"" ?>">
								<a href="<?= base_url('aboutus')?>">
									<span class="sub-item">About Us</span>
								</a>
							</li>
							<li class="<?php echo(in_array($this->uri->segment(1),array("faq")))?"active":"" ?>">
								<a href="<?= base_url('faq')?>">
									<span class="sub-item">FAQs</span>
								</a>
							</li>
							<li class="<?php echo(in_array($this->uri->segment(1),array("contactus")))?"active":"" ?>">
								<a href="<?= base_url('contactus')?>">
									<span class="sub-item">Contact Us</span>
								</a>
							</li>
							<li class="<?php echo(in_array($this->uri->segment(1),array("meetteam")))?"active":"" ?>">
								<a href="<?= base_url('meetteam')?>">
									<span class="sub-item">Team Members</span>
								</a>
							</li>
							<li class="<?php echo(in_array($this->uri->segment(1),array("banner")))?"active":"" ?>">
								<a href="<?= base_url('banner')?>">
									<span class="sub-item">Home banner</span>
								</a>
							</li>
							<li class="<?php echo(in_array($this->uri->segment(1),array("how_we_operate")))?"active":"" ?>">
								<a href="<?= base_url('how_we_operate')?>">
									<span class="sub-item">How we Operate</span>
								</a>
							</li>
							<li class="<?php echo(in_array($this->uri->segment(1),array("terms_conditions")))?"active":"" ?>">
								<a href="<?= base_url('terms_conditions')?>">
									<span class="sub-item">Terms Conditions</span>
								</a>
							</li>
							<li class="<?php echo(in_array($this->uri->segment(1),array("privacy_policy")))?"active":"" ?>">
								<a href="<?= base_url('privacy_policy')?>">
									<span class="sub-item">Privacy Policy</span>
								</a>
							</li>
						</ul>
					</div>
				</li>
				<li class="nav-item <?php echo($this->uri->segment(1) == "testimonial" || $this->uri->segment(1) =="")?"active":"";?>">
					<a href="<?=base_url('testimonial');?>">
						<i class="fas fa-comment-dots"></i>
						<p>Testimonials</p>
					</a>
				</li>
				<li class="nav-item divider-item<?php echo($this->uri->segment(1) == "users" || $this->uri->segment(1) =="")?"active":"";?> ">
					<a href="<?= base_url('users')?>">
						<i class="fas fa-users"></i>
						<p>All Users</p>
					</a>
				</li>
				<li class="nav-item <?php echo(in_array($this->uri->segment(1),array("tag","theme","subtheme","video","blog_tag")))?"active":"" ?>" >
					<a data-toggle="collapse" href="#master">
						<i class="fas fa-layer-group"></i>
						<p>Master</p>
						<span class="caret"></span>
					</a>
					<div class="collapse" id="master">
						<ul class="nav nav-collapse">
							<li class="<?php echo(in_array($this->uri->segment(1),array("tag")))?"active":"" ?>">
								<a href="<?= base_url('tag');?>">
									<span class="sub-item">Tags</span>
								</a>
							</li>
							<li class="<?php echo(in_array($this->uri->segment(1),array("blog_tag")))?"active":"" ?>">
								<a href="<?= base_url('blog_tag');?>">
									<span class="sub-item">Blog Tags</span>
								</a>
							</li>
							<li class="<?php echo(in_array($this->uri->segment(1),array("theme")))?"active":"" ?>">
								<a href="<?= base_url('theme')?>">
									<span class="sub-item">Theme Master</span>
								</a>
							</li>
							<li class="<?php echo(in_array($this->uri->segment(1),array("subtheme")))?"active":"" ?>">
							<a href="<?= base_url('subtheme')?>">
									<span class="sub-item">Sub Theme Master</span>
								</a>
							</li>
							<li class="<?php echo(in_array($this->uri->segment(1),array("video")))?"active":"" ?>">
								<a href="<?= base_url('video')?>">
									<span class="sub-item">Video Master</span>
								</a>
							</li>
						</ul>
					</div>
				</li>
				<li class="nav-item <?php echo($this->uri->segment(1) == "profile" || $this->uri->segment(1) =="")?"active":"";?>">
				<a href="<?= base_url("profile")?>">
						<i class="fas fa-address-card"></i>
						<p>Profile</p>
					</a>
				</li>
				<li class="nav-item ">
					<a href="<?= base_url("home/logOut")?>">
						<i class="fas fa-sign-out-alt"></i>
						<p>Logout</p>
					</a>
				</li>

			</ul>
		</div>
	</div>
</div>
<!-- End Sidebar -->
