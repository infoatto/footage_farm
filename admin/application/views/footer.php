</div><!-- Wrapper end --started in main-header.php -->			<!--   Core JS Files   -->
			<!-- <script src="<?php echo base_url("assets/js/jquery.3.2.1.min.js"); ?>"></script> -->
			<script src="<?php echo base_url("assets/js/popper.min.js"); ?>"></script>
			<script src="<?php echo base_url("assets/js/bootstrap.min.js"); ?>"></script>

			<!-- jQuery UI -->
			<script src="<?php echo base_url("assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js"); ?>"></script>
			<script src="<?php echo base_url("assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"); ?>"></script>

			<!-- jQuery Scrollbar -->
			<script src="<?php echo base_url("assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js"); ?>"></script>

			<!-- Moment JS -->
			<script src="<?php echo base_url("assets/js/plugin/moment/moment.min.js"); ?>"></script>

			<!-- Chart JS -->
			<script src="<?php echo base_url("assets/js/plugin/chart.js/chart.min.js"); ?>"></script>

			<!-- jQuery Sparkline -->
			<script src="<?php echo base_url("assets/js/plugin/jquery.sparkline/jquery.sparkline.min.js"); ?>"></script>

			<!-- Chart Circle -->
			<script src="<?php echo base_url("assets/js/plugin/chart-circle/circles.min.js"); ?>"></script>

			<!-- Datatables -->
			<script src="<?php echo base_url("assets/js/plugin/datatables/datatables.min.js"); ?>"></script>

			<!-- Bootstrap Notify -->
			<script src="<?php echo base_url("assets/js/plugin/bootstrap-notify/bootstrap-notify.min.js"); ?>"></script>

			<!-- Bootstrap Toggle -->
			<script src="<?php echo base_url("assets/js/plugin/bootstrap-toggle/bootstrap-toggle.min.js"); ?>"></script>

			<!-- jQuery Vector Maps -->
			<script src="<?php echo base_url("assets/js/plugin/jqvmap/jquery.vmap.min.js"); ?>"></script>
			<script src="<?php echo base_url("assets/js/plugin/jqvmap/maps/jquery.vmap.world.js"); ?>"></script>

			<!-- Google Maps Plugin -->
			<script src="<?php echo base_url("assets/js/plugin/gmaps/gmaps.js"); ?>"></script>

			<!-- Dropzone -->
			<script src="<?php echo base_url("assets/js/plugin/dropzone/dropzone.min.js"); ?>"></script>

			<!-- Fullcalendar -->
			<script src="<?php echo base_url("assets/js/plugin/fullcalendar/fullcalendar.min.js"); ?>"></script>

			<!-- DateTimePicker -->
			<script src="<?php echo base_url("assets/js/plugin/datepicker/bootstrap-datetimepicker.min.js"); ?>"></script>

			<!-- Bootstrap Tagsinput -->
			<script src="<?php echo base_url("assets/js/plugin/bootstrap-tagsinput/bootstrap-tagsinput.min.js"); ?>"></script>

			<!-- Bootstrap Wizard -->
			<script src="<?php echo base_url("assets/js/plugin/bootstrap-wizard/bootstrapwizard.js"); ?>"></script>

			<!-- jQuery Validation -->
			<script src="<?php echo base_url("assets/js/plugin/jquery.validate/jquery.validate.min.js"); ?>"></script>

			<!-- Summernote -->
			<script src="<?php echo base_url("assets/js/plugin/summernote/summernote-bs4.min.js"); ?>"></script>

			<!-- Select2 -->
			<script src="<?php echo base_url("assets/js/plugin/select2/select2.full.min.js"); ?>"></script>

			<!-- Sweet Alert -->
			<script src="<?php echo base_url("assets/js/plugin/sweetalert/sweetalert.min.js"); ?>"></script>

			<!-- Owl Carousel -->
			<script src="<?php echo base_url("assets/js/plugin/owl-carousel/owl.carousel.min.js"); ?>"></script>

			<!-- Magnific Popup -->
			<script src="<?php echo base_url("assets/js/plugin/jquery.magnific-popup/jquery.magnific-popup.min.js"); ?>"></script>

			<!-- Select2 -->
			<script src="<?php echo base_url("assets/js/select2.full.min.js"); ?>"></script>	
			<!-- Fancybox 3-->
			<script src="<?php echo base_url("assets/js/jquery.fancybox.min.js"); ?>"></script>	
			<!-- Atlantis JS -->
			<script src="<?php echo base_url("assets/js/atlantis.min.js"); ?>"></script>	
			<!-- Custom JS Footage Farm -->
			<script src="<?php echo base_url("assets/js/custom.js"); ?>"></script>


		<script src="<?php echo base_url()?>assets/helpers/jquery.ui.widget.js")></script>
		<script src="<?php echo base_url()?>assets/helpers/jquery.iframe-transport.js")></script>
		<script src="<?php echo base_url()?>assets/helpers/jquery.fileupload.js")></script>
		<script src="https://cdn.jsdelivr.net/medium-editor/latest/js/medium-editor.min.js"></script>
		<script src="<?php echo base_url()?>assets/helpers/runtime.js")></script>
		<script src="<?php echo base_url()?>assets/helpers/sortable.js")></script>
		<script src="<?php echo base_url()?>assets/helpers/cycle.js")></script>
		<script src="<?php echo base_url()?>assets/helpers/cycle.center.js")></script>
		<script src="<?php echo base_url()?>assets/helpers/medium-insert.js")></script>
		<script src="<?php echo base_url()?>assets/helpers/autolist.js")></script>

		<script type="text/javascript">
			var autolist = new AutoList();
		    var editor = new MediumEditor('.my-text-editor',{
		        extensions: {
		            'autolist': autolist
		        },
		        placeholder: {
		            /* This example includes the default options for placeholder,
		            if nothing is passed this is what it used */
		            text: 'Description body',
		            hideOnClick: true
		        },
		        toolbar: {
		            buttons: ['bold', 'italic', 'underline', 'anchor', 'h2', 'h3', 'quote', 'unorderedlist','orderedlist']
		        }
		    });

		    $('.my-text-editor').mediumInsert({
		        editor: editor,
		        addons: {
		            images: {
		                uploadScript: null,
		                deleteScript: null,
		                autoGrid: 0,
		                captionPlaceholder: 'Type caption for image',
		                styles: {
		                    slideshow: {
		                        label: '',
		                        added: function ($el) {
		                            $el
		                            .data('cycle-center-vert', true)
		                            .cycle({
		                                slides: 'figure'
		                            });
		                        },
		                        removed: function ($el) {
		                            $el.cycle('destroy');
		                        }
		                    },grid: {
		                label: ''
		            },
		                },
		                actions: null
		            },
		            embeds: true
		        }
		    });

		    var editor = new MediumEditor('.my-text-editor1',{
		        extensions: {
		            'autolist': autolist
		        },
		        placeholder: {
		            /* This example includes the default options for placeholder,
		            if nothing is passed this is what it used */
		            text: 'Description body',
		            hideOnClick: true
		        },
		        toolbar: {
		            buttons: ['bold', 'italic', 'underline', 'anchor', 'h2', 'h3', 'quote', 'unorderedlist','orderedlist']
		        }
		    });

		    $('.my-text-editor1').mediumInsert({
		        editor: editor,
		        
		    });

		    var editor = new MediumEditor('.my-text-editor2',{
		        extensions: {
		            'autolist': autolist
		        },
		        placeholder: {
		            /* This example includes the default options for placeholder,
		            if nothing is passed this is what it used */
		            text: 'Description body',
		            hideOnClick: true
		        },
		        toolbar: {
		            buttons: ['bold', 'italic', 'underline', 'anchor', 'h2', 'h3', 'quote', 'unorderedlist','orderedlist']
		        }
		    });

		    $('.my-text-editor2').mediumInsert({
		        editor: editor,
		        
		    });

		    var editor = new MediumEditor('.my-text-editor3',{
		        extensions: {
		            'autolist': autolist
		        },
		        placeholder: {
		            /* This example includes the default options for placeholder,
		            if nothing is passed this is what it used */
		            text: 'Description body',
		            hideOnClick: true
		        },
		        toolbar: {
		            buttons: ['bold', 'italic', 'underline', 'anchor', 'h2', 'h3', 'quote', 'unorderedlist','orderedlist']
		        }
		    });

		    $('.my-text-editor3').mediumInsert({
		        editor: editor,
		        
		    });

		    var editor = new MediumEditor('.my-text-editor4',{
		        extensions: {
		            'autolist': autolist
		        },
		        placeholder: {
		            /* This example includes the default options for placeholder,
		            if nothing is passed this is what it used */
		            text: 'Description body',
		            hideOnClick: true
		        },
		        toolbar: {
		            buttons: ['bold', 'italic', 'underline', 'anchor', 'h2', 'h3', 'quote', 'unorderedlist','orderedlist']
		        }
		    });

		    $('.my-text-editor4').mediumInsert({
		        editor: editor,
		        
		    });

		    var editor = new MediumEditor('.my-text-editor5',{
		        extensions: {
		            'autolist': autolist
		        },
		        placeholder: {
		            /* This example includes the default options for placeholder,
		            if nothing is passed this is what it used */
		            text: 'Description body',
		            hideOnClick: true
		        },
		        toolbar: {
		            buttons: ['bold', 'italic', 'underline', 'anchor', 'h2', 'h3', 'quote', 'unorderedlist','orderedlist']
		        }
		    });

		    $('.my-text-editor5').mediumInsert({
		        editor: editor,
		        
		    });

		    var editor = new MediumEditor('.my-text-editor6',{
		        extensions: {
		            'autolist': autolist
		        },
		        placeholder: {
		            /* This example includes the default options for placeholder,
		            if nothing is passed this is what it used */
		            text: 'Description body',
		            hideOnClick: true
		        },
		        toolbar: {
		            buttons: ['bold', 'italic', 'underline', 'anchor', 'h2', 'h3', 'quote', 'unorderedlist','orderedlist']
		        }
		    });

		    $('.my-text-editor6').mediumInsert({
		        editor: editor,
		        
		    });

		    var editor = new MediumEditor('.my-text-editor7',{
		        extensions: {
		            'autolist': autolist
		        },
		        placeholder: {
		            /* This example includes the default options for placeholder,
		            if nothing is passed this is what it used */
		            text: 'Description body',
		            hideOnClick: true
		        },
		        toolbar: {
		            buttons: ['bold', 'italic', 'underline', 'anchor', 'h2', 'h3', 'quote', 'unorderedlist','orderedlist']
		        }
		    });

		    $('.my-text-editor7').mediumInsert({
		        editor: editor,
		        
		    });
			
		</script>	

		</div>
	</body>
</html>