<nav class="navbar navbar-expand-lg ff-navbar">
		<div class="container">
			<a class="navbar-brand" href="index.php">
				<img src="images/ff-color-logo.svg" alt="Footage Farm" id="top-logo">
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
				<span class="navbar-toggler-icon"></span>
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active">
						<a class="nav-link" href="#">Themes</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">About</a>
					</li>
					<li>
						<a href="#" class="nav-link">How we operate</a>
					</li>
					<li>
						<a href="#" class="nav-link">FAQs</a>
					</li>
					<li>
						<a href="#" class="nav-link">Blog</a>
					</li>
					<li>
						<a href="#" class="nav-link">Contact</a>
					</li>
				</ul>
				<ul class="navbar-nav ml-auto">
					<li class="nav-item dropdown signin-link-desktop">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Sign In <img src="images/down-arrow.svg" alt=""></a>
						<div class="dropdown-menu signin-flyout" aria-labelledby="navbarDropdown">
							<div class="signin-flyout-wrapper">
								<div class="create-account">
									<h3>New Here</h3>
									<p class="signin-text">New to Footage Farm? Create an account to get started today.</p>
									<a href="#/" class="btn-ff btn-secondary-dark" data-fancybox data-src="#verifyemail-content" data-options='{"touch" : false}'>Create an account</a>
								</div>
								<div class="sign-in-user">
									<h3>Registered Users</h3>
									<p class="signin-text">Have an account? Sign in now. </p>
									<a href="#/" class="btn-ff btn-primary-dark" data-fancybox data-src="#signin-content" data-options='{"touch" : false}'>Sign in</a>
								</div>
							</div>
						</div>
					</li>
					<li class="nav-item signin-link-mobile">
						<a href="#/" class="nav-link">Sign In</a>
					</li>
					<li class="nav-item">
						<a class="enquiry btn-ff btn-ff-icon btn-tertiary-dark" href="#">Enquiry</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<!-- Signin flyout mobile -->
	<div class="signin-flyout-mobile">
		<a href="#/" class="close-mobile-signin"><img src="images/close.svg" alt=""></a>
		<div class="signin-flyout-wrapper">
			<div class="create-account">
				<h3>New Here</h3>
				<p class="signin-text">New to Footage Farm? Create an account to get started today.</p>
				<a href="#/" class="btn-ff btn-secondary-dark">Create an account</a>
			</div>
			<div class="sign-in-user">
				<h3>Registered Users</h3>
				<p class="signin-text">Have an account? Sign in now. </p>
				<a href="#/" class="btn-ff btn-primary-dark" data-fancybox data-src="#signin-content" data-options='{"touch" : false}' >Sign in</a>
			</div>
		</div>
	</div>
	<!-- Signin flyout mobile -->
	<!-- Login/Signup -->	
	<div style="display: none;max-width:580px;" id="signin-content" class="ff-modal">
		<!-- Login Content -->
		<div class="modal-header">		
			<h3>Sign In</h3>		
		</div>
		<div class="multicolored-line">
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			<div></div>
		</div>
		<div class="ff-modal-content ff-login">
			<div class="modal-content-wrapper">		
				<div class="modal-section">
					<div class="header-login">
						<p class="login-text text-center">Sign in with</p>
						<a href="#/" class="google-login"><img src="images/google.svg" alt=""> Login With Google</a>
						<form>				
							<div class="form-group">
								<label for="username">Username</label>
								<input type="text" class="form-control ff-field-dark" id="username" name="username">	
							</div>
							<div class="form-group">
								<label for="password">Password</label>
								<input type="password" class="form-control ff-field-dark" id="password" name="password">
								<a href="#/" class="showpassword"><img src="images/show.svg" alt=""></a>	
							</div>
							<div class="form-group keepsignedin-wrapper">
								<div class="keepsignedin">
									<label class="checkbox-container-theme">
										<input type="checkbox"> <span class="checkmark-theme-label">Keep me signed in</span>
										<span class="checkmark-theme"></span>
									</label>
								</div>
								<div class="forgotpassword">
									<a href="#/" class="login-link ff-forgotpass-link">Forgot Password</a>
								</div>
							</div>
							<div class="form-group mb-0">
								<p class="login-text">New to Footage Farm? &nbsp;<br/><a href="#/" class="login-link ff-register-link">Create an Account</a></p>
							</div>					
						</form>			
					</div>
				</div>				
				<div class="modal-actions">					
					<input type="submit" class="btn-ff btn-primary-dark text-uppercase full-width-btn" value="Sign in">
				</div>			
			</div>			
		</div>
		<!-- Login Content -->
		<!-- Register Content -->		
		<div class="ff-modal-content ff-register">			
			<div class="modal-content-wrapper">	
				<div class="modal-section">
					<div class="header-login">
						<p class="login-text text-center">Already have an account? &nbsp;<br/><a href="#/" class="login-link ff-login-link">Log in</a></p>						
						<a href="#/" class="google-login mb-5"><img src="images/google.svg" alt=""> Login With Google</a>
						<p class="login-text text-center">or create with</p>	
						<form>				
							<div class="form-group">
								<label for="username">Name</label>
								<input type="text" class="form-control ff-field-dark" id="username" name="username">	
							</div>
							<div class="form-group">
								<label for="useremail">Email</label>
								<input type="email" class="form-control ff-field-dark" id="useremail" name="useremail">	
							</div>
							<div class="form-group">
								<label for="password">Set a Password</label>
								<input type="password" class="form-control ff-field-dark" id="password" name="password">
								<a href="#/" class="showpassword"><img src="images/show.svg" alt=""></a>	
							</div>
							<div class="form-group keepsignedin-wrapper mb-5">
								<div class="keepsignedin">
									<label class="checkbox-container-theme">
										<input type="checkbox"> <span class="checkmark-theme-label">I agree with your Terms of Service, Privacy Policy</span>
										<span class="checkmark-theme"></span>
									</label>
								</div>								
							</div>												
						</form>			
					</div>
				</div>				
				<div class="modal-actions">					
					<input type="submit" class="btn-ff btn-primary-dark text-uppercase full-width-btn" value="Create Account">
				</div>			
			</div>			
		</div>
		<!-- Register Content -->
		<!-- Forgot Password Content -->		
		<div class="ff-modal-content ff-forgotpass">
			<div class="modal-content-wrapper">		
				<div class="modal-section">
					<div class="header-login">												
						<form>
							<div class="form-group">
								<label for="resetpassword">Enter registered email id to reset your password</label>
								<input type="email" class="form-control ff-field-dark" id="resetpassword" name="resetpassword" placeholder="johndoe@gmail.com">	
							</div>
							<div class="form-group mb-0">
								<a href="#/" class="login-link ff-login-link">Back to Login</a></p>
							</div>								
						</form>			
					</div>
				</div>				
				<div class="modal-actions">					
					<input type="submit" class="btn-ff btn-primary-dark text-uppercase full-width-btn" value="Send Email">
				</div>			
			</div>			
		</div>
		<!-- Forgot Password Content -->
	</div>
<!-- Login/Signup -->
<!-- Verify Email -->	
<div style="display: none;max-width:580px;" id="verifyemail-content" class="ff-modal">
		<!-- Login Content -->
		<div class="modal-header">		
			<h3>Please verify your email</h3>		
		</div>
		<div class="multicolored-line">
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			<div></div>
		</div>					
		<div class="ff-modal-content">
			<div class="modal-content-wrapper">		
				<div class="modal-section">
					<div class="header-login">	
						<p class="login-text mb-20">We are glad to have you onboard! But before we start we will need to make sure we’ve got the right email for you.</p>														
						<a href="#/" class="btn-ff btn-primary-dark text-uppercase">Go to Homepage</a>
						<p class="login-text mt-20">Didn’t recieve email? &nbsp;<br><a href="#/" class="login-link">Click here to change/resend email.</a></p>		
					</div>
				</div>
			</div>			
		</div>		
	</div>
<!-- Verify Email -->