<!-- Footer -->
<div class="clearfix">&nbsp;</div>
<footer>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="footer-getintouch-wrapper">
					<div class="footer-getintouch">
						<p><span>Don’t see something you want? Contact us and we will help you look into our archive </span><a href="#/" class="btn-ff btn-tertiary-light">Get in Touch</a></p>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<div class="footer-intro-wrapper">
					<img src="images/ff-white-logo.svg" alt="" class="footer-logo">
					<p class="footer-intro">
						Footage Farm's aim is to help producers to not have to struggle with high licence fees and to enable them to access and afford archival material of high quality.
					</p>
				</div>
			</div>
			<div class="col-sm-8">
				<div class="footer-links-wrapper">
					<div class="footer-links-single">
						<h4>Learn More</h4>
						<ul>
							<li><a href="#/">About Us</a></li>
							<li><a href="#/">Blog</a></li>
							<li><a href="#/">How We Operate</a></li>
						</ul>
					</div>
					<div class="footer-links-single">
						<h4>SUPPORT</h4>
						<ul>
							<li><a href="#/">+44(0)207 631 3773</a></li>
							<li><a href="#/">Contact Us</a></li>
							<li><a href="#/">FAQs</a></li>
						</ul>
					</div>
				</div>
				<div class="social-links-wrapper">
					<a href="#/"><img src="images/vimeo.svg" alt=""></a>
					<a href="#/"><img src="images/youtube.svg" alt=""></a>
					<a href="#/"><img src="images/twitter.svg" alt=""></a>
					<a href="#/"><img src="images/facebook.svg" alt=""></a>
					<a href="#/"><img src="images/linkedin.svg" alt=""></a>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="footer-seperator"></div>
				<div class="copyright-wrapper">
					<p class="copyright-info">&copy; 2020 Footage Farm</p>
					<p class="privacy">
						<a href="#/">Terms and Conditions</a>
						<span>|</span>
						<a href="#/" class="privacy-link">Privacy Policy</a>
					</p>
					<p class="backtotop-mobile">
						<a href="#top-logo">Back to Top</a>
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="multicolored-line multicolred-footer-line">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
</footer>
<!-- Footer -->

